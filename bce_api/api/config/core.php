<?php
// show error reporting
error_reporting(E_ALL);
 
// set your default time-zone
date_default_timezone_set('Asia/Manila');
 
// variables used for jwt
$key = "bce_api_key";
$iss = "http://www.antennereunion.fr";
$aud = "http://www.antennereunion.fr";
$iat = 1356999524;
$nbf = 1357000000;
?>