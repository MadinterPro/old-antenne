<?php

class Database {

    private $dbConnection = null;
    private $host       = 'localhost';
    private $db_name    = 'antenne_art_spip';
    private $username   = 'antenne_spipAR';
    private $password   = 'antv3_2009';

    public function __construct()
    {

        try {
            $this->dbConnection = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->dbConnection->exec("set names utf8");
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function getConnection()
    {
        return $this->dbConnection;
    }
}
?>