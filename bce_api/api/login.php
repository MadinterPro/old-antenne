<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET,POST,PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// generate json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

include_once 'config/database.php';
include_once 'objects/article.php';
include_once 'objects/rubrique.php';
include_once 'objects/document.php';
include_once 'objects/logger.php';

$database = new Database();
$db = $database->getConnection();
$article = new Article($db);
$rubrique = new Rubrique($db);
$document = new Document($db);
$log = new Logger('api');
$key = 'B2L%DsfkùfkK56FXvjiaXDHu0ZiS';
 
// get posted data
$log->info('Récupération des données provenant du serveur HD');
$tmp = json_decode(file_get_contents("php://input"));
$log->info('Données cryptées : '. PHP_EOL . json_encode($tmp));
try {
  try {
    $data = JWT::decode($tmp, $key, array('HS256'));
    $log->info('Données décryptées : '. PHP_EOL . json_encode($data));
  } catch (Exception $e) {
    $log->info("Impossible de décryptées les données");
    $log->info("-------------------------");
    http_response_code(401);
    echo json_encode(array("message" => "Failed.")); exit();
  }

  /* Traitement */
  $is_deprogrammed = strtolower($data->Cancelled);
  $Img_size = $data->size;
        // Find ID rubrique et ID secteur
  $_rub = $rubrique->findRubrique($data->Program);
  $log->info("Vérification si le replay en question est un JT Weekend");
  $JtWeekend = $rubrique->IsJtWeekend($data->Activation_start, $data->Program);
  if ($JtWeekend) {
    $_rub = $JtWeekend;
  }

  $statut = ($_rub['id_rubrique'] == 4119 || $_rub['id_rubrique'] == 5873) ? 'prepa' : 'publie';

  // Data à inserer
  $articleData = array(
    'titre' => $data->Title,
    'id_rubrique' => $_rub['id_rubrique'],
    'texte' => isset($data->Synopsis) ? $data->Synopsis : "",
    'date' => $data->Activation_start,
    'statut' => $statut,
    'id_secteur' => $_rub['id_secteur'],
    'export' => 'oui',
    'date_redac' => $data->Activation_end,
    'accepter_forum' => 'pri',
    'lang' => 'fr',
    'broadcastID' => $data->BroadcastID
  );

  $dataImage = array(
    'extension' => 'jpg',
    'titre'  => $data->Title,
    'date' => $data->Activation_start,
    'fichier' => $data->vignette,
    'taille' => '7000',
    'largeur' => $Img_size[0],
    'hauteur' => trim($Img_size[1]),
    'mode' => 'image',
    'distant' => 'non',
    'statut' => 'publie'
  );
  $video = ($data->video) ? $data->video : 'https://cdn.jwplayer.com/manifests/' . $data->mediaID . '.m3u8';
  $dataVideo = array(
    'extension' => 'ts',
    'titre'  => $data->Title,
    'date' => $data->Activation_start,
    'fichier' => $video,
    'duree' => $data->Duration,
    'mode' => 'document',
    'distant' => 'oui',
    'statut' => 'publie',
    'media' => 'video'
  );

  $_art = $article->find($data->BroadcastID);

  try {
    if (!$_art) {
      $log->info("Insertion nouvel article : ". $data->Title);
      $_art = $article->insert($articleData);
      $log->info("Insertion effectué : ". $_art);
      $log->info("Insertion media ID dans link_media : ". $data->mediaID);
      $article->setMediaID($data->mediaID, $_art);
      $log->info("Media ID ajouté");
      
    } else {
      $_art = $_art->id_article;
      $log->info("Update article existant : ". $data->BroadcastID);
      $udpate = $article->update($articleData);
      $log->info("Update article effectué");
      $log->info("Mise à jour mediaID dans link_media");
      $article->updateMediaID($_art, $data->mediaID);
      $log->info("Update mediaID effectué");
      $log->info("Suppression ancien document");
      $document->deleteDocument($_art);
      $log->info("Ancien document supprimé");
    }

    $log->info("Insertion vignette dans spip_documents");
    $document->insertDocument($dataImage, $_art);
    $log->info("Vignette ajouté dans spip_documents");
    $log->info("Insertion vidéo dans spip_documents");
    $document->insertDocument($dataVideo, $_art);
    $log->info("Vidéo ajoutée dans spip_documents");
    $log->info("-------------------------");
  } catch (Exception $e) {
    $log->info("Création / update article échoué");
    $log->info($e->getMessage());
    $log->info("-------------------------");
    http_response_code(401);
    echo json_encode(array("message" => "Failed.")); exit();
  }

  /* Fin traitement */

  http_response_code(200);
  echo json_encode(array(
    "response" => 200,
    "message" => "Access autorise",
    // "data" => $decoded->data,
            // "jwt" => $jwt,
    "titre" => $data->Title,
    "date" => $data->Activation_start,
    "date_redac" => $data->Activation_end,
    'broadcastID' => $data->BroadcastID
  ));
  
} catch (Exception $e) {
    $log->info("Tentative de connexion échoué pour ");
    $log->info("-------------------------");
    http_response_code(401);
    echo json_encode(array("message" => "Failed."));
}