<?php 
/**
 * 
 */
class Document
{	
	private $db;
	
	public function __construct($db)
    {
        $this->db = $db;
    }

	public function insertDocument(Array $input, $id_article)
    {
        $champs = array_keys($input);
        $Query = "INSERT INTO spip_documents (" . implode(',', $champs) . ") VALUES (:" . implode(',:', $champs) . ")";
        try {
            $doc = $this->db->prepare($Query);
            $doc->execute($input);
            $id_document = $this->db->lastInsertId();
            $this->setLienDocument(array(
                'id_document' => $id_document,
                'id_objet'  => $id_article,
                'objet' => 'article'
            ));
            return $id_document;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function setLienDocument(Array $input)
    {
        $champs = array_keys($input);
        $Query = "INSERT INTO spip_documents_liens (" . implode(',', $champs) . ") VALUES (:" . implode(',:', $champs) . ")";
    	try {
    		$query = $this->db->prepare($Query);
    		$query->execute($input);
            return $query->rowCount();
    	} catch (\PDOException $e) {
    		exit($e->getMessage());
    	}
    }

    public function deleteDocument($id_article)
    {
        $Query = "SELECT id_document FROM spip_documents_liens WHERE id_objet= ".$id_article." AND objet = 'article'";
        $documents = $this->db->query($Query)->fetchAll();
        foreach ($documents as $doc) {
            $del = 'DELETE FROM spip_documents WHERE id_document = :id_document';
            $stmt = $this->db->prepare($del);
            $stmt->bindParam(':id_document', $doc['id_document'], PDO::PARAM_INT);   
            $stmt->execute();
        }
        $del = "DELETE FROM spip_documents_liens WHERE id_objet = :id_objet AND objet = :type_objet";
        $stmt = $this->db->prepare($del); 
        $stmt->execute( array(
            ':id_objet' => $id_article,
            ':type_objet' => 'article'
        ));
        return false;
    }

    public function updateStatut($id_article){
        $Query = "SELECT id_document FROM spip_documents_liens WHERE id_objet= ".$id_article." AND objet = 'article'";
        $documents = $this->db->query($Query)->fetchAll();
        foreach ($documents as $doc) {
            $query = " UPDATE spip_documents SET statut = :statut WHERE id_document = :id_document ";
            try {
                $query = $this->db->prepare($query);
                $query->execute(array(
                    'statut' => 'publie',
                    'id_document'  => $doc['id_document']
                ));
            } catch (\PDOException $e) {
                exit($e->getMessage());
            }
        }
        return true;
    }
}

?>