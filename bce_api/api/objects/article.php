<?php 
/**
 * Insertion et update article
 */
class Article
{
	private $db;
	
	public function __construct($db)
	{
        date_default_timezone_set('Indian/Reunion');
		$this->db = $db;
	}

	public function find($bid)
    {
        $Query = " SELECT id_article FROM spip_articles WHERE broadcastID LIKE '%".$bid."%' LIMIT 0,1";
        try {
            $res = $this->db->query($Query)->fetchObject();
            if ($res) {
                return $res;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }   
    }

    public function insert(Array $input)
    {
        $champs = array_keys($input);
        $Query = "INSERT INTO spip_articles (" . implode(',', $champs) . ") VALUES (:" . implode(',:', $champs) . ")";
        try {
            $insert_article = $this->db->prepare($Query);
            $insert_article->execute($input);
            $id_article = $this->db->lastInsertId();
            $this->addKeywords($id_article);
            $this->addAuthor($id_article);
            return $id_article;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function update(Array $input)
    {
        $query = "
            UPDATE spip_articles 
            SET titre = :titre, id_rubrique = :id_rubrique, texte = :texte, date = :date, statut = :statut, id_secteur = :id_secteur, maj = :maj, export = :export, date_redac = :date_redac, accepter_forum = :accepter_forum, lang = :lang, broadcastID = :broadcastID
            WHERE broadcastID = :broadcastID; ";

        try {
            $query = $this->db->prepare($query);
            $query->execute(array(
                'titre' => $input['titre'],
                'id_rubrique'  => $input['id_rubrique'],
                'texte' => $input['texte'],
                'date' => $input['date'],
                'statut' => $input['statut'],
                'id_secteur' => $input['id_secteur'],
                'maj' => date('Y-m-d H:i:s'),
                'export' => $input['export'] ,
                'date_redac' => $input['date_redac'],
                'accepter_forum' => $input['accepter_forum'],
                'lang' => $input['lang'],
                'broadcastID' => $input['broadcastID']
            ));
            return $query->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function setMediaID($media_id, $id_article){
        $Query = "INSERT INTO link_media (id_article, media_id) VALUES (:id_article, :media_id)";
        try {
            $insert_article = $this->db->prepare($Query);
            $insert_article->execute(array(
                'id_article' => $id_article,
                'media_id' => $media_id
            ));
            return $this->db->lastInsertId();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function updateMediaID($id_article, $media_id){
        $query = "
            UPDATE link_media 
            SET media_id = :media_id WHERE id_article = :id_article";
        try {
            $query = $this->db->prepare($query);
            $query->execute(array(
                'media_id' => $media_id,
                'id_article'  => $id_article
            ));
            return $query->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        } 
    }

    public function addKeywords($id_article){
        $Query = "INSERT INTO spip_mots_liens (id_mot, id_objet, objet) VALUES (:id_mot, :id_objet, :objet)";
        try {
            $insert_article = $this->db->prepare($Query);
            $insert_article->execute(array(
                'id_mot' => '1772',
                'id_objet' => $id_article,
                'objet' => 'article'
            ));
            return true;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function addAuthor($id_article){
        $Query = "INSERT INTO spip_auteurs_liens (id_auteur, id_objet, objet, vu) VALUES (:id_auteur, :id_objet, :objet, :vu)";
        try {
            $insert_article = $this->db->prepare($Query);
            $insert_article->execute(array(
                'id_auteur' => '406',
                'id_objet' => $id_article,
                'objet' => 'article',
                'vu' => 'non'
            ));
            return true;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findByMediaID($media_id){
        $Query = " SELECT id_article FROM link_media WHERE media_id LIKE '%".$media_id."%'";
        try {
            $res = $this->db->query($Query)->fetchObject();
            if ($res) {
                return $res->id_article;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function updateStatut($id_article){
        $query = " UPDATE spip_articles SET statut = :statut WHERE id_article = :id_article ";
        try {
            $query = $this->db->prepare($query);
            $query->execute(array(
                'statut' => 'publie',
                'id_article'  => $id_article
            ));
            return $query->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}
?>