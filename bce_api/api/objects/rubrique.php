<?php 

/**
 * 
 */
class Rubrique
{
	private $db;
	private $table = 'spip_rubriques';
	private $rubEquivalence = array();
    private $other_program = array();
    private $meteo_program = array();

	public function __construct($db)
	{
		$this->db = $db;
		$this->rubEquivalence = array(
	            "DODIL" => "DO DIL",
	            "INDIA A LOVE STORY" => "INDIA, A LOVE STORY",
	            "INFO SOIREE" => "INFO-SOIREE",
	            "KLB" => "KANAL LA BLAGUE",
	            "FRANCOFOLIES 2018" => "FRANCOFOLIES",
	            "50'INSIDE LE MAG" => "50'INSIDE",
	            "50'INSIDE L'ACTU" => "50'INSIDE",
	            "THE VOICE, LA SUITE" => "THE VOICE",
	            "WILD HORSE" => "WILD HORSES",
	            "RENDEZ-VOUS A LA FOIRE !" => "RENDEZ-VOUS A LA FOIRE PASSION TERROIR",
	            "RENDEZ-VOUS A LA FOIRE! PASSION ET TERROIR" => "RENDEZ-VOUS A LA FOIRE PASSION TERROIR",
	            "LE JOURNAL DE LA COUPE DU MONDE" => "COUPE DU MONDE 2018",
	            "BRAS-PANON, MA VILLE VÉLO" => "TOUR CYCLISTE ANTENNE REUNION",
	            "BRAS-PANON, MA VILLE VELO" => "TOUR CYCLISTE ANTENNE REUNION",
	            "REVE DE MISS" => "MISS RÉUNION",
	            "INTERVIEW MISS REUNION" => "MISS RÉUNION",
	            "BIEN-VIVRE AU TAMPON" => "BIEN VIVRE AU TAMPON",
	            "SEPT A HUIT" => "SEPT À HUIT",
	            "SEPT A HUIT LIFE" => "SEPT À HUIT",
	            "GENERATION RUN STAR, LA QUOTIDIENNE" => "GÉNÉRATION RUN STAR",
	            "GENERATION RUN STAR" => "GÉNÉRATION RUN STAR",
	            "LES FLORILEGES" => "LES FLORILÈGES",
	            "FISH'N TRUCK" => "FISH’N TRUCK",
	            "Fish&rsquo ;n truck" => "FISH’N TRUCK",
	            "20 DESANM" => "20 DÉSANM",
	            "LA MINUTE DU 20 DESANM" => "20 DÉSANM",
	            "ELECTION DE MISS FRANCE 2019" => "MISS FRANCE",
	            "DIONYCITE" => "DIONYCITÉ",
	            "DIONYCITE L'ACTU" => "DIONYCITÉ",
	            "DIONYCITE LE MAG" => "DIONYCITÉ",
	            "DIONYCITE L’ACTU" => "DIONYCITÉ",
	            "AU NOM DE L'AMOUR" => "AU NOM DE L’AMOUR",
	            "MARIANA &amp; SCARLETT" => "MARIANA & SCARLETT",
	            "MARIANA ET SCARLETT" => "MARIANA & SCARLETT",
	            "AU NOM DE L'AMOUR" => "AU NOM DE L’AMOUR",
	            "KOH-LANTA, LA GUERRE DES CHEFS" => "KOH-LANTA",
	            "JOSEPHINE, ANGE GARDIEN" => "JOSEPHINE ANGE GARDIEN",
	            "MASK SINGER, L'ENQUETE CONTINUE" => "MASK SINGER",
	            "LE 20H00 DE TF1" => "20H DE TF1",
	            "20 DESANM" => "20 DÉSANM",
	            "MOUK" => "MARMAILL'HEURE",
	            "SKYLAND" => "MARMAILL'HEURE",
	            "MON ROBOT ET MOI" => "MARMAILL'HEURE",
	            "DIDOU" => "MARMAILL'HEURE",
	            "DIEGO ET ZIGGY" => "MARMAILL'HEURE",
	            "DIEGO & ZIGGY" => "MARMAILL'HEURE",
	            "DIEGO &amp; ZIGGY" => "MARMAILL'HEURE",
	            "LE MONDE DE RAFI" => "MARMAILL'HEURE",
	            "CORNEIL ET BERNIE" => "MARMAILL'HEURE",
	            "CORNEIL &amp; BERNIE" => "MARMAILL'HEURE",
	            "CORNEIL & BERNIE" => "MARMAILL'HEURE",
	            "64 RUE DU ZOO" => "MARMAILL'HEURE",
	            "LES FONDAMENTAUX" => "NATION APPRENANTE",
	            "KOH-LANTA, L'ILE DES HÉROS" => "KOH-LANTA",
	            "KOH-LANTA,&nbsp;L'ILE DES HÉROS" => "KOH-LANTA",
	            "KOH-LANTA, L'ILE DES HEROS" => "KOH-LANTA",
	            "KOH-LANTA,&nbsp;L'ILE DES HEROS" => "KOH-LANTA",
	            "KOH-LANTA - L'ILE DES HEROS" => "KOH-LANTA",
	            "KOH-LANTA&nbsp;-&nbsp;L'ILE DES HEROS" => "KOH-LANTA",
	            "HISTOIRES DE MÉDECINS - SPÉCIAL COVID 19" => "HISTOIRES DE MÉDECINS",
	            "HISTOIRES DE MEDECINS - SPECIAL COVID 19" => "HISTOIRES DE MÉDECINS",
	            "TOUT CE QU'ON AIME" => "CES ANNÉES-LÀ",
	            "TOUT CE QU’ON AIME" => "CES ANNÉES-LÀ",
	            "DADA EK NENINN" => "CES ANNÉES-LÀ",
	            "FETE DE LA MUSIK" => "CES ANNÉES-LÀ",
	            "Fête de la Musik" => "CES ANNÉES-LÀ",
	            "REUSSIR SON CASTING MISS REUNION AVEC..." => "Miss Réunion",
	            "TOUS EN MUZIK !" => "Tous En Muzik !",
	            "Tous en musik !" => "Tous En Muzik !",
	            "MILO" => "MARMAILL'HEURE",
	            "RADIO FREECTION" => "Télé Freection",
	            "VENDREDI, TOUT EST PERMIS AVEC ARTHUR" => "Vendredi tout est permis",
	            "LES CONSEILS DES CHEFS" => "TOUS EN CUISINE",
	            "ATTEINDRE LES ETOILES" => "Atteindre les étoiles",
	            "La traque" => "Téléfilms et Cinéma",
	            "RENT DANN ROND" => "Rant Dann'Ron èk Saint-Paul",
	            "KOH-LANTA - LES ARMES SECRETES" => "KOH-LANTA",
	            "RANT DANN' RON EK SAINT-PAUL" => "Rant Dann'Ron èk Saint-Paul",
	            "Uefa euro 2020, le mag" => "Euro 2020",
	            "Le journal de l’euro 2020" => "Euro 2020",
	            "100% euro : le mag" => "Euro 2020",
	            "Le zoo" => "MARMAILL'HEURE",
	            "MONSTER IN A BOX" => "MARMAILL'HEURE",
	            "IVICK VON SALZA" => "MARMAILL'HEURE",
	            "GAZOON" => "MARMAILL'HEURE",
	            "FLEDGLINGS" => "MARMAILL'HEURE",
	            "JUNGLE BEAT SERIES" => "MARMAILL'HEURE",
	            "Harry & bip" => "MARMAILL'HEURE",
	            "HARRY &amp; BIP" => "MARMAILL'HEURE",
	            "Koyaa & the amazing objects" => "MARMAILL'HEURE",
	            "MAGIC CELLAR" => "MARMAILL'HEURE",
	            "ORIGINALOS" => "MARMAILL'HEURE",
	            "LUCHIEN" => "MARMAILL'HEURE",
	            "PIKKULI" => "MARMAILL'HEURE",
	            "Ma raison d’etre" => "Ma raison d’être"
	    );
	    $this->other_program = array(
            "DIVERTISSEMENTS" => "DIVERTISSEMENTS",
            "TELEFILMS DE NOEL" => "TELEFILMS DE NOEL",
            "EMISSION D’INFORMATION EVENEMENTIELLE" => "EMISSION D’INFORMATION EVENEMENTIELLE",
            "TALENTS DU WEB - BEST OF LIVE DILAFE" => "Best of",
            "Questions de foot 974" => "Questions de foot 974",
            "Kosa i di l’Euro" => "Kosa i di l’Euro",
            "Le Meilleur de l’Euro" => "Le Meilleur de l’Euro",
            "Le 12h30 semaine" => "Le 12h30"
    	);
    	$this->meteo_program = array(
            "METEO 12H20" => "MÉTÉO",
            "METEO 13H00" => "MÉTÉO",
            "METEO 13H50" => "MÉTÉO",
            "METEO 18H55" => "MÉTÉO",
            "METEO 19H40" => "MÉTÉO"
    	);
	}

	public function findRubrique($titre){
		$program = $titre;
		if (array_key_exists($program, $this->other_program)) {
            $program = $this->other_program[$program];
            $row = $this->getRubFourreTout($program);
        } elseif (array_key_exists($program, $this->meteo_program)) {
            return array(
            	'program' => $program,
            	'id_secteur' => 742,
            	'id_rubrique' => 4119
            );
        } elseif (array_key_exists($program, $this->rubEquivalence)) {
            $program = $this->rubEquivalence[$program];
            $row = $this->getRubrique($program);
        } else {
            $row = $this->getRubrique($program);
        }
        return array(
            	'program' => $program,
            	'id_secteur' => $row['id_secteur'],
            	'id_rubrique' => $row['id_rubrique']
        );
	}

	protected function getRubFourreTout($titre) {
	    $cQuery = 'SELECT * FROM  `correspondance_nonreplay` AS c WHERE `titre` =  "' . $titre . '"';
	    try {
	    	$res = $this->db->query($cQuery)->fetchObject();
	    	if ($res->id_rubrique == 6206) {
	    		// talent du web - best of live dilafe
	    		return array(
	            	'id_secteur' => 6135,
	            	'id_rubrique' => 6206
	        	);
	    	} elseif( $res->id_rubrique == 6749 ) {
	    		// Questions de foot 974 - Euro 2020
	    		return array(
	            	'id_secteur' => 732,
	            	'id_rubrique' => 6749
	        	);
	    	} elseif( $res->id_rubrique == 6751 ) {
	    		// Kosa i di l’Euro - Euro 2020
	    		return array(
	            	'id_secteur' => 732,
	            	'id_rubrique' => 6751
	        	);
	    	} elseif( $res->id_rubrique == 6750 ) {
	    		// Le Meilleur de l’Euro - Euro 2020
	    		return array(
	            	'id_secteur' => 732,
	            	'id_rubrique' => 6750
	        	);
	    	}
	    	$rQuery = 'SELECT * FROM  `spip_rubriques` AS r WHERE `id_parent`=' . $res->id_rubrique . ' AND  `titre` = "Replay"';
	    	$rub = $this->db->query($rQuery)->fetchObject();
	    	return array(
	            	'id_secteur' => $rub->id_secteur,
	            	'id_rubrique' => $rub->id_rubrique
	        );
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

	protected function getRubrique($titre) {
		$Query = 'SELECT * FROM  `correspondance_replay` WHERE `titre` =  "' . $titre . '"';
		try {
			$res = $this->db->query($Query)->fetchObject();
            if ($res && $res->id_rubrique > 0) {
				$rQuery = "SELECT id_secteur, id_rubrique FROM spip_rubriques WHERE id_parent = ".$res->id_rubrique." AND titre = 'Replay'";
            	$rub = $this->db->query($rQuery)->fetchObject();
	            return array(
	            	'id_secteur' => $rub->id_secteur,
	            	'id_rubrique' => $rub->id_rubrique
	            );
            } else {
            	return array(
            		'id_secteur' => 89,
            		'id_rubrique' => 89
            	);
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
	}

	public function IsJtWeekend($date_start, $program)
	{
		$array_jt = array(
            'LE 19H00' => 'LE 19H00',
            'LE 12H30' => 'LE 12H30'
        );
        $we_array = array('Fri', 'Sat', 'Sun');
        $jour_we = date('D', strtotime($date_start));
		if (array_key_exists(strtoupper($program), $array_jt) && in_array($jour_we, $we_array)) {
            $log  = date('d-m-Y H:i:s'). " -> JT Weekend ".PHP_EOL;
            if ($jour_we == 'Fri' && strtoupper($program) == 'LE 19H00') {
                // Save 'LE 19H00' in JT Weekend
                return array(
                  'id_rubrique' => 6268,
                  'id_secteur' => 742
                );
            } elseif ($jour_we != 'Fri') {
                return array(
                  'id_rubrique' => 6268,
                  'id_secteur' => 742
                );
            }
        } 
        return false;
	}
}

?>