<?php 
/**
 * 
 */
class Logger
{	
	private $texte;
    private $log_name;
	
	public function __construct($name)
    {
        date_default_timezone_set('Indian/Reunion');
        $this->texte = "";
        $this->log_name = 'logs_'.$name.'_'.date("d-m-Y").'.txt';
    }

	public function info($str)
    {
        try {
            $this->texte = date('d-m-Y H:i:s') . ' -> ' . $str .PHP_EOL;
            file_put_contents($this->log_name, $this->texte, FILE_APPEND);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }
}

?>