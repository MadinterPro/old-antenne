<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET,POST,PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// generate json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

include_once 'config/database.php';
include_once 'objects/article.php';
include_once 'objects/document.php';
include_once 'objects/logger.php';

$database = new Database();
$db = $database->getConnection();
$article = new Article($db);
$document = new Document($db);
$log = new Logger('trigger');
$key = 'B2L%DsfkùfkK56FXvjiaXDHu0ZiS';

/*$headers = [];
foreach ($_SERVER as $name => $value) {
	if (substr($name, 0, 5) == 'HTTP_') {
		$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
	}
}
$log->info('headers : '. PHP_EOL . json_encode($headers));*/

$data = json_decode(file_get_contents("php://input"), FALSE);
$log->info('Données : '. PHP_EOL . json_encode($data));

if(!empty($data) && !is_null($data)) {
  /*if (isset($data->webhook_id) && $data->webhook_id == 'rukOt4aB') {
    if ($data->media_id) {
      $log->info("Recherche id_article en fonction du mediaID");
      $id_article = $article->findByMediaID($data->media_id);
      if ($id_article) {
        $log->info("ID_ARTICLE : ". $id_article);
        $log->info("Update statut article");
        $article->updateStatut($id_article);
        $log->info("Update statut document");
        $document->updateStatut($id_article);
        http_response_code(200);
        echo json_encode(array(
          "response" => 200,
          "message" => "Modification statut effectué",
          "id_article" => $id_article
          'broadcastID' => $BroadcastID
        )); exit();
      } else {
        $eMessage = "Aucun article correspond au mediaID : " . $data->media_id;
        $error = true;
      }
    } else {
      $eMessage = "mediaID non défini";
      $error = true;
    }
  } else {
    $eMessage = "Données incorrectes, webhook non défini";
    $error = true;
  }*/
  http_response_code(200);
    echo json_encode(array(
          "response" => 200,
          "message" => "Données bien reçues"
  )); exit();
} else {
    $eMessage = "Données vide ou null";
    $error = true;
}
$log->info($eMessage);
$log->info("-------------------------");
http_response_code(401);
echo json_encode(array("message" => $eMessage));

exit();