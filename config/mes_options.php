<?php
$GLOBALS['ignore_auth_http'] = true;
$GLOBALS['spip_pipeline']['affichage_final'] .= '|cdn_cahri';
// Ajout Laurent taille + durée cache
$quota_cache = 100;
define('_DUREE_CACHE_DEFAUT', 900);
define('_MYSQL_SQL_MODE_TEXT_NOT_NULL',1);

$GLOBALS['nombre_de_logs'] = 10; // 4 fichiers au plus
$GLOBALS['taille_des_logs'] = 10000; // de 100ko au plus
//
//  forcer l'exécution, à chaque visite de page, des tâches routinières programmées
define('_DIRECT_CRON_FORCE',true);
// pour réduire fortement les temps de réponse du site,
// ne pas lancer le cron par fsockopen/cURL
// car la configuration du serveur ne le permet pas.
define('_HTML_BG_CRON_FORCE', TRUE);

if (function_exists("date_default_timezone_set")) {
    date_default_timezone_set("Indian/Reunion");
}

function cdn_cahri_asset($reg) {
    $original = $lien = $reg[2];
    if (!preg_match(",^https,", $lien)) {
        if ($_SERVER["SERVER_NAME"] == "www.antennereunion.fr") {
            $lien = "https://cdn.antenne.re/antenne/$lien";
        } else {
            $lien = "https://antenne.ser-rct-01.antennereunion.fr/$lien";
        }
        // $lien = "https://cdn.antenne.re/antenne/$lien";
        return str_replace($original, $lien, $reg[0]);
    } else {
        return $reg[0];
    }
}

function cdn_cahri($flux) {
    if (!is_dir('/Users')) {
        $flux = preg_replace_callback(",[[:space:]](url|src|href)=[\"\']((squelettes|IMG|images|assets)/[^\"\']*)[\"\'],", "cdn_cahri_asset", $flux);
    }
    return $flux;
} 


$GLOBALS['url_arbo_types']=array(
    'rubrique'=>'', // pas de type pour les rubriques
    'article'=>'',
    'mot'=>'tags'
);
    
    // déclarer la table  $prefix_top_replays 
$GLOBALS['table_des_tables']['top_replays']='top_replays';


function balise_ISOCODECOUNTRY($p) {
    // $ws points to a non-existing address, no need to waste resources
    $p->code = '';
    return $p;

    // legacy code:
    $url = getBaseUrl() . "squelettes/maxmind/api2.php?var_mode=calcul";
    if ($url === "http://art.antenne.re//squelettes/maxmind/api2.php?var_mode=calcul")
        $url = "http://antenne:art2014@art.antenne.re/squelettes/maxmind/api2.php?var_mode=calcul";
    $ip = $GLOBALS["_ENV"]["GEOIP_ADDR"];
    //$ip = "41.74.26.54";
//    $ip = get_client_ip();
//    if($ip === "41.74.26.54")
//    {
//        $p->code = "MADINTER";
//        return $p;
//    }
        
    $ws = $url."&ip=".$ip;
    //var_dump($ws);
    $isocode = file_get_contents($ws);
    //var_dump($isocode);die;
    $p->code = strtoupper($isocode);
    return $p;
}

function balise_ISOCODECOUNTRYIP($p) {
    $url = getBaseUrl() . "squelettes/maxmind/api2.php?var_mode=calcul";
    if ($url === "http://art.antenne.re//squelettes/maxmind/api2.php?var_mode=calcul")
        $url = "http://antenne:art2014@art.antenne.re/squelettes/maxmind/api2.php?var_mode=calcul";
    $ip = $GLOBALS["_ENV"]["GEOIP_ADDR"];        
    $ws = $url."&ip=".$ip;
    $isocode = file_get_contents($ws);
    $debug = "IP=".$ip." ; ISOCODE=".$isocode." ; WS=".$url;
    var_dump($debug); die;
    $p->code = $ip;
    return $p;
}

/**
 * Suppose, you are browsing in your localhost
 * http://localhost/myproject/index.php?id=8
 */
function getBaseUrl() {
    // output: /myproject/index.php
    $currentPath = $GLOBALS['_SERVER']['PHP_SELF'];

    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
    $pathInfo = pathinfo($currentPath);

    // output: localhost
    $hostName = $GLOBALS['_SERVER']['HTTP_HOST'];

    // output: http://
    // $protocol = strtolower(substr($GLOBALS['_SERVER']["SERVER_PROTOCOL"], 0, 5)) == 'https://' ? 'https://' : 'https://';
    $protocol = 'https://';

    // return: http://localhost/myproject/
    return $protocol . $hostName . $pathInfo['dirname'] . "/";
}

function get_client_ip() {
    $ipaddress = '';
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}


function couper_formatage($texte, $limite) {
    // la longueur du texte est <= $limite, on retourne le texte entier
    if (strlen($texte) <= $limite) return $texte;
    // on fait la coupure avant le 1e espace après $limite caractères
    $texte = nl2br($texte);
    $new_texte = substr($texte,0, $limite);
    $pos = strrpos($new_texte, " ");
    // s'il y a un espace après $limite caractères ou juste après $limite caractères
    // on retourne la partie de $texte jusqu'avant cet espace
    if (is_integer($pos) && $pos) return substr($new_texte, 0, $pos) . " (...)";
    // sinon (pas d'espace après $limite caractères ou juste après $limite caractères) on retourne le texte
    else return $new_texte;
}

define('_LOG_FILELINE', true) ;
define('_LOG_FILTRE_GRAVITE',8);
define('_DEBUG_SLOW_QUERIES', true);
define('_BOUCLE_PROFILER', 5000);

?>
