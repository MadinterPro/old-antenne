<?php
require_once 'Mobile_Detect.php';
session_start();
if(isset($_SESSION['affiche_popup_mobile']) && $_SESSION['affiche_popup_mobile'] == 1){//Efa nipoitra le popup ts apoitra tson
    echo json_encode(array('return' => 2));
}else{
    $detect = new Mobile_Detect;
    if($detect->isMobile() || $detect->isTablet()){
        $href = "";
        if($detect->isiOS())
            $href = "https://itunes.apple.com/fr/app/antenne-reunion-television/id927960182?mt=8";
        elseif($detect->isAndroidOS())
            $href = "https://play.google.com/store/apps/details?id=com.goodbarber.antennereunion";
        else
            $href = "http://www.antennereunion.fr/les-applications-mobiles";
            
        $_SESSION['affiche_popup_mobile'] = 1;
        echo json_encode(array("return" => 1, "href" => $href));
    }
    else{   
        echo json_encode(array('return' => 2));
    }
}

