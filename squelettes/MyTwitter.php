<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MyTwitter
 *
 * @author Lionnel
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/squelettes/twitteroauth/twitteroauth.php');

class MyTwitter extends TwitterOAuth {

    public static function DoDump(&$var, $var_name = NULL, $indent = NULL, $reference = NULL) {
	$do_dump_indent = "<span style='color:#eeeeee;'>|</span> &nbsp;&nbsp; ";
	$reference = $reference . $var_name;
	$keyvar = 'the_do_dump_recursion_protection_scheme';
	$keyname = 'referenced_object_name';

	if (is_array($var) && isset($var[$keyvar])) {
	    $real_var = &$var[$keyvar];
	    $real_name = &$var[$keyname];
	    $type = ucfirst(gettype($real_var));
	    echo "$indent$var_name <span style='color:#a2a2a2'>$type</span> = <span style='color:#e87800;'>&amp;$real_name</span><br>";
	} else {
	    $var = array($keyvar => $var, $keyname => $reference);
	    $avar = &$var[$keyvar];

	    $type = ucfirst(gettype($avar));
	    if ($type == "String")
		$type_color = "<span style='color:green'>";
	    elseif ($type == "Integer")
		$type_color = "<span style='color:red'>";
	    elseif ($type == "Double") {
		$type_color = "<span style='color:#0099c5'>";
		$type = "Float";
	    } elseif ($type == "Boolean")
		$type_color = "<span style='color:#92008d'>";
	    elseif ($type == "NULL")
		$type_color = "<span style='color:black'>";

	    if (is_array($avar)) {
		$count = count($avar);
		echo "$indent" . ($var_name ? "$var_name => " : "") . "<span style='color:#a2a2a2'>$type ($count)</span><br>$indent(<br>";
		$keys = array_keys($avar);
		foreach ($keys as $name) {
		    $value = &$avar[$name];
		    MyTwitter::DoDump($value, "['$name']", $indent . $do_dump_indent, $reference);
		}
		echo "$indent)<br>";
	    } elseif (is_object($avar)) {
		echo "$indent$var_name <span style='color:#a2a2a2'>$type</span><br>$indent(<br>";
		foreach ($avar as $name => $value)
		    MyTwitter::DoDump($value, "$name", $indent . $do_dump_indent, $reference);
		echo "$indent)<br>";
	    } elseif (is_int($avar))
		echo "$indent$var_name = <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $type_color$avar</span><br>";
	    elseif (is_string($avar))
		echo "$indent$var_name = <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $type_color\"$avar\"</span><br>";
	    elseif (is_float($avar))
		echo "$indent$var_name = <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $type_color$avar</span><br>";
	    elseif (is_bool($avar))
		echo "$indent$var_name = <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $type_color" . ($avar == 1 ? "TRUE" : "FALSE") . "</span><br>";
	    elseif (is_null($avar))
		echo "$indent$var_name = <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> {$type_color}NULL</span><br>";
	    else
		echo "$indent$var_name = <span style='color:#a2a2a2'>$type(" . strlen($avar) . ")</span> $avar<br>";

	    $var = $var[$keyvar];
	}
    }

    public function TwitterEntityDecode($tweet) {
	$regex = '/http([s]?):\/\/([^\ \)$]*)/';
	$link_pattern = '<a href="http$1://$2" rel="nofollow" title="$2">http$1://$2</a>';
	$tweetDecode = preg_replace($regex, $link_pattern, $tweet);
	$regex = '/@([a-zA-Z0-9_]*)/';
	$link_pattern = '<a href="http://twitter.com/$1" title="profil de $1 sur Twitter" rel="nofollow">@$1</a>';
	$tweetDecode = preg_replace($regex, $link_pattern, $tweetDecode);
	$regex = '/\#([a-zA-Z0-9_]*)/';
	$link_pattern = '<a href="http://search.twitter.com/search?q=%23$1" title="rechercher $1 sur Twitter" rel="nofollow">\#$1</a>';
	$tweet = preg_replace($regex, $link_pattern, $tweet);
	return $tweetDecode;
    }

    public function GetMentionsTimeline($count = 1){
	//$query_mt = "https://api.twitter.com/1.1/statuses/mentions_timeline.json?count=".$count;
	//$query_mt = "https://api.twitter.com/1.1/statuses/user_timeline.json";
	
	$query_mt = "https://api.twitter.com/1.1/search/tweets.json?q=%23KFLM&count=200";
	$content = $this->get($query_mt);
	return $content;
    }
    
    public function GetProfileImage($sn, $size = "bigger"){
	return "https://api.twitter.com/1/users/profile_image?screen_name=".$sn."&size=".$size;
    }
}