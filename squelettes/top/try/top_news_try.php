<?php

function getService() {
    // Creates and returns the Analytics service object.
    // Load the Google API PHP Client Library.
    require_once 'google-api-php-client/src/Google/autoload.php';

    // Create and configure a new client object.
    $client = new \Google_Client();

    putenv('GOOGLE_APPLICATION_CREDENTIALS=TopInfo.json');
    $client->useApplicationDefaultCredentials();
    $client->addScope('https://www.googleapis.com/auth/analytics.readonly');

    //return $analytics;
    return new \Google_Service_Analytics($client);
}

function getFirstprofileId(&$analytics) {
    // Get the user's first view (profile) ID.
    // Get the list of accounts for the authorized user.
    $accounts = $analytics->management_accountSummaries->listManagementAccountSummaries();

    if (count($accounts->getItems()) > 0) {
        $items = $accounts->getItems();
        $firstAccountId = $items[0]->getId();

        // Get the list of properties for the authorized user.
        $properties = $analytics->management_webproperties
                ->listManagementWebproperties($firstAccountId);

        if (count($properties->getItems()) > 0) {
            $items = $properties->getItems();
            $firstPropertyId = $items[0]->getId();

            // Get the list of views (profiles) for the authorized user.
            $profiles = $analytics->management_profiles
                    ->listManagementProfiles($firstAccountId, $firstPropertyId);

            if (count($profiles->getItems()) > 0) {
                $items = $profiles->getItems();

                // Return the first view (profile) ID.
                return $items[0]->getId();
            } else {
                throw new Exception('No views (profiles) found for this user.');
            }
        } else {
            throw new Exception('No properties found for this user.');
        }
    } else {
        throw new Exception('No accounts found for this user.');
    }
}

function getResults(&$analytics, $profileId) {
    ########## Google analytics Settings.. #############
    $google_analytics_dimensions = 'ga:pagePath,ga:pageTitle'; //no change needed (optional)
    $google_analytics_metrics = 'ga:pageviews'; //no change needed (optional)
    $google_analytics_sort_by = '-ga:pageviews'; //no change needed (optional)
    $google_analytics_max_results = '300'; //no change needed (optional)    
    //set start date to previous month
    $start_date = date("Y-m-d", strtotime("-2 day"));
    //end date as today
    $end_date = date("Y-m-d");
    //analytics parameters (check configuration file)
    $params = array('dimensions' => $google_analytics_dimensions,
        'sort' => $google_analytics_sort_by,
        'filters' => 'ga:pagePath=@news/',
        'max-results' => $google_analytics_max_results);

    //get results from google analytics
    //echo 'id_profil ok ok: '.$profileId;
    return $analytics->data_ga->get('ga:' . $profileId, $start_date, $end_date, $google_analytics_metrics, $params);
}

function printResults(&$results) {
    $page_url_prefix = 'http://www.antennereunion.fr';
    $mysqli = new mysqli('localhost', 'antenne_spipAR', 'antv3_2009', 'antenne_art_spip');
    $pages_e = array();
    $pages_sf = array();
    $pages_im = array();
    $pgrs = array();
    $nbr_e = 0;
    $nbr_sf = 0;
    $nbr_im = 0;
    $date_insert = date("Y-m-d H:i:s");
    $rows = $results->rows;
    if ($rows) {
        echo '<ul>';
        foreach ($rows as $row) {
            $title = $row[0];
            $tableau = explode('/', $title);
            $type = $tableau[1];
            $pgr = $tableau[2];
            $dernier = $tableau[count($tableau) - 1];
            $titre = explode('-', $dernier);
            $numero = $titre[0];
            if (!in_array($pgr, $pgrs)) {
                $pgrs[] = $pgr;
                if (is_numeric($numero)) {
                    if ($type == 'emissions' && $nbr_e <= 6) {
                        $pages_e[] = '("' . $row[0] . '","' . $numero . '",' . $row[2] . ',"' . $date_insert . '","' . $type . '")';
                        $nbr_e = $nbr_e + 1;
                    }
                    if ($type == 'series-et-fictions' && $nbr_sf <= 6) {
                        $pages_sf[] = '("' . $row[0] . '","' . $numero . '",' . $row[2] . ',"' . $date_insert . '","' . $type . '")';
                        $nbr_sf = $nbr_sf + 1;
                    }
                    if ($type == 'info-et-magazines' && $nbr_im <= 6) {
                        $pages_im[] = '("' . $row[0] . '","' . $numero . '",' . $row[2] . ',"' . $date_insert . '","' . $type . '")';
                        $nbr_im = $nbr_im + 1;
                    }
                }
            }
            echo '<li><a href="' . $page_url_prefix . $row[0] . '">' . $row[1] . '</a></li>';
        }
        echo '</ul>';
        echo "<pre>";
        print_r($pages_e);
        $count_e = count($pages_e);
        print_r($pages_sf);
        $count_sf = count($pages_sf);
        print_r($pages_im);
        $count_im = count($pages_im);
        echo "</pre>";

        if ($count_e > 0) {
            $suppr_e = "SELECT * FROM `spip_top_news` WHERE `type` LIKE 'emissions' ORDER BY date_insert ASC LIMIT 0,".$count_e."";
            if ($to_suppr_e = $mysqli->query($suppr_e)) {                
                while ($row_e = $to_suppr_e->fetch_assoc()) {
                    echo 'Suppression de la ligne : ' . $row_e["id"] . '</br>';
                    $mysqli->query("DELETE FROM spip_top_news WHERE id = '" . $row_e["id"] . "'");
                }
                if ($mysqli->query("INSERT INTO spip_top_news (page_uri, article_id, total_views, date_insert, type) VALUES " . implode(',', $pages_e) . "")) {
                    echo '<br />Records emissions updated...</br>';
                } else {
                    echo 'Error database...';
                    echo $mysqli->error;
                }
            } else {
                if ($mysqli->query("INSERT INTO spip_top_news (page_uri, article_id, total_views, date_insert, type) VALUES " . implode(',', $pages_e) . "")) {
                    echo '<br />Records emissions updated...</br>';
                } else {
                    echo 'Error database...';
                    echo $mysqli->error;
                }
            }
        } else {
            echo 'Aucun top du type emission remonté depuis Google Analytics, aucune insertion...</br>';
        }

        if ($count_sf > 0) {
            $suppr_sf = "SELECT * FROM `spip_top_news` WHERE `type` LIKE 'series-et-fictions' ORDER BY date_insert ASC LIMIT 0,".$count_sf."";
            if ($to_suppr_sf = $mysqli->query($suppr_sf)) {
                while ($row_sf = $to_suppr_sf->fetch_assoc()) {
                    echo 'Suppression de la ligne : ' . $row_sf["id"] . '</br>';
                    $mysqli->query("DELETE FROM spip_top_news WHERE id = '" . $row_sf["id"] . "'");
                }
                if ($mysqli->query("INSERT INTO spip_top_news (page_uri, article_id, total_views, date_insert, type) VALUES " . implode(',', $pages_sf) . "")) {
                    echo '<br />Records série et fiction updated...</br>';
                } else {
                    echo 'Error database...';
                    echo $mysqli->error;
                }
            } else {
                if ($mysqli->query("INSERT INTO spip_top_news (page_uri, article_id, total_views, date_insert, type) VALUES " . implode(',', $pages_sf) . "")) {
                    echo '<br />Records série et fiction updated...</br>';
                } else {
                    echo 'Error database...';
                    echo $mysqli->error;
                }
            }
        } else {
            echo 'Aucun top du type série et fiction remonté depuis Google Analytics, aucune insertion...</br>';
        }

        if ($count_im > 0) {
            $suppr_im = "SELECT * FROM `spip_top_news` WHERE `type` LIKE 'info-et-magazines' ORDER BY date_insert ASC LIMIT 0,".$count_im."";
            if ($to_suppr_im = $mysqli->query($suppr_im)) {
                while ($row_im = $to_suppr_im->fetch_assoc()) {
                    echo 'Suppression de la ligne : ' . $row_im["id"] . '</br>';
                    $mysqli->query("DELETE FROM spip_top_news WHERE id = '" . $row_im["id"] . "'");
                }
                if ($mysqli->query("INSERT INTO spip_top_news (page_uri, article_id, total_views, date_insert, type) VALUES " . implode(',', $pages_im) . "")) {
                    echo '<br />Records infos et magazines updated...</br>';
                } else {
                    echo 'Error database...';
                    echo $mysqli->error;
                }
            } else {
                if ($mysqli->query("INSERT INTO spip_top_news (page_uri, article_id, total_views, date_insert, type) VALUES " . implode(',', $pages_im) . "")) {
                    echo '<br />Records infos et magazines updated...</br>';
                } else {
                    echo 'Error database...';
                    echo $mysqli->error;
                }
            }
        } else {
            echo 'Aucun top du type infos et magazines remonté depuis Google Analytics, aucune insertion...</br>';
        }
    }
}

$analytics = getService();
$profile = getFirstProfileId($analytics);
$results = getResults($analytics, $profile);
printResults($results);
?>