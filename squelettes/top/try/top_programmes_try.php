<?php

function getService() {
    // Creates and returns the Analytics service object.
    // Load the Google API PHP Client Library.
    require_once 'google-api-php-client/src/Google/autoload.php';

    // Create and configure a new client object.
    $client = new \Google_Client();

    putenv('GOOGLE_APPLICATION_CREDENTIALS=TopInfo.json');
    $client->useApplicationDefaultCredentials();
    $client->addScope('https://www.googleapis.com/auth/analytics.readonly');

    //return $analytics;
    return new \Google_Service_Analytics($client);
}

function getFirstprofileId(&$analytics) {
    // Get the user's first view (profile) ID.
    // Get the list of accounts for the authorized user.
    $accounts = $analytics->management_accountSummaries->listManagementAccountSummaries();

    if (count($accounts->getItems()) > 0) {
        $items = $accounts->getItems();
        $firstAccountId = $items[0]->getId();

        // Get the list of properties for the authorized user.
        $properties = $analytics->management_webproperties
                ->listManagementWebproperties($firstAccountId);

        if (count($properties->getItems()) > 0) {
            $items = $properties->getItems();
            $firstPropertyId = $items[0]->getId();

            // Get the list of views (profiles) for the authorized user.
            $profiles = $analytics->management_profiles
                    ->listManagementProfiles($firstAccountId, $firstPropertyId);

            if (count($profiles->getItems()) > 0) {
                $items = $profiles->getItems();

                // Return the first view (profile) ID.
                return $items[0]->getId();
            } else {
                throw new Exception('No views (profiles) found for this user.');
            }
        } else {
            throw new Exception('No properties found for this user.');
        }
    } else {
        throw new Exception('No accounts found for this user.');
    }
}

function getResults(&$analytics, $profileId) {
    ########## Google analytics Settings.. #############
    $google_analytics_dimensions = 'ga:pagePath,ga:pageTitle'; //no change needed (optional)
    $google_analytics_metrics = 'ga:pageviews'; //no change needed (optional)
    $google_analytics_sort_by = '-ga:pageviews'; //no change needed (optional)
    $google_analytics_max_results = '500'; //no change needed (optional)
        
    //set start date to previous month
    $start_date = date("Y-m-d", strtotime("-1 day"));
    //end date as today
    $end_date = date("Y-m-d");
    
    //analytics parameters (check configuration file)
        $params = array('dimensions' => $google_analytics_dimensions,
            'sort' => $google_analytics_sort_by,
            'max-results' => $google_analytics_max_results);

    //get results from google analytics
    //echo 'id_profil ok ok: '.$profileId;
    return $analytics->data_ga->get('ga:' . $profileId, $start_date, $end_date, $google_analytics_metrics, $params);
}

function printResults(&$results) {
    $page_url_prefix = 'http://www.antennereunion.fr';
    $mysqli = new mysqli('localhost', 'antenne_spipAR', 'antv3_2009', 'antenne_art_spip');
    $pages = array();
    $pgrs = array();
    $rows = $results->rows;
    if ($rows) {
        echo '<ul>';
        foreach ($rows as $row) {

            $title = $row[0];
            $tableau = explode('/', $title);
            $type = $tableau[1];
            
            $titre = explode('?', $tableau[2]);
            $pgr = $titre[0];

            //output top page link
            if (count($tableau) == 3 && ($type == 'emissions' || $type == 'series-et-fictions' || $type == 'info-et-magazines' )) {
                if(!in_array($pgr, $pgrs))  {
                    $pgrs[] = $pgr;
                    $pages[] = '("' . $row[0] . '","' . $pgr . '",' . $row[2] . ',"' . $type . '")';
                    echo '<li>' . $row[0] . '</li>';
                }
                
            }
        }
        echo '</ul>';

        //empty table
         $mysqli->query("TRUNCATE TABLE spip_top_programmes");    
        //insert all new top pages in the table
         if ($mysqli->query("INSERT INTO spip_top_programmes (page_uri, rubrique_url, total_views,type) VALUES " . implode(',', $pages) . "")) {
          echo '<br />Records updated...';
          } else {
          echo $mysqli->error;
          } 
    }
}

$analytics = getService();
$profile = getFirstProfileId($analytics);
$results = getResults($analytics, $profile);
printResults($results);
?>