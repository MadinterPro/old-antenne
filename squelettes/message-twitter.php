<?php

function coder($str) {
    //echo $str;
    $str = htmlentities($str, ENT_QUOTES, "UTF-8");
    //echo $str;
    //$str = html_entity_decode($str);		
    //$str = toUtf8($str);
    $str = str_replace('á', '&#225;', $str);
    $str = str_replace('Á', '&#193;', $str);
    $str = str_replace('â', '&#226;', $str);
    $str = str_replace('Â', '&#194;', $str);
    $str = str_replace('à', '&#224;', $str);
    $str = str_replace('À', '&#192;', $str);
    $str = str_replace('å', '&#229;', $str);
    $str = str_replace('Å', '&#197;', $str);
    $str = str_replace('ã', '&#227;', $str);
    $str = str_replace('Ã', '&#195;', $str);
    $str = str_replace('ä', '&#228;', $str);
    $str = str_replace('Ä', '&#196;', $str);
    $str = str_replace('æ', '&#230;', $str);
    $str = str_replace('Æ', '&#198;', $str);
    $str = str_replace('ç', '&#231;', $str);
    $str = str_replace('Ç', '&#199;', $str);
    $str = str_replace('é', '&#233;', $str);
    $str = str_replace('é', '&#233;', $str);
    $str = str_replace('É', '&#201;', $str);
    $str = str_replace('ê', '&#234;', $str);
    $str = str_replace('Ê', '&#202;', $str);
    $str = str_replace('è', '&#232;', $str);
    $str = str_replace('È', '&#200;', $str);
    $str = str_replace('ë', '&#235;', $str);
    $str = str_replace('Ë', '&#203;', $str);
    $str = str_replace('í', '&#237;', $str);
    $str = str_replace('Í', '&#205;', $str);
    $str = str_replace('î', '&#238;', $str);
    $str = str_replace('Î', '&#206;', $str);
    $str = str_replace('ì', '&#236;', $str);
    $str = str_replace('Ì', '&#204;', $str);
    $str = str_replace('ï', '&#239;', $str);
    $str = str_replace('Ï', '&#207;', $str);
    $str = str_replace('ñ', '&#241;', $str);
    $str = str_replace('Ñ', '&#209;', $str);
    $str = str_replace('ó', '&#243;', $str);
    $str = str_replace('Ó', '&#211;', $str);
    $str = str_replace('ô', '&#244;', $str);
    $str = str_replace('Ô', '&#212;', $str);
    $str = str_replace('ò', '&#242;', $str);
    $str = str_replace('Ò', '&#210;', $str);
    $str = str_replace('ø', '&#248;', $str);
    $str = str_replace('Ø', '&#216;', $str);
    $str = str_replace('õ', '&#245;', $str);
    $str = str_replace('Õ', '&#213;', $str);
    $str = str_replace('ö', '&#246;', $str);
    $str = str_replace('Ö', '&#214;', $str);
    $str = str_replace('œ', '&#339;', $str);
    $str = str_replace('Œ', '&#338;', $str);
    $str = str_replace('š', '&#353;', $str);
    $str = str_replace('Š', '&#352;', $str);
    $str = str_replace('ß', '&#223;', $str);
    $str = str_replace('ð', '&#240;', $str);
    $str = str_replace('Ð', '&#208;', $str);
    $str = str_replace('þ', '&#254;', $str);
    $str = str_replace('Þ', '&#222;', $str);
    $str = str_replace('ú', '&#250;', $str);
    $str = str_replace('Ú', '&#218;', $str);
    $str = str_replace('û', '&#251;', $str);
    $str = str_replace('Û', '&#219;', $str);
    $str = str_replace('ù', '&#249;', $str);
    $str = str_replace('Ù', '&#217;', $str);
    $str = str_replace('ü', '&#252;', $str);
    $str = str_replace('Ü', '&#220;', $str);
    $str = str_replace('ý', '&#253;', $str);
    $str = str_replace('Ý', '&#221;', $str);
    $str = str_replace('ÿ', '&#255;', $str);
    $str = str_replace('ÿ', '&#376;', $str);
    $str = str_replace('&aacute;', '&#225;', $str);
    $str = str_replace('&Aacute;', '&#193;', $str);
    $str = str_replace('&acirc;', '&#226;', $str);
    $str = str_replace('&Acirc;', '&#194;', $str);
    $str = str_replace('&agrave;', '&#224;', $str);
    $str = str_replace('&Agrave;', '&#192;', $str);
    $str = str_replace('&aring;', '&#229;', $str);
    $str = str_replace('&Aring;', '&#197;', $str);
    $str = str_replace('&atilde;', '&#227;', $str);
    $str = str_replace('&Atilde;', '&#195;', $str);
    $str = str_replace('&auml;', '&#228;', $str);
    $str = str_replace('&Auml;', '&#196;', $str);
    $str = str_replace('&aelig;', '&#230;', $str);
    $str = str_replace('&AElig;', '&#198;', $str);
    $str = str_replace('&ccedil;', '&#231;', $str);
    $str = str_replace('&Ccedil;', '&#199;', $str);
    $str = str_replace('&eacute;', '&#233;', $str);
    $str = str_replace('&Eacute;', '&#201;', $str);
    $str = str_replace('&ecirc;', '&#234;', $str);
    $str = str_replace('&Ecirc;', '&#202;', $str);
    $str = str_replace('&egrave;', '&#232;', $str);
    $str = str_replace('&Egrave;', '&#200;', $str);
    $str = str_replace('&euml;', '&#235;', $str);
    $str = str_replace('&Euml;', '&#203;', $str);
    $str = str_replace('&iacute;', '&#237;', $str);
    $str = str_replace('&Iacute;', '&#205;', $str);
    $str = str_replace('&icirc;', '&#238;', $str);
    $str = str_replace('&Icirc;', '&#206;', $str);
    $str = str_replace('&igrave;', '&#236;', $str);
    $str = str_replace('&Igrave;', '&#204;', $str);
    $str = str_replace('&iuml;', '&#239;', $str);
    $str = str_replace('&Iuml;', '&#207;', $str);
    $str = str_replace('&ntilde;', '&#241;', $str);
    $str = str_replace('&Ntilde;', '&#209;', $str);
    $str = str_replace('&oacute;', '&#243;', $str);
    $str = str_replace('&Oacute;', '&#211;', $str);
    $str = str_replace('&ocirc;', '&#244;', $str);
    $str = str_replace('&Ocirc;', '&#212;', $str);
    $str = str_replace('&ograve;', '&#242;', $str);
    $str = str_replace('&Ograve;', '&#210;', $str);
    $str = str_replace('&oslash;', '&#248;', $str);
    $str = str_replace('&Oslash;', '&#216;', $str);
    $str = str_replace('&otilde;', '&#245;', $str);
    $str = str_replace('&Otilde;', '&#213;', $str);
    $str = str_replace('&ouml;', '&#246;', $str);
    $str = str_replace('&Ouml;', '&#214;', $str);
    $str = str_replace('&oelig;', '&#339;', $str);
    $str = str_replace('&OElig;', '&#338;', $str);
    $str = str_replace('&scaron;', '&#353;', $str);
    $str = str_replace('&Scaron;', '&#352;', $str);
    $str = str_replace('&szlig;', '&#223;', $str);
    $str = str_replace('&eth;', '&#240;', $str);
    $str = str_replace('&ETH;', '&#208;', $str);
    $str = str_replace('&thorn;', '&#254;', $str);
    $str = str_replace('&THORN;', '&#222;', $str);
    $str = str_replace('&uacute;', '&#250;', $str);
    $str = str_replace('&Uacute;', '&#218;', $str);
    $str = str_replace('&ucirc;', '&#251;', $str);
    $str = str_replace('&Ucirc;', '&#219;', $str);
    $str = str_replace('&ugrave;', '&#249;', $str);
    $str = str_replace('&Ugrave;', '&#217;', $str);
    $str = str_replace('&uuml;', '&#252;', $str);
    $str = str_replace('&Uuml;', '&#220;', $str);
    $str = str_replace('&yacute;', '&#253;', $str);
    $str = str_replace('&Yacute;', '&#221;', $str);
    $str = str_replace('&yuml;', '&#255;', $str);
    $str = str_replace('&Yuml;', '&#376;', $str);
    $str = str_replace('&rsquo;', '&#8217;', $str);
    $str = str_replace('|', ' ', $str);
    $str = str_replace('&nbsp;', ' ', $str);
    $str = str_replace('&amp;', '&#38;', $str);    
    $str = str_replace('&quot;', '&#34;', $str);
    $str = str_replace('&hearts;', '&#9829;', $str);
    $str = str_replace('&euro;', '&#8364;', $str);
    $str = str_replace('&hellip;', '&#8230;', $str);
    $str = str_replace('&infin;', '', $str); 
    $str = str_replace('&dagger;', '&#8224;', $str);
    $str = str_replace('&bull;', '&#8226;', $str);
    
    $str = str_replace('…', '', $str);
    $str = str_replace('⚓', '', $str);
    $str = str_replace('∞', '', $str);
    $str = str_replace('&#38;nbsp;', '', $str);
     
    
     	
    
    return $str;
}

function RecupererNom($chaine) {
    $explode = explode(" ", $chaine);
    $ret = "";
    foreach ($explode as $i => $elt) {
        if ($i != 0 && $i != count($explode) - 1 && $i != count($explode) - 2 && $i != count($explode) - 3) {
            $ret .= $elt;
            if ($i < count($explode) - 4) {
                $ret .= " ";
            }
        }
    }
    return $ret;
}

define('HOST', 'localhost');
define('USER', 'admin_INFO974');
define('PASS', 'info2009');
define('DB', 'ANT_info974');

$link = mysql_connect(HOST, USER, PASS);
if (!$link)
    echo "erreur de connexion au serveur " . $host;
mysql_select_db(DB) or die("Impossible de se connecter à la BDD ");
mysql_set_charset('utf8', $link);


header('Pragma: no-cache');
header('Content-Type: text/xml; charset=utf-8');
echo "<?xml
	version='1.0' encoding='utf-8'?>
    <items>
	<title>Messages Municipales Second Tour</title>	
	<language>fr</language>";

$i = 1;

$xml = simplexml_load_file("http://rss.coveritlive.com/rss.php?altcast_code=3b623a3b59");

$tableau = array();
foreach ($xml->channel->item as $item) {
    
    $tableau[] = $item;
}


usort($tableau, function($a, $b)
{
    return strcmp(new Datetime($a->pubDate), new Datetime($b->pubDate));
});


foreach ($tableau as $item) {
    $title = "";
    $description = "";
    $titles = explode(" ", $item->title);
    $title = $titles[1];
    
    if(preg_match('/via Twitter/',$item->title)){
        $description = strip_tags($item->description);
    }else{
        $description = $item->description;
    }
    
    $heure = date('H', $item->pubDate);
    
    if (!preg_match('/img src/',$description) && !preg_match('/Linfo.re/',$item->title) && !preg_match('/Antenne Réunion/',$item->title) && (mb_strlen($description) <= 130) && $title !="" ){
        $title = "";
        if(preg_match('/Commentaire de la part de/',$item->title)){
            $titles = explode("Commentaire de la part de ", $item->title);
            $title = $titles[1];
        }else{
            $title = RecupererNom($item->title);
        }
        
        
        $order = '';
        if ($i < 10) {
            $order = '0' . $i;
        } else {
            $order = $i;
        }
        $i++;
        $flux = "<item xml:lang='fr'>";
        $flux.="<order>" . $order . "</order>";
        $flux.="<nom>" . strtoupper(coder($title)) . "</nom>";
        $flux.="<message>" . coder(str_replace('"','',$description)) . "</message>";
        $flux.="</item>";
        echo $flux;
    }
    
    
}


$liste_messages = mysql_query("SELECT * FROM spip_nl_user_evenement WHERE fete = 'Municipales 1er tour' AND statut = 1 ORDER BY date_inscription ASC");
while ($row = mysql_fetch_array($liste_messages)) {
    //echo utf8_encode($row['message']);
    //$test_presence=$row['message'];
    //var_dump($row['message']);
    if(mb_strlen($row['message']) <= 130){
        $orig = '';
        if ($row['origine'] == 'twitter') {
            $orig = 'twitter';
        } else {
            $orig = $row['ville'];
        }

        $order = '';
        if ($i < 10) {
            $order = '0' . $i;
        } else {
            $order = $i;
        }
        $flux = "<item xml:lang='fr'>";
        $flux.="<order>" . $order . "</order>";
        $flux.="<nom>" . strtoupper(coder($row['nom'])) . "</nom>";
        $flux.="<message>" . coder($row['message']) . "</message>";
        $flux.="</item>";
        echo $flux;

        $i++;
    }
    
}


echo "</items>";
?>
