<?php

/**
 * Ce script récupère les POST de l'inscription du jeu quizz ART (ARM)
 * Lionnel
 */
session_start();
require 'PHPMailer-master/PHPMailerAutoload.php';
//définition des constantes
if ($_SERVER["SERVER_ADDR"] == "127.0.0.1") {
    define("DATA_BASE_HOST", "localhost");
    define("DATA_BASE_NAME", "art");
    define("DATA_BASE_LOGIN", "root");
    define("DATA_BASE_PASSWORD", "");
    define("DATA_BASE_TABLE_INSCRIPTION_QUIZZ", "inscription_quizz");
} else {
    //BDD linfo
    define("DATA_BASE_HOST", "localhost");
    define("DATA_BASE_NAME", "antenne_art_spip");
    define("DATA_BASE_LOGIN", "antenne_spipAR");
    define("DATA_BASE_PASSWORD", "antv3_2009");
    define("DATA_BASE_TABLE_INSCRIPTION_QUIZZ", "inscription_quizz");
}

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

$db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$step = filter_input(INPUT_GET, 'step', FILTER_SANITIZE_SPECIAL_CHARS);


switch ($step) {
    case "1":
        $civilite = filter_input(INPUT_POST, 'civilite', FILTER_SANITIZE_SPECIAL_CHARS);
        $bon_post = filter_input(INPUT_POST, 'bon', FILTER_SANITIZE_SPECIAL_CHARS);
        $bon = $bon_post === "option1" ? 1 : 0;
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
        $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS);
        $prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS);
        $date = filter_input(INPUT_POST, 'date_naissance', FILTER_SANITIZE_SPECIAL_CHARS);
        $date_naissance = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $date)));
        $code_postal = filter_input(INPUT_POST, 'code_postal', FILTER_SANITIZE_SPECIAL_CHARS);
        $adresse = filter_input(INPUT_POST, 'adresse', FILTER_SANITIZE_SPECIAL_CHARS);
        $ville = filter_input(INPUT_POST, 'ville', FILTER_SANITIZE_SPECIAL_CHARS);

        $g_recaptcha_response = $_POST["g_recaptcha_response"]; //POST à ne pas néttoyer

        $ip = get_client_ip();
        $reponse_recaptcha = PostRecaptcha($g_recaptcha_response, $ip);
        if (strpos($reponse_recaptcha, 'true') !== false) {
            $sql_select = "SELECT email FROM ".DATA_BASE_TABLE_INSCRIPTION_QUIZZ." WHERE email='".$email."' AND tirage=1";
            $req_select = $db->query($sql_select)->fetchAll();  
            
            if(count($req_select) > 0){ //l'adresse email existe déjà dans la base de données
                $_SESSION["id_inscription"] = $req_select[0]["id"];
                echo "existe";
                die;
            }
            
            $sql = "INSERT INTO " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " (civilite, bon, email, nom, prenom, date_naissance, code_postal, adresse, ville, date) "
                    . " VALUES ('$civilite', $bon, '$email', '$nom', '$prenom', '$date_naissance', '$code_postal', '$adresse', '$ville', Now())";
            $req = $db->exec($sql);
            $lastId = $db->lastInsertId();
            $_SESSION["id_inscription"] = $lastId;
            echo $lastId;
        } else {
            echo "captcha";
            die;
        }
        
        

         $body = '<html xmlns="http://www.w3.org/1999/xhtml">' .
                    '<head>' .
                        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                        '<title>ARM jeux</title>' .
                    '</head>' .

                    '<body>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                            '<tr height="20">' .
                                '<td align="center"></td>' .
                            '</tr>' .
                            '<tr>' .
                                '<td align="center"><a target="_blank" href="http://www.antennereunion.fr/"><img alt="Logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/logo-ar.png" /></a></td>'.
                            '</tr>' .

                       '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                            '<tr height="20">' .
                                '<td align="center">&nbsp;</td>' .
                                '<td align="center">&nbsp;</td>' .
                            '</tr>' .
                            '<tr>' .
                                '<td align="center" width="20">&nbsp;</td>' .
                                '<td align="left" width="580" style="color: #ffffff; font-size: 20px; font-family: Arial, Helvetica, sans-serif;">Bonjour ' . $prenom . ',<br />' .
                                    '<br />' .
                                    'Vous &ecirc;tes bien inscrit(e) au Grand Jeu des s&eacute;ries d\'Antenne R&eacute;union avec &agrave; la cl&eacute; : ' .
                                    '5 tablettes Samsung Tab 3 Lite et une montre Galaxy Gear 2!' .
                                    '<br />' .
                                    '<br />' .
                                    'Merci pour votre participation et &agrave; bient&ocirc;t sur Antenne R&eacute;union.<br /></td>' .
                            '</tr>' .
                            '<tr height="20">' .
                                '<td align="center">&nbsp;</td>' .
                                '<td align="center">&nbsp;</td>' .
                            '</tr>' .
                            '<tr>' .
                                '<td align="center" width="2">&nbsp;</td>' .
                                '<td align="center"><img alt="Lots" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/lots-jeux.png" /></td>' .
                            '</tr>' .
                       '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                            '<tr height="20">' .
                                '<td align="center" width="20">&nbsp;</td>' .
                                '<td align="center">&nbsp;</td>' .
                                '<td align="center" width="20">&nbsp;</td>' .
                            '</tr>' .
                            '<tr height="40" valign="middle">' .
                                '<td align="center" width="105">&nbsp;</td>' .
                                '<td align="center"  valign="middle" bgcolor="#014a99" style="color: #ffffff; font-size: 14px; font-family: Arial, Helvetica, sans-serif; "><a style="text-decoration: none; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif;" href="http://www.antennereunion.fr/series-et-fictions"> <strong>D&eacute;couvrez tout sur les s&eacute;ries Antenne R&eacute;union</strong></a></td>' .
                                '<td align="center" width="105">&nbsp;</td>' .
                            '</tr>' .
                            '<tr height="15">' .
                                '<td align="center" width="20">&nbsp;</td>' .
                                '<td align="center">&nbsp;</td>' .
                                '<td align="center" width="105">&nbsp;</td>' .
                            '</tr>' .

                            '<tr height="20">' .
                                '<td align="center" width="20">&nbsp;</td>' .
                                '<td align="center">&nbsp;</td>' .
                                '<td align="center" width="105">&nbsp;</td>' .
                            '</tr>' .
                        '</table>' .

                    '</body>' .
                '</html>';
                           
        $from = 'noreply@antennereunion.fr';
        $subject = utf8_decode("Inscription au grand jeu des séries Antenne Réunion") ;
        echo(WSMail($email, $subject, $from, $body ));
        
        break;

    case "2":
        if (isset($_SESSION["id_inscription"])) {
            $id_inscription = $_SESSION["id_inscription"];
            $point = 0;
            $coefficient = 5;
            $bonnes_reponses = array(
                "question1" => "option1",
                "question2" => "option3",
                "question3" => "option3",
                "question4" => "option1",
                "question5" => "option3"
            );

            $reponses = array();
            $reponses["question1"] = filter_input(INPUT_POST, 'question1', FILTER_SANITIZE_SPECIAL_CHARS);
            $reponses["question2"] = filter_input(INPUT_POST, 'question2', FILTER_SANITIZE_SPECIAL_CHARS);
            $reponses["question3"] = filter_input(INPUT_POST, 'question3', FILTER_SANITIZE_SPECIAL_CHARS);
            $reponses["question4"] = filter_input(INPUT_POST, 'question4', FILTER_SANITIZE_SPECIAL_CHARS);
            $reponses["question5"] = filter_input(INPUT_POST, 'question5', FILTER_SANITIZE_SPECIAL_CHARS);

            foreach ($reponses as $question => $reponse) {
                if ($reponse === $bonnes_reponses[$question]) {
                    $point += $coefficient;
                }
            }

            $sql = "UPDATE " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " SET point=" . $point . " WHERE id=".$id_inscription;
            $req = $db->exec($sql);
            echo $point;
        }
        break;
    case "3":
        if (isset($_SESSION["id_inscription"])) {
            $id_inscription = $_SESSION["id_inscription"];
            $email1 = filter_input(INPUT_POST, 'email1', FILTER_SANITIZE_SPECIAL_CHARS);
            $email2 = filter_input(INPUT_POST, 'email2', FILTER_SANITIZE_SPECIAL_CHARS);
            $email3 = filter_input(INPUT_POST, 'email3', FILTER_SANITIZE_SPECIAL_CHARS);
            $email4 = filter_input(INPUT_POST, 'email4', FILTER_SANITIZE_SPECIAL_CHARS);
            $email5 = filter_input(INPUT_POST, 'email5', FILTER_SANITIZE_SPECIAL_CHARS);

            $sql = "UPDATE " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " SET ami1='" . $email1 . "', ami2='" . $email2 . "', ami3='" . $email3 . "', ami4='" . $email4 . "', ami5='" . $email5 . "' WHERE id='".$_SESSION["id_inscription"]."'";
            $req = $db->exec($sql);

            //multiplication chance
            $sql = "SELECT * FROM `" . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . "` WHERE id=" . $id_inscription;
            $r = $db->query($sql)->fetch();
            $nombre_chance = 0;
            if ($email1 !== ""){
                $nombre_chance++;

                $body = '<html xmlns="http://www.w3.org/1999/xhtml">' .
                        '<head>' .
                        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                        '<title>ARM jeux</title>' .
                        '</head>' .
                        '<body>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center"></td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center"><a target="_blank" href="http://www.antennereunion.fr/"><img alt="Logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/logo-ar.png" /></a></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="left" width="580" style="color: #ffffff; font-size: 20px; font-family: Arial, Helvetica, sans-serif;">Bonjour,<br />' .
                        '<br />' .
                        $r["nom"] . ' ' . $r["prenom"] . ' vous invite &agrave; participer au Grand Jeu des s&eacute;ries d\'Antenne R&eacute;union. ' .
                        'A gagner : 5 tablettes Samsung Tab 3 Lite et une montre Galaxy Gear 2!' .
                        '<br />' .
                        '</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="2">&nbsp;</td>' .
                        '<td align="center"><img alt="Lots" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/lots-jeux.png" /></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="40" valign="middle">' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '<td align="center"  valign="middle" bgcolor="#014a99" style="color: #ffffff; font-size: 14px; font-family: Arial, Helvetica, sans-serif; "><a style="text-decoration: none; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif;" href="http://www.antennereunion.fr/jeu/jeu-des-series"> <strong>Je participe !</strong></a></td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="15">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '</table>' .
                        '</body>' .
                        '</html>';

                $from = 'noreply@antennereunion.fr';
                $subject = utf8_decode($r["prenom"] . " " . $r["nom"] . " vous invite au grand jeu des séries Antenne Réunion");
                echo(WSMail($email1, $subject, $from, $body));
            }
                
            if ($email2 !== ""){
                $nombre_chance++;

                $body = '<html xmlns="http://www.w3.org/1999/xhtml">' .
                        '<head>' .
                        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                        '<title>ARM jeux</title>' .
                        '</head>' .
                        '<body>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center"></td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center"><a target="_blank" href="http://www.antennereunion.fr/"><img alt="Logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/logo-ar.png" /></a></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="left" width="580" style="color: #ffffff; font-size: 20px; font-family: Arial, Helvetica, sans-serif;">Bonjour,<br />' .
                        '<br />' .
                        $r["nom"] . ' ' . $r["prenom"] . ' vous invite &agrave; participer au Grand Jeu des s&eacute;ries d\'Antenne R&eacute;union. ' .
                        'A gagner : 5 tablettes Samsung Tab 3 Lite et une montre Galaxy Gear 2!' .
                        '<br />' .
                        '</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="2">&nbsp;</td>' .
                        '<td align="center"><img alt="Lots" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/lots-jeux.png" /></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="40" valign="middle">' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '<td align="center"  valign="middle" bgcolor="#014a99" style="color: #ffffff; font-size: 14px; font-family: Arial, Helvetica, sans-serif; "><a style="text-decoration: none; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif;" href="http://www.antennereunion.fr/jeu/jeu-des-series"> <strong>Je participe !</strong></a></td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="15">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '</table>' .
                        '</body>' .
                        '</html>';

                $from = 'noreply@antennereunion.fr';
                $subject = utf8_decode($r["prenom"] . " " . $r["nom"] . " vous invite au grand jeu des séries Antenne Réunion");
                echo(WSMail($email2, $subject, $from, $body));
            }
                
            if ($email3 !== ""){
                $nombre_chance++;

                $body = '<html xmlns="http://www.w3.org/1999/xhtml">' .
                        '<head>' .
                        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                        '<title>ARM jeux</title>' .
                        '</head>' .
                        '<body>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center"></td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center"><a target="_blank" href="http://www.antennereunion.fr/"><img alt="Logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/logo-ar.png" /></a></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="left" width="580" style="color: #ffffff; font-size: 20px; font-family: Arial, Helvetica, sans-serif;">Bonjour,<br />' .
                        '<br />' .
                        $r["nom"] . ' ' . $r["prenom"] . ' vous invite &agrave; participer au Grand Jeu des s&eacute;ries d\'Antenne R&eacute;union. ' .
                        'A gagner : 5 tablettes Samsung Tab 3 Lite et une montre Galaxy Gear 2!' .
                        '<br />' .
                        '</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="2">&nbsp;</td>' .
                        '<td align="center"><img alt="Lots" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/lots-jeux.png" /></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="40" valign="middle">' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '<td align="center"  valign="middle" bgcolor="#014a99" style="color: #ffffff; font-size: 14px; font-family: Arial, Helvetica, sans-serif; "><a style="text-decoration: none; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif;" href="http://www.antennereunion.fr/jeu/jeu-des-series"> <strong>Je participe !</strong></a></td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="15">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '</table>' .
                        '</body>' .
                        '</html>';

                $from = 'noreply@antennereunion.fr';
                $subject = utf8_decode($r["prenom"] . " " . $r["nom"] . " vous invite au grand jeu des séries Antenne Réunion");
                echo(WSMail($email3, $subject, $from, $body));
            }

            if ($email4 !== ""){
                $nombre_chance++;

                $body = '<html xmlns="http://www.w3.org/1999/xhtml">' .
                        '<head>' .
                        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                        '<title>ARM jeux</title>' .
                        '</head>' .
                        '<body>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center"></td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center"><a target="_blank" href="http://www.antennereunion.fr/"><img alt="Logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/logo-ar.png" /></a></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="left" width="580" style="color: #ffffff; font-size: 20px; font-family: Arial, Helvetica, sans-serif;">Bonjour,<br />' .
                        '<br />' .
                        $r["nom"] . ' ' . $r["prenom"] . ' vous invite &agrave; participer au Grand Jeu des s&eacute;ries d\'Antenne R&eacute;union. ' .
                        'A gagner : 5 tablettes Samsung Tab 3 Lite et une montre Galaxy Gear 2!' .
                        '<br />' .
                        '</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="2">&nbsp;</td>' .
                        '<td align="center"><img alt="Lots" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/lots-jeux.png" /></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="40" valign="middle">' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '<td align="center"  valign="middle" bgcolor="#014a99" style="color: #ffffff; font-size: 14px; font-family: Arial, Helvetica, sans-serif; "><a style="text-decoration: none; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif;" href="http://www.antennereunion.fr/jeu/jeu-des-series"> <strong>Je participe !</strong></a></td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="15">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '</table>' .
                        '</body>' .
                        '</html>';

                $from = 'noreply@antennereunion.fr';
                $subject = utf8_decode($r["prenom"] . " " . $r["nom"] . " vous invite au grand jeu des séries Antenne Réunion");
                echo(WSMail($email4, $subject, $from, $body));
            }

            if ($email5 !== ""){
                $nombre_chance++;

                $body = '<html xmlns="http://www.w3.org/1999/xhtml">' .
                        '<head>' .
                        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                        '<title>ARM jeux</title>' .
                        '</head>' .
                        '<body>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center"></td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center"><a target="_blank" href="http://www.antennereunion.fr/"><img alt="Logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/logo-ar.png" /></a></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="left" width="580" style="color: #ffffff; font-size: 20px; font-family: Arial, Helvetica, sans-serif;">Bonjour,<br />' .
                        '<br />' .
                        $r["nom"] . ' ' . $r["prenom"] . ' vous invite &agrave; participer au Grand Jeu des s&eacute;ries d\'Antenne R&eacute;union. ' .
                        'A gagner : 5 tablettes Samsung Tab 3 Lite et une montre Galaxy Gear 2!' .
                        '<br />' .
                        '</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '</tr>' .
                        '<tr>' .
                        '<td align="center" width="2">&nbsp;</td>' .
                        '<td align="center"><img alt="Lots" src="http://www.antennereunion.fr/newsletter-element/ar-jeux/lots-jeux.png" /></td>' .
                        '</tr>' .
                        '</table>' .
                        '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#007fbe">' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="40" valign="middle">' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '<td align="center"  valign="middle" bgcolor="#014a99" style="color: #ffffff; font-size: 14px; font-family: Arial, Helvetica, sans-serif; "><a style="text-decoration: none; color: #ffffff; font-size: 16px; font-family: Arial, Helvetica, sans-serif;" href="http://www.antennereunion.fr/jeu/jeu-des-series"> <strong>Je participe !</strong></a></td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="15">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '<tr height="20">' .
                        '<td align="center" width="20">&nbsp;</td>' .
                        '<td align="center">&nbsp;</td>' .
                        '<td align="center" width="105">&nbsp;</td>' .
                        '</tr>' .
                        '</table>' .
                        '</body>' .
                        '</html>';

                $from = 'noreply@antennereunion.fr';
                $subject = utf8_decode($r["prenom"] . " " . $r["nom"] . " vous invite au grand jeu des séries Antenne Réunion");
                echo(WSMail($email5, $subject, $from, $body));
            }


            for ($i = 0; $i < $nombre_chance; $i++) {
                $civilite = $r["civilite"];
                $bon = $r["bon"];
                $email = $r["email"];
                $nom = $r["nom"];
                $prenom = $r["prenom"];
                $date_naissance = $r["date_naissance"];
                $code_postal = $r["code_postal"];
                $adresse = $r["adresse"];
                $ville = $r["ville"];
                $point = $r["point"];

                $sql = "INSERT INTO " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " (civilite, bon, email, nom, prenom, date_naissance, code_postal, adresse, ville, date, partage, point) "
                        . " VALUES ('$civilite', $bon, '$email', '$nom', '$prenom', '$date_naissance', '$code_postal', '$adresse', '$ville', Now(), 1, $point)";
                $req = $db->exec($sql);
            }
        }
        break;
    case "4":
        /*
        $to = "lionneljazz@yahoo.fr";
        $subject = "first test";
        $from = "lionnel.randrianoely@antennereunion.fr";
        $body = "test <b> test </b>";
        echo(WSMail($to, $subject, $from, htmlentities($body)));
         * 
         */
        break;
}

function get_client_ip() {
    $ipaddress = '';
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

function PostRecaptcha($response, $remoteip, $secret = "6LeFaQQTAAAAAFgcfLIYazYdG-9uTVfcD3lC38oU") {
    $url = "https://www.google.com/recaptcha/api/siteverify";

// Complétez le tableau associatif $postFields avec les variables qui seront envoyées par POST au serveur
    $postFields = array(
        "secret" => $secret,
        "response" => $response,
        "remoteip" => $remoteip
    );

// Tableau contenant les options de téléchargement
    $options = array(
        CURLOPT_URL => $url, // Url cible (l'url de la page que vous voulez télécharger)
        CURLOPT_RETURNTRANSFER => true, // Retourner le contenu téléchargé dans une chaine (au lieu de l'afficher directement)
        CURLOPT_HEADER => false, // Ne pas inclure l'entête de réponse du serveur dans la chaine retournée
        CURLOPT_FAILONERROR => true, // Gestion des codes d'erreur HTTP supérieurs ou égaux à 400
        CURLOPT_POST => true, // Effectuer une requête de type POST
        CURLOPT_POSTFIELDS => $postFields, // Le tableau associatif contenant les variables envoyées par POST au serveur
        CURLOPT_SSL_VERIFYPEER => false
    );

////////// MAIN
// Création d'un nouvelle ressource cURL
    $CURL = curl_init();
// Erreur suffisante pour justifier un die()
    if (empty($CURL)) {
        die("ERREUR curl_init : Il semble que cURL ne soit pas disponible.");
    }

    // Configuration des options de téléchargement
    curl_setopt_array($CURL, $options);

    // Exécution de la requête
    $content = curl_exec($CURL);            // Le contenu téléchargé est enregistré dans la variable $content. Libre à vous de l'afficher.
    // Si il s'est produit une erreur lors du téléchargement
    if (curl_errno($CURL)) {
        // Le message d'erreur correspondant est affiché
        echo "ERREUR curl_exec : " . curl_error($CURL);
    }

// Fermeture de la session cURL
    curl_close($CURL);
    return $content;
}


function WSMail($to, $subject, $from, $body) {
    $mail = new PHPMailer;

    //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp2.zeop.fr';  // Specify main and backup SMTP servers
    
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'antennereunion@smtp2.zeop.fr';                 // SMTP username
    $mail->Password = 'He#2w5yRaooI6wkg';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->From = $from;
    $mail->FromName = utf8_decode('Le Grand jeu des séries Antenne Réunion') ;
    $mail->addAddress($to);     // Add a recipient
    //$mail->addAddress('ellen@example.com');               // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body    = $body;
    $mail->AltBody = '';

    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent';
    }
}

