<?php 

define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "antenne_art_spip");
define("DATA_BASE_LOGIN", "antenne_spipAR");
define("DATA_BASE_PASSWORD", "antv3_2009");
define("DATA_BASE_TABLE_USER", "vote_participant_fnt");

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

try {
	$db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = array(
    	'id_article' => filter_input(INPUT_POST, "id_article", FILTER_VALIDATE_INT),
    	'ip' => filter_input(INPUT_POST, "ip", FILTER_SANITIZE_SPECIAL_CHARS),
    	'user_agent' => filter_input(INPUT_POST, "user_agent", FILTER_SANITIZE_SPECIAL_CHARS)
    );

    if ( !checkCaptchaVote($_POST["g-recaptcha"]) ) {
    	echo 'captcha';
    	unset($_POST['g-recaptcha']);
	} else {
        echo setVote($db, $data);
        die();
   	} 
} catch (Exception $e) {
	echo "connexion";
}

function checkCaptchaVote($captcha) {
    $sS = "6LdBMlEUAAAAAPG2pndmdpMHmdK_dwoBMk51Bdpy";
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = array('secret' => $sS, 'response' => $captcha);
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $response = file_get_contents($url, false, $context);
    $responseKeys = json_decode($response,true);

    return $responseKeys["success"];
}

function setVote($db, $aData){
    try {
        //$sql = 'SELECT id FROM participant WHERE mail LIKE :mail';
        $sql = 'SELECT COUNT(id) as nbr FROM vote_participant_fnt WHERE (DATE(created) = DATE(NOW())) AND (ip LIKE :ip)';
        $stmt = $db->prepare($sql);
        $stmt->execute(["ip" => $aData['ip']]);
        $mReturn = $stmt->fetch();

        if (!is_bool($mReturn) && $mReturn['nbr'] >= 10) return 0;

//        $aIns[] = 'INSERT INTO `candidate_pool`(`id_cdd`, `vote`) VALUES (:id_cdd, :vote) ON DUPLICATE KEY UPDATE id_cdd=id_cdd, vote=vote + 1';
        $aIns[] = 'INSERT INTO `vote_participant_fnt`(`id_article`, `ip`, `user_agent`) VALUES (:id_article, :ip, :user_agent)';

        $stmt = $db->prepare(implode(';', $aIns));
        return $stmt->execute([
            'id_article' => $aData['id_article'],
            'ip' => $aData['ip'],
            'user_agent' => $aData['user_agent']
        ]);
    } catch (Exception $e) {
        echo "Error" . $e;
    }
}
?>