<?php

//définition des constantes
define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "antenne_art_spip");
define("DATA_BASE_LOGIN", "antenne_spipAR");
define("DATA_BASE_PASSWORD", "antv3_2009");
define("DATA_BASE_TABLE_NEWSLETTER", "spip_nl_user");
define("DATA_BASE_TABLE_USER_NEWSLETTER", "spip_user_newsletter");

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

$erreur = array();

try {
    //connexion PDO
    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $secret="6LdBMlEUAAAAAPG2pndmdpMHmdK_dwoBMk51Bdpy";
    $response=$_POST["captcha"];

    //récupération et nettoyage des POSTS
    $nom = filter_input(INPUT_POST, "nom", FILTER_SANITIZE_SPECIAL_CHARS);
    $prenom = filter_input(INPUT_POST, "prenom", FILTER_SANITIZE_SPECIAL_CHARS);
    $civilite_user =  filter_input(INPUT_POST, "civilite", FILTER_SANITIZE_SPECIAL_CHARS);
    $mail_user = filter_input(INPUT_POST, "email_page", FILTER_VALIDATE_EMAIL);
    $date_inscription = date("Y-m-d H:i:s");
    $statut = filter_input(INPUT_POST, "acceptcond2_page", FILTER_SANITIZE_SPECIAL_CHARS) == "on" ? 1 : 0;
    $statut_partenaire = filter_input(INPUT_POST, "acceptcond3_page", FILTER_SANITIZE_SPECIAL_CHARS) == "on" ? 1 : 0;
    //$frequence = filter_input(INPUT_POST, "frequence", FILTER_SANITIZE_SPECIAL_CHARS);
    //$nl = preg_replace("/,$/i", "", rtrim(filter_input(INPUT_POST, "nl", FILTER_SANITIZE_SPECIAL_CHARS)));
    $nl = $_POST['nl'];
    if ($mail_user === false || $mail_user == "") {
        $erreur[] = "Adresse email invalide";
    }



    $verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
    $captcha_success=json_decode($verify);
/*    if ($captcha_success->success==false)
    {
      $erreur[]= "Captcha invalide"; 

    }*/
    if (count($erreur) > 0) {
        echo json_encode($erreur);
        die;
    }
    //vérifie si l'enregistrement existe déja dans la table DATA_BASE_TABLE_NEWSLETTER
    $sql = "SELECT * FROM " . DATA_BASE_TABLE_NEWSLETTER . " WHERE mail_user='" . $mail_user . "' LIMIT 1";
    $mail_user_exist = $db->query($sql)->fetch();
    
    $length_nl = count($nl);

    if ( is_null($mail_user_exist['id']) ) {//l'entrée n'existe pas encore (INSERT)
        $req_nl_user = $db->prepare("INSERT INTO " . DATA_BASE_TABLE_NEWSLETTER . " (nom,prenom,mail_user,civilite_user, date_inscription,date_modification, statut, statut_partenaire) VALUES (:nom,:prenom,:mail_user,:civilite_user, :date_inscription, :date_inscription, :statut, :statut_partenaire)");
        $req_nl_user->execute(array(
            "nom" => $nom,
            "prenom" => $prenom,
            "mail_user" => $mail_user,
            "civilite_user" => $civilite_user,
            "date_inscription" => $date_inscription,
            "date_modification" => $date_inscription,
            "statut" => $statut,
            "statut_partenaire" => $statut_partenaire
        ));
        $last_id = $db->lastInsertId();

        // Ajout choix optins de l'utilisateur dans spip_user_optins
        if ($statut == 1) {
          // Si l'utilisateur souhaite recevoir l'offre du groupe Linfo.Re et de antenne réunion
          //insert_optins($last_id, 1, $date_inscription);
          $req_nl_user = $db->prepare("INSERT INTO spip_user_optins (id_user,id_optin,date_abonnement,date_desinscription) VALUES (:id_user,:id_optin,:date_abonnement,:date_desinscription)");
          $req_nl_user->execute(array(
             "id_user" => $last_id,
             "id_optin" => 1,
             "date_abonnement" => $date_inscription,
             "date_desinscription" => $date_inscription
          ));
        }

        if ($statut_partenaire == 1) {
          // Si l'utilisateur souhaite recevoir l'offre des partenaires du groupe de antenne réunion
          //insert_optins($last_id, 2, $date_inscription);
          $req_nl_user = $db->prepare("INSERT INTO spip_user_optins (id_user,id_optin,date_abonnement,date_desinscription) VALUES (:id_user,:id_optin,:date_abonnement,:date_desinscription)");
          $req_nl_user->execute(array(
             "id_user" => $last_id,
             "id_optin" => 2,
             "date_abonnement" => $date_inscription,
             "date_desinscription" => $date_inscription
          ));
        }
        /* Fin insertion dans la table intermediaire des optins */

        $req = ("INSERT INTO " . DATA_BASE_TABLE_USER_NEWSLETTER . " (id_newsletter, id_nl_user, id_action_user , date_action, date_modif_action) VALUES ");
          $j = 0;
          foreach ($nl as $key => $value) {
            $j++;
            $id_action_user = 1;//( $nl[$i] == 1 ) ? 1 : 2; // 1 -> inscription; 2 -> description
            $req .= " (".$value.",".$last_id.",". $id_action_user .",'". $date_inscription ."','". $date_inscription ."' )";
            $req .= ($j == $length_nl) ? '' : ', ';
          }
          $db->query($req) or die('Impossible de traiter linsert');
        echo json_encode(1);

    } else {//l'entrée existe déjà (UPDATE)
        $sql_update = "UPDATE `" . DATA_BASE_TABLE_NEWSLETTER . "` SET nom = '". $nom ."', prenom = '". $prenom ."', civilite_user = '". $civilite_user ."', statut=" . $statut . ",statut_partenaire=" . $statut_partenaire.", date_modification='". date("Y-m-d H:i:s") . "' WHERE id='" . $mail_user_exist['id'] . "'";
        /*$sql_update = "UPDATE `" . DATA_BASE_TABLE_NEWSLETTER . "` SET statut=" . $statut . ",statut_partenaire=" . $statut_partenaire.", date_modification='". date("Y-m-d H:i:s") . "' WHERE mail_user='" . $mail_user . "'";*/
        $db->query($sql_update);

        $id_user = $mail_user_exist['id'];
        // Début traitement pour la table intermediaire des optins
        $sql = "SELECT * FROM `spip_user_optins` WHERE id_user='" . $id_user . "'";
        $resultats = $db->query($sql)->fetchAll();

        $liste_optins = array(1,2);
        $liste_user_optins = array();
        if ( count($resultats) > 0 ) {
          /* Update des dates sur les optins déjà enregistré */
          foreach ($resultats as $res) {
            array_push($liste_user_optins, $res['id_optin']);
            if ( ($res['id_optin'] == 1 && $statut == 0) || ($res['id_optin'] == 2 && $statut_partenaire == 0) ) {
                // MAJ date de desincription
                $sql_update = "UPDATE `spip_user_optins` SET date_desinscription='". $date_inscription ."' WHERE id_user_optins='" . $res['id_user_optins'] . "'";
                $db->query($sql_update);
            }
            if ( ($res['id_optin'] == 1 && $statut == 1) || ($res['id_optin'] == 2 && $statut_partenaire == 1) ) {
                // MAJ date abonnement
                $sql_update = "UPDATE `spip_user_optins` SET date_abonnement='". $date_inscription ."' WHERE id_user_optins='" . $res['id_user_optins'] . "'";
                $db->query($sql_update);
            }
          }
          // Fin update

          // Ajout optins dans la table intermédiaire si l'utilisateur coche des nouveaux options
          if ( count($resultats) < count($liste_optins) ) {
            for ($i=0; $i < count($liste_optins); $i++) { 
              if ( !in_array($liste_optins[$i], $liste_user_optins) ) {
                $req_nl_user = $db->prepare("INSERT INTO spip_user_optins (id_user,id_optin,date_abonnement,date_desinscription) VALUES (:id_user,:id_optin,:date_abonnement,:date_desinscription)");
                $req_nl_user->execute(array(
                   "id_user" => $id_user,
                   "id_optin" => $liste_optins[$i],
                   "date_abonnement" => $date_inscription,
                   "date_desinscription" => $date_inscription
                ));
              }
            }
          }

        }
        else{
          if ($statut == 1) {
            // Si l'utilisateur souhaite recevoir l'offre du groupe Linfo.Re et de antenne réunion
            $req_nl_user = $db->prepare("INSERT INTO spip_user_optins (id_user,id_optin,date_abonnement,date_desinscription) VALUES (:id_user,:id_optin,:date_abonnement,:date_desinscription)");
            $req_nl_user->execute(array(
               "id_user" => $id_user,
               "id_optin" => 1,
               "date_abonnement" => $date_inscription,
               "date_desinscription" => $date_inscription
            ));
          }

          if ($statut_partenaire == 1) {
            // Si l'utilisateur souhaite recevoir l'offre des partenaires du groupe de antenne réunion
            $req_nl_user = $db->prepare("INSERT INTO spip_user_optins (id_user,id_optin,date_abonnement,date_desinscription) VALUES (:id_user,:id_optin,:date_abonnement,:date_desinscription)");
            $req_nl_user->execute(array(
               "id_user" => $id_user,
               "id_optin" => 2,
               "date_abonnement" => $date_inscription,
               "date_desinscription" => $date_inscription
            ));
          }
        }        
        // Fin traitement table intermediaire des optins

        // Début traitement table intermediaire NL
        /* 
          Récupérer tous les newsletter dont l'utilisateur a été inscrit
        */
        $sql = "SELECT id_newsletter FROM " . DATA_BASE_TABLE_USER_NEWSLETTER . " WHERE id_nl_user=" . $id_user;
        $old_nl = $db->query($sql)->fetchAll();

        if ( $old_nl ) {
          /* 
            Pour chaque ancien NL ne figurant pas dans la liste des nouvelles NL souhaitées par l'utilisateur
            update la table et mettre l'action en désinscription 2
          */
          foreach (array_diff($old_nl, $nl) as $key => $value) {
            $sql = "UPDATE `" . DATA_BASE_TABLE_USER_NEWSLETTER . "` SET id_action_user=2 ,date_modif_action='". date("Y-m-d H:i:s") ."' WHERE id_nl_user='".$id_user."' AND id_newsletter ='". $value ."'";
            $db->query($sql) or die('Impossible de traiter la requete');
          }  
        }
        foreach ($nl as $key => $value) {
            // Vérification action utilisateur pour chaque newsletter
            $sql = "SELECT * FROM " . DATA_BASE_TABLE_USER_NEWSLETTER . " WHERE id_nl_user=" . $id_user . " AND id_newsletter ='". $value ."' LIMIT 1";
            $old_action = $db->query($sql)->fetch();
            if ($old_action) {
                // UPDATE
                $sql = "UPDATE `" . DATA_BASE_TABLE_USER_NEWSLETTER . "` SET date_modif_action='". date("Y-m-d H:i:s") ."' WHERE id_nl_user='".$id_user."' AND id_newsletter ='". $value ."'";
            }else{
                // INSERT
                $id_action_user = 1; // Nouveau inscription
                $sql = "INSERT INTO " . DATA_BASE_TABLE_USER_NEWSLETTER . " (id_newsletter, id_nl_user, id_action_user , date_action, date_modif_action) VALUES (".$value.",".$id_user.",". $id_action_user .",'". $date_inscription ."','". $date_inscription."' )";
            }
            $db->query($sql) or die('Impossible de traiter la requete');
          }

          // Fin traitement table intermediaire NL

        echo json_encode("L'entrée existe déjà et a été mise à jour");
    }
} catch (Exception $e) {
    echo $e->getMessage()."<br/>".$e->getLine();
    echo "Erreur";
    die;
}