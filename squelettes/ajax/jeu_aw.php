<?php

/**
 * Ce script récupère les POST de l'inscription du jeu quizz ART (ARM)
 * Lionnel
 */
session_start();
require 'PHPMailer-master/PHPMailerAutoload.php';
//définition des constantes
if ($_SERVER["SERVER_ADDR"] == "127.0.0.1") {
    define("DATA_BASE_HOST", "localhost");
    define("DATA_BASE_NAME", "antenne_art_spip");
    define("DATA_BASE_LOGIN", "antenne_spipAR");
    define("DATA_BASE_PASSWORD", "antv3_2009");
    define("DATA_BASE_TABLE_INSCRIPTION_QUIZZ", "inscription_quizz_airways");
} elseif ($_SERVER["SERVER_ADDR"] == "192.168.122.2") {
    define("DATA_BASE_HOST", "localhost");
    define("DATA_BASE_NAME", "art_spip");
    define("DATA_BASE_LOGIN", "root");
    define("DATA_BASE_PASSWORD", "Mad1T_15P");
    define("DATA_BASE_TABLE_INSCRIPTION_QUIZZ", "inscription_quizz_airways");
} else {
    //BDD linfo
    define("DATA_BASE_HOST", "localhost");
    define("DATA_BASE_NAME", "antenne_art_spip");
    define("DATA_BASE_LOGIN", "antenne_spipAR");
    define("DATA_BASE_PASSWORD", "antv3_2009");
    define("DATA_BASE_TABLE_INSCRIPTION_QUIZZ", "inscription_quizz_airways");
}

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

$db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$step = filter_input(INPUT_GET, 'step', FILTER_SANITIZE_SPECIAL_CHARS);

switch ($step) {
    case "1":
        $civilite = filter_input(INPUT_POST, 'civilite', FILTER_SANITIZE_SPECIAL_CHARS);
        $bon_post = filter_input(INPUT_POST, 'bon',  FILTER_SANITIZE_SPECIAL_CHARS);
        $bon = $bon_post === "option1" ? 1 : 0;
        $bon_partenaire_post = filter_input(INPUT_POST, 'bon_partenaire', FILTER_SANITIZE_SPECIAL_CHARS);
        $bon_partenaire = $bon_partenaire_post === "option2" ? 1 : 0;
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
        $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS);
        $prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS);
        $date = filter_input(INPUT_POST, 'date_naissance', FILTER_SANITIZE_SPECIAL_CHARS);
        $date_naissance = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $date)));
        $code_postal = filter_input(INPUT_POST, 'code_postal', FILTER_SANITIZE_SPECIAL_CHARS);
        $adresse = filter_input(INPUT_POST, 'adresse', FILTER_SANITIZE_SPECIAL_CHARS);
        $ville = filter_input(INPUT_POST, 'ville', FILTER_SANITIZE_SPECIAL_CHARS);

        $g_recaptcha_response = $_POST["g_recaptcha_response"]; //POST à ne pas néttoyer

//        $bon_post = '1';

        $ip = get_client_ip();
        $reponse_recaptcha = PostRecaptcha($g_recaptcha_response, $ip);
        if (strpos($reponse_recaptcha, 'true') !== false) {
            $sql_select = "SELECT email,id FROM " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " WHERE email='" . $email . "' AND tirage=1";
            $req_select = $db->query($sql_select)->fetchAll();

            if (count($req_select) > 0) { //l'adresse email existe déjà dans la base de données
                $_SESSION["id_inscription"] = $req_select[0]["id"];
                echo "existe";
                die;
            }

            $sql = "INSERT INTO " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " (civilite,bon_site ,bon_partenaire ,email, nom, prenom, date_naissance, code_postal, adresse, ville, date) "
                    . " VALUES ('$civilite', $bon,'$bon_partenaire', '$email', '$nom', '$prenom', '$date_naissance', '$code_postal', '$adresse', '$ville', Now())";
            $req = $db->exec($sql);
            $lastId = $db->lastInsertId();
            $_SESSION["id_inscription"] = $lastId;
            echo $lastId;
        } else {
            echo "captcha";
            die;
        }

//        Envoie email inscription

        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $email)) {
            $passage_ligne = "\r\n";
        } else {
            $passage_ligne = "\n";
        }
        $subject = utf8_encode("Inscription au Jeu XL Airways Antenne Réunion");
        $subject = utf8_decode($subject);
        $body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <title></title>
                </head>
                <body bgcolor="#e7e7e7" link="#ffffff" vlink="#ffffff" alink="#ffffff">
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#e7e7e7">
                                <tr>
                                        <!--<td id="webversion" style="font-family:Arial,Helvetica,sans-serif; font-size:10px; height:20px; text-align:center"> Si vous n\'arrivez pas &agrave; lire ce message vous pouvez le <span style="text-decoration:none"><a target="_blank" class="adminText" href="*|ARCHIVE|*" style="color: rgb(153, 0, 0); text-decoration: none;">visualiser en ligne</a></span></td>-->
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr><td height="10" bgcolor="#ffffff"></td></tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="10" height="132" bgcolor="#ffffff"></td><td width="580" height="132" bgcolor="#80A8DF" align="center"><a href="http://www.antennereunion.fr/" target="_blank"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/logo-AR.jpg" width="150" height="75" alt="image" style="display:block"></a></td><td width="10" height="132" bgcolor="#ffffff"></td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td bgcolor="#ffffff" align="center"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/top-trait.jpg" width="580" height="22" style="display:block"></td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="10" height="216" bgcolor="#ffffff"></td>
                                        <td width="580" height="216" bgcolor="#b3cbec" valign="top">
                                                <table width="580" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                                <td width="20"></td>
                                                                <td width="560" valign="top"><table width="560" border="0" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                                <td valign="top"><font face="Arial" size="4" color="#00347F" style="font-weight:normal;">Bonjour ' . $prenom . ',</font></td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td height="30"></td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td valign="top"><font face="Arial" size="4" color="#00347F" style="font-weight:normal;">Vous êtes bien inscrit(e) au Grand Jeu XL AIRWAYS d\'Antenne Réunion avec à la clé :</font></td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td valign="top"><font face="Arial" size="4" color="#00347F" style="font-weight:normal;"><strong>4 BILLETS ALLER/RETOUR</strong><br>REUNION – PARIS CDG</font></td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td height="30"></td>
                                                                        </tr>
                                                                        <tr>
                                                                                <td valign="top"><font face="Arial" size="4" color="#00347F" style="font-weight:normal;">Merci pour votre participation et à bientôt sur <a href="http://www.antennereunion.fr/" target="_blank" color="#00347F" style="text-decoration:none;color:#00347F;">AntenneReunion.fr</a></font></td>
                                                                        </tr>
                                                                        </table>
                                                                </td>
                                                        </tr>
                                                </table>
                                        </td>
                                        <td width="10" height="216" bgcolor="#ffffff"></td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td bgcolor="#ffffff" align="center"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/bottom-trait.jpg" width="580" height="45" style="display:block"></td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="10" bgcolor="#ffffff" height="338">&nbsp;</td>
                                        <td width="580" bgcolor="#010217" align="center" height="338"><a href="http://www.antennereunion.fr/jeu/XLAirways" target="_blank"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/img-xlairways.jpg" width="580" height="338" style="display:block;"></a></td>
                                        <td width="10" bgcolor="#ffffff" height="338">&nbsp;</td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="10" height="62" bgcolor="#ffffff"></td><td width="64" height="62" bgcolor="#ffffff"></td><td width="452" height="62" bgcolor="#ffffff" align="center"><a href="http://www.antennereunion.fr/jeu/XLAirways" target="_blank"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/decourez-bouton.jpg" width="580" height="75" alt="image" style="display:block;"></a></td><td width="64" height="62" bgcolor="#ffffff"></td><td width="10" height="62" bgcolor="#ffffff"></td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td height="10" bgcolor="#ffffff"></td>
                                </tr>
                        </table>
                </body>
        </html>';

        $header = "From: \"Le Grand jeu XL Airways - Antenne Réunion\"<noreply@antennereunion.fr>" . $passage_ligne;
        $header.= "Reply-to: \"Le Grand jeu XL Airways - Antenne Réunion\" <noreply@antennereunion.fr>" . $passage_ligne;
        $header.= "MIME-Version: 1.0" . $passage_ligne;
        $header.= 'Content-type: text/html; charset=utf-8' . "\r\n";
        if (mail($email, $subject, $body, $header)) {
            echo "Mail envoy&eacute;<br>";
        } else
            echo "Aucun mail à envoyer";

        break;

    case "2":
        if (isset($_SESSION["id_inscription"])) {
            $id_inscription = $_SESSION["id_inscription"];
            $point = 0;
            $coefficient = 5;
            $bonnes_reponses = array(
                "question1" => "option1",
                "question2" => "option3",
                "question3" => "option3",
                "question4" => "option1",
                "question5" => "option2"
            );

            $reponses = array();
            $reponses["question1"] = filter_input(INPUT_POST, 'question1', FILTER_SANITIZE_SPECIAL_CHARS);
            $reponses["question2"] = filter_input(INPUT_POST, 'question2', FILTER_SANITIZE_SPECIAL_CHARS);
            $reponses["question3"] = filter_input(INPUT_POST, 'question3', FILTER_SANITIZE_SPECIAL_CHARS);
            $reponses["question4"] = filter_input(INPUT_POST, 'question4', FILTER_SANITIZE_SPECIAL_CHARS);
            $reponses["question5"] = filter_input(INPUT_POST, 'question5', FILTER_SANITIZE_SPECIAL_CHARS);

            foreach ($reponses as $question => $reponse) {
                if ($reponse === $bonnes_reponses[$question]) {
                    $point += $coefficient;
                }
            }

            $sql = "UPDATE " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " SET point=" . $point . " WHERE id=" . $id_inscription;
            $req = $db->exec($sql);
            echo $point;
        }
        break;
    case "3":
        if (isset($_SESSION["id_inscription"])) {
            $id_inscription = $_SESSION["id_inscription"];
            $email1 = filter_input(INPUT_POST, 'email1', FILTER_SANITIZE_SPECIAL_CHARS);
            $email2 = filter_input(INPUT_POST, 'email2', FILTER_SANITIZE_SPECIAL_CHARS);
            $email3 = filter_input(INPUT_POST, 'email3', FILTER_SANITIZE_SPECIAL_CHARS);
            $email4 = filter_input(INPUT_POST, 'email4', FILTER_SANITIZE_SPECIAL_CHARS);
            $email5 = filter_input(INPUT_POST, 'email5', FILTER_SANITIZE_SPECIAL_CHARS);

            $sql = "UPDATE " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " SET ami1='" . $email1 . "', ami2='" . $email2 . "', ami3='" . $email3 . "', ami4='" . $email4 . "', ami5='" . $email5 . "' WHERE id='" . $_SESSION["id_inscription"] . "'";
            $req = $db->exec($sql);

            //multiplication chance
            $sql = "SELECT * FROM `" . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . "` WHERE id=" . $id_inscription;
            $r = $db->query($sql)->fetch();
            $nombre_chance = 0;
            $nom_ = $r["nom"];
            $prenom_ = $r["prenom"];
            $subject = utf8_encode($prenom_) . ' ' . utf8_encode($nom_) . ' ' . utf8_encode("vous invite au Jeu XL Airways Antenne Réunion");
            $subject = utf8_decode($subject);
            //ENVOI EMAIL A UN AMI INVITE 1
            if ($email1 !== "") {
                $nombre_chance++;
                mailInviteAmis($email1);
            }
            //ENVOI EMAIL A UN AMI INVITE 2
            if ($email2 !== "") {
                $nombre_chance++;
                mailInviteAmis($email2);
            }
            //ENVOI EMAIL A UN AMI INVITE 3
            if ($email3 !== "") {
                $nombre_chance++;
                mailInviteAmis($email3);
            }
            //ENVOI EMAIL A UN AMI INVITE 4
            if ($email4 !== "") {
                $nombre_chance++;
                mailInviteAmis($email4);
            }
            //ENVOI EMAIL A UN AMI INVITE 5
            if ($email5 !== "") {
                $nombre_chance++;
                mailInviteAmis($email5);
            }


            for ($i = 0; $i < $nombre_chance; $i++) {
                $civilite = $r["civilite"];
                $bon = $r["bon_site"];
                $bon_partenaire = $r["bon_partenaire"];
                $email = $r["email"];
                $nom = $r["nom"];
                $prenom = $r["prenom"];
                $date_naissance = $r["date_naissance"];
                $code_postal = $r["code_postal"];
                $adresse = $r["adresse"];
                $ville = $r["ville"];
                $point = $r["point"];

                $sql = "INSERT INTO " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " (civilite, bon_site,bon_partenaire, email, nom, prenom, date_naissance, code_postal, adresse, ville, date, partage, point) "
                        . " VALUES ('$civilite', $bon,$bon_partenaire, '$email', '$nom', '$prenom', '$date_naissance', '$code_postal', '$adresse', '$ville', Now(), 1, $point)";
                $req = $db->exec($sql);
            }
        }
        break;
    case "4":
        /*
          $to = "lionneljazz@yahoo.fr";
          $subject = "first test";
          $from = "lionnel.randrianoely@antennereunion.fr";
          $body = "test <b> test </b>";
          echo(WSMail($to, $subject, $from, htmlentities($body)));
         * 
         */
//        partage reseaux sociaux Tojo 
        if (isset($_SESSION["id_inscription"])) {
            $id_inscription = $_SESSION["id_inscription"];
            $sql = "SELECT * FROM `" . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . "` WHERE id=" . $id_inscription;
            $r = $db->query($sql)->fetch();
            $nombre_chance = 0;

            if (isset($_POST['fb_partage'])) {
                $nombre_chance++;
            }

            if (isset($_POST['tw_partage'])) {
                $nombre_chance++;
            }

            if (isset($_POST['gg_partage'])) {
                $nombre_chance++;
            }

            $partage = $r["partage_reseau_socio"];
            $nombre_chance = $nombre_chance + $partage;
            $sql = "UPDATE " . DATA_BASE_TABLE_INSCRIPTION_QUIZZ . " SET partage_reseau_socio='" . $nombre_chance . "' WHERE id='" . $_SESSION["id_inscription"] . "'";
            $req = $db->exec($sql);
        }

        break;
}

function get_client_ip() {
    $ipaddress = '';
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

function PostRecaptcha($response, $remoteip, $secret = "6LeFaQQTAAAAAFgcfLIYazYdG-9uTVfcD3lC38oU") {
    $url = "https://www.google.com/recaptcha/api/siteverify";
// Complétez le tableau associatif $postFields avec les variables qui seront envoyées par POST au serveur
    $postFields = array(
        "secret" => $secret,
        "response" => $response,
        "remoteip" => $remoteip
    );
// Tableau contenant les options de téléchargement
    $options = array(
        CURLOPT_URL => $url, // Url cible (l'url de la page que vous voulez télécharger)
        CURLOPT_RETURNTRANSFER => true, // Retourner le contenu téléchargé dans une chaine (au lieu de l'afficher directement)
        CURLOPT_HEADER => false, // Ne pas inclure l'entête de réponse du serveur dans la chaine retournée
        CURLOPT_FAILONERROR => true, // Gestion des codes d'erreur HTTP supérieurs ou égaux à 400
        CURLOPT_POST => true, // Effectuer une requête de type POST
        CURLOPT_POSTFIELDS => $postFields, // Le tableau associatif contenant les variables envoyées par POST au serveur
        CURLOPT_SSL_VERIFYPEER => false
    );

////////// MAIN
// Création d'un nouvelle ressource cURL
    $CURL = curl_init();
// Erreur suffisante pour justifier un die()
    if (empty($CURL)) {
        die("ERREUR curl_init : Il semble que cURL ne soit pas disponible.");
    }

    // Configuration des options de téléchargement
    curl_setopt_array($CURL, $options);

    // Exécution de la requête
    $content = curl_exec($CURL);            // Le contenu téléchargé est enregistré dans la variable $content. Libre à vous de l'afficher.
    // Si il s'est produit une erreur lors du téléchargement
    if (curl_errno($CURL)) {
        // Le message d'erreur correspondant est affiché
        echo "ERREUR curl_exec : " . curl_error($CURL);
    }

// Fermeture de la session cURL
    curl_close($CURL);
    return $content;
}

function mailInviteAmis($email) {
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $email)) {
        $passage_ligne = "\r\n";
    } else {
        $passage_ligne = "\n";
    }
    global $subject, $prenom_, $nom_;
    $body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <title></title>
                </head>
                <body bgcolor="#e7e7e7" link="#ffffff" vlink="#ffffff" alink="#ffffff">
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#e7e7e7">
                                <tr>
                                        <!--<td id="webversion" style="font-family:Arial,Helvetica,sans-serif; font-size:10px; height:20px; text-align:center"> Si vous n\'arrivez pas &agrave; lire ce message vous pouvez le <span style="text-decoration:none"><a target="_blank" class="adminText" href="*|ARCHIVE|*" style="color: rgb(153, 0, 0); text-decoration: none;">visualiser en ligne</a></span></td>-->
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr><td height="10" bgcolor="#ffffff"></td></tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="10" height="132" bgcolor="#ffffff"></td><td width="580" height="132" bgcolor="#80A8DF" align="center"><a href="http://www.antennereunion.fr/" target="_blank"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/logo-AR.jpg" width="150" height="75" alt="image" style="display:block"></a></td><td width="10" height="132" bgcolor="#ffffff"></td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td bgcolor="#ffffff" align="center"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/top-trait.jpg" width="580" height="22" style="display:block"></td>
                                </tr>
                        </table>
                                            <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                    <td width="10" height="216" bgcolor="#ffffff"></td>
                                    <td width="580" height="216" bgcolor="#b3cbec" valign="top">
                                            <table width="580" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                            <td width="20"></td>
                                                            <td width="560" valign="top">
                                                                    <table width="560" border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                    <td valign="top"><font face="Arial" size="4" color="#00347F" style="font-weight:normal;">Bonjour,</font></td>
                                                                            </tr>
                                                                            <tr>
                                                                                    <td height="30"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                    <td valign="top"><font face="Arial" size="4" color="#00347F" style="font-weight:normal;">' . $nom_ . ' ' . $prenom_ . ' vous invite à participer au Grand Jeu XL AIRWAYS d\'Antenne Réunion. </font></td>
                                                                            </tr>
                                                                            <tr>
                                                                                    <td valign="top"><font face="Arial" size="4" color="#00347F" style="font-weight:normal; color:#00347F;">A gagner :<br><strong>4 BILLETS ALLER/RETOUR</strong><br>REUNION – PARIS CDG</font></font></td>
                                                                            </tr>
                                                                    </table>
                                                            </td>
                                                    </tr>
                                            </table>
                                    </td>
                                    <td width="10" height="216" bgcolor="#ffffff"></td>
                            </tr>
                    </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td bgcolor="#ffffff" align="center"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/bottom-trait.jpg" width="580" height="45" style="display:block"></td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="10" bgcolor="#ffffff" height="338">&nbsp;</td>
                                        <td width="580" bgcolor="#010217" align="center" height="338"><a href="http://www.antennereunion.fr/jeu/XLAirways" target="_blank"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/img-xlairways.jpg" width="580" height="338" style="display:block;"></a></td>
                                        <td width="10" bgcolor="#ffffff" height="338">&nbsp;</td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="10" height="62" bgcolor="#ffffff"></td><td width="64" height="62" bgcolor="#ffffff"></td><td width="452" height="62" bgcolor="#ffffff" align="center"><a href="http://www.antennereunion.fr/jeu/XLAirways" target="_blank"><img src="http://www.antennereunion.fr/squelettes/ressources/img-jeu-XL/participerbouton.jpg" width="580" height="75" alt="image" style="display:block;"></a></td><td width="64" height="62" bgcolor="#ffffff"></td><td width="10" height="62" bgcolor="#ffffff"></td>
                                </tr>
                        </table>
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td height="10" bgcolor="#ffffff"></td>
                                </tr>
                        </table>
                </body>
        </html>';
    $header = "From: \"Le Grand jeu XL Airways - Antenne Réunion\"<noreply@antennereunion.fr>" . $passage_ligne;
    $header.= "Reply-to: \"Le Grand jeu XL Airways - Antenne Réunion\" <noreply@antennereunion.fr>" . $passage_ligne;
    $header.= "MIME-Version: 1.0" . $passage_ligne;
    $header.= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $header = utf8_encode($header);
    $header = utf8_decode($header);

    if (mail($email, $subject, $body, $header)) {
        echo "Mail envoy&eacute;<br>";
    } else
        echo "Aucun mail à envoyer";
}
