<?php
    define("PATH_RSS", "/home/antenne/public_html/squelettes/ajax/nocache/");
    
    function WriteXML($url, $filename){
        $content = file_get_contents($url);
        file_put_contents($filename, $content);
    }
    
    function GetSpipUrl($site, $template){
        return "http://www.".$site.".".($site === 'linfo' ? 're' : 'fr')."/spip.php?page=".$template."&". time();
    }    
    
    /*Antenne Réunion Alerte*/
    var_dump("Antenne Réunion Alerte");
    $url_newsletter_ar_flux_alerte = GetSpipUrl("antennereunion", "newsletter_alerte_flux_aweber");
    $filename_newsletter_ar_flux_alerte = PATH_RSS."ar_newsletter_alerte_flux.xml";
    WriteXML($url_newsletter_ar_flux_alerte, $filename_newsletter_ar_flux_alerte);
    
    /*Antenne Réunion Hebdo*/
    var_dump("Antenne Réunion Hebdo");
    $url_newsletter_ar_flux_hebdo = GetSpipUrl("antennereunion", "newsletter_hebdomadaire_flux_aweber");
    $filename_newsletter_ar_flux_hebdo = PATH_RSS."ar_newsletter_hebdo_flux.xml";    
    WriteXML($url_newsletter_ar_flux_hebdo, $filename_newsletter_ar_flux_hebdo);
    
    /*Linfo Alerte*/
    var_dump("Linfo Alerte");
    $url_newsletter_linfo_flux_alerte = GetSpipUrl("linfo", "newsletter_alerte_flux_aweber");
    $filename_newsletter_linfo_flux_alerte = PATH_RSS."linfo_newsletter_alerte_flux.xml";    
    WriteXML($url_newsletter_linfo_flux_alerte, $filename_newsletter_linfo_flux_alerte);
    
    /*Linfo Matin*/
    var_dump("Linfo Matin");
    $url_newsletter_linfo_flux_matin = GetSpipUrl("linfo", "newsletter_du_matin_flux_aweber");
    $filename_newsletter_linfo_flux_matin = PATH_RSS."linfo_newsletter_du_matin_flux.xml";          
    WriteXML($url_newsletter_linfo_flux_matin, $filename_newsletter_linfo_flux_matin);
