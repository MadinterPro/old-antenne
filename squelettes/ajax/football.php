<?php 

date_default_timezone_set('Indian/Reunion');
setlocale(LC_TIME, 'french.UTF-8', 'fr_FR.UTF-8');

include_once 'database.php';
include_once 'matchs.php';
$database = new Database();
$db = $database->getConnection();
$match = new Match($db);
$lists = array();

switch ($_GET['action']) {
	case 'all':
		// tous les matchs -> besoin du id_tournoi
		$lists = $match->findAll($id_tournoi);
		break;
	case 'groupe':
		// par groupe -> besoin du id_groupe && id_tournoi
		// classement par groupe + match par groupe
		$lists['classment'] = $match->getClassement($id_tournoi, $id_groupe);
		$lists['groupe'] = $match->findGroup($id_tournoi, $id_groupe);
		break;
	case 'pays':
		// matches par pays -> besoin du id_pays
		$lists = $match->findCountry($id_tournoi, $id_country);
		break;
	case 'phase':
		// matches à partir du 8e de final
		$lists = $match->findRound($id_tournoi, $id_round);
		break;
	
	default:
		# code...
		break;
}

echo json_decode($lists);

?>