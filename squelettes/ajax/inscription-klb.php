<?php

//définition des constantes
define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "antenne_art_spip");
define("DATA_BASE_LOGIN", "antenne_spipAR");
define("DATA_BASE_PASSWORD", "antv3_2009");
define("DATA_BASE_TABLE_KLB", "spip_klb_user");

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

$erreur = array();

try {
    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, array(PDO::ATTR_PERSISTENT    => true));
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
    echo $e->getMessage()."<br/>".$e->getLine();
    die;
}

$secret="6LdBMlEUAAAAAPG2pndmdpMHmdK_dwoBMk51Bdpy";
$response = $_POST["captcha"];

$verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
$captcha_success=json_decode($verify);

if ($captcha_success->success == false) {
    $erreur[] = "Captcha invalide";
} else if ($captcha_success->success == true && count($erreur) < 1) {
    $data = array(
        'nom' => filter_input(INPUT_POST, "nom", FILTER_SANITIZE_SPECIAL_CHARS),
        'prenom' => filter_input(INPUT_POST, "prenom", FILTER_SANITIZE_SPECIAL_CHARS),
        'email' => filter_input(INPUT_POST, "email_page", FILTER_VALIDATE_EMAIL),
        'phone' => filter_input(INPUT_POST, "phone", FILTER_SANITIZE_SPECIAL_CHARS),
        'civilite' => filter_input(INPUT_POST, "civilite", FILTER_SANITIZE_NUMBER_INT),
        'messenger' => filter_input(INPUT_POST, "messenger", FILTER_SANITIZE_SPECIAL_CHARS),
        'skype' => filter_input(INPUT_POST, "skype", FILTER_SANITIZE_SPECIAL_CHARS),
        'offre_groupe' => filter_input(INPUT_POST, "acceptcond2_page", FILTER_SANITIZE_SPECIAL_CHARS) == "on" ? 1 : 0,
        'offre_partenaire' => filter_input(INPUT_POST, "acceptcond3_page", FILTER_SANITIZE_SPECIAL_CHARS) == "on" ? 1 : 0
    );
    $champs = array_keys($data);
    $sql = "INSERT INTO spip_klb_user (" . implode(',', $champs) . ") VALUES (:" . implode(',:', $champs) . ")"; 

    $insert_user = $db->prepare($sql);
    $insert_user->execute($data);
    /*$sql = 'INSERT INTO spip_klb_user 
        (nom, prenom, email, phone, civilite, skype, offre_groupe, offre_partenaire) 
        VALUES (:nom, :prenom, :email, :phone, :civilite ,:skype, :offre_groupe, :offre_partenaire) 
        ON DUPLICATE KEY UPDATE nom=:nom, prenom=:prenom, email=:email, phone=:phone, civilite=:civilite , skype=:skype, offre_groupe=:offre_groupe, offre_partenaire:offre_partenaire';
    $insert_user = $db->prepare($sql);
    $insert_user->execute($data);*/
    echo json_encode(1);

} else {
    $erreur[] = 'Captcha invalide';
}