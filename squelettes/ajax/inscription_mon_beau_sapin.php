<?php
define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "antenne_art_spip");
define("DATA_BASE_LOGIN", "antenne_spipAR");
define("DATA_BASE_PASSWORD", "antv3_2009");
define("DATA_BASE_TABLE_USER", "user_monbeausapin");

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION
);

$erreur = array();

try {
    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $secret = "6LdBMlEUAAAAAPG2pndmdpMHmdK_dwoBMk51Bdpy";
    $response = $_POST["g-recaptcha-response"];

    $email = filter_input(INPUT_POST, "mail", FILTER_VALIDATE_EMAIL);

    $data = array(
        'nom'         => filter_input(INPUT_POST, "nom", FILTER_SANITIZE_SPECIAL_CHARS),
        'prenom'      => filter_input(INPUT_POST, "prenom", FILTER_SANITIZE_SPECIAL_CHARS),
        'civilite'    => $_POST['civilite'],
        'adresse'     => filter_input(INPUT_POST, "adresse", FILTER_SANITIZE_SPECIAL_CHARS),
        'CP'          => $_POST['cp'],
        'ville'       => $_POST['ville'],
        'telephone'   => $_POST['telephone'],
        'mail'        => $email,
        'nb_famille'  => $_POST['nb_famille'],
        'commentaire' => $_POST['commentaire'],
        'photo'       => 'no photo',
    );

    if ($email === false || $email == "") {
        $erreur[] = "Adresse email " . $email . " invalide";
    }

    $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
    $captcha_success = json_decode($verify);

    if ($captcha_success->success == false) {
        $erreur[] = "Captcha invalide";
    } else if ($captcha_success->success == true && count($erreur) < 1) {

        $sQuery = "SELECT id FROM " . DATA_BASE_TABLE_USER . " WHERE mail LIKE :mail";
        $stmt = $db->prepare($sQuery);
        $stmt->execute(["mail" => $email]);
        $mReturn = $stmt->fetch();

        if (!is_bool($mReturn)) {
            echo json_encode(['has_participate' => true]);
            die;
        }

        $uploads_dir = $_SERVER['DOCUMENT_ROOT'] . '/upload_mon_beau_sapin';
        if (!empty($_FILES["upfile"]['name'])) {
            $name = basename($_FILES["upfile"]['name']);
            $tmp_name = $_FILES["upfile"]['tmp_name'];
            move_uploaded_file($tmp_name, "$uploads_dir/$name");
            $data['photo'] = "https://cdn.antenne.re/antenne/upload_mon_beau_sapin/" . $name;
        }

        $sql = 'INSERT INTO ' . DATA_BASE_TABLE_USER . ' (nom, prenom, mail, civilite ,adresse, CP, ville, telephone, nb_famille, commentaire, photo)';
        $sql .= ' VALUES (:nom, :prenom, :mail, :civilite ,:adresse, :CP, :ville, :telephone, :nb_famille, :commentaire, :photo)';
        $sql .= ' ON DUPLICATE KEY UPDATE nom=:nom, prenom=:prenom, civilite=:civilite , adresse=:adresse, CP=:CP, ville=:ville, telephone=:telephone, nb_famille=:nb_famille, commentaire=:commentaire, photo=:photo';

        $insert_user = $db->prepare($sql);
        $insert_user->execute($data);
        echo json_encode(1);
        die;
    } else {
        echo json_encode($erreur);
        die;
    }

} catch (Exception $e) {
}

