<?php

//définition des constantes
define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "antenne_art_spip");
define("DATA_BASE_LOGIN", "antenne_spipAR");
define("DATA_BASE_PASSWORD", "antv3_2009");
define("DATA_BASE_TABLE_NEWSLETTER", "spip_nl_user");
define("DATA_BASE_TABLE_USER_NEWSLETTER", "spip_nl_user_letter");

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

$erreur = array();

try {
    //connexion PDO
    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //récupération et nettoyage des POSTS
    $mail_user = filter_input(INPUT_POST, "mail_user", FILTER_VALIDATE_EMAIL);
    $date_inscription = date("Y-m-d H:i:s");
    $statut = filter_input(INPUT_POST, "optin", FILTER_SANITIZE_SPECIAL_CHARS) == "on" ? 1 : 0;
    $statut_partenaire = filter_input(INPUT_POST, "optin_partenaire", FILTER_SANITIZE_SPECIAL_CHARS) == "on" ? 1 : 0;

    if ($mail_user === false || $mail_user == "") {
        $erreur[] = "Adresse email invalide";
    }
    if (count($erreur) > 0) {
        echo json_encode($erreur);
        die;
    }
    //vérifie si l'enregistrement existe déja dans la table DATA_BASE_TABLE_NEWSLETTER
    $sql = "SELECT COUNT(*) FROM " . DATA_BASE_TABLE_NEWSLETTER . " WHERE mail_user='" . $mail_user . "'";
    $mail_user_exist = $db->query($sql)->fetchColumn() == 0;

    if ($mail_user_exist) {//l'entrée n'existe pas encore (INSERT)
        $req_nl_user = $db->prepare("INSERT INTO " . DATA_BASE_TABLE_NEWSLETTER . " (mail_user, date_inscription, statut, statut_partenaire) VALUES (:mail_user, :date_inscription, :statut, :statut_partenaire)");
        $req_nl_user->execute(array(
            "mail_user" => $mail_user,
            "date_inscription" => $date_inscription,
            "statut" => $statut,
            "statut_partenaire" => $statut_partenaire
        ));
        $last_id = $db->lastInsertId();

        $req_nl_user_letter = $db->prepare("INSERT INTO " . DATA_BASE_TABLE_USER_NEWSLETTER . " (id_user, id_letter) VALUES (:id_user, :id_letter)");
        $req_nl_user_letter->execute(array(
            "id_user" => $last_id,
            "id_letter" => 2
        ));
        echo json_encode(1);
    } else {//l'entrée existe déjà (UPDATE)
        $sql_update = "UPDATE `" . DATA_BASE_TABLE_NEWSLETTER . "` SET statut=" . $statut . ",statut_partenaire=" . $statut_partenaire . " WHERE mail_user='" . $mail_user . "'";
        $db->query($sql_update);
        echo json_encode("L'entrée existe déjà et a été mise à jour");
    }
} catch (Exception $e) {
    //echo $e->getMessage()."<br/>".$e->getLine();
    echo "Erreur";
    die;
}