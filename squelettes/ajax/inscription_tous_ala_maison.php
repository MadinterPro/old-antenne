<?php

require "PHPMailer-master/PHPMailerAutoload.php";

define("DATA_BASE_HOST", "localhost");

define("DATA_BASE_NAME", "antenne_art_spip");

define("DATA_BASE_LOGIN", "antenne_spipAR");

define("DATA_BASE_PASSWORD", "antv3_2009");

define("DATA_BASE_TABLE_USER", "inscription_tous_a_la_maison");

$options = array(

    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",

    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION

);
//
//echo "<pre>";
//var_dump(INPUT_POST);
//var_dump($_POST);
//exit;

$erreur = array();

try {

    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//    $secret="6LdBMlEUAAAAAPG2pndmdpMHmdK_dwoBMk51Bdpy";
//
//
//
//    $response = $_POST["g-recaptcha-response"];

//
//     $verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
//     $captcha_success=json_decode($verify);
//
//    $captcha_verify = false;
//    if ($captcha_success->success==true) {
//        $captcha_verify = true;
//    }
    $email = filter_input(INPUT_POST, "mail", FILTER_VALIDATE_EMAIL);

    if ($email === false || $email == "") {
        $erreur[] = "Adresse email ".$email." invalide";
        die;
    }

    $target_dir = "/home/antenne/public_html/tous_a_la_maison_video/";

    // extension fichiers autorisés

    $tabExtensions = array("mp4","MP4");

    $file_video = "";

    $char = array("à", "ä", "â", "é", "è", "ë", "ê", "ö", "ô", "ì", "ï", "î", "ü", "û");

    if (isset($_FILES["file_video"]["name"]) && !empty($_FILES["file_video"]["name"])) {

        $tabVideo = explode(".", $_FILES["file_video"]["name"]);

        $file_video = $_POST["prenom"] . "_". $_POST["nom"] . date("dmy") . "_" . rand() . "." . $tabVideo[count($tabVideo)-1];

        $file_video = str_replace($char, "", $file_video);

        if (!in_array($tabVideo[count($tabVideo)-1], $tabExtensions)) {
            $erreur[]= "<p>Veuillez téléverser un fichier valide.</p>";
            echo json_encode($erreur);
            die;
        }

        $target_file = $target_dir . $file_video;
        if (!move_uploaded_file($_FILES["file_video"]["tmp_name"], $target_file)) {
            $erreur[]= "Une erreur est survenue lors du téléchargement d'un fichier";
            echo json_encode($erreur);
            die;
        }
    }


    $data = array(
        'civilite' => filter_input(INPUT_POST, "civilite", FILTER_SANITIZE_SPECIAL_CHARS),
        'nom' => filter_input(INPUT_POST, "nom", FILTER_SANITIZE_SPECIAL_CHARS),
        'prenom' => filter_input(INPUT_POST, "prenom", FILTER_SANITIZE_SPECIAL_CHARS),
        'age' => filter_input(INPUT_POST, "age", FILTER_SANITIZE_SPECIAL_CHARS),
        'email' => $email,
        'telephone' => filter_input(INPUT_POST, "phone", FILTER_SANITIZE_SPECIAL_CHARS),
        'video' => $file_video,
        'url_video' => filter_input(INPUT_POST, "url_video", FILTER_SANITIZE_SPECIAL_CHARS),
        'offre_groupe' => (isset($_POST["acceptcond2_page"]) ? filter_input(INPUT_POST, "acceptcond2_page", FILTER_SANITIZE_SPECIAL_CHARS):""),
        'offre_partenaire' => (isset($_POST["acceptcond3_page"]) ? filter_input(INPUT_POST, "acceptcond3_page", FILTER_SANITIZE_SPECIAL_CHARS):""),
        'date_inscription' => date("Y-m-d")
    );


//
//     if ($captcha_verify==false) {
//
//
//         $erreur[]= "Captcha invalide";
//
//
//     } else {

    if (count($erreur) < 1) {

        $sql = 'INSERT INTO '. DATA_BASE_TABLE_USER . ' (civilite, nom, prenom, age, email, telephone, video, url_video, date_inscription, offre_groupe, offre_partenaire)';
        $sql .= ' VALUES (:civilite, :nom, :prenom, :age, :email, :telephone, :video, :url_video, :date_inscription, :offre_groupe, :offre_partenaire)';

        $insert_user = $db->prepare($sql);
        $insert_user->execute($data);

//        sendMail($email);
        send_mail($email);

        echo json_encode(1);
        die;

    } else {
        echo json_encode($erreur);
        die;
    }

//     }
} catch (Exception $e) {
    $erreur[] = $e->getMessage();
    echo "Erreur interne: " . json_encode($erreur);
}

function sendMail($email) {

    $mail = new PHPMailer;
    $mail->CharSet = "UTF-8";
    $mail->Subject = 'Inscription au jeu DÉCLAREZ VOTRE AMOUR AVEC LU';
    $mail->setFrom('noreply@antennereunion.fr');
    $mail->addAddress($email);
    $mail->addBCC("geraldo.raobelinarisoa@antennereunion.fr");
    $mail->isHTML(true);

    $sBody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">
  <head>
    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">
    <meta name=\"format-detection\" content=\"telephone=no\">
    <title></title>
    <!--[if !mso]><!-->
    <meta http-equiv=\"x-ua-compatible\" content=\"IE=9; IE=8; IE=7; IE=edge\">
    <!--<![endif]-->
    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
    <meta name=\"x-apple-disable-message-reformatting\">
    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;\">
    <!--[if !mso]><!-->
    <!--<![endif]-->
    <!--[if mso]>
    <xml>
      <o:OfficeDocumentSettings>
        <o:AllowPNG />
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style>
      .outlook-group-fix {
      width:100% !important;
      }
    </style>
    <![endif]-->
    
  <style type=\"text/css\">
		*{
			box-sizing:border-box;
		}
		#outlook a{
			padding:0;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass *{
			line-height:100%;
		}
		body{
			margin:0;
			padding:0;
			-webkit-text-size-adjust:100%;
			-ms-text-size-adjust:100%;
		}
		table,td{
			border-collapse:collapse;
			mso-table-lspace:0;
			mso-table-rspace:0;
			font-family:Arial,Helvetica,sans-serif;
		}
		img{
			border:0;
			height:auto;
			line-height:100%;
			outline:0;
			text-decoration:none;
			-ms-interpolation-mode:bicubic;
		}
		p{
			display:block;
		}
		.couv{
			width:100%;
			max-width:600px;
			margin:0 auto;
		}
		.col_1_tr_1,.col_2_tr_1{
			width:200px !important;
		}
	@media only screen and (max-width: 600px){
		.hide{
			display:block;
		}

}	@media only screen and (max-width: 600px){
		width:320px{
			width:320px;
		}

}	@media only screen and (max-width: 600px){
		table{
			width:100% !important;
			height:auto !important;
			max-width:600px !important;
			border-collapse:collapse !important;
		}

}	@media only screen and (max-width: 600px){
		div{
			width:100% !important;
			display:block !important;
			max-width:100% !important;
		}

}	@media only screen and (max-width: 600px){
		img{
			width:100% !important;
			height:auto !important;
		}

}	@media only screen and (max-width: 600px){
		.mj-column-per-100{
			width:100% !important;
			max-width:100%;
		}

}	@media only screen and (max-width: 600px){
		.td_300{
			width:33.3333% !important;
			display:inline-block !important;
		}

}	@media only screen and (max-width: 600px){
		td{
			width:100% !important;
			display:block !important;
		}

}	@media only screen and (max-width: 600px){
		.couv_td_300{
			width:100% !important;
			display:block !important;
			max-width:600px !important;
		}

}	@media only screen and (max-width: 600px){
		.ml font{
			font-size:14px !important;
		}

}	@media only screen and (max-width: 600px){
		.largeur_100{
			width:100%;
			height:auto !important;
			display:block !important;
		}

}	@media only screen and (max-width: 600px){
		.stack-column{
			display:inline-block !important;
			width:33.3333% !important;
			max-width:100px !important;
			direction:ltr !important;
		}

}	@media only screen and (max-width: 600px){
		.foot_col{
			display:inline-block !important;
			width:100% !important;
			max-width:inherit !important;
			direction:ltr !important;
		}

}	@media only screen and (max-width: 600px){
		.col_moitie{
			display:inline-block !important;
			width:50% !important;
			max-width:inherit !important;
			min-width:100px !important;
			direction:ltr !important;
		}

}	@media only screen and (max-width: 600px){
		.col_1_tr_1_cheveux{
			display:inline-block !important;
			width:20% !important;
			max-width:inherit !important;
			min-width:inherit !important;
			direction:ltr !important;
			margin:-1px !important;
		}

}	@media only screen and (max-width: 600px){
		.spacer{
			height:20px !important;
			font-size:20px !important;
		}

}	@media only screen and (max-width: 600px){
		.stack-column-2{
			display:inline-block !important;
			width:50% !important;
			max-width:300px !important;
			direction:ltr !important;
		}

}</style></head>
  <body bgcolor=\"#ffffff\" alink=\"#333333\" vlink=\"#333333\" link=\"#333333\">
    <div style=\"background-color:#ffffff;\" class=\"couv\">
      <!--[if mso | IE]>
      <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:600px;\">
        <tr>
          <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:excatly;\">
            <![endif]-->
      <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;max-width:600px;\">
        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"border-collapse:collapse;border-spacing:0px;\">
          <tr>
            <td style=\"width:600px;\">
                <img src=\"https://cdn.antenne.re/antenne/newsletter-element/nl_decembre_2020/heade-NL-mondelez.jpg\" alt=\"Déclarez votre amour à vos proches ! avec lu\" style=\"border:0; display: block; outline: none; text-decoration: none; height: auto; width: 100%;\">
              </td>
            </tr>
            
          </table>
        </div>
        <!--[if mso]>
      </td>
    </tr>
  </table>
  <![endif]-->
        <!--[if mso | IE]>
      </td>
    </tr>
  </table>
  <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body-section-outlook\" style=\"width:600px;\" width=\"600\">
    <tr>
      <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">
        <![endif]-->
        <div class=\"body-section\" style=\"margin:0px auto;max-width:600px;background:#64b6e6;\">
          <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">
            <tbody>
              <tr>
                <td style=\"direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0;padding-top:0;text-align:center;\">
                  <!--[if mso | IE]>
                  <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                    <tr>
                      <td width=\"600\">
                        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:600px;\" width=\"600\">
                          <tr>
                            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">
                              <![endif]-->
                  <div style=\"background:#64b6e6;background-color:#64b6e6;margin:0px auto;max-width:600px;\">
                    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"background:#64b6e6;background-color:#64b6e6;width:100%;\">
                      <tbody>
                        <tr>
                          <td style=\"direction:ltr;font-size:0px;padding:20px 0;text-align:center;\">
                            <!--[if mso | IE]>
                            <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                              <tr>
                                <td style=\"vertical-align:top;width:500px;\">
                                  <![endif]-->
                            <div class=\"mj-column-per-100 mj-outlook-group-fix\" style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">
                              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">
                               
                                  <tr>
                                    <td align=\"center\" style=\"font-size:0px;padding:20px 0 0;\">
                                      <div style=\"font-family:Arial, 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight:normal;line-height:normal;text-align:center;color:#000000;\">
                                        <img alt=\"Merci de votreparticipation au jeu\" src=\"https://cdn.antenne.re/antenne/newsletter-element/nl_decembre_2020/Merci-de-votre-participation-au-jeu-lu.png\"></div>
                                      </td>
                                    </tr>
                                    
                                    <tr>
                                      <td height=\"10\" style=\"font-size:10px;\"> </td>
                                    </tr>                               
                                 </table>
                            </div>
                            <!--[if mso | IE]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                         	</td>
                        </tr>
                    </tbody>
                </table>
             </div>
            <!--[if mso | IE]>
            </td>
        </tr>
       </table>
      </td>
    </tr>
    </table>
    <![endif]-->
</td>
</tr>
</tbody>
</table>
</div>
    <!--[if mso | IE]>
                </td>
              </tr>
            </table>
            <![endif]-->
       </div>
    </body>
</html>    ";


    $mail->Body = $sBody;
    $mail->send();
}

function send_mail($email) {
    $mail = new PHPMailer;
    $mail->CharSet = "UTF-8";
    $mail->Subject = 'Inscription à l\'émission TOUS A LA MAISON';
    $mail->setFrom('noreply@antennereunion.fr');
    $mail->addAddress($email);
    $mail->addBCC("geraldo.raobelinarisoa@antennereunion.fr");
    $mail->addBCC("johary.rakoto@antennereunion.fr");
    $mail->isHTML(true);

    $sBody = "Votre inscription à l’émission Tous à la Maison a été prise en compte
Merci pour votre participation.";

    $mail->Body = $sBody;
    $mail->send();
}

?>