<?php

//définition des constantes
define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "antenne_art_spip");
define("DATA_BASE_LOGIN", "antenne_spipAR");
define("DATA_BASE_PASSWORD", "antv3_2009");
define("DATA_BASE_TABLE_VOTELOREAL", "voteloreal");

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

$erreur = array();

try {
    //connexion PDO
    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $numero = 1;//filter_input(INPUT_POST, "numero", FILTER_SANITIZE_SPECIAL_CHARS);
    $select_sql = "SELECT * FROM " . DATA_BASE_TABLE_VOTELOREAL;
    $o = $db->query($select_sql)->fetchAll();
    echo json_encode($o);
    die;
} catch (Exception $e) {
    echo $e->getMessage()."<br/>".$e->getLine();
    echo "Erreur";
    die;
}