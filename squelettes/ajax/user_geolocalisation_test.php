<?php
//définition des constantes
define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "antenne_art_spip");
define("DATA_BASE_LOGIN", "antenne_spipAR");
define("DATA_BASE_PASSWORD", "antv3_2009");
define("DATA_BASE_TABLE_NEWSLETTER", "spip_pleinte_geoloc");

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

require 'PHPMailer-master/PHPMailerAutoload.php';
$user = array();
$arr = array();
$erreur = array();
//récupération et nettoyage des POSTS
$user['email'] = filter_input(INPUT_POST, "mail", FILTER_VALIDATE_EMAIL);
$user['civilite'] = filter_input(INPUT_POST, "civilite", FILTER_SANITIZE_SPECIAL_CHARS);
$user['nom'] = filter_input(INPUT_POST, "nom", FILTER_SANITIZE_SPECIAL_CHARS);
$user['prenom'] = filter_input(INPUT_POST, "prenom", FILTER_SANITIZE_SPECIAL_CHARS);
$user['commentaires'] = filter_input(INPUT_POST, "commentaires", FILTER_SANITIZE_SPECIAL_CHARS);
$user['client_ip'] = get_client_ip();
$user['date_inscription'] = date("Y-m-d H:i:s");

function get_client_ip() {
    $ipaddress = "";
    if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}    

try {
    //connexion PDO
    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $req_nl_user = $db->prepare("INSERT INTO " . DATA_BASE_TABLE_NEWSLETTER . " (email,civilite,nom,prenom, commentaires, client_ip,date_inscription) VALUES (:email,:civilite,:nom,:prenom, :commentaires, :client_ip,:date_inscription)");
    $req_nl_user->execute(array(
            "email" => $user['email'],
            "civilite" => $user['civilite'],
            "nom" => $user['nom'],
            "prenom" => $user['prenom'],
            "commentaires" => $user['commentaires'],
            "client_ip" => $user['client_ip'],
            "date_inscription" => $user['date_inscription'],
        ));

        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->setFrom('noreply@antennereunion.fr', 'Antenne Réunion');
        $mails_envoi = array($user['email']);
        foreach ($mails_envoi as $mail_envoi) {
            $mail->addAddress($mail_envoi);
        }
        $mail->Subject  = 'Géolocalisation - plainte';
        $mail->isHTML(true);
        $html = "Bonjour " . $user['nom'] ."  " . $user['prenom'] .",<br/><br/>Certains de nos replays sont géolocalisés pour des raisons de droits de diffusion et ne sont donc pas disponibles hors de La Réunion.<br/>Si vous êtes à La Réunion, nos équipes techniques sont prévenues et travaillent à la résolution de votre problème.<br/><br/>Merci de votre compréhension.<br/>Bien à vous";

        $mail->Body = $html;
        if(!$mail->send())
        {
            echo "ko";
        }else{
            echo "ok" ; 
        }
} catch (Exception $e) {
    //echo $e->getMessage()."<br/>".$e->getLine();
    echo "ko";
}

