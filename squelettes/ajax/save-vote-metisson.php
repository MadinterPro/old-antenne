<?php

require "PHPMailer-master/PHPMailerAutoload.php";
define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "antenne_art_spip");
define("DATA_BASE_LOGIN", "antenne_spipAR");
define("DATA_BASE_PASSWORD", "antv3_2009");
define("DATA_BASE_TABLE_USER", "metisson_vote");

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

try {
    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $email = filter_input(INPUT_POST, "mail", FILTER_VALIDATE_EMAIL);

    if ($email === false || $email == "") {
        $erreur[] = "Adresse email ".$email." invalide";
        die;
    }

    $data = array(
        'email' => $email,
        'id_candidat' => filter_input(INPUT_POST, "id_candidat", FILTER_SANITIZE_SPECIAL_CHARS),
        'date_inscription' => date("Y-m-d")
    );


    // Tester si l'utilisateur a déjà voté
    $sql_select = 'SELECT COUNT(*) FROM metisson_vote WHERE email = "' . $data["email"]. '" AND date_inscription = "' . date("Y-m-d") . '"';
    $stmt = $db->query($sql_select);
    $data_result = $stmt->fetch();
    if (intval($data_result[0]) > 0) {
        $erreur[] = "Utilisateur a déjà voté";
        echo json_encode(2);
        die;
    }



    if (count($erreur) < 1) {

        $sql = 'INSERT INTO '. DATA_BASE_TABLE_USER . ' (email, id_candidat, date_inscription)';
        $sql .= ' VALUES (:email, :id_candidat, :date_inscription)';

        $insert_user = $db->prepare($sql);
        $insert_user->execute($data);

        echo json_encode(1);
        die;

    } else {
        echo json_encode($erreur);
        die;
    }
} catch (Exception $e) {
    echo "connexion";
}

?>