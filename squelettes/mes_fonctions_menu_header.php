<?php

function filtre_menu_replay($menu) {


      if (!($html = antenne_lire_cache('menu-replay-'.$menu))) {

        if ($menu == "emission") {
            $html = render_menu_replay("émissions", "/replay/emissions", 732);
        } else if ($menu == "serie_fiction") {
            $html = render_menu_replay("Séries et fictions", "/replay/series-et-fictions",14);
        } else {
            $html = render_menu_replay("Info et magazine", "/replay/info-et-magazines",742);
        }

        antenne_ecrire_cache('menu-replay-'.$menu, $html);
        
      }

    return $html;

}

function render_menu_replay($menu, $url, $id_secteur) {
    date_default_timezone_set("Indian/Reunion");
    // Récupération de tous les rubriques replay dans le secteur 732
    $min = date("i");
    if ($min < 15)
        $min = 00;
    elseif ($min < 30)
        $min = 15;
    elseif ($min < 45)
        $min = 30;
    else $min = 45;

    $date_du_jour = sprintf("%s%d:00", date("Y-m-d H:"), $min);

    $req1 = sql_query("SELECT * FROM (
                SELECT sa.id_article, sa.id_rubrique, sr.id_parent, sa.titre, sa.date, sr.titre as sr_titre
                FROM spip_articles AS sa
                INNER JOIN spip_rubriques AS sr ON sr.id_rubrique = sa.id_rubrique AND sr.id_secteur = $id_secteur
                WHERE sa.id_article IS NOT NULL
                AND sa.statut = 'publie'
                AND sa.date < '$date_du_jour'
                AND sr.titre = 'Replay'
                ORDER BY sa.id_article DESC
                LIMIT 0, 25) t1
            GROUP BY t1.id_rubrique
            ORDER BY t1.id_article DESC
            LIMIT 0,4" );
    $data_article = sql_fetch_all($req1);
    $html = '';

    $html .= '<div class="listLiens-programme">';

    $html .= '<div class="listLiensTitle-programme">';
    $html .= '<p><a href="'.$url.'">'.$menu.'</a></p>';
    $html .= '</div>';
    $html .= '<ul>';

    foreach ($data_article as $key => $row) {

        $url_article = generer_url_entite($row['id_article'], 'article', '','', true);
        $url_rubrique = generer_url_entite($row['id_rubrique'], 'rubrique', '','', true);
        $info_rubrique = info_rubrique($row['id_rubrique']);
        $info_rubrique_parent = info_rubrique($info_rubrique["id_parent"]);
        $info_image_article = info_image($row['id_article']);
        $info_image_parent = info_image($info_rubrique["id_parent"]);

        $data_image = "";

        $html .= '<li>';
        $html .= '<div class="progr-bloc-container1">';

        $html .= '<p class="image-news format-video">';
        $html .= '<a href="'.$url_article.'" class="fond_noir">';
        $html .= '<span class="roll">&nbsp;</span>';

        if (!empty($info_image_article)) {
            $data_image = traitement_image($info_image_article);
        } else {
            $data_image = traitement_image($info_image_parent);
        }

        $html .= '<img src="'.$data_image["fichier"].'" alt="'.$data_image["titre"].'"  height="auto" width="310"/>';

        $html .= '<span class="icon-video post-format"></span>';
        $html .= '</a>';
        $html .= '</p>';

        $html .= '<div class="chapo-bloc">';
        $html .= '</div>';
        $html .= '<p class="news-pr-description"> <a href="'.$url_article.'">'.tronque_chaine($row['titre'], 38).'</a>';
        $html .= '<span class="news-pr-description-title">';
        $html .= '<a href="'.$url_rubrique.'">'.$info_rubrique_parent["titre"].'</a>';
        $html .= '</span>';
        $html .= '</p>';
        $html .= '</div>';
        $html .= '</li>';

    }
    $html .= '</ul>';

    $html .= '</div>';

    return $html;
}

function tronque_chaine ($chaine, $lg_max) {
    if (strlen($chaine) > $lg_max)
    {
        $chaine = substr($chaine, 0, $lg_max);
        $last_space = strrpos($chaine, " ");
        $chaine = substr($chaine, 0, $last_space)." (...)";
    }

    return $chaine;
}

function info_rubrique ($id_rubrique) {
    $req1 = sql_query("SELECT * FROM spip_rubriques WHERE id_rubrique = '".$id_rubrique."'");
    $data_rubrique = sql_fetch($req1);

    return $data_rubrique;
}

function info_image ($id_obj) {

    $req1 = sql_query("SELECT t1.fichier, t1.titre FROM spip_documents AS t1, spip_documents_liens AS t2
                             WHERE t1.id_document = t2.id_document 
                            AND t1.extension  IN ('png','jpg','gif')
                             AND t2.id_objet = '".$id_obj."' ORDER BY t1.DATE ASC LIMIT 0,1");
    $data_rubrique = sql_fetch($req1);

    return $data_rubrique;
}

function traitement_image ($data_image) {

    //    Remplace http par https
    $fichier = str_replace("http:", "https:", $data_image["fichier"]);

    if (preg_match("/https/", $fichier)) {
        $data_image["fichier"] = $fichier;
    } else {
        $data_image["fichier"] = (substr($fichier, 0, 4) == '/IMG') ? $fichier : '/IMG/'.$fichier;
    }

    return $data_image;

}

function filtre_menu_programme($menu) {

      if (!($html = antenne_lire_cache('menu-programme-'.$menu))) {

        if ($menu == "emission") {
            $html .= render_menu_programme("émissions", "/emissions", 6693);
        } else if ($menu == "serie_fiction") {
            $html .= render_menu_programme("Séries et fictions", "/series-et-fictions", 6694);
        } else {
            $html .= render_menu_programme("Info et magazine", "/info-et-magazines", 6695);
        }

        antenne_ecrire_cache('menu-programme-'.$menu, $html);
        
      }

    return $html;

}

function render_menu_programme($menu, $url, $id_rub) {

    $req1 = sql_query("SELECT * FROM spip_articles WHERE id_rubrique = '".$id_rub."' AND statut='prepa'");
    $data_articles = sql_fetch_all($req1);

    $html = '';

    $html .= '<div class="listLiens">';
    $html .= '<div class="listLiensTitle">';
    $html .= '<p>';
    $html .= '<a href="'.$url.'">'.$menu.'</a>';
    $html .= '</p>';
    $html .= '</div>';
    $html .= '<ul>';

    foreach ($data_articles as $key => $item) {
        $html .= '<li>';
        $html .= '<a href="'.$item['chapo'].'"  style="text-transform: capitalize;">'.strtoupper($item['titre']).'</a>';
        $html .= '</li>';
    }

    $html .= '</ul>';
    $html .= '</div>';

    return $html;

}

?>
