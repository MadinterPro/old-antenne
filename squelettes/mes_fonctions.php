<?php
function balise_SECURE_LINK($p) {
    $url = interprete_argument_balise(1, $p);
    $p->code = "secure_link2($url)";
    return $p;
}

function secure_link2($file) {
    if (strpos($file, "cdn.antenne.re") !== false && strpos($file, "archivevideo") === false && strpos($file, "nas") === false) {
        $cdn = "https://cdn.antenne.re";
        if (strlen($file) > strlen($cdn)) {
            $file = substr($file, strlen($cdn));
            $secret = 'antenne_reunion'; // hash secret à changer et à nous communiquer
            $path = '/' . trim($file, '/');
            //$expire = time() + 3600; // durée d'expiration (1 heure ici)
            //$expire = time() + 108000; // durée d'expiration (30 jours ici)
            //$expire = 1431411406; // 13 Mai 2015
            //$expire = 1434263104;
            $expire = 1439108452; //09 Aout 2015

            $md5 = base64_encode(md5($secret . $path . $expire, true));
            $md5 = strtr($md5, '+/', '-_');
            $md5 = str_replace('=', '', $md5);

            return $cdn . $path . '?key=' . $md5 . '&e=' . $expire;
        } else
            return "";
    } else
        return $file;
}
//Pour meteo
function balise_NOMMAGE($params) {
    $params->code = "GetNommage()";
    $params->type = 'php';
    return $params;
}
function GetNommage() {
    $objDateTime = new DateTime('NOW');
    $heure = date('Hi');
    //$minute = date('i');
    $ret = "";
    if (($heure < 1330)) { //inférieur à 11h
        $objDateTime->modify('-1 day');
        $ret = "SOIR_" . $objDateTime->format('d-m-Y') . "_19-00";
    }elseif ($heure > 1330) { // entre 11h et 19h
        //$ret = "MIDI_".$objDateTime->format('d-m-Y')."_12-30";
        /* supposons qu'il n'y a pas de vidéo de midi */
        //$objDateTime->modify('-1 day');
        $ret = "SOIR_" . $objDateTime->format('d-m-Y') . "_19-00";
    }else{
        $ret = "SOIR_" . $objDateTime->format('d-m-Y') . "_19-00";
    }
    return $ret;
}
//Fin meteto
function balise_MAINTENANT($params) {
    $params->code = "date('dmY')";
    $params->type = 'php';
    return $params;
}

function balise_MAINTENANTFLUX($params) {
    $params->code = "date('Y-m-d H:i:s')";
    $params->type = 'php';
    return $params;
}

function filtre_afficher_reponses_sondage($id_form) {

    $r = '';
    $id_form = intval($id_form);

    $result = spip_query("SELECT * FROM spip_forms WHERE id_form=" . _q($id_form));
    if (!$row = spip_fetch_array($result))
        return '';
    $type_form = $row['type_form'];

    //$r .= "<div class='question_body'>\n";

    $res2 = spip_query("SELECT * FROM spip_forms_champs AS champs
  WHERE id_form=" . _q($id_form) . " AND type IN ('select','multiple','mot') ORDER BY champ");
    while ($row2 = spip_fetch_array($res2)) {
        // On recompte le nombre total de reponses reelles 
        // car les champs ne sont pas forcement obligatoires
        $row3 = spip_fetch_array(spip_query("SELECT COUNT(DISTINCT c.id_donnee) AS num " .
                        "FROM spip_forms_donnees AS r LEFT JOIN spip_forms_donnees_champs AS c USING (id_donnee) " .
                        "WHERE r.id_form=" . _q($id_form) . " AND r.confirmation='valide' AND c.champ=" . _q($row2['champ'])));
        if (!$row3 OR ! ($total_reponses = $row3['num']))
            continue;

        // Construire la liste des valeurs autorisees pour le champ
        $liste = array();
        if ($row2['type'] != 'mot') {
            $res3 = spip_query("SELECT * FROM spip_forms_champs_choix WHERE id_form=" . _q($id_form) . " AND champ=" . _q($row2['champ']));
            while ($row3 = spip_fetch_array($res3))
                $liste[$row3['choix']] = $row3['titre'];
        } else {
            $id_groupe = intval($row2['extra_info']);
            $res3 = spip_query("SELECT id_mot, titre FROM spip_mots WHERE id_groupe=$id_groupe ORDER BY titre");
            while ($row3 = spip_fetch_array($res3))
                $liste[$row3['id_mot']] = $row3['titre'];
        }

        // Nombre de reponses pour chaque valeur autorisee
        $query = "SELECT c.valeur, COUNT(*) AS num " .
                "FROM spip_forms_donnees AS r LEFT JOIN spip_forms_donnees_champs AS c USING (id_donnee) " .
                "WHERE r.id_form=" . _q($id_form) . " AND r.confirmation='valide' " .
                "AND c.champ=" . _q($row2['champ']) . " GROUP BY c.valeur";
        $result = spip_query($query);
        $chiffres = array();
        // Stocker pour regurgiter dans l'ordre
        while ($row = spip_fetch_array($result)) {
            $chiffres[$row['valeur']] = $row['num'];
        }


        // Afficher les resultats
        $r .= ($t = typo($row2['titre'])) ? "<h3 style='display:none'> $t :</h3/><ul class='resulat-wrapper' style='display:none'>" : "";
        $r .= "\n";
        foreach ($liste as $valeur => $nom) {
            $n = $chiffres[$valeur];
            $taux = round($n * 100.0 / $total_reponses, 2);
            if (!isset($n)) {
                $n = 0;
            }
            $r .= "<li><label>" . typo($nom) . " </label>";
            $r .= "<div class='feedback-result'><span class='feedback-per'>$taux&nbsp;%</span><span class='feedback-votes'>( $n )</span></div>";
            $r.= "<div class='progress-j'><div class='bar' style='width:" . $taux . "%;'></div></div></li>";
        }
        $r .= "<br />\n";
    }

    $query = "SELECT COUNT(*) AS num FROM spip_forms_donnees " .
            "WHERE id_form=" . _q($id_form) . " AND confirmation='valide'";
    $result = spip_query($query);
    list($num) = spip_fetch_array($result, SPIP_NUM);
    $r .= "</ul><div class='total-votes' style='display:none'> <label>TOTAL DES VOTES : </label> <span>" . $num . "</span></div>";

    //$r .= "</div>\n";

    return $r;
}

/* * modif 27112015 */

function template_mail($to, $subject, $contenu, $date = null) {
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: antennereunion.fr <no-reply@antennereunion.fr>' . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion() . "\n";
    if (is_null($date)) {
        setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
        $date_now = (strftime("%A %d %B %Y %H:%M"));
    } else {
        $date_now = $date;
    }

    $message = '<body bgcolor="#ffffff" link="#333333" vlink="#333333" alink="#333333">
  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
      <td id="webversion" style="font-family:Arial,Helvetica,sans-serif; font-size:10px; height:20px; text-align:center"> Si vous n\'arrivez pas &agrave; lire ce message, <a target="_blank" class="adminText" href="*|ARCHIVE|*" style="color: #1f4563; text-decoration: none;">cliquez-ici </a></td>
    </tr>
  </table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
      <td><a target="_blank" href="http://www.antennereunion.fr/"><img style="display:block;" width="580" height="72" alt="logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/header-ar-nl-alerte.jpg"></a></td>
    </tr>
  </table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
     <td height="15">  </td>
    </tr>
  </table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
     <td align="center"><a href="http://www.linfo.re/tags/regionales" target="_blank"><img src="http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/logo_90_mn.jpg" width="164" height="129" alt="image" style="display:block;"></a></td>
    </tr>
  </table>
   <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
     <td height="5">&nbsp;</td>
    </tr>
  </table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
     <td height="15">  </td>
    </tr>
  </table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
     <td height="15">  </td>
    </tr>
  </table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">' . $contenu . '</table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
     <td height="15">  </td>
    </tr>
  </table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
     <tr>
      <td width="580" align="center"><a target="_blank" href="http://www.linfo.re/tags/regionales"><img style="display:block;" width="253" height="28" alt="" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/btn-decouvrez.jpg"></a></td>
    </tr>
  </table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
     <td height="15">  </td>
    </tr>
  </table>
 <table width="488" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
      <td><a href="http://adserver.adtech.de/adlink/3.0/1165/5403507/0/1/ADTECH;grp=[group];cookie=no;uid=no;" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1165/5403507/0/1/ADTECH;grp=[group];cookie=no;uid=no" border="0" height="60" width="468" alt="[Alt-Text]"></a></td>
    </tr>
  </table>
  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr>
     <td height="15">  </td>
    </tr>
  </table>
  <table width="600" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td width="10">&nbsp;</td>
      <td width="580" valign="top" height="42"><table width="580" bgcolor="#101f2c" align="center" cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td height="42" width="108" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> Suivez-nous </td>
            <td height="42" width="38" align="center"> <a target="_blank" href="https://www.facebook.com/antennereunion?ref=ts&fref=ts"> <img style="display:block;" alt="facebook" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico_facebook.jpg"> </a> </td>
            <td height="42" width="36" align="center"> <a target="_blank" href="https://twitter.com/antennereunion"> <img style="display:block;" alt="twitter" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico-twitter.jpg"> </a> </td>
            <td height="42" width="38" align="center"> <a target="_blank" href="https://plus.google.com/107164283983194784588/posts"> <img style="display:block;" alt="plus google" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico-google.jpg"> </a> </td>
            <td height="42" width="120" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">&nbsp;  </td>
            <td height="42" width="100" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> <a target="_blank" href="https://itunes.apple.com/fr/app/antenne-reunion-television/id927960182?mt=8"> <img style="display:block;" alt="" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/app_store.jpg"> </a> </td>
            <td height="42" width="140" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> <a target="_blank" href="https://play.google.com/store/apps/details?id=com.goodbarber.antennereunion"> <img style="display:block;" alt="antennereunion" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/google_play.jpg"> </a> </td>
          </tr>
        </table></td>
      <td width="10">&nbsp;</td>
    </tr>
  </table>
  <table width="580" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
  <tr>
  <td height="10"></td>
  </tr>
  <tr>
      <td align="center"><p style="font-size:11px;color:#1f4563;"><font face="Arial" size="1">Si vous n\'arrivez &agrave; voir cet email :</font> <a href="*|ARCHIVE|*" target="_blank"><strong><font face="Arial" size="2">Voir ici</font></strong></a></p></td></tr>
  <tr>
  </tr><tr>
  <td height="10"></td>
  </tr>
    <tr>
      <td align="center" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif;"><font face="Arial" size="1">Si vous ne souhaitez plus recevoir de communication de la part d\'AntenneReunion.fr :</font> <a href="*|UNSUB|*" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif; text-decoration:none;"><strong><font face="Arial" size="2">Suivez ce lien</font></strong></a></td>
    </tr>
    <tr>
  <td height="15"></td>
  </tr>
  </table>
</body>
    ';
    return mail($to, $subject, $message, $headers);
}

//mise en ligne carroussel
function supprimer_blancs($texte) {
    return wordwrap($texte, 60, '<br />', 1);
}

//transforme la première lettre d'une phrase en minuscule (Christiane 19/08/2016)
if (!function_exists('lcfirst')) {

    function lcfirst($str) {
        return strtolower(substr($str, 0, 1)) . substr($str, 1);
    }

}

//filtre apostrophe
function apostrophe($texte) {
   $texte = (str_replace("&#8217;","'",$texte));
   return $texte;
}

function html_decode($texte)
{
  return html_entity_decode($texte);
}

function filtre_formater_date($annee, $mois, $jour) {
    $date = $annee;
    $date .= ($mois < 10) ? '0'.intval($mois) : $mois;
    $date .= ($jour < 10) ? '0'.intval($jour) : $jour;
    return $date;
}

/**
* fonction filtre pour afficher 6 mois avant
* @param id_rubrique
* 
* return date
*/
function filtre_affiche_date_recente($id_rubrique) {

  $id_rubrique =  intval($id_rubrique);

  $date_avant = date("Y-m-d", mktime(0,0,0,date("m")-6, date("d"), date("y")));
  
  return $date_avant;

}

/** 
* Filtre pour diviser les programmes du grille TV en Matinee - Apres midi - Soirée
* @param heure de diffusion
*/
function filtre_moment($heure)
{
    if($heure >= '03:00' && $heure < '12:30'){
        return 'Matin';
    }else if ($heure >= '12:30' && $heure < '19:00'){
        return 'Amidi';
    } else if ( $heure >= '19:00' && $heure <= '24:59' ){
        return 'Soiree';
    } else if ($heure >= '01:00' && $heure < '03:00'){
        return 'Soiree';
    }
}

/**
* fonction filtre tester si le programme du jour est disponible ou non
* @param date
* 
* return date
*/
function filtre_info_programme($date_programme)
{
  $daty = date('Y-m-d', strtotime($date_programme));
  $req = sql_select('id',"spip_programmes_semaines","date = '$daty'");
  return sql_count($req);
}

/**
* fonction filtre pour afficher le saison et l'épisode du programme
* @param date
* 
* return date
*/
function filtre_afficher_saison($saison, $episode)
{
  $titre = ( intval($saison) > 0 ) ? 'Saison '.$saison : '';
  $titre .= ( intval($saison) > 0 && intval($episode) > 0 ) ? ' - ' : '';
  $titre .= ( intval($episode) > 0 ) ? 'episode '.$episode : '';
  return $titre;
}

/**
* fonction filtre pour avoir l'interval de la date du programme semaine
* @param date
* 
* return date
*/
function filtre_date_start($daty)
{
  return date("Y-m-d", mktime(0,0,0,date("m"), date("d")-7, date("y")));
}

// Filtre permettant d'afficher la grille TV
function filtre_set_accordeon($site_url, $date_programme=null){
  $daty = (is_null($date_programme)) ? date('Y-m-d') : date('Y-m-d', strtotime($date_programme));
  $where = array(
    "date = '".$daty."'"
  );
  $req = sql_select('id',"spip_programmes_semaines",$where);
  if (sql_count($req) < 1) {
    return '<p>Aucune information disponible pour cette date.</p>';
  } else {
    // début affichage du programme
    $grille = accordeon_body(1, 'collapseOne', 'Matin', 'matin_collapse', $daty);
    $grille .= accordeon_body(2, 'collapseTwo', 'Amidi', 'midi_collapse', $daty);
    $grille .= accordeon_body(3, 'collapseThree', 'Soiree', 'soiree_collapse', $daty);

    return $grille;
  }
}

function accordeon_body($number, $collapse, $periode, $periode_collapse, $date) {
  $collapse_title =  ($periode == 'Matin') ? "Matinée" : ( ($periode == 'Amidi') ? "Après-midi" : "Soirée" );
  $active_accordeon = ($periode == 'Matin') ? 'active-accordion' : '';
  $active_panel = ($periode == 'Matin') ? 'active' : '';
  $accordeon_in = ($periode == 'Matin') ? 'in' : '';
  $string = '<div class="panel-group" id="accordion'.$number.'">
              <div class="panel panel-default grille_accordeon '.$active_accordeon.'">
                <div class="panel-heading ">
                  <h2 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion'.$number.'" href="#'.$collapse.'" class="head_accordeon '.$active_panel.'" data-col="'.$collapse.'" id="'.$periode_collapse.'"><span>'.$collapse_title.'</span></a>
                  </h2>
                </div>
                <div id="'.$collapse.'" class="panel-collapse collapse '.$accordeon_in.' grillenoborder">
                  <div class="panel-body ProgrammeGrilleTV">
                    <div class="panel-group" id="accordion'.$number.'1">
                      '.liste_programme($periode, $date).'
                    </div>
                  </div>
                </div>
              </div>
            </div>';
  return $string;
}

// Récupération liste des programmes par section
function liste_programme($periode, $daty){
  $horaire =  ($periode == 'Matin') ? "( horaire >= '03:00:00' AND horaire < '12:30:00' )" : 
              ( ($periode == 'Amidi') ? "( horaire >= '12:30:00' AND horaire < '19:00:00' )" : 
              " ( horaire >= '19:00:00' AND horaire < '24:59:00' ) "  
              );

  if ($periode == 'Soiree') {
    $request = spip_query("SELECT * FROM spip_programmes_semaines WHERE date = '" . $daty . "' AND ". $horaire ." UNION ALL SELECT * FROM spip_programmes_semaines WHERE date = '" . $daty . "' AND ( horaire >= '01:00:00' AND horaire < '03:00:00' ) ");
  } else {
    $request = spip_query("SELECT * FROM spip_programmes_semaines WHERE date = '" . $daty . "' AND ". $horaire ." ORDER BY horaire ASC ");
  }

  $list_program = '';
  $i = 1;
  while ($row = spip_fetch_array($request)) {
    $description = $row['description'];
    $id_rubrique = NULL;
    $titre = str_replace('&#38;', 'and', str_replace('&#63;', '', texte_backend($row['titre'])));
    $titre_lc = texte_backend(translitteration(strtolower($row['titre'])));
    $titre_lc = str_replace(' ', '', str_replace('&#38;', 'and', str_replace('&#63;', '', $titre_lc))); // replace{\h,'',S} à appliquer

          // Get correspondance dans spip_programmes_rubriques
          $req_rub = spip_query('SELECT * FROM spip_programmes_rubriques WHERE titre_sans_accents LIKE "'.$titre_lc.'" LIMIT 0,1');

          $data_rub = spip_fetch_array($req_rub);

          if (!empty($data_rub)) {
              $id_rubrique = $data_rub['id_rubrique'];
          } else {
              $req_rub = spip_query('SELECT * FROM spip_programmes_rubriques WHERE titre_sans_accents LIKE "%' . $titre_lc . '%" LIMIT 0,1');
              $rub = spip_fetch_array($req_rub);
              $id_rubrique = $rub['id_rubrique'];

          }

    $id_rubrique = ($titre_lc == "50'insidel'actu") ? 4035 : $id_rubrique;
    $list_program .= info_programme($row['horaire'], $titre, $id_rubrique, $i, $description, $periode);
    $i++;
  }
  return $list_program;
}

// Information à propos d'ne rubrique
function info_programme($horaire, $titre, $id_rubrique, $i, $description, $periode) {
  $is_replay = false;
  $url_replay = $url_rubrique = $desc_rubrique = $alt = $file = '';
  if (!is_null($id_rubrique)) {

    $rub_parent = spip_query("SELECT id_rubrique, texte, titre FROM spip_rubriques WHERE id_parent = " . $id_rubrique . " AND titre LIKE '%replay%' AND statut = 'publie' UNION ALL SELECT id_rubrique, texte, titre FROM spip_rubriques WHERE id_rubrique = " . $id_rubrique);

    while ($row_rub = spip_fetch_array($rub_parent)) {
      if ($row_rub['id_rubrique'] == $id_rubrique) {
        $url_rubrique = generer_url_entite($row_rub['id_rubrique'], 'rubrique', "", "", true);
        $desc_rubrique = $row_rub['texte'];
      } else {
        // check if emission has a replay
        $is_replay = true;
        if ($id_rubrique != 4035 && $id_rubrique != 4052){
          $url_replay = generer_url_entite($row_rub['id_rubrique'], 'rubrique', "", "", true);
        }
      }
    }

    // Document rattaché à la rubrique
    $doc = get_rubrique_document($id_rubrique);
  }
  return programme_body($periode, $i, $horaire, $titre, $is_replay, $url_replay, $url_rubrique, $doc, $description, $desc_rubrique, $id_rubrique);
}

// Récupérer document rattaché à la rubrique
function get_rubrique_document($id_rubrique){
  $rub_doc = spip_query("SELECT doc.* FROM spip_documents_liens AS docl 
                        INNER JOIN spip_documents AS doc ON docl.id_document = doc.id_document
                        WHERE docl.id_objet= ".$id_rubrique." 
                        AND docl.objet = 'rubrique' 
                        AND doc.extension IN ('jpg', 'jpeg', 'png') limit 0,1") ;
  while ($row_doc = spip_fetch_array($rub_doc)) {
    $file = str_replace('http://', 'https://', $row_doc['fichier']);
    return array(
      'alt' => $row_doc['titre'],
      'url' => (stristr($file, 'http') === FALSE) ? 'https://cdn.antenne.re/antenne/IMG/'.$file : $file
    );
  }
  return null;
}

function programme_body($periode, $i, $horaire, $titre, $is_replay, $url_replay, $url_rubrique, $doc, $description, $desc_rubrique, $id_rubrique){
    $date = date_create($horaire);
    $horaire = date_format($date,"H:i");
  $programme  = '<div class="panel contenuheure">
                  <a data-toggle="collapse" href="#collapse-'.$periode.$i.'" class="btn-times" id="bouton-'.$periode.$i.'">
                    <span class="time">'.$horaire.'</span>
                    <span class="title">'.ucfirst(strtolower($titre)).'</span>
                  </a>
                  <div id="collapse-'.$periode.$i.'" class="panel-collapse collapse program_inside">
                    <div class="panel-body details"><div class="imageUne">';
  if (!is_null($doc)) {
    $programme .= ($is_replay) ? '<a href="'.$url_replay.'" class="content_lien_replay">' : '';
    $programme .=  '<img alt="'.$doc['alt'].'" src="'.$doc['url'].'" />';
    $programme .= ($is_replay) ? '<span class="icone_play"></span></a>' : '';
  }
  $desc       = (strlen($description) > 10) ? substr($description,0,450).'...' : $desc_rubrique;
  $programme .= '</div><div class="details_txt '.$horaire.'"><p>'.$desc.'</p>';

  if ($id_rubrique == 4111) {
      $url_rubrique = "https://www.boutikantenne.fr/";
  }

    if ($url_rubrique != "") {
        $programme .= '<a href="'.$url_rubrique.'" title="Voir le site">
                        <img src="https://www.antennereunion.fr/squelettes/assets/images/icone_play.jpg"> Voir le site
                      </a>';
    }

    //id rubrique des boutons voir le replay qu'on n'affiche pas
    $tabHideReplay = array("5426", "4043", "4039", "4052");

  //titre rub des boutons voir le replay qu'on n'affiche pas
    $tabHideTitre = array("ASTUCE POUR ENFANTS ", "ASTUCE POUR ENFANTS");
    if (!in_array($id_rubrique, $tabHideReplay) && !in_array($titre, $tabHideTitre)) {
        if ($is_replay) {
            $programme .= ($url_replay == '') ? '' : '<a href="'.$url_replay.'" title="Voir le replay"><img src="https://www.antennereunion.fr/squelettes/assets/images/icone_play.jpg"> Voir le replay</a>';
        }
    }

  $programme .= '</div></div></div></div>';
  return $programme;
}

//champ extras
if (include_spip('inc/cextras_autoriser')) {
  restreindre_extras('article', array('champ_nom', 'champ_prenoms', 'champ_age', 'champ_ville', 'champ_taille', 'champ_info', 'input_facebook', 'input_instagram'), array(5349,5644,6601),'rubrique', true);
  restreindre_extras('article', array('input_prenom', 'input_age', 'input_profession', 'input_ville'), array(5438,5692), 'rubrique', true);
  restreindre_extras('rubrique', 'affiche_live', 5643);
  restreindre_extras('rubrique', 'affiche_candidat', 5644);
  restreindre_extras('rubrique', 'replay_une', 5643);
  restreindre_extras('rubrique', 'replay_avant', 5643);
  restreindre_extras('rubrique', 'affiche_bandeau', 5643);
  restreindre_extras('rubrique', 'affiche_menu_candidate', 5643);
  restreindre_extras('rubrique', 'affiche_candidate_2019', 5644);
  restreindre_extras('rubrique', 'afficher_fb_live', 5643);
  restreindre_extras('rubrique', 'afficher_live_apres_direct', 5643);
  restreindre_extras('rubrique', 'id_mot_gagnante', 5643);
  restreindre_extras('rubrique', 'mettre_en_hp', 5643);
  restreindre_extras('rubrique', 'embed_fb_live', 5643);
  restreindre_extras('rubrique', 'afficher_menu_influenceur', 5643);
  restreindre_extras('rubrique', array('premier_rubrique', 'deuxieme_rubrique', 'troisieme_rubrique', 'quatrieme_rubrique', 'dernier_replay'), 6359, 'rubrique', false);
}

/**
 * Test de l'environnement recette et prod
 * @param $url
 */
function filtre_remote_url($urlFile) {

    $return = $urlFile;
    if (strstr($urlFile, "https") == false || strstr($urlFile, "http") == false) {
        if ($_SERVER["SERVER_NAME"] == "www.antennereunion.fr") {
            $return = "https://cdn.antenne.re/antenne/" . $urlFile;
        } else if ($_SERVER["SERVER_NAME"] == "antenne.ser-rct-01.antennereunion.fr") {
            $return = "/" . $urlFile;
        } else {
            $return = "/" . $urlFile;
        }
    }


    return $return;
}

function filtre_modele_article($titre_rubrique, $type, $id_rubrique=null, $id_article=null, $partenaire=null, $annee=null)
{
    if ($type == 'meta') {
        // Modele meta
        $mapping = array(
            'News' => 'article/meta_news',
            'Vidéos' => 'article/meta_videos',
            'Photos' => 'article/meta_photos',
            'Replay' => 'article/meta_videos',
            'Episodes' => 'article/meta_episodes',
            'Personnages' => 'article/meta_personnages',
            'Casting' => 'article/meta_personnages',
            'Animateur(s)' => 'article/meta_personnages',
            'Candidats' => 'article/meta_personnages',
            'Jury' => 'article/meta_personnages',
            'Agriculteurs' => 'article/meta_personnages',
            'Recettes' => 'article/meta_personnages',
            'POUPETTES' => 'article/meta_personnages'
        );
        return (isset($mapping[$titre_rubrique])) ? $mapping[$titre_rubrique] : '';
    } elseif ( $type == 'css' ) {
        // Modele css
        $mapping = array(
            5332 => 'css/modele_css_missreunion',
            5349 => 'css/modele_css_missreunion',
            5333 => 'css/modele_css_missreunion',
            5351 => 'css/modele_css_missreunion',
            5352 => 'css/modele_css_missreunion',
            5335 => 'css/modele_css_missreunion',
            5334 => 'css/modele_css_missreunion',
            5643 => 'css/modele_css_missreunion_direct',
            5644 => 'css/modele_css_missreunion_direct',
            5645 => 'css/modele_css_missreunion_direct',
            5646 => 'css/modele_css_missreunion_direct',
            5647 => 'css/modele_css_missreunion_direct',
            5647 => 'css/modele_css_missreunion_direct',
            5648 => 'css/modele_css_missreunion_direct',
            5650 => 'css/modele_css_missreunion_direct',
            5651 => 'css/modele_css_missreunion_direct',
            5652 => 'css/modele_css_missreunion_direct',
            5668 => 'css/modele_css_generation_runstar_rubrique',
            6601 => 'css/modele_css_generation_runstar_rubrique',
            5692 => 'css/modele_css_generation_runstar',
            5692 => 'css/modele_css_generation_runstar_rubrique',
            5673 => 'css/modele_css_generation_runstar_rubrique',
            5672 => 'css/modele_css_generation_runstar_rubrique',
            5671 => 'css/modele_css_generation_runstar_rubrique',
            5669 => 'css/modele_css_generation_runstar_rubrique',
            5670 => 'css/modele_css_generation_runstar_rubrique',
            5768 => 'css/modele_css_generation_runstar_rubrique',
            5767 => 'css/modele_css_generation_runstar_rubrique',
            5674 => 'css/modele_css_generation_runstar_rubrique',
            5474 => 'css/modele_css_coupe_du_monde2018',
            5475 => 'css/modele_css_coupe_du_monde2018',
            5477 => 'css/modele_css_coupe_du_monde2018',
            5763 => 'css/modele_css_coupe_du_monde2018',
            5476 => 'css/modele_css_coupe_du_monde2018',
            5438 => 'css/modele_css_generation_runstar',
            5673 => 'css/modele_css_generation_runstar',
            5441 => 'css/modele_css_generation_runstar',
            5668 => 'css/modele_css_generation_runstar_prime',
            5669 => 'css/modele_css_generation_runstar_prime',
            5769 => 'css/modele_css_generation_runstar_prime',
            6714 => 'css/modele_css_coupe_du_monde2018',
        );
        return (isset($mapping[$id_rubrique])) ? $mapping[$id_rubrique] : 'css/modele_css';
    }
    elseif ( $type == 'body' ) {
        // Modele article
        $mapping = array(
            'News' => 'article/article_news',
            'Vidéos' => 'article/article_videos',
            'Photos' => 'article/article_photos',
            'Replay' => 'article/article_videos',
            'Episodes' => 'article/article_episodes',
            'Personnages' => 'article/article_personnages',
            'Casting' => 'article/article_personnages',
            'Animateur(s)' => 'article/article_personnages',
            'Candidats' => 'article/article_personnages',
            'Jury' => 'article/article_personnages',
            'Agriculteurs' => 'article/article_personnages',
            'Météo' => 'article/article_videos',
            'Recettes' => 'article/article_personnages',
            'Kaz Canal' => 'article/article_videos',
            'geoloc' => 'article/article_videos_geolocalisation',
            'Candidates 2016' => 'article/article_candidate_missreunion',
            'Bonus Vidéos' => 'article/article_videos',
            'Bonus photos' => 'article/article_photos'
        );

        if ( !is_null($id_rubrique) && !is_null($id_article) ) {
            // Modele article pour article 732
            $mapping_732 = array(
                5333 => 'article/article_news_mreunion',
                5651 => 'article/article_news_mreunion',
                5669 => 'article/article_news_grs2017',
                333474 => 'article/article_news_generation_runstar',
                336105 => 'article/article_news_elections_regionales',
                345715 => 'article/article_photos',
                694736 => 'article/article_photos',
                695046 => 'article/article_photos',
                695289 => 'article/article_photos',
                5474 => 'article/article_news_cdm2018',
                5670 => 'article/article_videos_grs2017',
                5335 => 'article/article_videos_mreunion',
                5650 => 'article/article_videos_mreunion',
                5475 => 'article/article_videos_cdm2018',
                5935 => 'article/article_videos_instant_de_reve',
                // article miss réunion 2018 5645
                'photos_2018' => 'article/article_photos_missreunion_2018',
                // article miss réunion 5645
                'photos_autre' => 'article/article_photos_missreunion',
                5672 => 'article/article_photos_grs2017',
                5476 => 'article/article_photos_cdm2018',
                5352 => 'article/article_videos_mreunion',
                5647 => 'article/article_videos_mreunion',
                5768 => 'article/article_videos_grs2017',
                5763 => 'article/article_videos_cdm2018',
                5934 => 'article/article_videos_instant_de_reve',
                5438 => 'article/article_candidat_generation_runstar',
                5692 => 'article/article_candidat_generation_runstar2017',
                5671 => 'article/article_personnages_grs2017',
                5767 => 'article/article_personnages_grs2017',
                // POUPETTES partenaires = 1
                'art_partenaire' => 'article/article_partenaire',
                // Canditates miss réunion 2018
                'canditates_2018' => 'article/article_candidate_missreunion_2018',
                // Canditates miss réunion
                'canditates_autres' => 'article/article_candidate_missreunion',
                5487 => 'article/article_personnages_fblive',
                4809 => 'article/article_videos',
                5759 => 'article/article_videos_mreunion',
                5813 => 'article/article_videos',
                5814 => 'article/article_videos',
                5823 => 'article/article_videos',
                5477 => 'article/article_news_cdm2018'
            );

            if ( isset($mapping_732[$id_article]) ) {
                return $mapping_732[$id_article];
            } elseif ( $titre_rubrique == 'Photos' ) {
                if ( $id_rubrique ==  5645 ) {
                    // Article photo MR 2018 ou article photo MR
                    return ( $annee == 2018 ) ? $mapping_732['photos_2018'] : $mapping_732['photos_autre'] ;
                }
            } elseif ( $titre_rubrique == 'Candidates' ){
                // Article candidate MR 2018 ou article candidate PR
                return ( $annee == 2018 ) ? $mapping_732['canditates_2018'] : $mapping_732['canditates_autres'] ;
            } elseif ( $partenaire == 1 ) {
                // Article partenaire
                return $mapping_732['art_partenaire'];
            } elseif ( isset($mapping_732[$id_rubrique]) ) {
                // Article pour les rubriques citées dans le mapping_732
                return $mapping_732[$id_rubrique];
            } else {
                return $mapping[$titre_rubrique];
            }
        }
    }

    // article 742 - 14 - 4
    return (isset($mapping[$titre_rubrique])) ? $mapping[$titre_rubrique] : 'article/article_news';

}

// filtre pour la rubrique-732.html
function filtre_rech_rubrique($ID_RUBRIQUE) {
    $return = "";
    switch($ID_RUBRIQUE)
    {
        case 5333:
        case 5651:
            $return = "programme_n-3/rubrique_news_missreunion";
            break;

        case 5707:
            $return = "programme_n-3/rubrique_news_test_cycliste";
            break;

        case 5669:
            $return = "programme_n-3/rubrique_news_test_grs2017";
            break;

        case 5924:
        case 5945:
            $return = "programme_n-3/rubrique_jeu_play_cmd2018";
            break;

        case 5474:
            $return = "programme_n-3/rubrique_news_cdm2018";
            break;

        case 5923:
            $return = "programme_n-3/rubrique_jeu_cdm2018";
            break;

        case 5477:
            $return = "programme_n-3/rubrique_news_test_cdm2018";
            break;

        case 5334:
        case 5645:
            $return = "programme_n-3/rubrique_photos_missreunion";
            break;

        case 5672:
            $return = "programme_n-3/rubrique_photos_grs2017";
            break;

        case 5476:
            $return = "programme_n-3/rubrique_photos_cdm2018";
            break;

        case 5352:
        case 5647:
            $return = "programme_n-3/rubrique_replays_missreunion";
            break;

        case 5335:
        case 5650:
            $return = "programme_n-3/rubrique_videos_missreunion";
            break;

        case 4809:
            $return = "programme_n-3/rubrique_videos_test";
            break;

        case 5670:
            $return = "programme_n-3/rubrique_videos_test_grs2017_bonus";
            break;

        case 5704:
            $return = "programme_n-3/rubrique_videos_test_cycliste";
            break;

        case 5768:
            $return = "programme_n-3/rubrique_videos_test_grs2017";
            break;

        case 5475:
        case 5763:
            $return = "programme_n-3/rubrique_videos_test_cdm2018";
            break;

        case 5671:
        case 5767:
            $return = "programme_n-3/rubrique_episodes_test_grs2017";
            break;

        case 5438:
            $return = "programme_n-3/rubrique_candidats_grs";
            break;

        case 5692:
            $return = "programme_n-3/rubrique_candidats_grs2017";
            break;

        case 6601:
            $return = "programme_n-3/rubrique_candidats_miss_ecologie_2020";
            break;

        case 5177:
            $return = "programme_n-3/rubrique_candidats_kohlanta";
            break;

        case 5705:
            $return = "programme_n-3/rubrique_episodes_test_cycliste";
            break;

        case 5769:
            $return = "programme_n-3/rubrique_jeu_generation_runstar2017";
            break;

        case 4996:
            $return = "tourcycliste/rubrique_accueil_test_tc";
            break;

        case 4973:
            $return = "runstar/rubrique_runstar_endirect";
            break;

        case 5332:
            $return = "programme_n-3/rubrique_accueil_missreunion_chat";
            break;

        case 5643:
            $return = "programme_n-3/rubrique_accueil_miss_reunion_2019";
            break;

        case 5434:
            $return = "programme_n-3/rubrique_accueil_grunstar";
            break;

        case 5668:
            $return = "responsive/runstar/rubrique_generation_runstar_normal_V1";
            break;

        case 5473:
            $return = "programme_n-3/rubrique_accueil_coupe_du_monde";
            break;

        case 5022:
            $return = "runstar/rubrique_runstar_continue";
            break;

        case 5295:
            $return = "programme_n-3/rubrique_news_test_euro";
            break;

        case 5351:
            $return = "programme_n-3/rubrique_partenaires_missreunion";
            break;

        case 5652:
            $return = "programme_n-3/rubrique_partenaires_missreunion_2018";
            break;

        case 5349:
            $return = "programme_n-3/rubrique_candidates_missreunion";
            break;

        case 5644:
            $return = "programme_n-3/rubrique_candidates_missreunion_2019";
            break;

        case 5673:
            $return = "programme_n-3/rubrique_partenaires_generation_runstar2017";
            break;

        case 5709:
            $return = "programme_n-3/rubrique_simples_cycliste";
            break;

        case 5815:
            $return = "programme_n-3/rubrique_simples_avent";
            break;

        case 5930:
            $return = "programme_n-3/rubrique_inscription_missreunion";
            break;

        case 5973:
            $return = "programme_n-3/rubrique_relooking";
            break;

        case 6051:
            $return = "programme_n-3/rubrique_castings_test";
            break;

        case 5851:
            $return = "programme_n-3/rubrique_accueil_missreunion_Dimanche_fblive_apres_apres";
            break;

        case 6281 :
            $return = "programme_n-3/rubrique_accueil_influenceur_mr2019";
            break;

        case 6289 :
            $return = "programme_n-3/rubrique_accueil_test";
            break;

        case 6382 :
            $return = "programme_n-3/rubrique-pronostic-jssp";
            break;

        case 6383 :
            $return = "programme_n-3/rubrique-quiz-jssp";
            break;
		
        case 6403 :
            $return = "programme_n-3/rubrique_vote_fnt";
            break;

        case 6489 :
            $return = "programme_n-3/rubrique_casting_miss_2020";
            break;

        case 6490 :
            $return = "responsive/concours_eloquence/rubrique_concours_elequonce";
            break;

        case 6491 :
            $return = "responsive/concours_eloquence/rubrique_replay_concours_elequonce";
            break;

        case 6492 :
            $return = "responsive/concours_eloquence/rubrique_news_concours_elequonce";
            break;

        case 6493 :
            $return = "responsive/concours_eloquence/rubrique_videos_concours_elequonce";
            break;

        case 6494 :
            $return = "responsive/concours_eloquence/rubrique_candidats_concours_elequonce";
            break;

        case 6503 :
            $return = "programme_n-3/inscription_tous_en_cuisine";
            break;

        case 6603 :
            $return = "programme_n-3/rubrique_formulaire_mondelez";
            break;

        case 6683 :
            $return = "programme_n-3/rubrique_inscription_tous_ala_maison";
            break;

        case 6714:
            $return = "programme_n-3/rubrique_matchs_euro_2020";
            break;

        case 6749:
            $return = "programme_n-3/rubrique_videos_test";
            break;

        case 6750:
            $return = "programme_n-3/rubrique_videos_test";
            break;

        case 6751:
            $return = "programme_n-3/rubrique_videos_test";
            break;

        case 6847 :
            $return = "programme_n-3/rubrique_vote_metisson";
            break;

        case 6316 :
            $return = "programme_n-3/rubrique_accueil_tec";
            break;

        default:
            $return = "programme_n-3/rubrique_simples";
    }

    return $return;
}

function filtre_diff_date($date, $diff=null) {
    if (is_null($diff))
        return date('Y-m-d H:i:s', strtotime($date. ' - 8 days'));
    return date('Y-m-d H:i:s', strtotime($date. ' - '.$diff.' days'));
}

function filtre_set_rss_alerte($site_url){
    $champs = array('spip_articles.*');
    $where = array(
        "spip_articles.statut = 'publie'",
        "spip_mots_liens.id_mot = 3220",
        "spip_mots_liens.objet = 'article'", 
        "( (DATE_FORMAT(date_redac, '%Y-%m-%d') = CURRENT_DATE()) OR ( DATE_FORMAT(date_redac, '%Y-%m-%d') = '0000-00-00' AND DATE_FORMAT(date, '%Y-%m-%d') = CURRENT_DATE() ) )",
    );
    $req = sql_select('spip_articles.*',"spip_articles INNER JOIN spip_mots_liens ON spip_articles.id_article = spip_mots_liens.id_objet", $where, '', 'spip_articles.date DESC', '0,1');

    $rss = "<item>";
    while ($row = sql_fetch($req)) {
        $date = new DateTime($row['date']);
        //$link_article = generer_url_entite($row['id_article'], 'article', "", "", true);
        $link_article = (trim($row['ps']) != '') ? $row['ps'] : '';
        $rss .= '<title>'.str_replace('&nbsp;', ' ', $row['soustitre']).'</title>';
        $rss .= '<link>'.$site_url.'</link>';
        $rss .= '<pubDate>'.$date->format(DateTime::RFC822).'</pubDate>';
        $rss .= '<description>';
        $rss .= header_rss($row['titre']);
        $rss .= image_article_rss($row['id_article'], $site_url, $link_article);
        $rss .= chapo_article_rss($row['chapo']);
        $rss .= footer_rss($link_article);
        $rss .= '</description>';
    }
    $rss .= "</item>";
    return $rss;
}

function header_rss($titre){
  $rss_header .= '<![CDATA[';
  $rss_header .= '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td><a target="_blank" href="http://www.antennereunion.fr/"><img style="display:block;" width="600" height="72" alt="logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/header-ar-nl-alerte.jpg"></a></td>
                    </tr>
                  </table>
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td height="15">  </td>
                    </tr>
                  </table>
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td style="font-family:Maven Pro,Arial,Helvetica,sans-serif; font-size:16px; height:20px; text-align:left; color:#1f4563; text-transform:uppercase;font-weight: bold">'.$titre.'</td>
                    </tr>
                  </table>
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td height="15">  </td>
                    </tr>
                  </table>';
  $rss_header .= ']]>';
  return $rss_header;
}

function image_article_rss($id_article, $site_url, $link_article){
  $champs = array('spip_documents.fichier');
  $where = array(
    "spip_documents_liens.id_objet = ".$id_article,
    "spip_documents_liens.objet = 'article'",
    "spip_documents.extension IN ('jpg','jpeg','png')"
  );
  $req = sql_select($champs,"spip_documents INNER JOIN spip_documents_liens ON spip_documents.id_document = spip_documents_liens.id_document", $where, '', 'spip_documents.date DESC', '0,1'); 

  $rss_image = '';
  while ($row = sql_fetch($req)) {
    $link_image = (strstr($row['fichier'], 'http')) ? $row['fichier'] : 'http://www.antennereunion.fr/IMG/'.$row['fichier'];
    $rss_image .= '<![CDATA[';
    $rss_image .= '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                      <tr>
                        <td>
                          <a target="_blank" href="'.$link_article.'">
                            <img style="display:block;" alt="" src="'.$link_image.'" width="600" > 
                          </a>
                        </td>
                      </tr>
                    </table>';
    $rss_image .= ']]>';
  }
  return $rss_image;
}

function chapo_article_rss($chapo){
  $rss_chapo .= '<![CDATA[';
  $rss_chapo .= '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td height="15">  </td>
                    </tr>
                  </table>
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td id="webversion" style="font-family:Maven Pro,Arial,Helvetica,sans-serif; font-size:17px; text-align:justify; color:#1f4563; line-height : 150%">'.$chapo.'</td>
                    </tr>
                  </table>
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td height="15">  </td>
                    </tr>
                  </table>';
  $rss_chapo .= ']]>';
  return $rss_chapo;
}

function footer_rss($link_article){
  $rss_footer .= '<![CDATA[';
  $rss_footer .= '<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td>
                        <a target="_blank" href="'.$link_article.'">
                          <img style="display:block; visibility:hidden;" width="600" height="1" alt="superchef" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/trait.jpg">
                        </a>
                      </td>
                    </tr>
                  </table>
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td height="10">   </td>
                      <td height="10">   </td>
                    </tr>
                    <tr>
                      <td>
                        <img style="display:block;" width="327" height="28" alt="#" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/blanc.jpg">
                      </td>
                      <td>
                        <a target="_blank" href="'.$link_article.'">
                          <img style="display:block;" width="253" height="28" alt="" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/btn-decouvrez.jpg">
                        </a>
                      </td>
                    </tr>
                  </table>
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td height="5">  </td>
                    </tr>
                  </table>';
  $rss_footer .= ']]>';
  return $rss_footer;
}

function filtre_rubrique_jt($date){
    $day = date('D');
    $hour = date('H');
    if ( ($day == 'Sat' || $day == 'Sun') || ($day == 'Mon' && $hour < 14)) {
        return '6268';
    } else {
        return ($hour > 13 && $hour < 20) ? '4604' : '4606';
    }
}

/* Filtre utilisé par l'affichage des tabs sur la HP de Miss Réunion 2019 
  @id_rubrique : une table d'ID_RUBRIQUE à la quelle on veut récupérer des articles
  @limit : nombre d'article à récupérer
*/
function filtre_tabs_article($date,$site_url,$id_rubrique,$limit,$id_mot)
{
  $result = "";
    $champs = array('DISTINCT (spip_articles.id_article), spip_articles.titre, spip_articles.chapo, spip_articles.id_rubrique, spip_documents.titre AS doc_titre, spip_documents.fichier, spip_documents.extension, spip_articles.date');
    $where = array(
          "spip_articles.id_rubrique IN ( ".implode(",", $id_rubrique).") ",
          "spip_articles.statut = 'publie' ",
          "spip_articles.titre != 'Nouvel article' ",
          "spip_documents_liens.objet = 'article' ",
          "spip_documents.extension IN ('png','jpg','gif','jpeg') ",
          "spip_mots_liens.objet = 'article' ",
          "spip_mots_liens.id_mot = ".$id_mot, 
          "spip_articles.id_rubrique NOT IN (5644,5652,6326,6217,5930)"
      );
    $req = sql_select($champs,
      "spip_articles 
       INNER JOIN spip_mots_liens ON spip_articles.id_article = spip_mots_liens.id_objet 
       INNER JOIN spip_documents_liens ON spip_articles.id_article = spip_documents_liens.id_objet 
       INNER JOIN spip_documents ON spip_documents_liens.id_document = spip_documents.id_document", 
      $where, 
      " spip_articles.id_article", 
      " spip_articles.date DESC", 
      "0,".$limit
    );
  $res = sql_fetch_all($req);
  foreach ($res as $row) {
    $article = "<li class='clearfix item'><div class='content-box'><div class='thumb-wrapper format-video'>";
    $lien_article = $site_url.'/'.generer_url_entite($row['id_article'], 'article', "", "", true);
    $article .= "<a href='".$lien_article."' class='fond_noir' style='text-align: inherit;'>";
    if (strpos($row['fichier'], 'http') !== false) {
      $image = $row['fichier'];
    } else {
    $image = (substr($row['fichier'], 0, 4) == '/IMG') ? $row['fichier'] : '/IMG/'.$row['fichier'];
    }
    //$image = (substr($row['fichier'], 0, 4) == '/IMG') ? $row['fichier'] : '/IMG/'.$row['fichier'];
    //$image = (strstr($image, 'http')) ? $image : $site_url.$image;
    $article .= "<img src='".$image."' alt='".$row['doc_titre']."' width='311' height='207' class='img-responsive'/>";
    $titre = substr(strip_tags($row['titre']), 0, 100);
    $article .= "</a></div><div class='text-holder'><h2 class='topic-title'>";
    $article .= "<a href='".$lien_article."'>".$titre."</a></h2>";
    $article .= "<div class='txt-extract'>";
    $article .= "<a href='".$lien_article."'>".substr(strip_tags($row['chapo']), 0, 120)."</a></div>";
    $article .= "<a href='".$lien_article."' class='readnext'>EN SAVOIR +</a>";
    $article .= '</div></div></li>';
    $result .= $article;
  }

  // Get article
  return $result;
}

function filtre_news_hp($url_site_spip, $sondage = true){
    $render = '';
    $req = sql_query("
        SELECT * FROM (
            SELECT sa.*, sr.titre AS titre_rubrique, sr.id_parent, sp.titre AS titre_parent FROM spip_articles AS sa 
            INNER JOIN spip_rubriques AS sr ON sa.id_rubrique = sr.id_rubrique
            INNER JOIN spip_rubriques AS sp ON sr.id_parent = sp.id_rubrique
            WHERE sa.titre != 'Nouvel article' AND sa.statut = 'publie' AND sa.date <= NOW()
            AND sa.id_rubrique IN (
                SELECT DISTINCT id_rubrique
                FROM spip_rubriques
                WHERE titre IN ('News', 'Photos', 'Vidéos') AND id_rubrique != 5295 
                AND id_secteur IN (14, 732, 742) AND statut = 'publie'
            )
            ORDER BY sa.date DESC
            LIMIT 0,500
        ) AS t
        GROUP BY id_rubrique
        ORDER BY date desc
        LIMIT 0,6
    ");
    $i = 1;
    while ($row = sql_fetch($req)) {
        // Récupération document image parent
        $req_img_parent =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $row['id_parent'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%rubrique%'");
        while ($d_parent = sql_fetch($req_img_parent)) {
            $img_article = $d_parent['fichier'];
        }
        if ($sondage == 'true') {
          $class_name = ($i == 3 || $i == 5) ? 'item-4' : '';
        } else {
          $class_name = ($i == 4 || $i == 7) ? 'item-4' : '';
        }
        $format = ($row['titre_rubrique'] == 'Vidéos') ? 'format-video' : '';
        $url_article = generer_url_entite($row['id_article'], 'article', "", "", true);
        $url_rubrique = generer_url_entite($row['id_parent'], 'rubrique', "", "", true);

        $render .= '<li class="' .$class_name. '">';
        $render .= '<div class="format-list '. $format . '">';
        $render .= '<div class="post-fig" style="width: 310px;height: 174px">';
        $render .= '<a href="'.$url_article.'" class="fond_noir">';

        if ($row['titre_rubrique'] == 'Vidéos') {
            $render .= '<span class="icon-video post-format"></span><span style="opacity: 0;" class="roll ">&nbsp;</span>';
        } elseif ($row['titre_rubrique'] == 'News') {
            $render .= '<span class="roll " style="opacity: 0;">&nbsp;</span>';
        }
        // Recupération image article
        $req_img_article =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $row['id_article'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%article%'");
        while ($d_article = sql_fetch($req_img_article)) {
            $img_article = $d_article['fichier'];
        }
        $img_article = (strpos($img_article, 'http') === false) ? 'IMG/' . $img_article : $img_article;


        $render .= '<img src="'.$img_article.'" alt="'.$row['titre'].'"/>';
        $render .= '</a>';
        $render .= '<div class="tile-title"> 
                        <div><a href="'.$url_rubrique.'">'.$row['titre_parent'].'</a></div>
                    </div>';
        $render .= '</div>';
        $render .= '<div class="post-title">
                        <h3 class="title-post"><a href="'.$url_article.'">'.strip_tags($row['titre']).'</a></h3>
                    </div>';
        $render .= '</div>';
        $render .= '</li>';
        $i++;
    }
    return $render;
}

function filtre_replay_hp($url_site_spip){
    $render = '';
    $ID_ARTICLES = '';
    $req = sql_query("
        SELECT t1.*, sr.titre AS titre_parent
        FROM (
          SELECT sa.*, sr.id_parent
          FROM spip_articles AS sa
          INNER JOIN spip_rubriques AS sr ON sa.id_rubrique = sr.id_rubrique
          WHERE sa.statut = 'publie' AND sa.date <= NOW()
          AND sa.id_rubrique IN (
            SELECT id_rubrique
            FROM spip_rubriques
            WHERE titre = 'Replay' AND id_secteur IN (14, 732, 742)
            AND id_rubrique NOT IN (4606, 4604) AND statut = 'publie'
          )
          AND DATE_FORMAT(sa.date, '%Y-%m-%d') > DATE_SUB(CURRENT_DATE, INTERVAL 8 DAY)
          ORDER BY sa.popularite DESC
          LIMIT 0,2
        ) AS t1
        LEFT JOIN spip_rubriques sr on sr.id_rubrique = t1.id_parent
    ");

    while ($row = sql_fetch($req)) {
        // Récupération document image parent
        $req_img_parent =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $row['id_parent'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%rubrique%'");
        while ($d_parent = sql_fetch($req_img_parent)) {
            $img_article = $d_parent['fichier'];
        }
        $url_article = generer_url_entite($row['id_article'], 'article', "", "", true);
        $url_rubrique = generer_url_entite($row['id_parent'], 'rubrique', "", "", true);

        $render .= '<li>';
        $render .= '<div class="format-list format-video">';
        $render .= '<div class="post-fig">';
        $render .= '<a href="'.$url_article.'" class="fond_noir">';
        $render .= '<span style="opacity: 0;" class="roll ">&nbsp;</span>';

        // Recupération image article
        $req_img_article =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $row['id_article'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%article%'");
        while ($d_article = sql_fetch($req_img_article)) {
            $img_article = $d_article['fichier'];
        }

        if (strpos($img_article, 'http') !== false) {
          $img_article = $img_article;
        } else {
          $img_article = (substr($img_article, 0, 4) == '/IMG') ? $img_article : '/IMG/'.$img_article;
        }

        $render .= '<img src="'.$img_article.'" alt="'.$row['titre'].'"/>';
        $render .= '<span class="icon-video post-format"></span>';
        $render .= '</a>';
        $render .= '<div class="tile-title"><div><a href="'.$url_rubrique.'">'.$row['titre_parent'].'</a></div></div>';
        $render .= '</div>';
        $render .= '<div class="post-texts">
                      <h3 class="title-post"><a href="'.$url_article.'">'.$row['titre'].'</a></h3>
                    </div>';
        $render .= '</div>';
        $render .= '</li>';  
        $ID_ARTICLES .= $row['id_article'] . ',';
    }

    $ID_ARTICLES = trim($ID_ARTICLES, ',');

    // bloc météo
    $render .= '<li>
                  <div class="format-list format-video">
                      <div class="post-fig">
                          <a href="http://www.antennereunion.fr/info-et-magazines/meteo" class="fond_noir">
                              <span style="opacity: 0;" class="roll ">&nbsp;</span>
                              <img alt="Météo de la Réunion" src="squelettes/assets/vignettes/Vignette-meteo-V2.jpg">
                              <span class="icon-video post-format"></span>
                          </a>
                          <div class="tile-title"><div><a href="http://www.antennereunion.fr/info-et-magazines/meteo">Météo de la Réunion</a></div></div>
                      </div>
                      <div class="post-texts">
                          <h3 class="title-post"><a href="http://www.antennereunion.fr/info-et-magazines/meteo">Le bulletin météo en vidéo</a></h3>
                      </div>
                  </div>
              </li>';

    $req_2 = sql_query("
      SELECT sa.*, sr.titre AS titre_rubrique, sr.id_parent, sp.titre AS titre_parent
      FROM spip_articles AS sa 
      INNER JOIN spip_rubriques AS sr ON sa.id_rubrique = sr.id_rubrique
      INNER JOIN spip_rubriques AS sp ON sr.id_parent = sp.id_rubrique
      WHERE sa.titre != 'Nouvel article' AND sa.statut = 'publie' 
      AND sa.date <= NOW() 
      AND sa.id_rubrique IN ( 
          SELECT DISTINCT id_rubrique FROM spip_rubriques WHERE titre = 'Replay' AND id_secteur IN (14,732,742) AND id_rubrique NOT IN (4606, 4604) AND statut = 'publie' 
      ) AND id_article NOT IN (".$ID_ARTICLES.")
      ORDER BY sa.date DESC LIMIT 0,3
    ");
    $j = 1;
    while ($row = sql_fetch($req_2)) {
      // Récupération document image parent
        $req_img_parent =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $row['id_parent'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%rubrique%'");
        while ($d_parent = sql_fetch($req_img_parent)) {
            $img_article = $d_parent['fichier'];
        }
        $url_article = generer_url_entite($row['id_article'], 'article', "", "", true);
        $url_rubrique = generer_url_entite($row['id_parent'], 'rubrique', "", "", true);
        $item_class = '';
        if ($j == 1) {
          $item_class = "item-4";
        }

        $render .= '<li class="'.$item_class .'">';
        $render .= '<div class="format-list format-video">';
        $render .= '<div class="post-fig">';
        $render .= '<a href="'.$url_article.'" class="fond_noir">';
        $render .= '<span style="opacity: 0;" class="roll ">&nbsp;</span>';

        // Recupération image article
        $req_img_article =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $row['id_article'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%article%'");
        while ($d_article = sql_fetch($req_img_article)) {
            $img_article = $d_article['fichier'];
        }

        if (strpos($img_article, 'http') !== false) {
          $img_article = $img_article;
        } else {
          $img_article = (substr($img_article, 0, 4) == '/IMG') ? $img_article : '/IMG/'.$img_article;
        }

        $render .= '<img src="'.$img_article.'" alt="'.$row['titre'].'"/>';
        $render .= '<span class="icon-video post-format"></span>';
        $render .= '</a>';
        $render .= '<div class="tile-title"><div><a href="'.$url_rubrique.'">'.$row['titre_parent'].'</a></div></div>';
        $render .= '</div>';
        $render .= '<div class="post-texts">
                      <h3 class="title-post"><a href="'.$url_article.'">'.$row['titre'].'</a></h3>
                    </div>';
        $render .= '</div>';
        $render .= '</li>';
        $j++;
    }
    return $render;
}

function filtre_autres_articles($id_article, $id_rubrique){
  $render = $img_article = '';

  if ($id_article) {
    $req = sql_query("SELECT sa.id_article, sa.titre, sr.titre AS rub, sr.id_parent
      FROM spip_articles AS sa
      INNER JOIN spip_rubriques AS sr ON sa.id_rubrique = sr.id_rubrique
      WHERE sa.date <= NOW() AND (sa.date_redac LIKE '%0000-00-00%' OR sa.date_redac > NOW()) 
      AND sa.id_article != $id_article AND sa.id_rubrique = $id_rubrique AND sa.statut = 'publie'
      ORDER BY sa.date DESC 
      LIMIT 0,3"
    );

    $i = 1;

    while ($row = sql_fetch($req)) {
      $url_article = generer_url_entite($row['id_article'], 'article', '', '', true);
      if ($i == 1) {
        $req_img_parent =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $row['id_parent'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%rubrique%'");
        while ($d_parent = sql_fetch($req_img_parent)) {
            $img_article = $d_parent['fichier'];
        }
      }


      // Recupération image article
      $req_img =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $row['id_article'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%article%'");
      while ($img = sql_fetch($req_img)) {
        $img_article = $img['fichier'];
      }

      if (strpos($img_article, 'http') !== false) {
        $img_article = $img_article;
      } else {
        $img_article = (substr($img_article, 0, 4) == '/IMG') ? $img_article : '/IMG/'.$img_article;
        $img_article = $url_site . $img_article;
      }

      $render .= '<li>';
      if (in_array($row['rub'], array('Vidéos', 'Replay'))) {
        $render .= '<div class="thumbimage format-video">';
        $render .= '<a href="'.$url_article.'" class="fond_noir">';
        $render .= '<span class="roll">&nbsp;</span>';
        $render .= '<img alt="'.$row['titre'].'" src="'.$img_article.'" width="206" height="115" >';
        $render .= '<span class="icon-video post-format"></span>';
        $render .= '</a>';
        $render .= '</div>';
      } else {
        $render .= '<div class="thumbimage">';
        $render .= '<a href="'.$url_article.'" class="fond_noir">';
        $render .= '<span class="roll">&nbsp;</span>';
        $render .= '<img alt="'.$row['titre'].'" src="'.$img_article.'" width="206" height="115" >';
        $render .= '</a>';
        $render .= '</div>';
      }

      $render .= '<h3 class="cat-title"><a href="'.$url_article.'">'.$row['titre'].'</a></h3>';
      $render .= '</li>';
      $i++;
    }
  }
  return $render;
}

function filtre_flux_json($url_site, $id_secteur, $all=null){
  $render = array();
  $Query = "SELECT sa.id_article, sa.titre, sa.date, sa.chapo, sp.titre AS parent, sp.id_parent, sd.fichier AS video
    FROM spip_articles AS sa
    INNER JOIN spip_mots_liens AS sml ON sa.id_article = sml.id_objet
    INNER JOIN spip_rubriques AS sr ON sa.id_rubrique = sr.id_rubrique
    INNER JOIN spip_rubriques AS sp ON sr.id_parent = sp.id_rubrique
    INNER JOIN spip_documents_liens AS sdl ON sa.id_article = sdl.id_objet
    INNER JOIN spip_documents AS sd ON sdl.id_document = sd.id_document
    WHERE sml.objet = 'article' AND sml.id_mot = 1772 
    AND sd.extension IN ('ts', 'mp4', 'm3u8')
    AND sa.statut = 'publie' AND sa.titre != 'Nouvel article'
    AND sa.date <= NOW() ";
  if (!is_null($all)) {
    $Query .= " AND sa.id_secteur IN (14,732,742) AND sa.id_rubrique NOT IN (4604,4606,4830,4829) ";
  } elseif ($id_secteur == 14) {
    $Query .= " AND sa.id_secteur = 14 AND sa.id_rubrique != 5873 ";
  } elseif ($id_secteur == 742) {
    $Query .= " AND sa.id_secteur = 742 AND sa.id_rubrique NOT IN (4606,4604,6268) ";
  } else {
    return '';
  }
  $Query .= " ORDER BY sa.date DESC LIMIT 0,50";

  $fQuery = "SELECT t1.*, d.fichier FROM ($Query) AS t1
    LEFT JOIN (spip_documents AS d, spip_documents_liens AS dl) ON dl.id_objet = t1.id_article 
    AND d.id_document = dl.id_document 
    WHERE dl.objet LIKE '%article%' AND d.extension IN ('png', 'jpg', 'gif', 'jpeg') ";

  $req = sql_query($fQuery);
  while ($row = sql_fetch($req)) {
    $thumbnail = $row['fichier'];

    if (trim($thumbnail) !== '') {
      // récupération image parent
      $req_img_parent =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $row['id_parent'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%rubrique%' ORDER BY date DESC LIMIT 0,1");
      while ($d_parent = sql_fetch($req_img_parent)) {
        $thumbnail = $d_parent['fichier'];
      }
    }

    if (strpos($thumbnail, 'http') !== false) {
      $thumbnail = $thumbnail;
    } else {
      $thumbnail = (substr($thumbnail, 0, 4) == '/IMG') ? $thumbnail : '/IMG/'.$thumbnail;
      $thumbnail = $url_site . $thumbnail;
    }

    $_date = date_create($row['date']);
    $render[] = array(
      "id" => $row['id_article'],
      "type" => "video",
      "subtype" => "custom",
      "title" => $row['titre'],
      "url" => $url_site . '/' . generer_url_entite($row['id_article'], 'article', '', '', true),
      "categories" => [],
      "date" => $_date->format('c'),
      "author" => "",
      "nbComments" => 0,
      "content" => $row['chapo'],
      "summary" => $row['chapo'],
      "smallThumbnail" => $thumbnail,
      "thumbnail" => $thumbnail,
      "largeThumbnail" => $thumbnail,
      "videoUrls" => array(
        "mp4" => $row['video']
      )
    );
  }
  return json_encode($render);
}

function filtre_page_replay($url_site, $id_secteur){
  $render = '';
  $Query = "SELECT t1.*, sp.titre AS titre_parent FROM (
    SELECT sa.id_article, sa.id_rubrique, sa.id_secteur, sa.titre, sa.date, sa.date_redac, sr.id_parent, sd.fichier
    FROM spip_articles AS sa
    INNER JOIN spip_rubriques AS sr ON sa.id_rubrique = sr.id_rubrique
    LEFT JOIN (spip_documents AS sd, spip_documents_liens AS sdl) ON sdl.id_objet = sa.id_article AND sd.id_document = sdl.id_document
    WHERE sa.statut = 'publie' AND sa.date < NOW() AND sa.titre != 'Nouvel article' AND sa.id_secteur = $id_secteur
    AND sr.titre LIKE '%Replay%' AND sd.extension IN ('png','jpg','gif') ";
  if ($id_secteur == 14) {
    $Query .= " AND (date_redac LIKE '%0000-00-00%' OR date_redac >= NOW()) ";
  }
  $Query .= " GROUP BY sa.id_article
    ORDER BY sa.date DESC
    LIMIT 0,1000
  ) AS t1
  LEFT JOIN spip_rubriques AS sp ON t1.id_parent = sp.id_rubrique
  GROUP BY t1.id_rubrique
  ORDER BY t1.date DESC";
  $data = sql_fetch_all(sql_query($Query));
  $array_emissions = array(47,46,5426,4042,3625,1243,1032,5573,4035,4807);
  $date_jour = date('Y-m-d H:i:s');
  $count = 0;
  foreach ($data as $key => $row) {
    if (in_array($row['id_parent'], $array_emissions) && $date_jour > $row['date_redac'] ) {
      continue;
    } else if ($row['id_parent'] == 5563 && $date_jour > $row['date_redac']){
      continue;
    }
    $count++;

    $render .= render_replay($row, $url_site);
    if ($count % 3 == 0) {
      $render .= '<li class="item_separator"></li>';
    } 
    if ($count == 9) {
      break;
    }
  }
  return $render;
}

function render_replay($data, $url_site){
  $html = '';
  $thumbnail = image_article($data, $url_site);
  $url_article = generer_url_entite($data['id_article'], 'article', '', '', true);
  $html .= '<li><div class="panel-item">';
  $html .= '<div class="wrapper-image format-video">';
  $html .= '<a href="'.$url_article.'" class="fond_noir">';
  $html .= '<span style="opacity: 0;" class="roll ">&nbsp;</span>';
  $html .= '<img src="'.$thumbnail.'" alt="'.$data['titre'].'"/>';
  $html .= '<span class="icon-video post-format"></span>';
  $html .= '</a>';
  $html .= '<h3 class="item-title"><a href="'.$url_article.'">'.$data['titre_parent'].'</a></h3>';
  $html .= '</div>';
  $html .= '<div class="title-panel3"><a href="'.$url_article.'">'.$data['titre'].'</a></div>';
  $html .= '</div></li>';
  return $html;
}

function image_article($data, $url_site){
  $thumbnail = $data['fichier'];
  if (trim($thumbnail) === '') {
    $req_img_parent =  sql_query("SELECT d.* FROM spip_documents AS d, spip_documents_liens AS dl WHERE dl.id_objet = " . $data['id_parent'] . " AND d.id_document = dl.id_document AND d.extension IN ('png','jpg','gif', 'jpeg') AND dl.objet LIKE '%rubrique%' ORDER BY date DESC LIMIT 0,1");
    while ($d_parent = sql_fetch($req_img_parent)) {
      $thumbnail = $d_parent['fichier'];
    }
  }

  if (strpos($thumbnail, 'http') !== false) {
    $thumbnail = $thumbnail;
  } else {
    $thumbnail = (substr($thumbnail, 0, 4) == '/IMG') ? $thumbnail : '/IMG/'.$thumbnail;
    $thumbnail = $url_site . $thumbnail;
  }
  return $thumbnail;
}

function filtre_replay_programme($url_site_spip, $id_secteur, $pagination = 0, $start = 0){
  $array_parent = array(47,5498,46,5426,33325,3725,5418,4042,3625,1243,1032,5573,5178,4035,4807,5563);
  $date_jour = date('Y-m-d H:i:s');
  $render = '';
  $start = ($start) ? $start : 0;
  if ($start == 0) {
    $render .= '<ul class="clearfix list_replay">';
  }
  $count = 0;
  $Query = "SELECT t1.*, sp.titre AS titre_parent
    FROM (
    SELECT sa.id_rubrique, sa.date, sa.date_redac, sa.id_article, sa.titre, sd.fichier, sr.id_parent
    FROM spip_articles AS sa
    INNER JOIN spip_rubriques AS sr ON sa.id_rubrique = sr.id_rubrique
    INNER JOIN `antenne_art_spip`.spip_mots_liens AS sml ON ( sml.id_objet = sa.id_article AND sml.objet='article')
    LEFT JOIN (spip_documents AS sd, spip_documents_liens AS sdl) ON sdl.id_objet = sa.id_article AND sd.id_document = sdl.id_document 
    WHERE sml.id_mot = 1772 AND sd.extension IN ('png','jpg','gif')
    AND sa.statut = 'publie' AND sa.date < NOW() AND sa.titre != 'Nouvel article' AND sa.id_secteur = $id_secteur
    AND sa.id_rubrique NOT IN (47,5498,46,5426,33325,3725,5418,4042,3625,5573,5178,4035,4807,5563,5642)
    GROUP BY sa.id_article
    ORDER BY sa.date DESC
    LIMIT $start,40) AS t1
    LEFT JOIN spip_rubriques AS sp ON t1.id_parent = sp.id_rubrique";
  $req = sql_query($Query);
  while ($row = sql_fetch($req)) {
    if (in_array($row['id_parent'], $array_parent) && $date_jour > $row['date_redac'] ) {
      continue;
    }
    $count++;

    $thumbnail = image_article($row, $url_site);;
    $render .= render_replay($row, $url_site);
    if ($count % 3 == 0) {
      $render .= '<li class="item_separator"></li>';
    }
    if ($count == 21) {
      break;
    }
  }
  if ($start == 0) {
    $render .= '</ul>';
  }
  if ($pagination == 0) {
    $render .= '<section class="section-pagination clearfix">
      <span class="prev disabled">
        <a href="#" class="lien_pagination prev_button" rel="prev">page précédente</a><span class="sep"> | </span>
      </span>
      <span class="pages">
        <a href="#" class="lien_pagination on" rel="nofollow" id="numpage-1" data-num="1">1</a><span class="sep"> | </span>
        <a href="#" class="lien_pagination" rel="nofollow" id="numpage-2" data-num="2">2</a><span class="sep"> | </span>
        <a href="#" class="lien_pagination" rel="nofollow" id="numpage-3" data-num="3">3</a><span class="sep"> | </span>
        <a href="#" class="lien_pagination" rel="nofollow" id="numpage-4" data-num="4">4</a>
      </span>
      <span class="next"><span class="sep"> | </span>
        <a href="#" class="lien_pagination next_button" rel="next">page suivante</a>
      </span>
    </section>';
  }
  return $render;
}

// ajouter jours
function ajouter_jours($date, $jours) {
  return date('Y-m-d H:i:s', strtotime($date)+$jours*86400);
}


function antenne_fichier_cache($cle) {
  return _DIR_RACINE._NOM_TEMPORAIRES_INACCESSIBLES.'cache/antenne/'.basename(filter_var($cle));
}

function antenne_lire_cache($cle) {
  $fichier = antenne_fichier_cache($cle);
  if (is_file($fichier)) {
    $data = unserialize(file_get_contents($fichier));
    if ($data['expires'] >= time()) {
      unlink($fichier);
      return false;
    } else {
      return $data['data'];
    }
  } else {
    return false;
  }
}

function antenne_ecrire_cache($cle, $data, $ttl = 300) {
  $fichier = antenne_fichier_cache($cle);
  if (!is_dir(dirname($fichier))) {
    mkdir(dirname($fichier));
  }
  file_put_contents($fichier, serialize(['data' => $data, 'expires' => time()+$ttl]));
  return true;
}

include_once "mes_fonctions_menu_header.php";