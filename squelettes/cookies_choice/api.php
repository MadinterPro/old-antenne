<?php
require_once '../Mobile-Detect-2.8.5/Mobile_Detect.php';
session_start();
if(isset($_SESSION['Antennereunion_cookiechoice']) && $_SESSION['Antennereunion_cookiechoice'] === "1" ){//Efa nipoitra le popup ts apoitra tson
    $time = time() + 365 * 24 * 3600;
    setcookie('Antennereunion_cookiechoice',1,$time);
    echo json_encode(array("return" => 2));
}elseif(isset($_COOKIE['Antennereunion_cookiechoice']) && $_COOKIE['Antennereunion_cookiechoice'] === 1 ){
    echo json_encode(array("return" => 2));
}else{
    $detect = new Mobile_Detect;
    $mobile = 0;
    if($detect->isMobile() || $detect->isTablet())
        $mobile = 1;
    $_SESSION['Antennereunion_cookiechoice'] = 1;
    echo json_encode(array("return" => 1, "mobile" => $mobile));
}

