$(window).load(function() {
    $(".flexslider").flexslider({
        animation: "slide",
        slideshow: false,
        start: function(slider) {
            slider.removeClass('loading');
        }  
    });
});