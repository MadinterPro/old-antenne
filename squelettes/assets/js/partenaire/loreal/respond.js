 /*diff --git a/drush/respondjs.drush.inc b/drush/respondjs.drush.inc
index 5dc2b61..9acda52 100644
--- a/drush/respondjs.drush.inc
+++ b/drush/respondjs.drush.inc
@@ -6,11 +6,6 @@
  */
 
 /**
- * The URI to the respond.js library.
- */
-define('RESPONDJS_DOWNLOAD_URI', 'https://raw.githubusercontent.com/scottjehl/Respond/master/dest/respond.min.js');
-
-/**
  * Implementation of hook_drush_command().
  *
  * In this hook, you specify which commands your
@@ -71,14 +66,13 @@ function respondjs_drush_respondjs_download() {
   if (isset($args[0])) {
     $path = $args[0];
   }
-  elseif (function_exists('libraries_get_path')) {
-    $path = libraries_get_path('respondjs');
-    if ($path === FALSE) {
-      $path = 'sites/all/libraries/respondjs';
-    }
-  }
   else {
-    $path = drupal_get_path('module', 'respondjs') . '/lib';
+    if (function_exists('libraries_get_path') && libraries_get_path('respondjs')) {
+      $path = libraries_get_path('respondjs');
+    }
+    else {
+      $path = RESPONDJS_DOWNLOAD_LOCATION;
+    }
   }
 
   // Create the path if it does not exist yet.
diff --git a/respondjs.install b/respondjs.install
index 47188f2..071d9ff 100644
--- a/respondjs.install
+++ b/respondjs.install
@@ -31,8 +31,8 @@ function respondjs_requirements($phase) {
       $requirements['respondjs']['value'] = t('Respond.js is not correctly using Libraries API');
       $requirements['respondjs']['severity'] = REQUIREMENT_WARNING;
       $requirements['respondjs']['description'] = t('Please install !respondjs in <b>@libraries</b>. The module is using its included copy at <b>@default</b>.', array(
-        '!respondjs' => l('respond.min.js', 'https://raw.github.com/scottjehl/Respond/master/respond.min.js'),
-        '@libraries' => 'sites/all/libraries/respondjs',
+        '!respondjs' => l('respond.min.js', RESPONDJS_DOWNLOAD_URI),
+        '@libraries' => RESPONDJS_DOWNLOAD_LOCATION,
         '@default' => drupal_get_path('module','respondjs') . '/lib')
       );
     }
@@ -41,7 +41,10 @@ function respondjs_requirements($phase) {
     if (!file_exists($library_path)) {
       $requirements['respondjs']['value'] = t('Respond.js is not correctly installed');
       $requirements['respondjs']['severity'] = REQUIREMENT_ERROR;
-      $requirements['respondjs']['description'] = t('Please install <a href="https://raw.github.com/scottjehl/Respond/master/respond.min.js">Respond.js</a> in the repsondjs folder under /lib');
+      $requirements['respondjs']['description'] = t('Please install !respondjs in <strong>@libraries</strong>', array(
+        '!respondjs' => l('Respond.js', RESPONDJS_DOWNLOAD_URI),
+        '@libraries' => RESPONDJS_DOWNLOAD_LOCATION,
+      ));
     }
   }
   return $requirements;
diff --git a/respondjs.module b/respondjs.module
index 6f2ca21..852a55c 100644
--- a/respondjs.module
+++ b/respondjs.module
@@ -5,6 +5,8 @@
  * Main file for the Respond.js module
  */
 
+define('RESPONDJS_DOWNLOAD_URI', 'https://raw.githubusercontent.com/scottjehl/Respond/master/dest/respond.min.js');
+define('RESPONDJS_DOWNLOAD_LOCATION', 'sites/all/libraries/respondjs');
 define('RESPONDJS_SCOPE_DEFAULT','header');
 define('RESPONDJS_QUIET_DEFAULT',NULL);
 