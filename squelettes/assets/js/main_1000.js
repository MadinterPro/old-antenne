//(function(window, $) {
//
//})(window, jQuery);

$(document).ready(function() {
    $("#mainheader").sticky({topSpacing: 0});

    $().UItoTop();
    $("#toTop").click(function() {
        $("html, body").animate({scrollTop: 0}, 100);
        return false;
    });
    
    /*Recherche*/
    affiche = false;
    $('.bleu-alt-btn').on('click', function(e) {
        $('.search-form').css('display', 'block');
        $('.search-expand').animate({"width": "315px"},"slow");
        $('.bleu-alt-btn').hide();

        $(".input-search").focus();
        affiche = true;
    });

    $('.search-form').clickoutside(function() {
        if (affiche !== true) {
            $('.search-expand').animate({"width": "0px"},"slow");
            $(".input-search").css("background-color", "hsla(0, 0%, 100%, 0.24)");
            $('.bleu-alt-btn').show();
        }
        affiche = false;
    });

    $('.button-search').click(function() {
        search = $("input.input-search").val();
        console.log(search)
        if (search === "") {
            $(".input-search").css("background-color", "#f5a9a9");
        }
        else{
            $(".search-form").submit();
        }
    });
    /*Recherche*/
});

(function(jQuery) {
    jQuery.fn.clickoutside = function(callback) {
        var outside = 1, self = $(this);
        self.cb = callback;
        this.click(function() {
            outside = 0;
        });
        $(document).click(function() {
            outside && self.cb();
            outside = 1;
        });
        return $(this);
    }
})(jQuery);
