function getLocationOld() {
    var out = false;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            let lat = position.coords.latitude;
            let lng = position.coords.longitude;

            let url = 'squelettes/maxmind/api_geoloc_gps.php?lat='+lat+'&lng='+lng+'&'+Math.random();
            var response = $.ajax({
                type: "GET",
                url: url,
                async: false
            }).responseText;

            if (response === 'RE') {
                setCookie('country', response, 90);
                out = true;
            } else {
                out = false;
            }
        });
    }
    return out;
}

function getCountry(position) {
    let lat = position.coords.latitude;
    let lng = position.coords.longitude;

    let url = 'squelettes/maxmind/api_geoloc_gps.php?lat='+lat+'&lng='+lng+'&'+Math.random();
    $.get(url, function(response) {
        if (response === 'RE') {
            setCookie('country', response, 90);
            $("#loader_video").css("display", "block");
            $("#video_not_authorized").css("display", "none");
            return true;
        } else {
            $("#loader_video").css("display", "none");
            $("#video_not_authorized").css("display", "block");
            return false;
        }
    });
}

function setCookie(cname, cvalue, days) {
    var d = new Date();
    d.setTime(d.getTime()+(days*24*60*60*1000));
    var expires = 'expires='+d.toGMTString();
    document.cookie = cname+'='+cvalue+';'+expires+';path=/';
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return false;
}