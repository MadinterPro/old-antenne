function devmodal(e) {
    var o = {top: 25, overlay: 0.45, closeButton: ".modalnav"};
    var overlay = $("<div id='lean_overlay'></div>");
    $("body").append(overlay);
    var modal_id = "#newsletter_modal";
    $("#lean_overlay").click(function() {
        close_modal(modal_id);
    });
    $(o.closeButton).click(function() {
        close_modal(modal_id);
    });
    var modal_height = $(modal_id).outerHeight();
    var modal_width = $(modal_id).outerWidth();
    $("#lean_overlay").css({"display": "block", opacity: 0});
    $("#lean_overlay").fadeTo(200, o.overlay);
    $(modal_id).css({"display": "block", "position": "fixed", "opacity": 0, "z-index": 11000, "left": 50 + "%", "margin-left": -(modal_width / 2) + "px", "top": o.top + "%"});
    $(modal_id).fadeTo(200, 1);
    e.preventDefault();
}

function close_modal(modal_id) {
    $("#lean_overlay").fadeOut(200);
    $(modal_id).css({"display": "none"});
}

window.onload = function()
{
    $('.nl, .nl-ar').click(function(e) {
        devmodal(e);
    });

    /*
     $("#mail_user").keyup(function() {
     $("#ajax_submit_newsletter").attr("disabled", "");
     if (validateEmail($("#mail_user").val()))
     {
     $("#ajax_submit_newsletter").removeAttr("disabled");
     }
     });
     */

    $("#ajax_submit_newsletter").click(function(e) {
        e.preventDefault();

        if (!validateEmail($("#mail_user").val())) {
            var i = 0;
            myVar = setInterval(function() {
                //console.log("i=" + i + ";i%2=" + i % 2);
                if (i++ < 6) {
                    if (i % 2 !== 0)
                        $("#mail_user").css("box-shadow", "0 0 5px red");
                    else
                        $("#mail_user").css("box-shadow", "0 0 5px #9dc7ea");
                }
                else
                {
                    $("#mail_user").css("box-shadow", "0 0 5px #9dc7ea");
                    clearInterval(myVar);
                }
            }, 100);
            return;
        }

        $.post("squelettes/ajax/newsletter.php", {
            mail_user: $("#mail_user").val(),
            statut: $("#optin").val()
        }, function(data) {
            console.log(data);
            $("#succes").css("display", "block");
            $(".scan").css("display", "none");
        }, "json");
    });

    $(".color-m, .color-m2").hover(function() {
        $(this).next().addClass("hover-m");
        $(this).addClass("hover-this");

    }, function() {
        $(this).next().removeClass("hover-m");
        $(this).removeClass("hover-this");
    });

    $(".color-m").hover(function() {
        $(this).children(".categoriesN1").show();
        $(this).children(".menu-n1").addClass("hover-show");
        $(".nav_haut li:first-child").addClass("sousm-actif");

    }, function() {
        $(this).children(".categoriesN1").hide();
        $(this).children(".menu-n1").removeClass("hover-show");
        $(".nav_haut li").removeClass("sousm-actif");
    });


    $(".tabs_menu").hover(function() {
        $(".tabs_menu").removeClass("sousm-actif");
        $(this).addClass("sousm-actif");

    });


    /* Picto News */
    $(".roll").css("opacity", "0");

    // ON MOUSE OVER
    $(".roll").hover(
            function() {

                // SET OPACITY TO 70%
                $(this).prev('.ico_video').css({display: 'none'});
                $(this).css("opacity", "1");
                $(this).next('img').addClass("hover-opacity");

            },
            // ON MOUSE OUT
                    function() {

                        // SET OPACITY BACK TO 50%
                        $(this).prev('.ico_video').css({display: 'block'});
                        $(this).css("opacity", "0");
                        $(this).next('img').removeClass("hover-opacity");

                    });
            /* Fin Picto News */

            if (typeof redirect404 === 'function') {
                redirect404();
            }
        };

function validateEmail(email)
{
    var re = /^[a-zA-Z0-9\-_]+[a-zA-Z0-9\.\-_]*@[a-zA-Z0-9\-_]+\.[a-zA-Z\.\-_]{1,}[a-zA-Z\-_]+/;
    return re.test(email);
}

/*Faire en sorte que le popup newsletter apparaîsse une fois par jour*/
$(function() {
    var cookienl = "cookienl";
    console.log("cookienl = " + readCookie(cookienl));
    if (readCookie(cookienl) !== "oui") {
        createCookie(cookienl, "oui", 1);
        //devmodal();  //à décommenter pour apparaître le popup
        console.log("create popup newsletter");
    }
    else
    {
        console.log("do not show popup newsletter");
    }
});

/*fonctions utiles pour les cookies*/
function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else
        expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
