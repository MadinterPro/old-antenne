function showDate(nbjours, div) {
    var today = new Date();
    var mdate = today.getDate();
    var mday = today.getDay();
    var affdate = 0;
    var mois = ["Janvier", "F&eacute;vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao&ucirc;t", "Septembre", "Octobre", "Novembre", "D&eacute;cembre"][today.getMonth()];
    if (today.getMonth() == 0)
        var lastmois = "D&eacute;cembre";
    else
        lastmois = ["Janvier", "F&eacute;vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao&ucirc;t", "Septembre", "Octobre", "Novembre", "D&eacute;cembre"][today.getMonth() - 1];
    if (today.getMonth() == 11)
        var nextmois = "Janvier";
    else
        nextmois = ["F&eacute;vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao&ucirc;t", "Septembre", "Octobre", "Novembre", "D&eacute;cembre"][today.getMonth()];
    if (mday == 0)
        var jour = "Samedi";
    else
        var jour = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"][mday - 1];

    var datefin = new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate();
    var datefin_prec_mois = new Date(today.getFullYear(), today.getMonth(), 0).getDate();

    for (var i = 1; i <= nbjours; i++) {
        if (mdate == 1 && i == 1) {
            var affmois = lastmois;
            affdate = datefin_prec_mois;
        } else if (mdate == 1 && i > 1) {
            affmois = nextmois;
            affdate = 1;
            mois = nextmois;
            mdate++;
        } else {
            affmois = mois
            affdate = parseInt(mdate - 1);
        }
        var appendnav = '<li class="item-jours"><div class="emissionInfo"><a rel="' + i + '" href="#"><span class="dateEmission">' + jour + '</span>' +
                '<span class="datejour">' + affdate + '</span><span class="mmois"> ' + affmois + '</span></a>' +
                '</div></li>';

        $(div + ' .navJours').append(appendnav);
        jour = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"][mday];
        mday++;
        if (mdate > datefin)
            mdate = 1;
        else
            mdate++;

        if (mday == 7)
            mday = 0;
    }
}

function callHeight() {
    $('.table-container-semaine').css('min-height', '');
    $('.table-container-anime .programme-tv').css({'height': '', 'max-height': ''});
    $('.programme-tv ul .item-jours').css('height', '');
    var max = -1;
    $('.programme-tv ul .item-jours').each(function() {
        var h = $(this).height();
        max = h > max ? h : max;
    });
    var maximum = max + 150;
    $('.table-container-semaine').css('min-height', maximum + 'px');
    $('.table-container-anime .programme-tv').css({'height': max + 'px', 'max-height': max + 'px'});
    $('.programme-tv ul .item-jours').css('height', max + 'px');
}

$(function() {
    showDate(7, '#grille_soirees');
    $('#grille_soirees .navJours .item-jours:eq(2)').addClass('active');

    /** Affichages des dates et jours en haut de la grille TV **/
    var nbjours = $('#table_grilles_programmes .table-container-anime .navJours .item-jours').length;
    showDate(nbjours, '#grille_programmes');

    $('#grille_programmes .item-jours:eq(1)').addClass('active');
    var ssgrille = $('#grille_programmes').closest('.grille_programmes').next();
    $(ssgrille).find('ul:eq(0)>li:eq(1)').addClass('active');
    $('#table_grilles_programmes .item-jours').click(function(event) {
        event.preventDefault();
    });

    /** Pour le filtrage et l'affichage du programme de la semaine **/
    $('#grille_programmes').next().find('.navJours .item-jours').each(function() {
        $(this).find('.nav-heure ul .item-heure').each(function() {
            var horaire = $.trim($(this).find('span:first').text());
            var heure = horaire.split(':')[0];
            var minute = horaire.split(':')[1];
            if (parseInt(heure + minute) < 1230 && parseInt(heure) >= 3)
                $(this).attr('rel', 'matin');
            else if (parseInt(heure + minute) >= 1230 && parseInt(heure) < 19)
                $(this).attr('rel', 'apm');
            else if (parseInt(heure) == 1){
                $(this).attr('rel', 'soir');
                $(this).appendTo($(this).closest('ul'));
            }
            else if (parseInt(heure) >= 19){
                $(this).attr('rel', 'soir');
                if(parseInt(heure)==24){
                    $(this).appendTo($(this).closest('ul'));
                    var heurepro=$(this).find('span:first').text().replace('24','00');
                    $(this).find('span:first').text(heurepro);
                }
            }
        });
    });

    /** Filtre suivant la selection (matin�e, Apr�s-mid, Soir, Nuit) **/
    $('#grille_programmes').next().find('.navJours .item-jours .nav-heure ul .item-heure').css('display', 'none');
    $('#grille_programmes').next().find('.navJours .item-jours .nav-heure ul .item-heure[rel="matin"]').css('display', 'block');
    $('.table-container-semaine .titre_programmes .bloc-nav li').click(function(event) {
        event.preventDefault();
        var temps = $(this).find('a').attr('rel');
        $(this).parent().find('a').removeClass('active');
        $(this).find('a').addClass('active');
        $(this).closest('.table-container-semaine').find('.table-container-anime .navJours .item-jours .nav-heure ul .item-heure').css('display', 'none');
        $(this).closest('.table-container-semaine').find('.table-container-anime .navJours .item-jours .nav-heure ul .item-heure[rel="' + temps + '"]').css('display', 'block');
    });
});

$(document).ready(function() {
    //Slider
    $('#camera_wrap_1').camera();

    setTimeout(function(){
       callHeight(); 
    },1000);

    $('.table-container-semaine .titre_programmes .bloc-nav li').click(function(event) {
        callHeight();
    });

    $('.table-container .item-jours .emissionInfo a').click(function(event) {
        event.preventDefault();
        var diff = $(this).closest('.navJours').find('li.active a').attr('rel') - $(this).attr('rel');
        var mrel = $(this).attr('rel');
        diff *= 670;
        var left = parseInt($('.sliding-window-top').css('left').replace('px', ''));
        $('.navJours').find('li').removeClass('active');
        $('.item-jours').each(function(index) {
            if ($(this).find('a').attr('rel') == mrel)
                $(this).addClass('active');
        });
        $(this).closest('.item-jours').addClass('active');
        $('.table-container-anime .programme-tv .navJours').find('.item-jours:eq(' + (mrel - 1) + ')').addClass('active');
        $(this).closest('.main-inner').find('.table-container-soiree .sliding-window-top').animate({left: left + diff + 'px'}, 'fast');
    });

    $('.nav-heure ul .item-heure').click(function() {
        window.open($(this).find('a').attr('href'),"_self");
    });



});