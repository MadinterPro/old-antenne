<?php

function create_csv_string($data) {

//    $link= mysql_connect("localhost", "spipAR", "antv3_2009");
//    mysql_select_db("art_spip");
    
    $user= 'antenne_spipAR';
    $pass='antv3_2009';
    $dbh = new PDO('mysql:host=localhost;dbname=antenne_art_spip', $user, $pass);

//    $data = mysql_query('SELECT Identification_de_la_demande,Numero_carte_fidelite,Nom,Prenom,Date_naissance,Telephone,Adresse_mail,Magasin_concurrent,Prix_constate,Date_constat,Codebarre_produit,Magasin_ELeclerc,Prix_KILEMOINSCHER,Date_KILEMOINSCHER,Date_heurre_demande,Fichier_obligatoire,Fichier_facultative,Adresse_ip FROM demande_avec');
//    $data2 = mysql_query('SELECT id_dmd,numero,Nom,Prenom,Date_naissance,Telephone,Adresse_mail,enseigne,prix_client,date_const,code,magasin,prix_appli,date_appli,date_dmd,fichier,fichier_fac,ip FROM demande_N');
//    
     
    $data1 = 'SELECT Identification_de_la_demande,Numero_carte_fidelite,Nom,Prenom,Date_naissance,Telephone,Adresse_mail,Magasin_concurrent,Prix_constate,Date_constat,Codebarre_produit,Magasin_ELeclerc,Prix_KILEMOINSCHER,Date_KILEMOINSCHER,Date_heurre_demande,Fichier_obligatoire,Fichier_facultative,Adresse_ip FROM demande_avec';
    $stmt1 = $dbh->query($data1); 
    
    
    $data2 = 'SELECT id_dmd,numero,Nom,Prenom,Date_naissance,Telephone,Adresse_mail,enseigne,prix_client,date_const,code,magasin,prix_appli,date_appli,date_dmd,fichier,fichier_fac,ip FROM demande_N';
    $stmt2 = $dbh->query($data2); 

// Open temp file pointer
    if (!$fp = fopen('php://temp', 'w+'))
        return FALSE;

    fputcsv($fp, array());

    $line0 = array('Identification de la demande', 'Numero carte de fidelite', 'Nom', 'Prenom', 'Date de naissance',
        'Telephone', 'Adresse mail', 'Magasin concurrent', 'Prix constate', 'Date du constat', 'Code barre du produit', 'Magasin E.Leclerc de comparaison',
        'Prix dans KILEMOINSCHER', 'Date dans KILEMOINSCHER', 'Date et heurre de la demande', 'Fichier obligatoire', 'Piece facultative', 'Adresse IP');
    fputcsv($fp, $line0,';');

    // Loop data and write to file pointer
//    while ($line = mysql_fetch_assoc($data))
            while ($line = $stmt1->fetch(PDO::FETCH_ASSOC))
        fputcsv($fp, $line,';');

    $blank = array();
    fputcsv($fp, $blank);
    fputcsv($fp, $blank);
    fputcsv($fp, $blank);


    $line1 = array('Identification de la demande', 'Numero demande de la carte de fidelite', 'Nom', 'Prenom', 'Date de naissance',
        'Telephone', 'Adresse mail', 'Magasin concurrent', 'Prix constate', 'Date du constat', 'Code barre du produit', 'Magasin E.Leclerc de comparaison',
        'Prix dans KILEMOINSCHER', 'Date dans KILEMOINSCHER', 'Date et heurre de la demande', 'Fichier obligatoire', 'Piece facultative', 'Adresse IP');
    fputcsv($fp, $line1, ';');

//    while ($line2 = mysql_fetch_assoc($data2))
            while ($line2 = $stmt2->fetch(PDO::FETCH_ASSOC))
    // {
    //$line2b= array($line2[0],'',$line2[1]);
    //}
        fputcsv($fp, $line2, ';');
    // Place stream pointer at beginning
    rewind($fp);

    // Return the data
    return stream_get_contents($fp);
//     mysql_close($link);
    
}

function send_csv_mail($csvData, $body, $to = 'johary.rakoto@antennereunion.fr,klmc@e-leclerc.re,lgrondin@e-leclerc.re,julien.jeanson@antennereunion.fr', $subject = 'Rapport sur les demandes de remboursement', $from = 'Pacte Pouvoir d\'Achat Antenne Reunion et Leclerc <No-reply>\n') {

    // This will provide plenty adequate entropy
    $multipartSep = '-----' . md5(time()) . '-----';

    // Arrays are much more readable
    $headers = array(
        "From: $from",
        "Reply-To: $from",
        "Content-Type: multipart/mixed; boundary=\"$multipartSep\""
    );
//    //$headers = "MIME-Version: 1.0\n";
//    $headers = "From: \"Pacte Pouvoir d’Achat Antenne Réunion et Leclerc\" <No-reply>\n";
//    $headers.= "Content-type: multipart/mixed;\n";

    // Make the attachment
    $attachment = chunk_split(base64_encode(create_csv_string($csvData)));


    // Make the body of the message
    $body = "--$multipartSep\r\n"
            . "Content-Type: text/plain; charset=ISO-8859-1; format=flowed\r\n"
            . "Content-Transfer-Encoding: 7bit\r\n"
            . "\r\n"
            . "$body\r\n"
            . "--$multipartSep\r\n"
            . "Content-Type: text/csv\r\n"
            . "Content-Transfer-Encoding: base64\r\n"
            . "Content-Disposition: attachment; filename=\"Rapport_Demandes_Leclerc_du-" . date("j-m-Y") . ".csv\"\r\n"
            . "\r\n"
            . "$attachment\r\n"
            . "--$multipartSep--";

    // Send the email, return the result
    return @mail($to, $subject, $body, implode("\r\n", $headers));
}

$array = array(array(1, 2, 3, 4, 5, 6, 7), array(1, 2, 3, 4, 5, 6, 7), array(1, 2, 3, 4, 5, 6, 7));

send_csv_mail($array, "Merci de trouver en piece jointe le rapport des demandes de remboursement.");

