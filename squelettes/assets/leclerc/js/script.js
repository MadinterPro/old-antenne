/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready(function ($) {
    $('input[type="radio"]').click(function () {
        if ($(this).attr("value") == "red") {
            $(".red ").css('display', 'block');
            $(".green").css('display', 'none');
        }
        if ($(this).attr("value") == "green") {
//            $(".green").css('display', 'block');
//            $(".red ").css('display', 'none');                                       
            var cid = $("#cid").text();
            cid = parseInt(cid);
            cid = cid+1;
            $("#cid").text(cid);
            $.MessageBox({
                buttonDone: "ok",
                buttonFail: "Retour",
                message: "<p>Si vous le souhaitez vous pouvez créer une carte de fidélité. Elle sera automatiquement créditée de votre remboursement </p>\n\
<p><a href='http://www.e-leclerc.re/index.php/external/carte_fidelite/?partner=antenne_reunion&cid=" + cid + " ' target='_blank'>cliquer ici </a></p> ",
            }).done(function () {
                $(".green").css('display', 'block');
                $(".red ").css('display', 'none');
//                var href = "http://www.e-leclerc.re/index.php/external/carte_fidelite/?partner=antenne_reunion&cid=" + cid + "";
//                $("#action_blank").attr("href", href).attr("target", "_blank");
//                document.getElementById("action_blank").click();
            }).fail(function () {
                $(".red ").css('display', 'block');
                $(".green").css('display', 'none');
                $('input:radio[value=green]').prop('checked', false);
                $('input:radio[value=red]').prop('checked', true);
            });


//            $.MessageBox("<p>Merci de vous créer une carte de fidélité. Elle sera automatiquement créditée de votre remboursement ?</p>\n\
//<p><a href='#'>cliquer ici</a></p>  ");
        }
        var th = $(this), name = th.attr('name');
        if (th.is(':checked')) {
            $(':checkbox[name="' + name + '"]').not($(this)).prop('checked', false);
        }
    });
});



jQuery(document).ready(function (jq) {
    jq(function () {
        jq("#date_appli").datepicker({
            //dateFormat: 'mm/dd/yy',
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
           yearRange: '-100y:c+nn',
//            maxDate: '-1d'
        });
        jq("#date_naiss").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-100y:c+nn',
//            maxDate: '-1d'

            onSelect: function (selected, evnt) {
                majeur(selected);
            }
        });
        function majeur(value) {
            jq('#input').val(value);
            var age = getAge();
            if (age < 18)
            {
                //jq.MessageBox("Le remboursement est ouvert au + de 18 ans ");
                jq.MessageBox({
                    buttonDone: "ok",
                    message: "Le remboursement est ouvert au plus de 18 ans"
                }).done(function () {
                    //location.reload();
                    jq("#date_naiss").val('');

                });

            }

            function getAge() {
                var today = new Date(),
                        birthday = jq('#date_naiss').datepicker("getDate"),
                        //birthday = value,
                        age = (
                                (today.getMonth() > birthday.getMonth())
                                ||
                                (
                                        today.getMonth() == birthday.getMonth() &&
                                        today.getDate() >= birthday.getDate())
                                ) ? today.getFullYear() - birthday.getFullYear() : today.getFullYear() - birthday.getFullYear() - 1;

                //alert("Age: " + age);
                return age;
            }
        }
        ;

        jq("#date_const").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-100y:c+nn',
//            maxDate: '-1d'
            onSelect: function (selected, evnt) {
                maxi(selected);
            }
        });

        function maxi(jr) {
            //jq('#input').val(jr);
            var jour = getdays();
            if (jour > 10)
            {
//                jq.MessageBox("Désolé, vous devez effectuer votre demande de remboursement dans les 10 jours après le constat");
//                location.reload();
                jq.MessageBox({
                    buttonDone: "ok",
                    message: "Désolé, vous devez effectuer votre demande de remboursement dans les 10 jours après le constat",
                }).done(function () {                   
                    jq("#date_const").val('');
                });
            }

            function getdays() {
                var today = new Date(),
                        a = jq("#date_const").datepicker('getDate').getTime(),
                        b = today.getTime(),
                        c = 24 * 60 * 60 * 1000,
                        diffDays = Math.round(Math.abs((a - b) / (c)));
                console.log(diffDays); //show difference
                return diffDays;
            }
        }
        ;
    });
});




$().ready(function () {
    // validate the comment form when it is submitted
    //jq("#commentForm").validate();

    jQuery.validator.addMethod("mynumber", function (value, element) {
        //return this.optional(element) || /^\s*?([\d\,]+(\.\d{1,2})?|\.\d{1,2})\s*$/.test(value);
        return this.optional(element) || /^\s*?([\d\.]+(\.\d{1,2})?)\s*$/.test(value);
    }, "Veuillez saisir uniquement des chiffres");
    
   $.validator.addMethod("enseigne", function(value, element, arg){
    return arg != value;
   }, "Veuillez selectionner le magasin concurrent où le prix a été constaté");

    $.validator.addMethod("magasin", function(value, element, arg){
    return arg != value;
   }, "Veuillez selectionner le magasin E.Leclerc de comparaison");

    // validate signup form on keyup and submit
    $("#oui_form").validate({
        rules: {
            nom: {
                required: true,
                minlength: 2
            },
            numero: {
                required: true,
                number: true,
                minlength: 13,
                maxlength: 13
            },
            prenom: {
                required: true,
                minlength: 2
            },
            date_naiss: {
                required: true
            },
            tel: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
            },
            mail: {
                required: true,
                email: true
            },
            enseigne: {
                enseigne: "0"            
            },
            prix_cli: {
                required: true,
                mynumber: true
            },
            date_const: {
                required: true
            },
            code: {
                required: true,
                number: true,
                minlength: 13,
                maxlength: 13
            },
            magasin: {
                magasin: "0"               
            },
            prix_appli: {
                required: true,
                mynumber: true
            },
            date_appli: {
                required: true
            },
            file: {
                required: true
            }
        },
        messages: {
            numero: {
                required: "Veuillez entrer le numéro de la carte",
                number: "Votre numéro de carte de fidélité doit comporter uniquement des chiffres",
                minlength: "Votre numéro de carte de fidélité doit comporter 13 chiffres",
                maxlength: "Votre numéro de carte de fidélité doit comporter 13 chiffres"
            },
            nom: {
                required: "Veuillez entrer votre nom",
                minlength: "minimum 2"
            },
            prenom: {
                required: "Veuillez entrer votre prénom",
                minlength: "minimum 2"
            },
            date_naiss: {
                required: "Veuillez entrer la date de votre naissance"               
            },
            tel: {
                required: "Veuillez entrer votre numéro de téléphone",
                number: "Merci d’entrer un numéro de téléphone ",
                minlength: "Merci d’entrer un numéro de téléphone ",
                maxlength: "Merci d’entrer un numéro de téléphone "
            },
            mail: {
                required: "Veuillez entrer votre adresse mail",
                email: " L'adresse mail est invalide"
            },
            enseigne: {
                required: "Veuillez selectionner magasin concurrent où le prix a été constaté"                
            },
            prix_cli: {
                required: "Veuillez entrer le prix constaté chez le concurrent",
                mynumber: "Veuillez saisir uniquement des chiffres \n et un point (.) pour les décimales"
            },
            date_const: {
                required: "Veuillez entrer la date de constatation du prix"               
            },
            code: {
                required: "Veuillez entrer le code barre du produit",
                number: "Le format du code barre est incorrect ",
                minlength: "Le format du code barre est incorrect ",
                maxlength: "Le format du code barre est incorrect "
            },
            magasin: {
                required: "Veuillez selectionner magasin concurrent où le prix a été constaté"                
            },
            prix_appli: {
                required: "Veuillez entrer le prix affiché dans KILEMOINSCHER",
                mynumber: "Veuillez saisir uniquement des chiffres \n et un point (.) pour les décimales"
            },
            date_appli: {
                required: "Veuillez entrer la date affichée dans KILEMOINSCHER"               
            },
            file: {
                required: "Veuillez charger"               
            },
            
        }
    });

});

//si NON

jQuery(document).ready(function (jq) {
    jq(function () {
        jq("#date_appliN").datepicker({
            //dateFormat: 'mm/dd/yy',
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-100y:c+nn',
//            maxDate: '-1d'
        });

        jq("#date_constN").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-100y:c+nn',
//            maxDate: '-1d'
            onSelect: function (selected, evnt) {
                maxi(selected);
            }
        });

        function maxi(jr) {
            //jq('#input').val(jr);
            var jour = getdays();
            if (jour > 10)
            {
//                jq.MessageBox("Désolé, vous devez effectuer votre demande de remboursement dans les 10 jours après le constat");
//                location.reload();
                jq.MessageBox({
                    buttonDone: "ok",
                    message: "Désolé, vous devez effectuer votre demande de remboursement dans les 10 jours après le constat"
                }).done(function () {
                    jq("#date_constN").val('');
                });
            }

            function getdays() {
                var today = new Date(),
                        a = jq("#date_constN").datepicker('getDate').getTime(),
                        b = today.getTime(),
                        c = 24 * 60 * 60 * 1000,
                        diffDays = Math.round(Math.abs((a - b) / (c)));
                console.log(diffDays); //show difference
                return diffDays;
            }
        }
        ;
    });
});

$().ready(function () {
    // validate the comment form when it is submitted
    //jq("#commentForm").validate();

    // validate signup form on keyup and submit
    $("#non_form").validate({
        rules: {
            enseigneN: {
                enseigne: "0"            
            },
            prix_cliN: {
                required: true,
                mynumber: true
            },
            date_constN: {
                required: true
            },
            codeN: {
                required: true,
                number: true,
                minlength: 13,
                maxlength: 13
            },
            magasinN: {
                magasin: "0"               
            },
            prix_appliN: {
                required: true,
                mynumber: true
            },
             date_appliN: {
                required: true
            },
             fileN: {
                required: true
            }
        },
        messages: {
           enseigneN: {
                required: "Veuillez selectionner magasin concurrent où le prix a été constaté"                
            },
            prix_cliN: {
                required: "Veuillez entrer le prix constaté chez le concurrent",
                mynumber: "Veuillez saisir uniquement des chiffres \n et un point (.) pour les décimales"
            },
            date_constN: {
                required: "Veuillez entrer la date de constatation du prix"               
            },
            codeN: {
                required: "Veuillez entrer le code barre du produit",
                number: "Le format du code barre est incorrect",
                minlength: "Le format du code barre est incorrect",
                maxlength: "Le format du code barre est incorrect"
            },
            magasinN: {
                required: "Veuillez selectionner magasin concurrent où le prix a été constaté"                
            },
            prix_appliN: {
                required: "Veuillez entrer le prix affiché dans KILEMOINSCHER",
                mynumber: "Veuillez saisir uniquement des chiffres \n et un point (.) pour les décimales"
            },
            date_appliN: {
                required: "Veuillez entrer la date afficher dans KILEMOINSCHER"               
            },
            fileN: {
                required: "Veuillez charger"               
            }
        }
    });

});


// show name of file OUI
 function myFunction(){
    var x = document.getElementById("file");
    var txt = "";
    if ('files' in x) {
        if (x.files.length == 0) {
            txt = "Veuillez charger la pièce obligatoire";
        } else {
            for (var i = 0; i < x.files.length; i++) {
                //txt += "<br><strong>" + (i+1) + ". file</strong><br>";
                var file = x.files[i];
                if ('name' in file) {
                    txt += "" + file.name + "<br>";
                }
            }
        }
    } 
    else {
        if (x.value == "") {
            txt += "Veuillez charger la pièce obligatoire";
        } else {
            txt += "The files property is not supported by your browser!";
            txt  += "<br>The path of the selected file: " + x.value; // If the browser does not support the files property, it will return the path of the selected file instead. 
        }
    }
    document.getElementById("filename").innerHTML = txt;
}

 function myFunction2(){
    var x2 = document.getElementById("file2");
    var txt2 = "";
    if ('files' in x2) {
        if (x2.files.length == 0) {
            txt2 = "";
        } else {
            for (var i = 0; i < x2.files.length; i++) {
                //txt += "<br><strong>" + (i+1) + ". file</strong><br>";
                var file2 = x2.files[i];
                if ('name' in file2) {
                    txt2 += "" + file2.name + "<br>";
                }
            }
        }
    } 
    else {
        if (x2.value == "") {
            txt2 += "";
        } else {
            txt2 += "The files property is not supported by your browser!";
            txt2  += "<br>The path of the selected file: " + x2.value; // If the browser does not support the files property, it will return the path of the selected file instead. 
        }
    }
    document.getElementById("filename2").innerHTML = txt2;
}


// show name of file NON
 function myFunctionN(){
    var x = document.getElementById("fileN");
    var txt = "";
    if ('files' in x) {
        if (x.files.length == 0) {
            txt = "Veuillez charger la pièce obligatoire";
        } else {
            for (var i = 0; i < x.files.length; i++) {
                //txt += "<br><strong>" + (i+1) + ". file</strong><br>";
                var file = x.files[i];
                if ('name' in file) {
                    txt += "" + file.name + "<br>";
                }
            }
        }
    } 
    else {
        if (x.value == "") {
            txt += "Veuillez charger la pièce obligatoire";
        } else {
            txt += "The files property is not supported by your browser!";
            txt  += "<br>The path of the selected file: " + x.value; // If the browser does not support the files property, it will return the path of the selected file instead. 
        }
    }
    document.getElementById("filenameN").innerHTML = txt;
}

// show name of file
 function myFunctionN2(){
    var x2 = document.getElementById("fileN2");
    var txt2 = "";
    if ('files' in x2) {
        if (x2.files.length == 0) {
            txt2 = "";
        } else {
            for (var i = 0; i < x2.files.length; i++) {
                //txt += "<br><strong>" + (i+1) + ". file</strong><br>";
                var file2 = x2.files[i];
                if ('name' in file2) {
                    txt2 += "" + file2.name + "<br>";
                }
            }
        }
    } 
    else {
        if (x2.value == "") {
            txt2 += "";
        } else {
            txt2 += "The files property is not supported by your browser!";
            txt2  += "<br>The path of the selected file: " + x2.value; // If the browser does not support the files property, it will return the path of the selected file instead. 
        }
    }
    document.getElementById("filenameN2").innerHTML = txt2;
}

jQuery(document).ready(function ($) {
   $(function(){
    $('#pop').click(function(){
        $('#popup').bPopup();
    });
});
});


//video hp
function pause() {
    var video = document.getElementById("videoPlayer");   
        video.pause();   
}

function play() {
    var vid = document.getElementById("videoPlayer");   
        vid.play();   
}
//script pause/play video slide
function vidplay() {
    var video = document.getElementById("videoPlayer");
    if (video.paused) {
        video.play();
    } else {
        video.pause();
    }
}




//video  1
function pause1() {
    var video1 = document.getElementById("videoPlayer1");   
        video1.pause();   
}

function play1() {
    var vid1 = document.getElementById("videoPlayer1");   
        vid1.play();   
}
//script pause/play video slide
function vidplay1() {
    var video1 = document.getElementById("videoPlayer1");
    if (video1.paused) {
        video1.play1();
    } else {
        video1.pause1();
    }
}



//video  2
function pause2() {
    var video2 = document.getElementById("videoPlayer2");   
        video2.pause();   
}

function play2() {
    var vid2 = document.getElementById("videoPlayer2");   
        vid2.play();   
}
//script pause/play video slide
function vidplay2() {
    var video2 = document.getElementById("videoPlayer2");
    if (video2.paused) {
        video2.play2();
    } else {
        video2.pause2();
    }
}





//video  3
function pause3() {
    var video3 = document.getElementById("videoPlayer3");   
        video3.pause();   
}

function play3() {
    var vid3 = document.getElementById("videoPlayer3");   
        vid3.play();   
}
//script pause/play video slide
function vidplay3() {
    var video3 = document.getElementById("videoPlayer3");
    if (video3.paused) {
        video3.play3();
    } else {
        video3.pause3();
    }
}