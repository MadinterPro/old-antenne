<?php

$wsdl = "http://www.b2sms.com/index.php?service=api~optin&module=jWSDL&action=WSDL:wsdl";
$service = new SoapClient($wsdl);

try {

    $session_id = $service->auth('cmmapi', md5('Rallye974'), 'en_EN');
} catch (Exception $e) {

    exit('Connect to webservice failed');
}

//     setUserRecord ( auth,	email,	defLst,	data,	subsLst )
//        Create or modify user's data and subscriptions.
//        Parameters :
//           auth (associative array of string) : Unique "sessionId" or both "user" / "password" (required).
//           email (string) : User's email (primary key).
//           defLst (array of string) : Array of all the lists' name defined in the module configuration file.
//           data (associative array of string) : Associative array of the user's data.
//           subsLst (array of string) : Array of the lists subscribed to by the user.
//        Return : boolean  
//        
//            
//     setUserRecord ( auth,email,defLst,data,subsLst ) 
//           email: User's email (primary key)
//           defLst:  Tableau de tous les noms des listes définie dans le fichier de configuration du module
//           data:  tableau associatif des données de l'utilisateur
//           subsLst:  tableau des listes souscrits par l'utilisateur 


//    $list = array();
//    $list = array("civ","nom","prenom","ville","mobile","email2","concessionnaire");

$list = array('Toyota');

$data = array(
    'civ' => 'Mme',
    'nom' => 'adresse',
    'prenom' => 'adresse',
    'ville' => 'adresse',
    'mobile' => '0123456789',
    'email2' => 'mail@mail.com',
    'concessionnaire' => 'Toyota',
);

$taballservices = $service->setUserRecord(array('sessionId' => $session_id), array(), $list, $data, array());
//     $taballservices = $service->unregisterUser ( array('sessionId' => $session_id),'andriamandimby@gmail.com', $data);

print_r($taballservices);

//$service->logout($session_id);