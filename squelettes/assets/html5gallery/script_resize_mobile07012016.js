resizewindow(0);

window.onresize = function() {
    resizewindow(1);
}

function resizewindow(redim) {
    width_resize = $(window).width();
    if (width_resize < 1024 && width_resize >= 768)
    {
        $("#popup_mobile #pub_area").css('display', 'none');
        new_width = 769;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            new_width = 769;
        }
        $("#popup_mobile .content-slidepopup").css('cssText', $("#popup_mobile .content-slidepopup").css('cssText') + ";width: " + new_width + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-container-0").css('cssText', $("#popup_mobile .html5gallery-container-0").css('cssText') + ";width: " + new_width + "px;height:620px!important;padding: 10px;");
        $("#popup_mobile .html5gallery-box-0").css('cssText', $("#popup_mobile .html5gallery-box-0").css('cssText') + ";width: " + new_width + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-elem-0").css('cssText', $("#popup_mobile .html5gallery-elem-0").css('cssText') + ";width: " + new_width + "px;padding: 10px;");
        $("#popup_mobile .html5gallery-elem-img-0").css('cssText', $("#popup_mobile .html5gallery-elem-img-0").css('cssText') + ";width: " + new_width + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-elem-image-0").css('cssText', $("#popup_mobile .html5gallery-elem-image-0").css('cssText') + ";width: " + (new_width * 39) / 100 + "px;float:left;height:auto;padding: 10px;");
        $("#popup_mobile .html5gallery-title-0").css('cssText', $("#popup_mobile .html5gallery-title-0").css('cssText') + ";width: " + (new_width * 52) / 100 + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-car-0").css('cssText', $("#popup_mobile  .html5gallery-car-0").css('cssText') + ";width: " + (new_width * 94) / 100 + "px;padding: 10px;");
        $("#popup_mobile .html5gallery-car-list-0").css('cssText', $("#popup_mobile .html5gallery-car-list-0").css('cssText') + ";width: " + (new_width * 86) / 100 + "px;padding: 10px;");
        $("#popup_mobile .html5gallery-car-mask-0").css('cssText', $("#popup_mobile .html5gallery-car-mask-0").css('cssText') + ";width: " + (new_width * 65.8) / 100 + "px!important;left:60px!important;padding: 10px;");
        $("#popup_mobile .html5gallery-title-text-0").css('cssText', $("#popup_mobile .html5gallery-title-text-0").css('cssText') + ";font-size:10px;line-height:16px");
        $("#popup_mobile").css('cssText', $("#popup_mobile").css('cssText') + ";width:85%!important;left:4%");
        $("#popup_mobile .html5gallery-right-0").css('cssText', $("#popup_mobile .html5gallery-right-0").css('cssText') + ";left:none;");
         $("#popup_mobile .html5gallery-elem-video-0").css('cssText', $("#popup_mobile .html5gallery-elem-video-0").css('cssText') + ";width:380px;height:436px");
        
        if(redim==1){
            $("#popup_mobile .html5gallery-car-list-0").css('cssText', $("#popup_mobile .html5gallery-car-list-0").css('cssText') + ";left:40px;");
            $("#popup_mobile .html5gallery-right-0").css('cssText', $("#popup_mobile .html5gallery-right-0").css('cssText') + ";left:-9px;");
        }
    } else if (width_resize < 767) {
        $("#popup_mobile #pub_area").css('display', 'none');
        new_width = 769;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            new_width = 769;
        }
        $("#popup_mobile .content-slidepopup").css('cssText', $("#popup_mobile .content-slidepopup").css('cssText') + ";width: " + new_width + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-container-0").css('cssText', $("#popup_mobile .html5gallery-container-0").css('cssText') + ";width: " + new_width + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-box-0").css('cssText', $("#popup_mobile .html5gallery-box-0").css('cssText') + ";width: " + new_width + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-elem-0").css('cssText', $("#popup_mobile .html5gallery-elem-0").css('cssText') + ";width: " + new_width + "px;padding: 10px;");
        $("#popup_mobile .html5gallery-elem-img-0").css('cssText', $("#popup_mobile .html5gallery-elem-img-0").css('cssText') + ";width: " + new_width + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-elem-image-0").css('cssText', $("#popup_mobile .html5gallery-elem-image-0").css('cssText') + ";width: " + (new_width * 39) / 100 + "px;float:left;height:auto;padding: 10px;");
        $("#popup_mobile .html5gallery-title-0").css('cssText', $("#popup_mobile .html5gallery-title-0").css('cssText') + ";width: " + (new_width * 52) / 100 + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-car-0").css('cssText', $("#popup_mobile  .html5gallery-car-0").css('cssText') + ";width: " + (new_width * 94) / 100 + "px;padding: 10px;");
        $("#popup_mobile .html5gallery-car-list-0").css('cssText', $("#popup_mobile .html5gallery-car-list-0").css('cssText') + ";width: " + (new_width * 86) / 100 + "px;padding: 10px;");
        $("#popup_mobile .html5gallery-car-mask-0").css('cssText', $("#popup_mobile .html5gallery-car-mask-0").css('cssText') + ";width: " + (new_width * 65.8) / 100 + "px!important;left:60px!important;padding: 10px;");
        $("#popup_mobile .html5gallery-title-text-0").css('cssText', $("#popup_mobile .html5gallery-title-text-0").css('cssText') + ";font-size:10px;line-height:13px");
        $("#popup_mobile").css('cssText', $("#popup_mobile").css('cssText') + ";width:85%;left:4%");
        $("#popup_mobile .html5gallery-right-0").css('cssText', $("#popup_mobile .html5gallery-right-0").css('cssText') + ";left:none;");
         $("#popup_mobile .html5gallery-elem-video-0").css('cssText', $("#popup_mobile .html5gallery-elem-video-0").css('cssText') + ";width:380px;height:436px");
        if(redim==1){
            $("#popup_mobile .html5gallery-car-list-0").css('cssText', $("#popup_mobile .html5gallery-car-list-0").css('cssText') + ";left:40px;");
            $("#popup_mobile .html5gallery-right-0").css('cssText', $("#popup_mobile .html5gallery-right-0").css('cssText') + ";left:-9px;");
        }
    } else {
        $("#popup_mobile #pub_area").css('display', 'block');
        new_width = 769;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $("#popup_mobile #pub_area").css('display', 'none');
            new_width = 769;
        }
        $("#popup_mobile .content-slidepopup").css('cssText', $("#popup_mobile .content-slidepopup").css('cssText') + ";width: " + new_width + "px");
        $("#popup_mobile .html5gallery-container-0").css('cssText', $("#popup_mobile .html5gallery-container-0").css('cssText') + ";width: " + new_width + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-box-0").css('cssText', $("#popup_mobile .html5gallery-box-0").css('cssText') + ";width: " + new_width + "pxt;height:620px!important;padding: 10px;");
        $("#popup_mobile .html5gallery-elem-0").css('cssText', $("#popup_mobile .html5gallery-elem-0").css('cssText') + ";width: " + new_width + "px;padding: 10px;");
        $("#popup_mobile .html5gallery-elem-img-0").css('cssText', $("#popup_mobile .html5gallery-elem-img-0").css('cssText') + ";width: " + new_width + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-elem-image-0").css('cssText', $("#popup_mobile .html5gallery-elem-image-0").css('cssText') + ";width: " + (new_width * 39) / 100 + "px;float:left;height:auto;padding: 10px;");
        $("#popup_mobile .html5gallery-title-0").css('cssText', $("#popup_mobile .html5gallery-title-0").css('cssText') + ";width: " + (new_width * 52) / 100 + "px;height:620px;padding: 10px;");
        $("#popup_mobile .html5gallery-car-0").css('cssText', $("#popup_mobile  .html5gallery-car-0").css('cssText') + ";width: " + (new_width * 94) / 100 + "px;padding: 10px;");
        $("#popup_mobile .html5gallery-car-list-0").css('cssText', $("#popup_mobile .html5gallery-car-list-0").css('cssText') + ";width: " + (new_width * 86) / 100 + "px;padding: 10px;");
        $("#popup_mobile .html5gallery-car-mask-0").css('cssText', $("#popup_mobile .html5gallery-car-mask-0").css('cssText') + ";width: " + (new_width * 64.8) / 100 + "px!important;left:60px!important;padding: 10px;");
        $("#popup_mobile .html5gallery-title-text-0").css('cssText', $("#popup_mobile .html5gallery-title-text-0").css('cssText') + ";font-size:14px;line-height:20px");
        $("#popup_mobile").css('cssText', $("#popup_mobile").css('cssText') + ";width:75%!important;left:1%;");
        $("#popup_mobile .html5gallery-right-0").css('cssText', $("#popup_mobile .html5gallery-right-0").css('cssText') + ";left:none;");
         $("#popup_mobile .html5gallery-elem-video-0").css('cssText', $("#popup_mobile .html5gallery-elem-video-0").css('cssText') + ";width:380px;height:436px");
        if(redim==1){
            $("#popup_mobile .html5gallery-car-list-0").css('cssText', $("#popup_mobile .html5gallery-car-list-0").css('cssText') + ";left:40px;");
            $("#popup_mobile .html5gallery-right-0").css('cssText', $("#popup_mobile .html5gallery-right-0").css('cssText') + ";right:-9px;");
            $("#popup_mobile .html5gallery-car-mask-0").css('cssText', $("#popup_mobile .html5gallery-car-mask-0").css('cssText') + ";left:67px!important;");
        }
    }
}