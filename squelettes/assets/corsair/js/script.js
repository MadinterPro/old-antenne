/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready(function ($) {
    $('.civilite').click(function () {
        var th = $(this), name = th.attr('name');
        if (th.is(':checked')) {
            $(':checkbox[name="' + name + '"]').not($(this)).prop('checked', false);
        }
    });




    $("#date_naiss").datepicker({
        //dateFormat: 'mm/dd/yy',
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100y:c+nn'
//            maxDate: '-1d'
    });


    // validate the comment form when it is submitted
    //jq("#commentForm").validate();

    jQuery.validator.addMethod("mynumber", function (value, element) {
        //return this.optional(element) || /^\s*?([\d\,]+(\.\d{1,2})?|\.\d{1,2})\s*$/.test(value);
        return this.optional(element) || /^\s*?([\d\.]+(\.\d{1,2})?)\s*$/.test(value);
    }, "Veuillez saisir uniquement des chiffres");

    jQuery.validator.addMethod('customphone', function (value, element) {
        return this.optional(element) || /^[0-9-+]+$/.test(value);
    }, "Merci d’entrer un numéro de téléphone");

   


    // validate signup form on keyup and submit
    $("#adhesion").validate({
        rules: {
            civilite: {
                required: true             
            },
            nom: {
                required: true,
                minlength: 2
            },
            prenom: {
                required: true,
                minlength: 2
            },
            date_naiss: {
                required: true
            },
                  
            voie: {
                required: true
            },
            
            code: {
                required: true
            },
            ville: {
                required: true
            },
            pays: {
                required: true
            },
            telmobile: {
                customphone:true,
                minlength: 10
            },
//                required: true,
//                phoneUS: true
//            },
            telfixe: {
                customphone:true,
                minlength: 10                
            },
//            telfixe: "customphone",
            email: {
                required: true,
                email: true
            },
            email2: {
                required: true,
                equalTo: email
            },
//            telpro: {
////                required: true,
//                phoneUS: true
//            },
            telpro: {
                customphone:true,
                minlength: 10                
            },
            
            
            accepte: {
                required: true
            }
        },
        messages: {
            civilite: {
                required: "Veuillez séléctionner un genre"               
            },
            nom: {
                required: "Veuillez entrer votre nom",
                minlength: "minimum 2"
            },
            prenom: {
                required: "Veuillez entrer votre prénom",
                minlength: "minimum 2"
            },
            date_naiss: {
                required: "Veuillez entrer la date de votre naissance"
            },
           
            voie: {
                required: "Veuillez entrer numéro et voie de votre adresse"
            },
            
            code: {
                required: "Veuillez entrer votre code postal"
            },
            ville: {
                required: "Veuillez entrer le nom de votre ville"
            },
            pays: {
                required: "Veuillez entrer votre pays"
            },
            telmobile: {
//                required: "Veuillez entrer votre numéro de téléphone mobile",
//                phoneUS: "Merci d’entrer un numéro de téléphone ",
                minlength: "Merci d’entrer un numéro de téléphone "
//                maxlength: "Merci d’entrer un numéro de téléphone "
            },
            telfixe: {
//                required: "Veuillez entrer votre numéro de téléphone fixe",
//                phoneUS: "Merci d’entrer un numéro de téléphone ",
                minlength: "Merci d’entrer un numéro de téléphone "
//                maxlength: "Merci d’entrer un numéro de téléphone "
            },
            email: {
                required: "Veuillez entrer votre adresse mail",
                email: " L'adresse mail est invalide"
            },
            email2: {
                required: "Veuillez resaisir votre adresse mail",
                equalTo: " Veuillez resaisir votre adresse mail"
            },
            telpro: {
//                required: "Veuillez entrer votre numéro de téléphone professionnelle",
//                phoneUS: "Merci d’entrer un numéro de téléphone professionnelle",
                minlength: "Merci d’entrer un numéro de téléphone professionnelle "
//                maxlength: "Merci d’entrer un numéro de téléphone professionnelle"
            },
            
            accepte: {
                required: "Veuillez accepter pour valider"
            }
        }
    });

});

