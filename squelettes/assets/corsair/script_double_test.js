/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready(function ($) {

 $("#num_billet").on('keyup', function (e)
    {
        var $that = $(this),
                maxlength = 13;
        if ($that.val().length > maxlength)
        {
            $that.val($that.val().substr(0, maxlength));
        }
    });



   $("#date_aller").datepicker({
        //dateFormat: 'mm/dd/yy',
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100y:c+nn'
//            maxDate: '-1d'
    });

    $("#date_retour").datepicker({
        //dateFormat: 'mm/dd/yy',
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: '-100y:c+nn'
//            maxDate: '-1d'
    });
 $("#num_billet").keydown(function (e) {
        var oldvalue = $(this).val();
        var field = this;
        setTimeout(function () {
            if (field.value.indexOf('923') !== 0) {
                $(field).val(oldvalue);
            }
        }, 1);
    });

    // validate the comment form when it is submitted
    //jq("#commentForm").validate();

    jQuery.validator.addMethod("mynumber", function (value, element) {
        //return this.optional(element) || /^\s*?([\d\,]+(\.\d{1,2})?|\.\d{1,2})\s*$/.test(value);
        return this.optional(element) || /^\s*?([\d\.]+(\.\d{1,2})?)\s*$/.test(value);
    }, "Veuillez saisir uniquement des chiffres");

    jQuery.validator.addMethod('customphone', function (value, element) {
        return this.optional(element) || /^[0-9-+]+$/.test(value);
    }, "Merci d’entrer un numéro de téléphone");

    jQuery.validator.addMethod("emaildomain", function (value, element) {
//  return this.optional(element) || /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(value);
        return this.optional(element) || /^\w+([-+.']\w+)*@\w+([-]\w+)*\.\w{2,9}(\w{2,9})*$/.test(value);
    }, "L\'adresse email est invalide");


    // validate signup form on keyup and submit
    $("#doubleeuro").validate({
        rules: {
            user: {
                required: true,
                minlength: 2
            },
            nom: {
                required: true,
                minlength: 2
            },
            prenom: {
                required: true,
                minlength: 2
            },
            telmobile: {
                customphone: true,
                minlength: 8,
                maxlength: 12
            },
            email: {
                required: true,
                emaildomain: true

            },
            num_billet: {
                required: true,
                number: true,
                minlength: 13
            },
            date_aller: {
                required: true
            },
            num_vol: {
                required: true
            },
            arrive: {
                required: true
            },
            depart: {
                required: true
            }

        },
        messages: {
            user: {
                required: "Veuillez entrer votre n° d'adhérent",
                minlength: "minimum 2"
            },
            nom: {
                required: "Veuillez entrer votre nom",
                minlength: "minimum 2"
            },
            prenom: {
                required: "Veuillez entrer votre prénom",
                minlength: "minimum 2"
            },
            telmobile: {
                customphone: "Veuillez entrer un numéro de téléphone valide ",
                minlength: "Veuillez entrer un numéro de téléphone valide ",
                maxlength: "Veuillez entrer un numéro de téléphone valide "
            },
            email: {
                required: "Veuillez entrer votre adresse mail",
                emaildomain: " L'adresse email est invalide"
            },
            num_billet: {
                required: "Veuillez entrer le numéro de votre billet",
                number: "Veuillez saisir uniquement des chiffres",
                minlength: "Veuillez saisir les 13 chiffres"
            },
            date_aller: {
                required: "Veuillez entrer la date de votre vol"
            },
            num_vol: {
                required: "Veuillez entrer le numéro de votre vol"
            },
            arrive: {
                required: "Veuillez entrer l'aéroport d'arrivé"
            },
            depart: {
                required: "Veuillez entrer l'aéroport de départ"
            }
        }
    });

    $("#doubleeuro").submit(function () {
        $.post("http://www.antennereunion.fr/squelettes/ajax/newsletter.php", {
            mail_user: $("#email").val(),
            optin: $("#optin").val(),
            optin_partenaire: $("#optin_partenaire").val()
        }, function (data) {
            console.log(data);
        }, "json");
    });

    $('#control').click(function(e){

    $.ajax ({
        url: "squelettes/ajax/date.php",
        type: "POST",
        data: JSON.stringify({date:$("#date_retour").val()}),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(result){
            var res = JSON.parse(JSON.stringify(result));
            //console.log(res);
            if(res.date == "sup"){
               devmodal_user(e);
               $('#next').css('display','none'); 
           }else{
              $('#next').css('display','block');
           }
        }
    });
    });

    $('#date_retour').on("change paste keyup", function() {
      alert($(this).val()); 
    });

    function devmodal_user(e)
    {
        var o = {top: 25, overlay: 0.45, closeButton: ".modalnav"};
        var overlay = $("<div id='lean_overlay'></div>");
        $("body").append(overlay);
        var modal_id = "#user_modal";
        $("#lean_overlay").click(function () {
            close_modal(modal_id);
        });
        $(o.closeButton).click(function () {
            close_modal(modal_id);
        });
        $(".fermer").click(function () {
            close_modal(modal_id);
        });
        var modal_height = $(modal_id).outerHeight();
        var modal_width = $(modal_id).outerWidth();
        $("#lean_overlay").css({"display": "block", opacity: 0});
        $("#lean_overlay").fadeTo(200, o.overlay);
        $(modal_id).css({"display": "block", "position": "fixed", "opacity": 0, "z-index": 9999999, "left": 50 + "%", "margin-left": -(modal_width / 2) + "px", "top": o.top + "%"});
        $(modal_id).fadeTo(200, 1);
        e.preventDefault();
    }

    function close_modal(modal_id) {
        $("#lean_overlay").fadeOut(200);
        $(modal_id).css({"display": "none"});
    }

});

