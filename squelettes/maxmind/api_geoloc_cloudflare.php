<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
    // you want to allow, and if so:
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 1000');
}

//Database configuration
define('DB_HOST', 'localhost');
define('DB_NAME', 'antenne_art_spip');
define('DB_USER', 'antenne_spipAR');
define('DB_PASSWORD', 'antv3_2009');

//Get article ID
$id_article = filter_input(INPUT_GET, 'id_article', FILTER_SANITIZE_SPECIAL_CHARS);

//Get customer ip address with status and country if exist
$ipAddress = getIpAddress();

//Check if ip address is allowed in file [local_range|manual]
$authorized = checkIpAddress($ipAddress);

//Update json file
updateJsonFile($ipAddress, $authorized);

//Return response
if ($authorized) {
    /*$secure_source = secureVideoSourceToJSON($id_article);
    if ($id_article) {
        echo $secure_source;
    } else {
        echo "OK";
    }*/
    echo "OK";
} else {
    echo "NOK";
    /*if ($id_article) {
        echo "0";
    } else {
        echo "NOK";
    }*/
}

/**
 * Get IP Address
 * @return array
 */
function getIpAddress()
{
    //Initialisation à une requete ne passant pas par cloudflare
    $response = array(
        'ip' => $_SERVER['REMOTE_ADDR'],
        'status' => false
    );

    //Test si l'IP passe par CloudFlare
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $response['ip'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        $response['status'] = true;
    }

    //Si le pays est détecté alors on l'ajoute dans le tableau
    if (isset($_SERVER["HTTP_CF_IPCOUNTRY"])) {
        $response['country'] = $_SERVER["HTTP_CF_IPCOUNTRY"];
    }

    return $response;
}

/**
 * Get allowed IPs
 * @param $filename
 * @return array
 */
function getAllowedIPs($filename)
{
    $ips = array();
    $file = fopen("../geolocalisation/".$filename, 'r');
    if ($file) {
        while (!feof($file)) {
            $buffer = fgets($file, 4096);
            $buffer = trim($buffer);
            if (strlen($buffer) >= 7 ) {
                $ips[] = $buffer;
            }
        }
        fclose($file);
    }

    return $ips;
}

/**
 * Check ip address
 * @param $ipAddress
 * @return bool
 */
function checkIpAddress($ipAddress)
{
    //Initialisation de l'autorisation à faux
    $authorized = false;
    //Récupération des IPs autorisés dans "manual"
    $allowedIPs_manual = getAllowedIPs('manual');

    //Si requete provient de la réunion
    if ($ipAddress['country'] == 'RE') {
        $authorized = true;
    } else {
        //Vérification si l'IP est dans la liste des IPs manual
        if (in_array($ipAddress['ip'], $allowedIPs_manual)) {
            $authorized = true;
        }
    }

    return $authorized;
}

/**
 * Get secure video source
 * @param $id_article
 * @return bool|string
 */
function secureVideoSourceToJSON($id_article) {
    //Connexion SQL
    try {
        $bdd = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    } catch(Exception $e) {
        exit('Impossible de se connecter &agrave; la base de donn&eacute;es.');
    }
    //Requête SQL
    $sqlgeo = "SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 0,1
                    ";
    $reqgeo = $bdd->query($sqlgeo) or die(print_r($bdd->errorInfo()));
    $rowgeo = $reqgeo->fetch();
    $rawflvgeo = $rowgeo['fichier'];
    $rawmp4geo = str_replace(".flv", ".mp4", $rawflvgeo);
    $rawokgeo = $rawflvgeo . '**' . $rawmp4geo;


    $sqlmaxmind = "SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 1,2
                    ";
    $reqmaxmind = $bdd->query($sqlmaxmind) or die(print_r($bdd->errorInfo()));
    $rowmaxmind = $reqmaxmind->fetch();
    $rawflvmaxmind = $rowmaxmind['fichier'];
    $rawmp4maxmind = str_replace(".flv", ".mp4", $rawflvmaxmind);
    $rawokmaxmind = $rawflvmaxmind . '**' . $rawmp4maxmind;

    if ($count = $reqmaxmind->rowCount() == 0) {
        $raw = $rawokgeo;
    } else {
        $raw = $rawokgeo.'&nbsp;||&nbsp;'.$rawokmaxmind;
    }
    $clean = preg_replace("/\s+/", "", $raw);
    $explode_playlists = explode("||", $clean);
    $returns = array();
    $cdn = "http://cdn.antenne.re";
    foreach ($explode_playlists as $playlist) {
        $explode_properties = explode("**", $playlist);
        if (count($explode_properties) === 2) {
            $file1 = $explode_properties[0]; //flv
            $file2 = $explode_properties[1]; //mp4
            if (strpos($explode_properties[0], "cdn.antenne.re") !== false && strpos($explode_properties[1], "cdn.antenne.re") !== false && strpos($explode_properties[0], "archivevideo") === false && strpos($explode_properties[1], "archivevideo") === false && strpos($explode_properties[0], "nas") === false && strpos($explode_properties[1], "nas") === false) {
                $returns[] = array(
                    "file1" => $cdn . secureLink(substr($file1, strlen($cdn))),
                    "file2" => $cdn . secureLink(substr($file2, strlen($cdn)))
                );
            } else {
                $returns[] = array(
                    "file1" => $file1,
                    "file2" => $file2
                );
            }
        }
    }
    $haystack = json_encode($returns);
    $last_crochet = strrpos($haystack, "]");
    $first_crochet = strpos($haystack, "[");
    if (isset($id_article)) {
        return substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
    } else {
        $crochet = "[]";
        return $crochet;
    }
}

/**
 * Generate secure link
 * @param $file
 * @return string
 */
function secureLink($file) {
    $secret = 'antenne_reunion'; // hash secret à changer et à nous communiquer
    $path = '/' . trim($file, '/');
    //$expire = time() + 90000; // durée d'expiration (1 heure ici)
    $expire = 1439108452;

    $md5 = base64_encode(md5($secret . $path . $expire, true));
    $md5 = strtr($md5, '+/', '-_');
    $md5 = str_replace('=', '', $md5);

    return $path . '?key=' . $md5 . '&e=' . $expire;
}

/**
 * Update success.json or failed.json
 * @param $ipAddress
 * @param $authorized
 */
function updateJsonFile($ipAddress, $authorized)
{
    $data = array();
    if ($ipAddress['status']) {
        if ($authorized) {
            $file = "../geolocalisation/success.json";
        } else {
            $file = "../geolocalisation/failed.json";
        }
    } else {
        $file = "../geolocalisation/other.json";
    }

    if (is_file($file)) {
        $data = json_decode(file_get_contents($file), true);
        if (!array_key_exists($ipAddress['ip'], $data)) {
            $data[$ipAddress['ip']] = '';
            if ($ipAddress['country']) {
                $data[$ipAddress['ip']] = $ipAddress['country'];
            }
            file_put_contents($file, json_encode($data));
        }
    } else {
        $data[$ipAddress['ip']] = '';
        if ($ipAddress['country']) {
            $data[$ipAddress['ip']] = $ipAddress['country'];
        }
        file_put_contents($file, json_encode($data));
    }
}

/**
 * Check if IP address in range
 * @param $ip
 * @param $range
 * @return bool
 */
function ipInRange($ip, $range) {
    if (strpos($range, '/') !== false) {
        // $range is in IP/NETMASK format
        list($range, $netmask) = explode('/', $range, 2);
        if (strpos($netmask, '.') !== false) {
            // $netmask is a 255.255.0.0 format
            $netmask = str_replace('*', '0', $netmask);
            $netmask_dec = ip2long($netmask);
            return ( (ip2long($ip) & $netmask_dec) == (ip2long($range) & $netmask_dec) );
        } else {
            // $netmask is a CIDR size block
            // fix the range argument
            $x = explode('.', $range);
            while(count($x)<4) $x[] = '0';
            list($a,$b,$c,$d) = $x;
            $range = sprintf("%u.%u.%u.%u", empty($a)?'0':$a, empty($b)?'0':$b,empty($c)?'0':$c,empty($d)?'0':$d);
            $range_dec = ip2long($range);
            $ip_dec = ip2long($ip);

            # Strategy 1 - Create the netmask with 'netmask' 1s and then fill it to 32 with 0s
            #$netmask_dec = bindec(str_pad('', $netmask, '1') . str_pad('', 32-$netmask, '0'));

            # Strategy 2 - Use math to create it
            $wildcard_dec = pow(2, (32-$netmask)) - 1;
            $netmask_dec = ~ $wildcard_dec;

            return (($ip_dec & $netmask_dec) == ($range_dec & $netmask_dec));
        }
    } else {
        // range might be 255.255.*.* or 1.2.3.0-1.2.3.255
        if (strpos($range, '*') !==false) { // a.b.*.* format
            // Just convert to A-B format by setting * to 0 for A and 255 for B
            $lower = str_replace('*', '0', $range);
            $upper = str_replace('*', '255', $range);
            $range = "$lower-$upper";
        }

        if (strpos($range, '-')!==false) { // A-B format
            list($lower, $upper) = explode('-', $range, 2);
            $lower_dec = (float)sprintf("%u",ip2long($lower));
            $upper_dec = (float)sprintf("%u",ip2long($upper));
            $ip_dec = (float)sprintf("%u",ip2long($ip));
            return ( ($ip_dec>=$lower_dec) && ($ip_dec<=$upper_dec) );
        }
        return false;
    }
}
