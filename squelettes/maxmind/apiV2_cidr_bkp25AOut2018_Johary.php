<?php
header("Access-Control-Allow-Origin: *");
// initialisation 
$cidrs = array();
$ip_add_manuals = array();
$IpFromPHP = get_client_ip();
$IpFromPHP = str_replace("::ffff:","",$IpFromPHP);
$GLOBALS['is974'] = 0;

// lecture fichier
$handle = @fopen("../geolocalisation/local_ranges", "r" );
if ($handle)
{
    while (!feof($handle))
    {
        $buffer = fgets($handle, 4096);
        $buffer = trim($buffer);
        if(strlen($buffer) >= 7 ){
          $cidrs[] = $buffer;
        }
    }
    fclose($handle);
}
$ip_manual = @fopen("../geolocalisation/manual", "r" );
if ($ip_manual)
{
    while (!feof($ip_manual))
    {
        $buffer_ip = fgets($ip_manual, 4096);
        $buffer_ip = trim($buffer_ip);
        if(strlen($buffer) >= 7 )
        {
         $ip_add_manuals[] = $buffer_ip;
        }
    }
    fclose($ip_manual);
}
function ipCIDRCheck($ip, $cidrs)
{
    $bool = false ;
    foreach ($cidrs as $cidr)
    {
        list ($net, $mask) = explode('/', $cidr);
        $ipNet = ip2long($net);
        $ipMask = ~((1 << (32 - $mask)) - 1);
        $ipIp = ip2long($ip);
        $ipIpNet = $ipIp & $ipMask;
        if ($ipIpNet == $ipNet)
        {
            $bool = true;
            break;
        }
   }
   return $bool;
}

function ipManualCheck($ip, $ip_add_manuals)
{
 return in_array($ip,$ip_add_manuals);
}

if ((ipCIDRCheck($IpFromPHP, $cidrs)) || (ipManualCheck($IpFromPHP,$ip_add_manuals))) {
    //L'IP est dans CIDR ou dans le fichier ajout manuel
    $varok1 = "OK";
    $GLOBALS['is974'] = 1;
    echo $varok1;
} else {
    //L'IP n'est pas dans CIDR - access denied
    $varok2 = "NOK";
    $GLOBALS['is974'] = 0;
    echo $varok2;
}

/*function get_client_ip() {
    $ipaddress = "";
    if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}*/
function get_client_ip() {
    $ipaddress = "";
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

function appel_curl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_TIMEOUT, 720000);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}