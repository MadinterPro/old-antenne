<?php
$id_article = filter_input(INPUT_GET, 'id_article', FILTER_SANITIZE_SPECIAL_CHARS);

require_once 'vendor/autoload.php';
require_once 'RemoteAddress.php';

use GeoIp2\Database\Reader;

//
$reader = new Reader('GeoIP2-Country.mmdb');
//$ip = filter_input(INPUT_GET, 'ip', FILTER_SANITIZE_SPECIAL_CHARS);


$authorize = false;
$Ip = "";
try {
    $Ip = get_client_ip();
    $Ip = str_replace("::ffff:","",$Ip);
    echo $Ip."<br>";
    if ($Ip == "41.188.49.140" || $Ip == "197.227.34.251") {//Telma madinter
        $authorize = true;
    }
    $record = $reader->country($Ip);
    $isoCode = $record->country->isoCode;
    echo strtoupper($isoCode);
    if (strtoupper($isoCode) === "RE") {
        $authorize = true;
    }
} catch (GeoIp2\Exception\AddressNotFoundException $e) {
}

if (!$authorize) {
    try {
        $Ip = $GLOBALS["_ENV"]["GEOIP_ADDR"]; //from spip
        if(!is_null($Ip)){
            $record = $reader->country($Ip);
            $isoCode = $record->country->isoCode;
            if (strtoupper($isoCode) === "RE") {
                $authorize = true;
            }
        }
    } catch (GeoIp2\Exception\AddressNotFoundException $e) {

    }
}

if (!$authorize) {
    try {
        $Ip = IpFromZendFramework();
        $record = $reader->country(IpFromZendFramework($Ip));
        $isoCode = $record->country->isoCode;
        if (strtoupper($isoCode) === "RE") {
            $authorize = true;
        }
    } catch (GeoIp2\Exception\AddressNotFoundException $e) {
    }
}

//Pour autoriser un IP
/*
  if($Ip == "41.21.54.58"){
  $authorize = true;
  }
 
if ($Ip == "87.98.168.187") {//Telma madinter
    $authorize = true;
}*/

if ($authorize) {
//if(true){
    //$source = VideoSourceToJSON($id_article);
    $secure_source = SecureVideoSourceToJSON($id_article);
    //var_dump($source);echo "<br/>";
    echo($secure_source);
} else {
    echo "0";
}

die;

function IpFromZendFramework() {
    $o = new RemoteAddress();
    return $o->getIpAddress();
}
function get_client_ip() {
    $ipaddress = '';
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

function SecureVideoSourceToJSON($id_article) {
    //Connexion SQL
    $baselinfo='antenne_art_spip';
    $host='localhost';
    $user='antenne_spipAR';
    $pass='antv3_2009';

    try {
        $bdd = new PDO("mysql:host=$host;dbname=$baselinfo",$user,$pass);
    } catch(Exception $e) {
        exit('Impossible de se connecter &agrave; la base de donn&eacute;es.');
    }
    //Requête SQL
    $sqlgeo ="SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 0,1
                    ";
    $reqgeo = $bdd->query($sqlgeo)or die(print_r($bdd->errorInfo()));
    $rowgeo = $reqgeo ->fetch();
    $rawflvgeo = $rowgeo['fichier'];
    $rawmp4geo = str_replace(".flv", ".mp4", $rawflvgeo);
    $rawokgeo = $rawflvgeo . '**' . $rawmp4geo;


    $sqlmaxmind ="SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 1,2
                    ";
    $reqmaxmind = $bdd->query($sqlmaxmind)or die(print_r($bdd->errorInfo()));
    $rowmaxmind = $reqmaxmind ->fetch();
    $rawflvmaxmind = $rowmaxmind['fichier'];
    $rawmp4maxmind = str_replace(".flv", ".mp4", $rawflvmaxmind);
    $rawokmaxmind = $rawflvmaxmind . '**' . $rawmp4maxmind;

    if($count = $reqmaxmind->rowCount()==0)
    {
        $raw = $rawokgeo;
    }else{
        $raw = $rawokgeo.'&nbsp;||&nbsp;'.$rawokmaxmind;
    }
    $clean = preg_replace("/\s+/", "", $raw);
    $explode_playlists = explode("||", $clean);
    $returns = array();
    $cdn = "http://cdn.antenne.re";
    foreach ($explode_playlists as $playlist) {
        $explode_properties = explode("**", $playlist);
        if (count($explode_properties) === 2) {
            $file1 = $explode_properties[0]; //flv
            $file2 = $explode_properties[1]; //mp4
            if (strpos($explode_properties[0], "cdn.antenne.re") !== false && strpos($explode_properties[1], "cdn.antenne.re") !== false && strpos($explode_properties[0], "archivevideo") === false && strpos($explode_properties[1], "archivevideo") === false && strpos($explode_properties[0], "nas") === false && strpos($explode_properties[1], "nas") === false) {
                $returns[] = array(
                    "file1" => $cdn . secure_link(substr($file1, strlen($cdn))),
                    "file2" => $cdn . secure_link(substr($file2, strlen($cdn)))
                );
            } else {
                $returns[] = array(
                    "file1" => $file1,
                    "file2" => $file2
                );
            }
        }
    }
    $haystack = json_encode($returns);
    $last_crochet = strrpos($haystack, "]");
    $first_crochet = strpos($haystack, "[");
    if(isset($id_article)) {
        return substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
    }else{
        $crochet = "[]";
        return $crochet;
    }
}
function secure_link($file) {
    $secret = 'antenne_reunion'; // hash secret à changer et à nous communiquer
    $path = '/' . trim($file, '/');
    //$expire = time() + 90000; // durée d'expiration (1 heure ici)
    $expire = 1439108452;

    $md5 = base64_encode(md5($secret . $path . $expire, true));
    $md5 = strtr($md5, '+/', '-_');
    $md5 = str_replace('=', '', $md5);

    return $path . '?key=' . $md5 . '&e=' . $expire;
}