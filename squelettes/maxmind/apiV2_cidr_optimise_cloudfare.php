<?php
$id_article = filter_input(INPUT_GET, 'id_article', FILTER_SANITIZE_SPECIAL_CHARS);

// initialisation 
$cidrs = array();
$ip_add_manuals = array();
$Ip = get_client_ip();
$country = (isset($_SERVER["HTTP_CF_IPCOUNTRY"])) ? $_SERVER["HTTP_CF_IPCOUNTRY"] : 'UNKNOWN';
echo($Ip .' - '.$country);die();
$Ip = str_replace("::ffff:","",$Ip);
$GLOBALS['is974'] = 0;

// lecture fichier
$handle = @fopen("../geolocalisation/local_ranges", "r" );
if ($handle)
{
    while (!feof($handle))
    {
        $buffer = fgets($handle, 4096);
        $buffer = trim($buffer);
        if(strlen($buffer) >= 7 ){
          $cidrs[] = $buffer;
        }
    }
    fclose($handle);
}
$ip_manual = @fopen("../geolocalisation/manual", "r" );
if ($ip_manual)
{
    while (!feof($ip_manual))
    {
        $buffer_ip = fgets($ip_manual, 4096);
        $buffer_ip = trim($buffer_ip);
        if(strlen($buffer) >= 7 )
        {
         $ip_add_manuals[] = $buffer_ip;
        }
    }
    fclose($ip_manual);
}

function ipCIDRCheck($ip, $cidrs)
{
    $bool = false ;
    foreach ($cidrs as $cidr)
    {
        list ($net, $mask) = explode('/', $cidr);
        $ipNet = ip2long($net);
        $ipMask = ~((1 << (32 - $mask)) - 1);
        $ipIp = ip2long($ip);
        $ipIpNet = $ipIp & $ipMask;
        if ($ipIpNet == $ipNet)
        {
            $bool = true;
            break;
        }
   }
   return $bool;
}

function ipManualCheck($ip, $ip_add_manuals)
{
 return in_array($ip,$ip_add_manuals);
}
$authorize = false;
if ((ipCIDRCheck($Ip, $cidrs)) || (ipManualCheck($Ip,$ip_add_manuals))) {
    //L'IP est dans CIDR ou dans le fichier ajout manuel
   $authorize = true;
   $GLOBALS['is974'] = 1;
   //echo $authorize;
} else {
    //L'IP n'est pas dans CIDR - access denied
   $authorize = false;
   $GLOBALS['is974'] = 0;
   //echo $authorize;
}

if ($authorize) {
//if(true){
    //$source = VideoSourceToJSON($id_article);
    $secure_source = SecureVideoSourceToJSON($id_article);
    //var_dump($source);echo "<br/>";
    echo($secure_source);
} else {
    echo "0";
}

die;

function get_client_ip() {
    $ipaddress = '';
    if ($_SERVER["HTTP_CF_CONNECTING_IP"])  
        $ipaddress = $_SERVER["HTTP_CF_CONNECTING_IP"];
    else if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

function SecureVideoSourceToJSON($id_article) {
    //Connexion SQL
    $baselinfo='antenne_art_spip';
    $host='localhost';
    $user='antenne_spipAR';
    $pass='antv3_2009';

    try {
        $bdd = new PDO("mysql:host=$host;dbname=$baselinfo",$user,$pass);
    } catch(Exception $e) {
        exit('Impossible de se connecter &agrave; la base de donn&eacute;es.');
    }
    //Requête SQL
    $sqlgeo ="SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 0,1
                    ";
    $reqgeo = $bdd->query($sqlgeo)or die(print_r($bdd->errorInfo()));
    $rowgeo = $reqgeo ->fetch();
    $rawflvgeo = $rowgeo['fichier'];
    $rawmp4geo = str_replace(".flv", ".mp4", $rawflvgeo);
    $rawokgeo = $rawflvgeo . '**' . $rawmp4geo;


    $sqlmaxmind ="SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 1,2
                    ";
    $reqmaxmind = $bdd->query($sqlmaxmind)or die(print_r($bdd->errorInfo()));
    $rowmaxmind = $reqmaxmind ->fetch();
    $rawflvmaxmind = $rowmaxmind['fichier'];
    $rawmp4maxmind = str_replace(".flv", ".mp4", $rawflvmaxmind);
    $rawokmaxmind = $rawflvmaxmind . '**' . $rawmp4maxmind;

    if($count = $reqmaxmind->rowCount()==0)
    {
        $raw = $rawokgeo;
    }else{
        $raw = $rawokgeo.'&nbsp;||&nbsp;'.$rawokmaxmind;
    }
    $clean = preg_replace("/\s+/", "", $raw);
    $explode_playlists = explode("||", $clean);
    $returns = array();
    $cdn = "http://cdn.antenne.re";
    foreach ($explode_playlists as $playlist) {
        $explode_properties = explode("**", $playlist);
        if (count($explode_properties) === 2) {
            $file1 = $explode_properties[0]; //flv
            $file2 = $explode_properties[1]; //mp4
            if (strpos($explode_properties[0], "cdn.antenne.re") !== false && strpos($explode_properties[1], "cdn.antenne.re") !== false && strpos($explode_properties[0], "archivevideo") === false && strpos($explode_properties[1], "archivevideo") === false && strpos($explode_properties[0], "nas") === false && strpos($explode_properties[1], "nas") === false) {
                $returns[] = array(
                    "file1" => $cdn . secure_link(substr($file1, strlen($cdn))),
                    "file2" => $cdn . secure_link(substr($file2, strlen($cdn)))
                );
            } else {
                $returns[] = array(
                    "file1" => $file1,
                    "file2" => $file2
                );
            }
        }
    }
    $haystack = json_encode($returns);
    $last_crochet = strrpos($haystack, "]");
    $first_crochet = strpos($haystack, "[");
    if(isset($id_article)) {
        return substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
    }else{
        $crochet = "[]";
        return $crochet;
    }
}
function secure_link($file) {
    $secret = 'antenne_reunion'; // hash secret à changer et à nous communiquer
    $path = '/' . trim($file, '/');
    //$expire = time() + 90000; // durée d'expiration (1 heure ici)
    $expire = 1439108452;

    $md5 = base64_encode(md5($secret . $path . $expire, true));
    $md5 = strtr($md5, '+/', '-_');
    $md5 = str_replace('=', '', $md5);

    return $path . '?key=' . $md5 . '&e=' . $expire;
}