<?php
/*
194.250.129.161/32
194.3.180.40/29
194.3.187.95/32
194.3.187.69/32
194.3.183.8/29
194.3.187.60/32
194.3.181.208/28
194.3.182.32/28
194.3.187.91/32
194.3.187.21/32
212.234.102.9/32
81.252.50.176/30
194.250.129.179/32
194.3.176.32/29
194.3.187.15/32
194.3.180.144/29
 */
$cidrs = array();
$handle = @fopen("cidr.txt", "r" );
if ($handle)
{
    while (!feof($handle))
    {
        $buffer = fgets($handle, 4096);
        $cidrs[] = $buffer;
    }
    fclose($handle);
}
if ($cidrs)
{
    echo"<pre>";
    print_r($cidrs);
    echo"</pre>";
}
else
{
    echo "Le remplissage du tableau a �chou�";
}
$IpFromPHP = get_client_ip();

//$IpFromPHP = '194.3.187.21';
$IpFromPHP = str_replace("::ffff:","",$IpFromPHP);
//echo $IpFromPHP;
if (testUserIP($IpFromPHP, $cidrs)) {
    //L'IP est dans CIDR
    $varok1 = "OK";
    echo $varok1;
} else {
    //L'IP n'est pas dans CIDR - access denied
    $varok2 = "NOK";
    echo $varok2;
}

function testUserIP($user_ip, $cidrs) {
    $ipu = explode('.', $user_ip);
    foreach ($ipu as &$v)
        $v = str_pad(decbin($v), 8, '0', STR_PAD_LEFT);
    $ipu = join('', $ipu);
    $res = false;
    foreach ($cidrs as $cidr) {
        $parts = explode('/', $cidr);
        $ipc = explode('.', $parts[0]);
        foreach ($ipc as &$v) $v = str_pad(decbin($v), 8, '0', STR_PAD_LEFT);
        $ipc = substr(join('', $ipc), 0, $parts[1]);
        $ipux = substr($ipu, 0, $parts[1]);
        $res = ($ipc === $ipux);
        if ($res) break;
    }
    return $res;
}

function get_client_ip() {
    $ipaddress = "";
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}
function appel_curl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_TIMEOUT, 720000);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}