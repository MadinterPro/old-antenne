<?php

echo "MG";
die;

require_once 'vendor/autoload.php';
require_once 'RemoteAddress.php';

use GeoIp2\Database\Reader;

//
$reader = new Reader('GeoIP2-Country.mmdb');
//$ip = filter_input(INPUT_GET, 'ip', FILTER_SANITIZE_SPECIAL_CHARS);

try {
    $IpFromPHP = get_client_ip();
    $record = $reader->country($IpFromPHP);
    $isoCode = $record->country->isoCode;
    if (strtoupper($isoCode) === "RE") {
        echo $isoCode;
        die;
    }
} catch (GeoIp2\Exception\AddressNotFoundException $e) {
}

try {
    $IpFromSPIP = $GLOBALS["_ENV"]["GEOIP_ADDR"];
    $record = $reader->country($IpFromSPIP);
    $isoCode = $record->country->isoCode;
    if (strtoupper($isoCode) === "RE") {
        echo $isoCode;
        die;
    }
} catch (GeoIp2\Exception\AddressNotFoundException $e) {
}

try {
    $IpFromZend = IpFromZendFramework();
    $record = $reader->country(IpFromZendFramework($IpFromZend));
    $isoCode = $record->country->isoCode;
    if (strtoupper($isoCode) === "RE") {
        echo $isoCode;
        die;
    }
} catch (GeoIp2\Exception\AddressNotFoundException $e) {
}

echo "IP non autoris&eacute; : ".$IpFromPHP." - ".$IpFromSPIP." - ".$IpFromZend;
die;

function IpFromZendFramework() {
    $o = new RemoteAddress();
    return $o->getIpAddress();
}

function get_client_ip() {
    $ipaddress = '';
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}
