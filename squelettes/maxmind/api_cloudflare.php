<?php
header("Access-Control-Allow-Origin: *");
$userCountry = false;
//Check to see if HTTP_CF_IPCOUNTRY exists
if(isset($_SERVER["HTTP_CF_IPCOUNTRY"])){
    //If it is exists, use it.
    $userCountry = $_SERVER["HTTP_CF_IPCOUNTRY"];
    echo $userCountry;
}

//Getting the CF-Connecting-IP header in PHP.
$ipAddress = $_SERVER["HTTP_CF_CONNECTING_IP"];

echo $ipAddress;