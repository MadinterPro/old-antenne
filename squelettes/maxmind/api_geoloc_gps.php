<?php
header("Access-Control-Allow-Origin: *");
//Database configuration
define('DB_HOST', 'localhost');
define('DB_NAME', 'antenne_art_spip');
define('DB_USER', 'antenne_spipAR');
define('DB_PASSWORD', 'antv3_2009');


$id_article = (isset($_GET['id_article']) && $_GET['id_article']) ? $_GET['id_article'] : null;
$country = (isset($_GET['country']) && $_GET['country']) ? $_GET['country'] : null;
$lat = (isset($_GET['lat']) && $_GET['lat']) ? $_GET['lat'] : null;
$lng = (isset($_GET['lng']) && $_GET['lng']) ? $_GET['lng'] : null;

// country is set in query
if ($country && $country == 'RE') {
    echo "OK";
    /*if ($id_article) {
        echo secureVideoSourceToJSON($id_article);
    } else {
        echo "OK";
    }*/
}
// lat and lng is set in query
elseif ($lat && $lng) {
    $country = getCountry($lat, $lng);
    if ($country && $country == 'RE') {
        echo "OK";
        /*if ($id_article) {
            echo secureVideoSourceToJSON($id_article);
        } else {
            echo "OK";
        }*/
    } else {
        echo "NOK";
        /*if ($id_article) {
            echo "0";
        } else {
            echo "NOK";
        }*/
    }
}
//country, lat and lng not set in query
else {
    echo "NOK";
    /*if ($id_article) {
        echo "0";
    } else {
        echo "NOK";
    }*/
}

/**
 * Get country by geocode
 * @param $lat
 * @param $lng
 * @return string|null
 */
function getCountry($lat, $lng) {
    $country = null;
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&key=AIzaSyD_p5eZGXDEBZF2-6o1gxjtk9YVsBFBLJU';
    $contents = json_decode(file_get_contents($url));
    foreach ($contents->results as $result) {
        foreach ($result->address_components as $address) {
            if (in_array('country', $address->types)) {
                $country = $address->short_name;
            }
        }
    }

    return $country;
}

/**
 * Get secure video source
 * @param $id_article
 * @return bool|string
 */
function secureVideoSourceToJSON($id_article) {
    //Connexion SQL
    try {
        $bdd = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    } catch(Exception $e) {
        exit('Impossible de se connecter &agrave; la base de donn&eacute;es.');
    }
    //Requête SQL
    $sqlgeo = "SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 0,1
                    ";
    $reqgeo = $bdd->query($sqlgeo) or die(print_r($bdd->errorInfo()));
    $rowgeo = $reqgeo->fetch();
    $rawflvgeo = $rowgeo['fichier'];
    $rawmp4geo = str_replace(".flv", ".mp4", $rawflvgeo);
    $rawokgeo = $rawflvgeo . '**' . $rawmp4geo;


    $sqlmaxmind = "SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 1,2
                    ";
    $reqmaxmind = $bdd->query($sqlmaxmind) or die(print_r($bdd->errorInfo()));
    $rowmaxmind = $reqmaxmind->fetch();
    $rawflvmaxmind = $rowmaxmind['fichier'];
    $rawmp4maxmind = str_replace(".flv", ".mp4", $rawflvmaxmind);
    $rawokmaxmind = $rawflvmaxmind . '**' . $rawmp4maxmind;

    if ($count = $reqmaxmind->rowCount() == 0) {
        $raw = $rawokgeo;
    } else {
        $raw = $rawokgeo.'&nbsp;||&nbsp;'.$rawokmaxmind;
    }
    $clean = preg_replace("/\s+/", "", $raw);
    $explode_playlists = explode("||", $clean);
    $returns = array();
    $cdn = "http://cdn.antenne.re";
    foreach ($explode_playlists as $playlist) {
        $explode_properties = explode("**", $playlist);
        if (count($explode_properties) === 2) {
            $file1 = $explode_properties[0]; //flv
            $file2 = $explode_properties[1]; //mp4
            if (strpos($explode_properties[0], "cdn.antenne.re") !== false && strpos($explode_properties[1], "cdn.antenne.re") !== false && strpos($explode_properties[0], "archivevideo") === false && strpos($explode_properties[1], "archivevideo") === false && strpos($explode_properties[0], "nas") === false && strpos($explode_properties[1], "nas") === false) {
                $returns[] = array(
                    "file1" => $cdn . secureLink(substr($file1, strlen($cdn))),
                    "file2" => $cdn . secureLink(substr($file2, strlen($cdn)))
                );
            } else {
                $returns[] = array(
                    "file1" => $file1,
                    "file2" => $file2
                );
            }
        }
    }
    $haystack = json_encode($returns);
    $last_crochet = strrpos($haystack, "]");
    $first_crochet = strpos($haystack, "[");
    if (isset($id_article)) {
        return substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
    } else {
        $crochet = "[]";
        return $crochet;
    }
}

/**
 * Generate secure link
 * @param $file
 * @return string
 */
function secureLink($file) {
    $secret = 'antenne_reunion'; // hash secret à changer et à nous communiquer
    $path = '/' . trim($file, '/');
    //$expire = time() + 90000; // durée d'expiration (1 heure ici)
    $expire = 1439108452;

    $md5 = base64_encode(md5($secret . $path . $expire, true));
    $md5 = strtr($md5, '+/', '-_');
    $md5 = str_replace('=', '', $md5);

    return $path . '?key=' . $md5 . '&e=' . $expire;
}

