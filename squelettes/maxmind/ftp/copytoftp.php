<!DOCTYPE html>
<!--
    Copie fichier vers un serveur FTP
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

        <?php
        $ftp_server = "127.0.0.1";
        $ftp_user_name = "serveur_B_user";
        $ftp_user_pass = "serveur_B_pass";
        $conn_id = ftp_connect($ftp_server);

        $repertoire_zip = "G:/serveur_A/zips/"; //Chemin du dossier des fichiers zips
        $files_zip = scandir($repertoire_zip); //Liste la liste des fichiers dans le tableau $files_zip

        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
        if (is_array($files_zip)) {       
            foreach ($files_zip as $files) {                
                if ($files != "." && $files != "..") {
                    $path = $repertoire_zip . $files;
                    $fp = fopen($path, 'r');    
                    if (ftp_fput($conn_id, $files, $fp, FTP_BINARY)) { //Pour modifier le répertoire destination "test/".$files
                        echo "<div style='color:green'>Chargement avec succès du fichier $files </div>";
                    } else {
                        echo "<div style='color:red'>Il y a eu un problème lors du chargement du fichier $files </div>";
                    }
                    fclose($fp);
                }                
            }
        }
        ftp_close($conn_id);
        ?>

    </body>
</html>