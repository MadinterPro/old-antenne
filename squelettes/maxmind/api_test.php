<?php
/*
$ip = $_SERVER['REMOTE_ADDR']; // Recuperation de l'IP du visiteur
$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip)); //connection au serveur de ip-api.com et recuperation des données
if($query && $query['status'] == 'success')
{
    //code avec les variables
    echo "Bonjour visiteur de " . $query['country'] . "," . $query['city'];
}
194.250.129.161/32
194.3.180.40/29
194.3.187.95/32
194.3.187.69/32
194.3.183.8/29
194.3.187.60/32
194.3.181.208/28
194.3.182.32/28
194.3.187.91/32
194.3.187.21/32
212.234.102.9/32
81.252.50.176/30
194.250.129.179/32
194.3.176.32/29
194.3.187.15/32
194.3.180.144/29

 */
function testUserIP($user_ip, $cidrs) {
    $ipu = explode('.', $user_ip);
    foreach ($ipu as &$v)
        $v = str_pad(decbin($v), 8, '0', STR_PAD_LEFT);
    $ipu = join('', $ipu);
    $res = false;
    foreach ($cidrs as $cidr) {
        $parts = explode('/', $cidr);
        $ipc = explode('.', $parts[0]);
        foreach ($ipc as &$v) $v = str_pad(decbin($v), 8, '0', STR_PAD_LEFT);
        $ipc = substr(join('', $ipc), 0, $parts[1]);
        $ipux = substr($ipu, 0, $parts[1]);
        $res = ($ipc === $ipux);
        if ($res) break;
    }
    return $res;
}
function get_client_ip() {
    $ipaddress = '';
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

//$Ip = get_client_ip();
$Ip = '212.234.102.9';
$Ip = str_replace("::ffff:","",$Ip);
echo $Ip;
$cidrs = array('212.234.102.9/32', '194.3.183.8/29');
if (testUserIP($Ip, $cidrs)) {
    //user ip is ok
    echo "user ip is ok<br>";
} else {
    //access denied
    echo "access denied<br>";
}
$details = json_decode(file_get_contents("http://ipinfo.io/{$Ip}"));
echo $details->country; // -> "RE ou FR"
function get_country($ip) {
    return file_get_contents("http://ipinfo.io/{$ip}/country");
}
echo "Isocode : ".get_country($Ip); // => US