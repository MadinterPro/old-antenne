<?php
function file_get_contents_curl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}
$raw = file_get_contents_curl("http://www.antennereunion.fr/spip.php?page=geoloc&var_mode=calcul&id_article=342048");
$clean = preg_replace("/\s+/", "", $raw);
$explode_playlists = explode("||", $clean);
$returns = array();
$cdn = "http://cdn.antenne.re";
foreach ($explode_playlists as $playlist) {
$explode_properties = explode("**", $playlist);
if (count($explode_properties) === 2) {
$file1 = $explode_properties[0]; //flv
$file2 = $explode_properties[1]; //mp4
if (strpos($explode_properties[0], "cdn.antenne.re") !== false && strpos($explode_properties[1], "cdn.antenne.re") !== false && strpos($explode_properties[0], "archivevideo") === false && strpos($explode_properties[1], "archivevideo") === false && strpos($explode_properties[0], "nas") === false && strpos($explode_properties[1], "nas") === false) {
$returns[] = array(
"file1" => $cdn . secure_link(substr($file1, strlen($cdn))),
"file2" => $cdn . secure_link(substr($file2, strlen($cdn)))
);
} else {
$returns[] = array(
"file1" => $file1,
"file2" => $file2
);
}
}
}
$haystack = json_encode($returns);
$last_crochet = strrpos($haystack, "]");
$first_crochet = strpos($haystack, "[");
$res =  substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
var_dump($res);