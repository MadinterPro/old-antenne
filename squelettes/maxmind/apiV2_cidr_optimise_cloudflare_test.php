<?php
//Database configuration
define('DB_HOST', 'localhost');
define('DB_NAME', 'antenne_art_spip');
define('DB_USER', 'antenne_spipAR');
define('DB_PASSWORD', 'antv3_2009');

//Get article ID
$id_article = filter_input(INPUT_GET, 'id_article', FILTER_SANITIZE_SPECIAL_CHARS);

//Get customer ip address with status and country if exist
$ipAddress = getIpAddress();

//Check if ip address is allowed in file [local_range|manual]
$authorized = checkIpAddress($ipAddress);

//Update json file
updateJsonFile($ipAddress, $authorized);

//Return secure video
if ($authorized) {
    $secure_source = secureVideoSourceToJSON($id_article);
    if ($id_article) {
        echo $secure_source;
    } else {
        echo "OK";
    }
} else {
    if ($id_article) {
        echo "0";
    } else {
        echo "NOK";
    }
}

/**
 * Get IP Address
 * @return array
 */
function getIpAddress()
{
    $response = array(
        'ip' => $_SERVER['REMOTE_ADDR'],
        'status' => false
    );

    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $response['ip'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        $response['status'] = true;

        if (isset($_SERVER["HTTP_CF_IPCOUNTRY"])) {
            $response['country'] = $_SERVER["HTTP_CF_IPCOUNTRY"];
        }
    }

    return $response;
}

/**
 * Get allowed IPs
 * @param $filename
 * @return array
 */
function getAllowedIPs($filename)
{
    $ips = array();
    $file = fopen("../geolocalisation/".$filename, 'r');
    if ($file) {
        while (!feof($file)) {
            $buffer = fgets($file, 4096);
            $buffer = trim($buffer);
            if (strlen($buffer) >= 7 ) {
                $ips[] = $buffer;
            }
        }
        fclose($file);
    }

    return $ips;
}

/**
 * Check ip address
 * @param $ipAddress
 * @return bool
 */
function checkIpAddress($ipAddress)
{
    $authorized = false;
    $allowedIPs_manual = getAllowedIPs('manual');
    $allowedIPs_local_range = getAllowedIPs('local_range');
    if ($ipAddress['status']) {
        //check in manual
        if (in_array($ipAddress['ip'], $allowedIPs_manual)) {
            $authorized = true;
        }

        //check in local_range
        foreach ($allowedIPs_local_range as $cidr) {
            list ($net, $mask) = explode('/', $cidr);
            $ipNet = ip2long($net);
            $ipMask = ~((1 << (32 - $mask)) - 1);
            $ipIp = ip2long($ipAddress['ip']);
            $ipIpNet = $ipIp & $ipMask;
            if ($ipIpNet == $ipNet) {
                $authorized = true;
                break;
            }
        }
    }

    return $authorized;
}

/**
 * Get secure video source
 * @param $id_article
 * @return bool|string
 */
function secureVideoSourceToJSON($id_article) {
    //Connexion SQL
    try {
        $bdd = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    } catch(Exception $e) {
        exit('Impossible de se connecter &agrave; la base de donn&eacute;es.');
    }
    //Requête SQL
    $sqlgeo = "SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 0,1
                    ";
    $reqgeo = $bdd->query($sqlgeo) or die(print_r($bdd->errorInfo()));
    $rowgeo = $reqgeo->fetch();
    $rawflvgeo = $rowgeo['fichier'];
    $rawmp4geo = str_replace(".flv", ".mp4", $rawflvgeo);
    $rawokgeo = $rawflvgeo . '**' . $rawmp4geo;


    $sqlmaxmind = "SELECT documents.fichier, documents.id_document FROM spip_documents AS `documents`
                    INNER JOIN spip_documents_liens AS `L1` ON ( L1.id_document = documents.id_document )
                    WHERE (documents.statut = 'publie')
                    AND documents.date_publication <= NOW()
                    AND (documents.mode IN ('image','document'))
                    AND (documents.taille > 0 OR documents.distant='oui')
                    AND (L1.objet = 'article')
                    AND (L1.id_objet = '".$id_article."')
                    AND ((documents.extension IN ('flv','mp4')))
                    LIMIT 1,2
                    ";
    $reqmaxmind = $bdd->query($sqlmaxmind) or die(print_r($bdd->errorInfo()));
    $rowmaxmind = $reqmaxmind->fetch();
    $rawflvmaxmind = $rowmaxmind['fichier'];
    $rawmp4maxmind = str_replace(".flv", ".mp4", $rawflvmaxmind);
    $rawokmaxmind = $rawflvmaxmind . '**' . $rawmp4maxmind;

    if ($count = $reqmaxmind->rowCount() == 0) {
        $raw = $rawokgeo;
    } else {
        $raw = $rawokgeo.'&nbsp;||&nbsp;'.$rawokmaxmind;
    }
    $clean = preg_replace("/\s+/", "", $raw);
    $explode_playlists = explode("||", $clean);
    $returns = array();
    $cdn = "http://cdn.antenne.re";
    foreach ($explode_playlists as $playlist) {
        $explode_properties = explode("**", $playlist);
        if (count($explode_properties) === 2) {
            $file1 = $explode_properties[0]; //flv
            $file2 = $explode_properties[1]; //mp4
            if (strpos($explode_properties[0], "cdn.antenne.re") !== false && strpos($explode_properties[1], "cdn.antenne.re") !== false && strpos($explode_properties[0], "archivevideo") === false && strpos($explode_properties[1], "archivevideo") === false && strpos($explode_properties[0], "nas") === false && strpos($explode_properties[1], "nas") === false) {
                $returns[] = array(
                    "file1" => $cdn . secureLink(substr($file1, strlen($cdn))),
                    "file2" => $cdn . secureLink(substr($file2, strlen($cdn)))
                );
            } else {
                $returns[] = array(
                    "file1" => $file1,
                    "file2" => $file2
                );
            }
        }
    }
    $haystack = json_encode($returns);
    $last_crochet = strrpos($haystack, "]");
    $first_crochet = strpos($haystack, "[");
    if (isset($id_article)) {
        return substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
    } else {
        $crochet = "[]";
        return $crochet;
    }
}

/**
 * Generate secure link
 * @param $file
 * @return string
 */
function secureLink($file) {
    $secret = 'antenne_reunion'; // hash secret à changer et à nous communiquer
    $path = '/' . trim($file, '/');
    //$expire = time() + 90000; // durée d'expiration (1 heure ici)
    $expire = 1439108452;

    $md5 = base64_encode(md5($secret . $path . $expire, true));
    $md5 = strtr($md5, '+/', '-_');
    $md5 = str_replace('=', '', $md5);

    return $path . '?key=' . $md5 . '&e=' . $expire;
}

/**
 * Update success.json or failed.json
 * @param $ipAddress
 * @param $authorized
 */
function updateJsonFile($ipAddress, $authorized)
{
    $data = array();
    if ($ipAddress['status']) {
        if ($authorized) {
            $file = "../geolocalisation/success.json";
        } else {
            $file = "../geolocalisation/failed.json";
        }
    } else {
        $file = "../geolocalisation/other.json";
    }

    if (is_file($file)) {
        $data = json_decode(file_get_contents($file), true);
        if (!array_key_exists($ipAddress['ip'], $data)) {
            $data[$ipAddress['ip']] = '';
            if ($ipAddress['country']) {
                $data[$ipAddress['ip']] = $ipAddress['country'];
            }
            file_put_contents($file, json_encode($data));
        }
    } else {
        $data[$ipAddress['ip']] = '';
        if ($ipAddress['country']) {
            $data[$ipAddress['ip']] = $ipAddress['country'];
        }
        file_put_contents($file, json_encode($data));
    }
}
