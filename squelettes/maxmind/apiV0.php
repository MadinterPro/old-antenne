<?php

$id_article = filter_input(INPUT_GET, 'id_article', FILTER_SANITIZE_SPECIAL_CHARS);
require_once 'vendor/autoload.php';
require_once 'RemoteAddress.php';

use GeoIp2\Database\Reader;

//
$reader = new Reader('GeoIP2-Country.mmdb');
//$ip = filter_input(INPUT_GET, 'ip', FILTER_SANITIZE_SPECIAL_CHARS);


$authorize = false;
$Ip = "";
try {
    $Ip = get_client_ip();
    $record = $reader->country($Ip);
    $isoCode = $record->country->isoCode;
    if (strtoupper($isoCode) === "RE") {
        $authorize = true;
    }
} catch (GeoIp2\Exception\AddressNotFoundException $e) {
    
}

if (!$authorize) {
    try {
        $Ip = $GLOBALS["_ENV"]["GEOIP_ADDR"]; //from spip
        $record = $reader->country($Ip);
        $isoCode = $record->country->isoCode;
        if (strtoupper($isoCode) === "RE") {
            $authorize = true;
        }
    } catch (GeoIp2\Exception\AddressNotFoundException $e) {
        
    }
}

if (!$authorize) {
    try {
        $Ip = IpFromZendFramework();
        $record = $reader->country(IpFromZendFramework($Ip));
        $isoCode = $record->country->isoCode;
        if (strtoupper($isoCode) === "RE") {
            $authorize = true;
        }
    } catch (GeoIp2\Exception\AddressNotFoundException $e) {
        
    }
}

//Pour autoriser un IP
/*
if($Ip == "41.21.54.58"){
    $authorize = true;
}
*/

if($Ip == "41.188.49.140"){
    $authorize = true;
}

if($Ip == "80.11.237.202"){
    $authorize = true;
}

if ($authorize) {
//if(true){
    //$source = VideoSourceToJSON($id_article);
    $secure_source = SecureVideoSourceToJSON($id_article);
    //var_dump($source);echo "<br/>";
    echo($secure_source);
} else {
    echo "0";
}

die;

function IpFromZendFramework() {
    $o = new RemoteAddress();
    return $o->getIpAddress();
}

function get_client_ip() {
    $ipaddress = '';
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

function VideoSourceToJSON($id_article) {
    $raw = file_get_contents("http://www.antennereunion.fr/spip.php?page=geoloc&var_mode=calcul&id_article=" . $id_article);
    $clean = preg_replace("/\s+/", "", $raw);
    $explode_playlists = explode("||", $clean);
    $returns = array();
    foreach ($explode_playlists as $playlist) {
        $explode_properties = explode("**", $playlist);
        if (count($explode_properties) === 2) {
            $file1 = $explode_properties[0]; //flv
            $file2 = $explode_properties[1]; //mp4
            $returns[] = array(
                "file1" => $file1,
                "file2" => $file2
            );
        }
    }
    $haystack = json_encode($returns);
    $last_crochet = strrpos($haystack, "]");
    $first_crochet = strpos($haystack, "[");
    return substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
}

function SecureVideoSourceToJSON($id_article) {
    $raw = file_get_contents("http://www.antennereunion.fr/spip.php?page=geoloc&var_mode=calcul&id_article=" . $id_article);
    $clean = preg_replace("/\s+/", "", $raw);
    $explode_playlists = explode("||", $clean);
    $returns = array();
    $cdn = "http://cdn.antenne.re";
    foreach ($explode_playlists as $playlist) {
        $explode_properties = explode("**", $playlist);
        if (count($explode_properties) === 2) {
            $file1 = $explode_properties[0]; //flv
            $file2 = $explode_properties[1]; //mp4
            $returns[] = array(
                "file1" => $cdn.secure_link(substr($file1, strlen($cdn)) ),
                "file2" => $cdn.secure_link(substr($file2, strlen($cdn)) )
            );
        }
    }
    $haystack = json_encode($returns);
    $last_crochet = strrpos($haystack, "]");
    $first_crochet = strpos($haystack, "[");
    return substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
}

/*
function secure_link($file) {
    //$last_index = strrchr($file);
    //$new_file = substr($file, 0, $last_index - 1).substr($file, $last_index + 1, strlen($file));
    //var_dump($new_file);die;
    $secret = 'antenne_reunion'; // hash secret à changer et à nous communiquer
    $path = '/' . trim($file, '/');
    
    var_dump("path = ".$path);die;
    
    $expire = time() + 90000; // durée d'expiration (1 heure ici)

    $md5 = base64_encode(md5($secret . $path . $expire, true));
    $md5 = strtr($md5, '+/', '-_');
    $md5 = str_replace('=', '', $md5);

    return $path . '?key=' . $md5 . '&e=' . $expire;
}
*/

function secure_link($file) {
    $cdn = "http://cdn.antenne.re";
    if (strlen($file) > strlen($cdn)) {
        $file = substr($file, strlen($cdn));
        $secret = 'antenne_reunion'; // hash secret à changer et à nous communiquer
        $path = '/' . trim($file, '/');
        //$expire = time() + 3600; // durée d'expiration (1 heure ici)
        //$expire = time() + 108000; // durée d'expiration (30 jours ici)
        //$expire = 1431411406; // 13 Mai 2015
        //$expire = 1434263104;
        $expire = 1439108452; //09 Aout 2015

        $md5 = base64_encode(md5($secret . $path . $expire, true));
        $md5 = strtr($md5, '+/', '-_');
        $md5 = str_replace('=', '', $md5);

        return $cdn . $path . '?key=' . $md5 . '&e=' . $expire;
    } else {
        return "";
    }
}
