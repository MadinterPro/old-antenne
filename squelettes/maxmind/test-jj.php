<?php

$id_article = filter_input(INPUT_GET, 'id_article', FILTER_SANITIZE_SPECIAL_CHARS);

require_once 'vendor/autoload.php';
require_once 'RemoteAddress.php';

use GeoIp2\Database\Reader;

//
$reader = new Reader('GeoIP2-Country.mmdb');
//$ip = filter_input(INPUT_GET, 'ip', FILTER_SANITIZE_SPECIAL_CHARS);


$authorize = false;
$Ip = array();
array_push($Ip, "80.8.78.164","82.197.96.77","5.102.75.6", "80.69.208.133","80.11.237.202","213.223.45.36","81.248.205.119","213.222.123.167","154.67.57.237","195.115.117.7","168.253.178.110","81.248.207.155","90.10.254.74","80.8.44.77","80.8.0.31","80.8.45.71","80.8.80.227","81.248.219.49","80.8.108.173","90.10.213.144","90.10.252.73","90.10.247.113","109.122.173.191","90.10.11.236","90.10.249.182","90.10.254.62","90.10.125.108","90.10.125.134","82.197.104.27","90.10.122.65","81.248.219.104","90.10.253.122","81.248.208.116","90.10.82.172","90.10.247.8","90.10.251.86","90.10.250.95","89.16.13.148","81.248.251.228","90.10.209.143","90.10.85.175","81.248.229.199","81.248.229.63","81.248.255.159","90.10.252.140","80.69.208.133","195.115.117.9","90.10.123.254","90.10.185.60","81.248.255.132","81.248.212.102","90.10.11.47","80.8.1.216","66.249.93.87","81.248.223.209","90.10.252.201","90.10.249.41","90.10.119.227","80.8.110.58","9.10.15.17","80.8.76.218","109.122.145.5","90.10.254.240","90.10.125.158","90.10.85.184","154.67.91.235","81.248.226.209");

for ($i = 0; $i <= 100; $i++) {
	try {
		$record = $reader->country($Ip[$i]);
		$isoCode = $record->country->isoCode;
		if (strtoupper($isoCode) === "RE") {
			$authorize = true;
		}
	} catch (GeoIp2\Exception\AddressNotFoundException $e) {
	}
	echo "IP = " . $Ip[$i] . "<br />\nISOCODE = " . $isoCode ."<br />\n<br />\n";
}
die;

function IpFromZendFramework() {
    $o = new RemoteAddress();
    return $o->getIpAddress();
}

function get_client_ip() {
    $ipaddress = '';
    if ($GLOBALS['_SERVER']['HTTP_X_REAL_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_REAL_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_CLIENT_IP'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_CLIENT_IP'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_X_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_X_FORWARDED'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED_FOR'];
    else if ($GLOBALS['_SERVER']['HTTP_FORWARDED'])
        $ipaddress = $GLOBALS['_SERVER']['HTTP_FORWARDED'];
    else if ($GLOBALS['_SERVER']['REMOTE_ADDR'])
        $ipaddress = $GLOBALS['_SERVER']['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

function VideoSourceToJSON($id_article) {
    //$raw = file_get_contents("http://www.antennereunion.fr/spip.php?page=geoloc&var_mode=calcul&id_article=" . $id_article);
    $clean = preg_replace("/\s+/", "", $raw);
    $explode_playlists = explode("||", $clean);
    $returns = array();
    foreach ($explode_playlists as $playlist) {
        $explode_properties = explode("**", $playlist);
        if (count($explode_properties) === 2) {
            $file1 = $explode_properties[0]; //flv
            $file2 = $explode_properties[1]; //mp4
            $returns[] = array(
                "file1" => $file1,
                "file2" => $file2
            );
        }
    }
    $haystack = json_encode($returns);
    $last_crochet = strrpos($haystack, "]");
    $first_crochet = strpos($haystack, "[");
    return substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
}

function SecureVideoSourceToJSON($id_article) {
    //file_get_contents("http://www.antennereunion.fr/spip.php?page=geoloc&var_mode=calcul&id_article=" . $id_article);
	echo $raw;
    $clean = preg_replace("/\s+/", "", $raw);
    $explode_playlists = explode("||", $clean);
    $returns = array();
    $cdn = "http://cdn.antenne.re";
    foreach ($explode_playlists as $playlist) {
        $explode_properties = explode("**", $playlist);
        if (count($explode_properties) === 2) {
            $file1 = $explode_properties[0]; //flv
            $file2 = $explode_properties[1]; //mp4
            if (strpos($explode_properties[0], "cdn.antenne.re") !== false && strpos($explode_properties[1], "cdn.antenne.re") !== false && strpos($explode_properties[0], "archivevideo") === false && strpos($explode_properties[1], "archivevideo") === false && strpos($explode_properties[0], "nas") === false && strpos($explode_properties[1], "nas") === false) {
                $returns[] = array(
                    "file1" => $cdn . secure_link(substr($file1, strlen($cdn))),
                    "file2" => $cdn . secure_link(substr($file2, strlen($cdn)))
                );
            } else {
                $returns[] = array(
                    "file1" => $file1,
                    "file2" => $file2
                );
            }
        }
    }
    $haystack = json_encode($returns);
    $last_crochet = strrpos($haystack, "]");
    $first_crochet = strpos($haystack, "[");
    return substr($haystack, $first_crochet, $last_crochet - $first_crochet + 1);
}

function secure_link($file) {
    $secret = 'antenne_reunion'; // hash secret à changer et à nous communiquer
    $path = '/' . trim($file, '/');
    //$expire = time() + 90000; // durée d'expiration (1 heure ici)
    $expire = 1439108452;

    $md5 = base64_encode(md5($secret . $path . $expire, true));
    $md5 = strtr($md5, '+/', '-_');
    $md5 = str_replace('=', '', $md5);

    return $path . '?key=' . $md5 . '&e=' . $expire;
}


?>