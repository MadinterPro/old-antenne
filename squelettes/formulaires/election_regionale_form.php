<?php
define("DATA_BASE_TABLE_NEWSLETTER", "spip_nl_user");
define("DATA_BASE_TABLE_USER_NEWSLETTER", "spip_nl_user_letter");
define("DATA_BASE_TABLE_ENREGISTREMENT_QUESTIONNAIRE", "spip_questionnaire_debat");

function formulaires_election_regionale_form_charger_dist() {
    $valeurs = array(
        'nom' => '',
        'prenom' => '',
        'mail' => '',
        'datenaissance' => '',
        'profession' => '',
        'newsletter' => '',
    );
    return $valeurs;
}

function formulaires_election_regionale_form_verifier_dist() {
    $erreurs = array();
    foreach (array('nom', 'prenom', 'mail', 'datenaissance', 'profession') as $obligatoire)
        if (!_request($obligatoire))
            $erreurs[$obligatoire] = 'Ce champ est obligatoire';

    include_spip('inc/filtres');
    if (_request('mail') AND ! email_valide(_request('mail')))
        $erreurs['mail'] = 'Adresse email non valide';

    $id_exist = sql_getfetsel('id_reservation', array(DATA_BASE_TABLE_ENREGISTREMENT_QUESTIONNAIRE), array("mail = '" . trim(_request('mail')) . "'"));
    if (!is_null($id_exist)) {
        $erreurs['mail'] = 'L\'adresse mail est déjà utilisé.';
    }
    if (count($erreurs))
        $erreurs['message_erreur'] = 'Votre saisie contient des erreurs !';
    return $erreurs;
}

/**
 *
 * @return type
 *
 */
function formulaires_election_regionale_form_traiter_dist() {
    include_spip('inc/session');
    $nom = ucfirst(trim(addslashes(_request('nom'))));
    $prenom = ucfirst(trim(addslashes(_request('prenom'))));
    $mail = trim(_request('mail'));
    $date = split('/', _request('datenaissance'));
    $timestamp = mktime(0, 0, 0, $date[1], $date[0], $date[2]);
    $datenaissance = date("Y-m-d H:i:s", $timestamp);
    $profession = ucfirst(trim(addslashes(_request('profession'))));


    $return_enregistrement = sql_insertq(DATA_BASE_TABLE_ENREGISTREMENT_QUESTIONNAIRE, array(
        'nom' => $nom,
        'prenom' => $prenom,
        'mail' => $mail,
        'datenaissance' => $datenaissance,
        'profession' => $profession,
        'date_reservation' => date("Y-m-d H:i:s")
    ));
    if (!is_null($return_enregistrement)) {
        //Envoie de mail
        $to = $mail;
        $subject = 'Antenne Réunion – Débat des Régionales';
        $texte = '<tr><td><font face="Arial" size="3" color="#333333">Bonjour,</font></td></tr>
        <tr><td height="12">&nbsp;</td></tr>
        <tr><td><font face="Arial" size="3" color="#333333">Le d&eacute;bat entre les candidats des &eacute;lections r&eacute;gionales : c\'est ce soir en direct sur <i><strong><a href="http://www.antennereunion.fr/" target="_blank" color="#333333">Antenne R&eacute;union.</a></strong></i></font></td></tr>
        <tr><td height="12">&nbsp;</td></tr>
        <tr><td><font face="Arial" size="3" color="#333333"><strong>90 minutes pour convaincre, en direct sur Antenne R&eacute;union à 19h50.</strong></font></td></tr>
        <tr><td height="29">&nbsp;</td></tr>';
        $envoi = template_mail($to, $subject, utf8_decode($texte));
        //Fin de l'envoi de mail
        $id_reservation = mysql_insert_id();
        session_set('id_reservation', $id_reservation);
        if (_request('newsletter') == "on") {
            $id_exist = sql_getfetsel('id', array(DATA_BASE_TABLE_NEWSLETTER), array("mail_user = '" . trim(_request('mail')) . "'"));
            if (is_null($id_exist)) {
                $last__newsletterid = sql_insertq(DATA_BASE_TABLE_NEWSLETTER, array(
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'mail_user' => $mail,
                    'statut' => 1
                ));
                sql_insertq(DATA_BASE_TABLE_USER_NEWSLETTER, array(
                    'id_user' => $last__newsletterid,
                    'id_letter' => 2
                ));
            } else {
                sql_update(DATA_BASE_TABLE_NEWSLETTER, array('nom' => $nom, 'prenom' => $prenom), "id=$id_exist");
            }
            formulaires_election_regionale_form_charger_dist();
            if ($envoi) {
                session_set('session_inscription_ok', 'ok');
                return array('message_ok' => 'Votre inscription est validée.Vous allez recevoir une confirmation par mail. Il vous faudra l’imprimer et la présenter à  l’accueil,le 9 décembre.');
            } else {
                session_set('session_inscription_ok', 'ok');
                return array('message_erreur' => 'Votre reservation a bien été enregistrée mais le mail de confirmation n\'a pas pu être envoyé.Merci!');
            }
        }
    } else {
        return array('message_erreur' => 'Veuillez imprimer votre mail de réservation.');
    }
}
