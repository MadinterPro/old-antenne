<?php

function formulaires_oui_charger_dist() {
    $valeurs = array(
        'numero' => '',
        'nom' => '',
        'prenom' => '',
        'date_naiss' => '',
        'tel' => '',
        'mail' => '',
        'enseigne' => '',
        'prix_cli' => '',
        'date_const' => '',
        'code' => '',
        'magasin' => '',
        'prix_appli' => '',
        'date_appli' => '',
        'file' => '',
        'file2' => '',
        'captcha' => '',
    );
    return $valeurs;
}

function formulaires_leclercoui_verifier_dist() {
    $erreurs = array();
    return $erreurs;
}

function formulaires_leclercoui_traiter_dist() {

    /* session */
    @session_start();
    $_SESSION["numero"] = _request('numero');
    $_SESSION["nom"] = _request('nom');
    $_SESSION["prenom"] = _request('prenom');
    $_SESSION["date_naiss"] = _request('date_naiss');
    $_SESSION["tel"] = _request('tel');
    $_SESSION["mail"] = _request('mail');
    $_SESSION["prix_cli"] = _request('prix_cli');
    $_SESSION["date_const"] = _request('date_const');
    $_SESSION["code"] = _request('code');
    $_SESSION["prix_appli"] = _request('prix_appli');
    $_SESSION["date_appli"] = _request('date_appli');


    $numero = _request('numero');
    $nom = _request('nom');
    $prenom = _request('prenom');
    $date_naiss = _request('date_naiss');
    $tel = _request('tel');
    $mail = _request('mail');
    $enseigne = _request('enseigne');
    $prix_cli = _request('prix_cli');
    $date_const = _request('date_const');
    $code = _request('code');
    $magasin = _request('magasin');
    $prix_appli = _request('prix_appli');
    $date_appli = _request('date_appli');
    $date = date('Y-m-d h:i:s a', time());
    $fichier = _request('file');
    $fichier2 = _request('file2');
    $captcha = _request('g-recaptcha-response');
    $letter = _request('letter');

    // get date/IP demande
    $date_ip = date('Y-m-d');
    $ip = $_SERVER['REMOTE_ADDR'];
    $currDate = date('Y-m-d');
    $currIp = $_SERVER['REMOTE_ADDR'];

    // transform format date 
    $newDate_naiss = date('Y-m-d', strtotime($date_naiss));
    $newDate_const = date('Y-m-d', strtotime($date_const));
    $newDate_appli = date('Y-m-d', strtotime($date_appli));


    if (!$captcha) {
        header('Location: http://www.antennereunion.fr/partenaire/leclerc?var_mode=calcul');
        $_SESSION["erreur_captcha"] = "1";
        echo
        "<script type='text/javascript'>
           
              alert(\"veuillez checker captcha\");
              return false;
             </script>";
//        document.location.href=\"http://www.antennereunion.fr/partenaire/leclerc?cpt=false\";
        // echo "veuillez checker le captcha";
        exit;
    }
    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lej4Q4TAAAAAIte-jtR6J6Gq-SFiUCxVz4UUEba&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
    if ($response . success == false) {

        echo
        "<script type='text/javascript'>
               $(document).ready(function(){                  
                $.MessageBox(\"robot\");
                });
             </script>";
    } else {
        session_unset();
        
        // newsletter
         if (isset($letter)) {
            // MAil2
                    $to3 = " $mail,  tojo.andriamandimbinirina@antennereunion.fr";
                    $subject3 = "newletter ";

                    $message3 ="
                           <html lang=\"fr\">
<head>
    <meta charset=\"utf-8\">
</head>
<body>
    <table align=\"center\" width=\"700px\" style=\"background: #fff; width: 700px; margin-top: 15px; margin-bottom: 15px;\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
        <tr>
            <td valign=\"top\" colspan=\"2\"><a href=\"http://www.e-leclerc.re\" style=\"border: 0;\"><img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/header.jpg\" alt=\"\" style=\"display: block; border: 0; margin-bottom: 45px;\" width=\"700\" height=\"\"/></a></td>
        </tr>
        <tr>
            <td valign=\"top\" align=\"center\">
                <div style=\"padding: 15px;\">
                    <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/visu-mobile.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"232\" height=\"\"/>
                </div>
            </td>
            <td valign=\"top\" align=\"left\">
                <div style=\"padding: 15px;\">
                    <h1 style=\"font-size: 32px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #FF8F00; text-transform: uppercase; margin: 0;\">LE PACTE<br>POUVOIR D&#8217;ACHAT</h1>
                    <p style=\"font-size: 30px; font-family: Arial, Helvetica, sans-serif; color: #025FA7; margin: 0 0 15px 0;\">
                        Antenne R&eacute;union<br>
                        et votre magasin E.Leclerc<br>
                        unis contre la vie ch&egrave;re
                    </p>
                    <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/logos.jpg\" alt=\"\" style=\"display: block; border: 0; margin-bottom: 20px;\" width=\"331\" height=\"\"/>
                </div>
            </td>
        </tr>

        <tr>
            <td colspan=\"2\">
                <table align=\"center\" style=\"width: 700px; background-color: #ff8f00; margin-bottom: 30px;\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
                    <tr>
                        <td align=\"center\" colspan=\"2\">
                            <div style=\"padding: 30px;\">
                                <p style=\"font-size: 22px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #FFF; text-transform: uppercase; margin: 0 0 15px 0;\">SI VOUS TROUVEZ MOINS CHER, E.LECLERC S'ENGAGE<br>&Agrave; VOUS OFFRIR LA DIFF&Eacute;RENCE</p>
                                <a href=\"http://www.e-leclerc.re/index.php/page/kilemoinscher#video\" target=\"_blank\"><img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/push-video.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"641\" height=\"\"/></a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan=\"2\">
                <table align=\"center\" style=\"width: 700px;\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">
                    <tr>
                        <td colspan=\"2\" align=\"center\">
                            <h1 style=\"font-size: 32px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #025FA7; text-transform: uppercase; margin: 0;\">Comment &ccedil;a marche ?</h1>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/picto-telecharge.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"71\" height=\"\"/>
                        </td>
                        <td>
                            <p style=\"font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #025FA7;\"><strong style=\"font-weight: bold; color: #FF8F00;\">1. T&eacute;l&eacute;chargez</strong> l&#8217;application kilemoinscher depuis votre smartphone et/ou tablette.</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\" align=\"center\">
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/hr.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"629\" height=\"\"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/picto-scan.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"71\" height=\"\"/>
                        </td>
                        <td>
                            <p style=\"font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #025FA7;\"><strong style=\"font-weight: bold; color: #FF8F00;\">2. Scannez</strong> le produit* dans une autre enseigne que E.Leclerc, situ&eacute;e sur l'&icirc;le de la R&eacute;union et v&eacute;rifiez le prix.</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\" align=\"center\">
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/hr.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"629\" height=\"\"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/picto-photo.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"71\" height=\"\"/>
                        </td>
                        <td>
                            <p style=\"font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #025FA7;\"><strong style=\"font-weight: bold; color: #FF8F00;\">3.</strong> Si le prix constat&eacute; est moins cher que chez E.Leclerc, <strong style=\"font-weight: bold; color: #FF8F00;\">photographiez</strong> l'&eacute;tiquette du produit avec son libell&eacute;, son prix et relevez le code barre du produit.</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\" align=\"center\">
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/hr.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"629\" height=\"\"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/picto-tv.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"71\" height=\"\"/>
                        </td>
                        <td>
                            <p style=\"font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #025FA7;\"><strong style=\"font-weight: bold; color: #FF8F00;\">4.</strong> Rendez-vous sur le site <a href=\"http://www.antennereunion.fr/partenaire/leclerc\" style=\"font-weight: bold; color: #FF8F00;\">www.antennereunion.fr</a> et remplissez le formulaire.</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\" align=\"center\">
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/hr.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"629\" height=\"\"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/picto-carte-fid.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"71\" height=\"\"/>
                        </td>
                        <td>
                            <p style=\"font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #025FA7;\"><strong style=\"font-weight: bold; color: #FF8F00;\">5. La diff&eacute;rence vous sera offerte</strong>** exclusivement sur votre carte fid&eacute;lit&eacute; E.Leclerc cr&eacute;&eacute;e &agrave; la R&eacute;union. Si vous ne l'avez pas encore vous pouvez en faire la demande via le formulaire disponible sur antennereunion.fr ou en magasin.</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\" align=\"center\">
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/hr.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"629\" height=\"\"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\" align=\"center\">
                            <p style=\"font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #025FA7;\">Plus d&#8217;informations sur <a href=\"http://www.antennereunion.fr/partenaire/leclerc\" style=\"font-weight: bold; color: #FF8F00;\">www.antennereunion.fr</a></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\">
                            <p style=\"font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #025FA7; margin: 0 0 7px 0;\">Liste des conditions d&eacute;taill&eacute;es sur antennereunion.fr</p>
                            <p style=\"font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #025FA7; margin: 0 0 7px 0;\">* Hors marques de distributeurs, hors produits en promotion, hors produits non alimentaires (bazar, textile), hors produits frais &agrave; poids variable, hors fruits et l&eacute;gumes.</p>
                            <p style=\"font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #025FA7; margin: 0 0 7px 0;\">** Dans la limite d&#8217;un article par r&eacute;f&eacute;rence, par jour et dans la limite de 5 articles diff&eacute;rents par jour et par porteur de carte. Montant plafonn&eacute; &agrave; 25 euros par carte de fid&eacute;lit&eacute; et par mois civil. L&#8217;offre de la diff&eacute;rence sur un m&ecirc;me produit est limit&eacute;e &agrave; une fois, par porteur de carte et par mois civil.</p>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan=\"2\">
                <table align=\"center\" width=\"700px\" style=\"width: 700px; background-color: #f5f5f5;\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">
                    <tr>
                        <td valign=\"middle\" colspan=\"2\" align=\"center\" style=\"padding-top: 30px; padding-bottom: 15px;\">
                            <img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/2.jpg\">
                        </td>
                    </tr>
                    <tr>
                        <td valign=\"top\" colspan=\"2\" style=\"padding-top: 15px; padding-bottom: 15px;\">
                            <table width=\"700px\" style=\"width: 700px;\" cellpadding=\"10\" cellspacing=\"0\" border=\"0\">
                                <tr>
                                    <td valign=\"top\" align=\"left\"><a href=\"https://www.facebook.com/ELeclercPortailPiton?ref=bookmarks\" style=\"border: 0; text-decoration: none; color: #0062ad; font-family: Helvetica, Arial, sans-serif;\"><img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/logo.jpg\" alt=\"\" style=\"float:left; margin: 0 15px 0 10px; border: 0;\" width=\"55\" height=\"\"/> <strong>E.LECLERC</strong><br>LE PORTAIL<br>PITON ST-LEU</a></td>
                                    <td valign=\"middle\"><img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/separator.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"5\" height=\"\"/></td>
                                    <td valign=\"top\" align=\"left\"><a href=\"https://www.facebook.com/ELeclercTamponCentre?ref=bookmarks\" style=\"border: 0; text-decoration: none; color: #0062ad; font-family: Helvetica, Arial, sans-serif;\"><img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/logo.jpg\" alt=\"\" style=\"float:left; margin: 0 15px 0 10px; border: 0;\" width=\"55\" height=\"\"/> <strong>E.LECLERC</strong><br>LE TAMPON<br>CENTRE</a></td>
                                    <td valign=\"middle\"><img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/separator.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"5\" height=\"\"/></td>
                                    <td valign=\"top\" align=\"left\"><a href=\"https://www.facebook.com/Leclercravinedescabris?ref=bookmarks\" style=\"border: 0; text-decoration: none; color: #0062ad; font-family: Helvetica, Arial, sans-serif;\"><img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/logo.jpg\" alt=\"\" style=\"float:left; margin: 0 15px 0 10px; border: 0;\" width=\"55\" height=\"\"/> <strong>E.LECLERC</strong><br>RAVINE DES<br>CABRIS</a></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign=\"top\" colspan=\"2\" style=\"padding-top: 15px; padding-bottom: 15px;\"><img src=\"http://www.e-leclerc.re/public/newsletters/pacte-pouvoir-achat/3.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"700\" height=\"\"/></td>
        </tr>
        <tr>
            <td colspan=\"2\">
                <table align=\"center\" width=\"700px\" style=\"width: 700px; margin-bottom: 15px;\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">
                    <tr>
                        <td valign=\"top\">
                            <p style=\"text-align: center; font-family: helvetica, sans-serif; font-size: 11px; color: #74ACB6;\">A LA UNE</p>
                            <ul style=\"font-family: helvetica, sans-serif; font-size: 12px;\">
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/catalogues-reunion\">Catalogues</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/marque-repere\">Marque Rep&egrave;re</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/nos-regions-ont-du-talent\">Nos r&eacute;gions ont du talent</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/eco-plus\">Eco+</a></li>
                            </ul>
                        </td>
                        <td valign=\"top\">
                        <p style=\"text-align: center; font-family: helvetica, sans-serif; font-size: 11px; color: #A7415F;\">VOS AVANTAGES</p>
                            <ul style=\"font-family: helvetica, sans-serif; font-size: 12px;\">
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/carte_fidelite\">Demandez la carte E.LECLERC</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/carte-e-leclerc\">La carte E.Leclerc</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/videos\">La web TV E.Leclerc &amp; Vous</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/recettes\">Recettes</a></li>
                            </ul>
                        </td>
                        <td valign=\"top\">
                            <p style=\"text-align: center; font-family: helvetica, sans-serif; font-size: 11px; color: #E2983C\">SERVICES</p>
                            <ul style=\"font-family: helvetica, sans-serif; font-size: 12px;\">
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/magasin-le-plus-proche\">Nos magasins</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/carte-cadeau\">Les cartes cadeaux E.Leclerc</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/carte-reglo\">Offre r&eacute;glo</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/recrutement\">Espace m&eacute;tiers</a></li>
                            </ul>
                        </td>
                        <td valign=\"top\">
                            <p style=\"text-align: center; font-family: helvetica, sans-serif; font-size: 11px; color: #797942\">NOS ENGAGEMENTS</p>
                            <ul style=\"font-family: helvetica, sans-serif; font-size: 12px;\">
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/qui-sommes-nous\">Qui sommes-nous ?</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/nos-engagements\">Nos engagements</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/partenaires\">Partenaires</a></li>
                            </ul>
                        </td>
                        <td valign=\"top\">
                            <p style=\"text-align: center; font-family: helvetica, sans-serif; font-size: 11px; color: #56494D\">AIDE</p>
                            <ul style=\"font-family: helvetica, sans-serif; font-size: 12px;\">
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/plan-du-site\">Plan du site</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/faq\">Foire aux questions</a></li>
                                <li><a style=\"text-decoration: none; color: #444;\" href=\"http://www.e-leclerc.re/index.php/page/contact\">Contactez-nous</a></li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign=\"top\" colspan=\"2\"><img src=\"http://www.e-leclerc.re/public/newsletters/15RUN22-reglo/7.jpg\" alt=\"\" style=\"display: block; border: 0;\" width=\"700\" height=\"\"/></td>
        </tr>
    </table>
</body>
</html>";
                    // Always set content-type when sending HTML email
                    $headers3 = "MIME-Version: 1.0" . "\r\n";
                    $headers3 .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
//            $headers .= 'From: <andriamandimbytj@gmail.com>' . "Pacte Pouvoir d’Achat Antenne Réunion et Leclerc\r\n";
//            $headers .= 'Cc: myboss@example.com' . "\r\n";
                    //$headers2 .= 'From: <Reply-to>' . "$mail\r\n";
                    $headers3 .= 'From: <No-reply>' . "Pacte Pouvoir d’Achat Antenne Réunion et Leclerc\r\n";
                    $headers3 .= '' . "E.Leclerc \r\n";

                    mail($to3, $subject3, $message3, $headers3);
        } else {
            //$stok is nog checked and value=0
            $letter = 0;
            //echo $stok;
        }
        //fin newsletter
                                            

        $mysqlserver = "localhost";
        $mysqlusername = "antenne_spipAR";
        $mysqlpassword = "antv3_2009";

        $link = mysql_connect($mysqlserver, $mysqlusername, $mysqlpassword) or die("Error connecting to mysql server: " . mysql_error());
        $dbname = 'antenne_art_spip';
        mysql_select_db($dbname, $link) or die("Error selecting specified database on mysql server: " . mysql_error());

        $query1 = "SELECT * FROM demande_avec ORDER BY Identification_de_la_demande DESC LIMIT 1";
        $result1 = mysql_query($query1) or die("Query to get data from firsttable failed: " . mysql_error());

        while ($row = mysql_fetch_array($result1)) {
            $id = $row["Identification_de_la_demande"];
            $folder = $id + 1;


            $query = "SELECT *,COUNT(*) FROM demande_avec WHERE date_ip = '" . $currDate . "' AND Adresse_ip='" . $currIp . "'";
            $result = mysql_query($query) or die("Query to get data from firsttable failed: " . mysql_error());


            while ($row = mysql_fetch_array($result)) {
//        $Date = $row["date_ip"];
                $count = $row ["COUNT(*)"];

                if ($count > 4) {
                    echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/jquery-1.10.2.js'></script>";
                    echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/jquery-ui.js'></script>";
                    echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/messagebox.js'></script>";
                    echo "<link href='https://www.antennereunion.fr/squelettes/assets/leclerc/css/messagebox.css' rel='stylesheet'>";
                    echo
                    "<script type='text/javascript'>
               $(document).ready(function(){                  
                $.MessageBox(\"Désolé, vous avez déjà déposé 5 demandes de remboursement \");
                });
             </script>";
                } else {



                    //creer dossier
                    //save file 
                    //andrana
//Creation du dossier de destination
                    $target_dir = "/home/antenne/public_html/squelettes/assets/leclerc/avec_cf/" . $folder . "/"; // Where the file is going to be placed
                    mkdir($target_dir, 0777);

                    //fich obl
                    $file = $_FILES['file']['name'];
                    $path = pathinfo($file);
                    $filename = $path['filename'];
                    $newfilename = "avec_CF_demande".$folder."_"."piece-1";
                    //rename ($filename,$newfilename);
                    $ext = $path['extension'];
                    $temp_name = $_FILES['file']['tmp_name'];
                    $pathfilenameext = $target_dir . $newfilename . "." . $ext;
                    move_uploaded_file($temp_name, $pathfilenameext);
                    $lien = "https://www.antennereunion.fr/squelettes/assets/leclerc/avec_cf/" . $folder . "/" . $newfilename . "." . $ext . "";


                    //fich fac
                    $tmp_fac_file = $_FILES['file2']['name'];
                    //if (is_uploaded_file($tmp_fac_file)) {
                    if (is_uploaded_file($_FILES['file2']['tmp_name'])) {    
                        $path2 = pathinfo($tmp_fac_file);
                        $filename2 = $path2['filename'];
                        $newfilename2 = "avec_CF_demande".$folder."_"."piece-2";
                        $ext2 = $path2['extension'];
                        $temp_name2 = $_FILES['file2']['tmp_name'];
                        $pathfilenameext2 = $target_dir . $newfilename2 . "." . $ext2;
                        move_uploaded_file($temp_name2, $pathfilenameext2);
                        $lien2 = "https://www.antennereunion.fr/squelettes/assets/leclerc/avec_cf/" . $folder . "/" . $newfilename2 . "." . $ext2 . "";
                    }
 
                    
                    
                    $sql = "INSERT INTO demande_avec VALUES (NULL,'$numero', '$nom', ' $prenom', '$newDate_naiss', "
                            . "'$tel', '$mail', '$enseigne', '$prix_cli', '$newDate_const', '$code',"
                            . "'$magasin', '$prix_appli', '$newDate_appli','$date','$lien','$lien2','$date_ip','$ip','$letter')";

                    if (!mysql_query($sql)) {
                        die('Error: ' . mysql_error());
                    }

                    echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/jquery-1.10.2.js'></script>";
                    echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/jquery-ui.js'></script>";
                    echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/messagebox.js'></script>";
                    echo "<link href='https://www.antennereunion.fr/squelettes/assets/leclerc/css/messagebox.css' rel='stylesheet'>";
                    echo
                    "<script type='text/javascript'>
               $(document).ready(function(){                  
                $.MessageBox(\"Votre demande a été bien enregistré. Merci \");
                });
             </script>";

                     mysql_close();
                    
                    // MAil
                    $to = "$mail";
                    $subject = "Pacte Pouvoir d’Achat : E.Leclerc vous offre la différence si vous trouvez moins cher ailleurs";

                    $message = "


<html >
<head>
    <title>Antenne & Leclerc</title>
<link href=\"https://fonts.googleapis.com/css?family=Maven+Pro:400,700,500\" rel=\"stylesheet\" type=\"text/css\">

<style type=\"text/css\">
		#outlook a{
			padding:0;
		}
		a:active,a:focus{
			outline:none;
		}
		img{
			outline:none;
			text-decoration:none;
		}
		a img{
			border:none;
		}
		table td{
			border-collapse:collapse;
		}
		table{
			border-collapse:collapse;
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		a{
			color:inherit;
			text-decoration:none;
		}
</style></head>
<body style=\"background:#ffffff;\">
<div id=\"global\" style=\"background-color:#ffffff\">
  <table cellpadding=\"0\" cellspacing=\"0\" width=\"598\" style=\"margin:0 auto;\" bgcolor=\"#ffffff\">
    <tr>
      <td><a href=\"http://www.antennereunion.fr/partenaire/leclerc\" title=\"Linfo.re\" target=\"_blank\"><img src=\"https://www.antennereunion.fr/squelettes/assets/leclerc/images/header-logo.png\" alt=\"linfo.re\"></a></td>
    </tr>
  </table>
  
    <table cellpadding=\"0\" cellspacing=\"0\" width=\"598\" style=\"margin:0 auto;\" bgcolor=\"#ffffff\">
    <tr>
    <td width=\"560\">
  <table cellpadding=\"0\" cellspacing=\"0\" width=\"560\" style=\"margin:0 auto;\" bgcolor=\"#ffffff\">
    <tr>
      <td height=\"64\">&nbsp;
      
      </td>
    </tr>
    <tr>
    <td align=\"left\"><h1 style=\"font-family:Verdana, Geneva, sans-serif; font-size: 28px; margin:0;\">LE PACTE POUVOIR D’ACHAT   
</h1></td>
    </tr>
    <tr>
    <td align=\"right\"><h1 style=\"font-family:Verdana, Geneva, sans-serif; font-size: 28px; margin:0;\"> ANTENNE REUNION & E.LECLERC</h1></td>
    </tr>
    <tr>
    	<td height=\"26\"></td>
    </tr>
    <tr><td><p style=\"font-size:17px;margin:0;padding:0; font-family:Arial, Helvetica, sans-serif;\">Cher(e) Madame, Monsieur </p></td></tr>  
    <tr><td height=\"33\">&nbsp;</td></tr>
    <tr><td><p style=\"font-size:16px;margin:0;padding:0; font-family:Arial, Helvetica, sans-serif;color:#333333;\">Nous vous confirmons que nous avons bien pris en compte votre demande relative  à la réclamation de la différence de prix liée au Pacte Pourvoir d’Achat Antenne Réunion – E. Leclerc<br/>
Restez connecté, nous ne manquerons pas de vous répondre dans les meilleurs délais après les vérifications d’usage liées aux modalités de l’opération.
</p></td></tr>
    <tr>
    <td height=\"70\">&nbsp;</td>
    </tr>
    <tr><td><p style=\"font-size:16px;margin:0;padding:0; font-family:Arial, Helvetica, sans-serif;color:#333333;\">Avec le Pacte Pouvoir d’Achat, Antenne Réunion et E.Leclerc s’engagent aussi au côté des Réunionnais.</p></td></tr>
    <tr>
    <td height=\"97\">&nbsp;</td>
    </tr>
    </table>
  <table cellpadding=\"0\" cellspacing=\"0\" width=\"598\" style=\"margin:0 auto;\" bgcolor=\"#111111\">
    <tr height=\"14\">
      <td colspan=\"6\" height=\"14\">&nbsp;</td>
    </tr>
    <tr>
      <td width=\"160\" align=\"center\" style=\"color:#ffffff; font-size: 18px;\">Suivez-nous sur</td>
      <td width=\"50\"><a href=\"https://www.facebook.com/linfo.re?ref=ts&fref=ts\" target=\"_blank\"><img style=\"display:block\" src=\"http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/facebook_linfo.jpg\" alt=\"linfo.re facebook\"></a></td>
      <td width=\"50\"><a href=\"https://twitter.com/linfore\" target=\"_blank\"><img style=\"display:block\" src=\"http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/twitter_linfo.jpg\"></a></td>
      <td width=\"50\"><a href=\"https://plus.google.com/110043057610796001535/posts\" target=\"_blank\"><img style=\"display:block\" src=\"http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/google_plus_linfo.jpg\"></a></td>
      <td width=\"147\" align=\"right\" valign=\"middle\"><a href=\"https://itunes.apple.com/fr/app/linfo/id640496786?mt=8\" target=\"_blank\"><img style=\"display:block\" src=\"http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/appstore_linfo.jpg\"></a></td>
      <td width=\"124\" align=\"center\" valign=\"middle\"><a href=\"https://play.google.com/store/apps/details?id=com.goodbarber.linfo&hl=fr_FR\" target=\"_blank\"><img style=\"display:block\" src=\"http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/google_playlinfo.jpg\"></a></td>
    </tr>
    <tr height=\"14\">
      <td colspan=\"6\" height=\"14\">&nbsp;</td>
    </tr>
  </table>
  <table>
    <tr>
      <td height=\"5\"></td>
    </tr>
  </table>
  <table cellpadding=\"0\" cellspacing=\"0\" width=\"598\" style=\"margin:0 auto;\">
    <tr>
      <td align=\"center\"><p style=\"font-size:11px;color:#000000;\">Si vous n’arrivez à voir cet email : <a href=\"#\" style=\"font-size:12px; font-weight: bold;\">Voir ici</a></p>
        <p style=\"font-size:11px;color:#000000;\">Si vous ne souhaitez plus recevoir les communications de la part d'antennereunion.fr : <a href=\"*|UNSUB|*\" style=\"font-size:12px;font-weight:bold;color:#000000;\">Suivez ce lien</a></p></td>
    </tr>
  </table>
</td></tr></table></div>
</body>
</html>

";

// Always set content-type when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
//            $headers .= 'From: <andriamandimbytj@gmail.com>' . "Pacte Pouvoir d’Achat Antenne Réunion et Leclerc\r\n";
//            $headers .= 'Cc: myboss@example.com' . "\r\n";

                    $headers .= 'From: <No-reply>' . "Pacte Pouvoir d’Achat Antenne Réunion et Leclerc\r\n";
                    $headers .= '' . "E.Leclerc \r\n";

                    mail($to, $subject, $message, $headers);




                    // MAil2
                    $to2 = " klmc@e-leclerc.re, tojoandria1@gmail.com , tojo.andriamandimbinirina@antennereunion.fr, aingasoft@gmail.com,aportier@e-leclerc.re ";
                    $subject2 = "Demande de remboursement ";

                    $message2 = "


<html >
<head>
    <title>Antenne & Leclerc</title>
<link href=\"https://fonts.googleapis.com/css?family=Maven+Pro:400,700,500\" rel=\"stylesheet\" type=\"text/css\">

<style type=\"text/css\">
		#outlook a{
			padding:0;
		}
		a:active,a:focus{
			outline:none;
		}
		img{
			outline:none;
			text-decoration:none;
		}
		a img{
			border:none;
		}
		table td{
			border-collapse:collapse;
		}
		table{
			border-collapse:collapse;
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		a{
			color:inherit;
			text-decoration:none;
		}
</style></head>
<body style=\"background:#ffffff;\">
<div id=\"global\" style=\"background-color:#ffffff\">
  
  
    <table cellpadding=\"0\" cellspacing=\"0\" width=\"598\" style=\"margin:0 auto;\" bgcolor=\"#ffffff\">
    <tr>
    <td width=\"560\">
  <table cellpadding=\"0\" cellspacing=\"0\" width=\"560\" style=\"margin:0 auto\"; border:\"1px solid #000\"; border-collapse:\"collapse\" bgcolor=\"#ffffff\">
    <tr>
      <td height=\"64\">&nbsp;     
      </td>
    </tr>   
    <tr>
    <td align=\"center\"><h1 style=\"font-family:Verdana, Geneva, sans-serif; font-size: 28px; margin:0;\"> DEMANDE DE REMBOURSEMENT</h1></td>
    </tr>
    <tr>
    	<td height=\"26\"></td>
    </tr>
    <tr><td><p style=\"font-size:17px;margin:0;padding:0; font-family:Arial, Helvetica, sans-serif;\">Informations du client: </p></td></tr>  
    <tr><td height=\"33\">&nbsp;</td></tr>    
    <tr><td>Nom:</td><td width=\"300\">$nom</td></tr>
    <tr><td>Prenom:</td><td width=\"300\">$prenom</td></tr>
    <tr><td>Numero de la carte:</td><td width=\"300\">$numero</td></tr>
    <tr><td>Date de naissance:</td><td width=\"300\">$newDate_naiss</td></tr>
    <tr><td>Téléphone:</td><td width=\"300\">$tel</td></tr>
    <tr><td>Mail:</td><td width=\"300\">$mail</td></tr>
    <tr><td>Enseigne concurent:</td><td width=\"300\">$enseigne</td></tr>
    <tr><td>Prix concurent:</td><td width=\"300\">$prix_cli</td></tr>
    <tr><td>date constat:</td><td width=\"300\">$date_const</td></tr>
    <tr><td>Code barre produit:</td><td width=\"300\">$code</td></tr>
    <tr><td>Magasin leclerc:</td><td width=\"300\">$magasin</td></tr>
    <tr><td>Prix dans l'appli:</td><td width=\"300\">$prix_appli</td></tr>
    <tr><td>Lien fichier obligatoire:</td><td width=\"300\">$lien</td></tr>
    <tr><td>Lien fichier facultative:</td><td width=\"300\">$lien2</td></tr>
   <tr><td>Date dans appli:</td><td width=\"300\">$date_appli</td></tr>
    <tr><td>Date/heure de la demande:</td><td width=\"300\">$date</td></tr>
    <tr><td>IP machine:</td><td width=\"300\">$ip</td></tr>
    <tr><td>Newsletter:</td><td width=\"300\">$letter</td></tr>    
    </table>
  <table cellpadding=\"0\" cellspacing=\"0\" width=\"598\" style=\"margin:0 auto;\" >
    <tr height=\"14\">
      <td colspan=\"6\" height=\"14\">&nbsp;</td>
    </tr>   
    <tr height=\"14\">
      <td colspan=\"6\" height=\"14\">&nbsp;</td>
    </tr>
  </table>
  <table>
    <tr>
      <td height=\"5\"></td>
    </tr>
  </table> 
</td></tr></table></div>
</body>
</html>

";

// Always set content-type when sending HTML email
                    $headers2 = "MIME-Version: 1.0" . "\r\n";
                    $headers2 .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
//            $headers .= 'From: <andriamandimbytj@gmail.com>' . "Pacte Pouvoir d’Achat Antenne Réunion et Leclerc\r\n";
//            $headers .= 'Cc: myboss@example.com' . "\r\n";
                    //$headers2 .= 'From: <Reply-to>' . "$mail\r\n";
                    $headers .= 'From: ' . $mail . "\r\n";
                    $headers .= 'cc: ' . $mail . "\r\n";
                    $headers2 .= '' . "E.Leclerc \r\n";

                    mail($to2, $subject2, $message2, $headers2);
                }
            }
        }
        
//         mysql_close();
    }
}
