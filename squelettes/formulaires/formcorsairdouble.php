<?php

define("DATA_BASE_TABLE_DOUBLEEURO", "corsair_doubleeuro_test");

function formulaires_formcorsairdouble_charger_dist() {
    $valeurs = array(
        'num_billet' => '',
        'date_aller' => '',
        'date_retour' => '',      
        'num_vol' => '',
        'arrive' => '',
        'depart' => '',
        'user' => '',
        'nom' => '',
        'prenom' => '',
        'telmobile' => '',
        'email' => '',
        'optin_email' => '',
        'optin_sms' => '',
    );
    return $valeurs;
}

function formulaires_formcorsairdouble_verifier_dist() {
    $erreurs = array();
    return $erreurs;
}

function formulaires_formcorsairdouble_traiter_dist() {
    $user = _request('user');
    $nom = _request('nom');
    $prenom = _request('prenom');
    $telmobile = _request('telmobile');
    $email = _request('email');
    $num_billet = _request('num_billet');
    $date_aller = _request('date_aller');
    $date_retour = _request('date_retour');   
    $num_vol = _request('num_vol');
    $arrive = _request('arrive');
    $depart = _request('depart');
    $optin_email = _request('optin_email');
    $optin_sms = _request('optin_sms');
    $date_dmd = date('Y-m-d h:i:s a', time());
    
    
    if (isset($email)){
    $return_enregistrement = sql_insertq(DATA_BASE_TABLE_DOUBLEEURO, array(
        'user' => $user,
        'nom' => $nom,
        'prenom' => $prenom,
        'telmobile' => $telmobile,
        'email' => $email,
        'num_billet' => $num_billet,
        'date_aller' => $date_aller, 
        'date_retour' => $date_retour,        
        'num_vol' => $num_vol,
        'arrive' => $arrive,
        'depart' => $depart,
        'optin_email' => $optin_email,
        'optin_sms' => $optin_sms,
        'date_dmd' => $date_dmd,

    ));
    }
    
    
    
    
    if (!is_null($return_enregistrement)) {
        echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/jquery-1.10.2.js'></script>";
        echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/jquery-ui.js'></script>";
        echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/messagebox.js'></script>";
        echo "<link href='https://www.antennereunion.fr/squelettes/assets/leclerc/css/messagebox.css' rel='stylesheet'>";
        echo
        "<script type='text/javascript'>
               $(document).ready(function(){                
                $.MessageBox(\"<h2 class='titre-popup'>Je double mes €</h2><p class='contenu-popup' style='text-align:center;'>Félicitations ! Votre demande a bien été prise en compte. Vos € seront doublés, directement sur votre carte CLUB Corsair.</p><p class='contenu-popup' style='text-align:center;'> Sous quelques jours, vous recevrez une confirmation de Corsair, envoyée à votre adresse email.</p><p class='contenu-popup' style='text-align:center;'>Pour toutes questions, merci de vous adresser directement au service de fidélité CLUB Corsair.</p>\");
                });
             </script>";
    } else {
        echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/jquery-1.10.2.js'></script>";
        echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/jquery-ui.js'></script>";
        echo "<script src='https://www.antennereunion.fr/squelettes/assets/leclerc/js/messagebox.js'></script>";
        echo "<link href='https://www.antennereunion.fr/squelettes/assets/leclerc/css/messagebox.css' rel='stylesheet'>";
        echo
        "<script type='text/javascript'>
               $(document).ready(function(){                  
                $.MessageBox(\"Erreur\");
                });
             </script>";
    }
}

?>