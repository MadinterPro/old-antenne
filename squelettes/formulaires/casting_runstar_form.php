<?php

define("DATA_BASE_TABLE_NEWSLETTER", "spip_nl_user");
define("DATA_BASE_TABLE_USER_NEWSLETTER", "spip_nl_user_letter");

function formulaires_casting_runstar_form_charger_dist() {
    $valeurs = array(
        'nom' => '',
        'prenom' => '',
        'mail' => '',
        'naissance' => '',
        'telephone' => '',
        'datelieuxprod' => '',        
        'video' => '',
        'reglement' => '',
        'newsletter' => '',
    );

    return $valeurs;
}

function formulaires_casting_runstar_form_verifier_dist() {
    $erreurs = array();
    foreach (array('nom','prenom','mail','naissance','telephone','reglement') as $obligatoire)
        if (!_request($obligatoire))
            $erreurs[$obligatoire] = 'Ce champ est obligatoire';

    if (_request('telephone') && strlen(_request('telephone')) < 10 && !is_int(_request('telephone')))
        $erreurs['telephone'] = 'Ce champ est invalide';
    
    if(strlen(_request('datelieuxprod')) > 200)
        $erreurs['datelieuxprod'] = 'Ce champ ne doit pas dépasser de 200 caractères';
    
    
    if (_request('video')){
        if(filter_var(_request('video'), FILTER_VALIDATE_URL)) {//Test url
            $rx = '~
                    ^(?:https?://)?              # Optional protocol
                     (?:www\.)?                  # Optional subdomain
                     (?:youtube\.com|youtu\.be)  # Mandatory domain name
                     /watch\?v=([^&]+)           # URI with video id as capture group 1
                     ~x';
            $has_match = preg_match($rx, _request('video'), $matches);
            if (!$has_match) {
                $erreurs['video'] = 'Vidéo Youtube invalide';
            }
        }
    }

    include_spip('inc/filtres');
    if (_request('mail') AND ! email_valide(_request('mail')))
        $erreurs['mail'] = 'Adresse email non valide';

    if (count($erreurs))
        $erreurs['message_erreur'] = 'Votre saisie contient des erreurs !';
    return $erreurs;
}

/**
 * 
 * @return type
 * 
 */
function formulaires_casting_runstar_form_traiter_dist() {
    $rx = '~
            ^(?:https?://)?              # Optional protocol
             (?:www\.)?                  # Optional subdomain
             (?:youtube\.com|youtu\.be)  # Mandatory domain name
             /watch\?v=([^&]+)           # URI with video id as capture group 1
             ~x';
    $has_match = preg_match($rx, _request('video'), $matches);
    $ps = $matches[1];
    $descriptif = ucfirst(trim(addslashes(_request('nom')))) . " " . ucfirst(trim(addslashes(_request('prenom'))));
    $titre = "Casting de " . ucfirst(trim(addslashes(_request('nom')))) . " " . ucfirst(trim(addslashes(_request('prenom'))));
    $nom = ucfirst(trim(addslashes(_request('nom'))));
    $prenom = ucfirst(trim(addslashes(_request('prenom'))));
    $phone = _request('telephone');
    $mail = trim(_request('mail'));
    $date = split('/', _request('naissance'));
    $timestamp = mktime(0, 0, 0, $date[1], $date[0], $date[2]);
    $naissance = date("Y-m-d H:i:s", $timestamp);
    $date_inscription = date("Y-m-d H:i:s");
    $texte = "Nom : " . $nom . "\n";
    $texte .= "Prénom : " . $prenom . "\n";
    $texte .= "Email : " . $mail . "\n";
    $texte .= "Date de naissance : " . _request('naissance') . "\n";
    $texte .= "Téléphone : " . _request('telephone') . "\n";
    $texte .= "Lieux et dates de production : " . _request('datelieuxprod') . "\n";
    $texte .= "Vidéo : " . _request('video') . "\n";


    $return_article = sql_insertq('spip_articles', array(
                    'surtitre' => $prenom,
                    'descriptif' => $descriptif,
                    'ps' => $ps,
                    'titre' => $titre,
                    'id_rubrique' => 4909,
                    'texte' => $texte,
                    'date' => date("Y-m-d H:i:s"),
                    'statut' => 'prepa',
                    'id_secteur' => 732 //4907 => ovaina 732 ref mis en prod
                ));
    if (!is_null($return_article)) {

        if (_request('newsletter') == "on") {
            $id_exist = sql_getfetsel('id', array(DATA_BASE_TABLE_NEWSLETTER), array("mail_user = '" . trim(_request('mail')) . "'"));
            if (is_null($id_exist)) {
                $last__newsletterid = sql_insertq(DATA_BASE_TABLE_NEWSLETTER, array(
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'mail_user' => $mail,
                    'date_inscription' => $date_inscription,
                    'tel' => $phone,
                    'jour_naissance' => $naissance,
                    'statut' => 1
                ));
                sql_insertq(DATA_BASE_TABLE_USER_NEWSLETTER, array(
                    'id_user' => $last__newsletterid,
                    'id_letter' => 2
                ));
            } else {
                sql_update(DATA_BASE_TABLE_NEWSLETTER, array('nom' => $nom, 'prenom' => $prenom, 'tel' => $phone, 'jour_naissance' => $naissance), "id=$id_exist");
            }
        }
        
        formulaires_casting_runstar_form_charger_dist();
        return array(
            'message_ok' => 'Votre casting a bien été enregistré !',
            );
    } else {
        return array('message_erreur' => 'Votre casting n\'a pas pu être enregistré.');
    }
}
