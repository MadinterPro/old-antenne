<?php

function formulaires_envoyer_selfie_charger_dist() {
    $valeurs = array(
        'pseudo' => '',
        'nom' => '',
        'prenom' => '',
        'adresse' => '',
        'code_postal' => '',
        'ville' => '',
        'telephone' => '',
        'texte' => '',
        'selfie_imgs' => '',
        'mail' => '',
        'villeselfie' => ''
    );

    return $valeurs;
}

function formulaires_envoyer_selfie_verifier_dist() {
    $erreurs = array();
    foreach (array('pseudo','nom', 'prenom', 'adresse', 'code_postal', 'ville', 'telephone', 'villeselfie','mail') as $obligatoire)
        if (!_request($obligatoire))
            $erreurs[$obligatoire] = 'Ce champ est obligatoire';

    if (_request('code_postal') && strlen(_request('code_postal')) != 5 && !is_int(_request('code_postal')))
        $erreurs['code_postal'] = 'Ce champ est invalide';

    if (_request('telephone') && strlen(_request('telephone')) < 10 && !is_int(_request('telephone')))
        $erreurs['telephone'] = 'Ce champ est invalide';

    if (!file_exists($_FILES['selfie_imgs']['tmp_name'][0])) {
        $erreurs['selfie_imgs'] = 'Ce champ est obligatoire';
    } else {
        $extensions_valides = array('jpg', 'jpeg', 'png');
        $extension_upload = strtolower(substr(strrchr($_FILES['selfie_imgs']['name'], '.'), 1));
        if (!in_array($extension_upload, $extensions_valides)) {
            $erreurs['selfie_imgs'] = 'Erreur extension image';
        }
        /*$image_sizes = getimagesize($_FILES['selfie_imgs']['tmp_name']);
        if ($image_sizes[0] > 1024 OR $image_sizes[1] > 650) {
            $erreurs['selfie_imgs'] = 'Erreur dimension image';
        }*/
    }
    
    include_spip('inc/filtres');
    if (_request('mail') AND !email_valide(_request('mail')))
            $erreurs['mail'] = 'Adresse email non valide';
    
    if (count($erreurs))
        $erreurs['message_erreur'] = 'Votre saisie contient des erreurs !';

    return $erreurs;
}

/**
 * 
 * @return type
 * Surtitre : Nom ; Titre : Selfie du Nom Prenom, Soustitre :Prenom, descriptif : adresse complet(adresse, ville, codepostal), Texte : texte, Chapo : Telephone
 */
function formulaires_envoyer_selfie_traiter_dist() {
    $erreur = 0;
    $message = "";
    $texte = "Nom : ".addslashes(_request('nom'))."\n";
    $texte .= "Prénom : ".addslashes(_request('prenom'))."\n";
    $texte .= "Adresse : ".addslashes(_request('adresse'))."\n";
    $texte .= "Code Postal : "._request('code_postal')."\n";
    $texte .= "Ville : ".addslashes(_request('ville'))."\n";
    $texte .= "Pseudo : ".addslashes(_request('pseudo'))."\n";
    $texte .= "Adresse Mail : ".addslashes(_request('mail'))."\n";
    $texte .= "Téléphone : "._request('telephone')."\n";
    $texte .= "Ville où votre selfie a été pris : "._request('villeselfie')."\n";
    $texte .= "Texte : ".addslashes(_request('texte'))."\n";
    
    
    $surtitre = addslashes(_request('pseudo'));
    $ps = addslashes(_request('villeselfie'));
    $descriptif = addslashes(_request('texte/'));
    $titre = "Selfie de " . addslashes(_request('nom')) . " " . addslashes(_request('prenom'));
    
    //PS = Ville ; Surtitre : Pseudo
    $return_article = spip_query("INSERT INTO spip_articles (descriptif,ps,surtitre,titre,id_rubrique,texte,date,statut,id_secteur)"
            . "VALUES ('".$descriptif."','" . $ps . "','" . $surtitre . "', '" . $titre . "', 4706, '" . $texte . "', NOW(), 'prepa', 4704)");
    
    
    if ($return_article) {
        $id_article = mysql_insert_id();        
        $dossier = 'IMG/upload/selfie/';       
        $fichier = time() . basename($_FILES['selfie_imgs']['name']);
        $image_sizes = getimagesize($_FILES['selfie_imgs']['tmp_name']);
        $extension_upload = strtolower(substr(strrchr($_FILES['selfie_imgs']['name'], '.'), 1)); 
        $taille = $_FILES['selfie_imgs']['size'];        
        if (!move_uploaded_file($_FILES['selfie_imgs']['tmp_name'], $dossier . $fichier)) {
            $erreur ++;
            $message .= "Erreur d'upload image.<br />";
        }
        else{
            $return_document = spip_query("INSERT INTO spip_documents (id_vignette,extension,titre,date,fichier,taille, largeur, hauteur, mode,distant,statut,brise,media)"
            . "VALUES ('0', '" . $extension_upload . "','',NOW(), 'upload/selfie/" . $fichier. "', '" . $taille . "', '" .$image_sizes[0] . "', '".$image_sizes[1]."', 'image', 'non', 'prepa',0,'image')");
            if ($return_document){
                $id_document = mysql_insert_id();
                $return_doc_lien = spip_query("INSERT INTO spip_documents_liens (id_document,id_objet,objet,vu)"
                        . "VALUES (".$id_document.",".$id_article.",'article','non')");
                if(!$return_doc_lien){
                    $erreur++;
                    $message .= "Erreur d'insertion document liaison article image.<br />";
                    //Normalement tokon supprimena lay article sy document
                }
            }
            else{
                $erreur ++;
                $message .= "Erreur d'insertion document dans la bb.<br />";
            }
        }
    } else {
        $erreur ++;
        $message .= "Erreur d'insertion du selfie.<br />";
    }

    if ($erreur > 0) {
        return array('message_erreur' => $message);
    } else {
        return array('message_ok' => 'Votre selfie a bien été enregistré !');
    }
}
