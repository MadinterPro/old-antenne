<?php
define("DATA_BASE_TABLE_NEWSLETTER", "spip_nl_user");
define("DATA_BASE_TABLE_USER_NEWSLETTER", "spip_nl_user_letter");
define("DATA_BASE_TABLE_GRAND_CASTING", "spip_casting");

//SELECT `id_reservation`, `nom`, `prenom`, `mail`, `date_naissance`, `age`, `profession`, `adresse`, `cp`, `ville`, `portable`, `urlvideo` FROM `spip_casting`

function formulaires_grand_casting_test_charger_dist() {
    $valeurs = array(
        'nom' => '',
        'prenom' => '',
        'mail' => '',
        'date_naissance' => '',
        'age' => '',
        'profession' => '',
        'adresse' => '',
        'cp' => '',
        'ville' => '',
        'portable' => '',
        'file1' => '',
        'file2' => '',
        'filevideo' => '',
        'opt1' => '',
        'opt2' => '',
        'urlvideo' => '',
        'newsletter' => ''
    );
    return $valeurs;
}

function formulaires_grand_casting_test_verifier_dist() {
    $erreurs = array();
    if (file_exists($_FILES['file1']['tmp_name'])) {
        $taille_maxi = 2097152;
        $extensions_valides = array('jpg', 'jpeg', 'png');
        $extension_upload = strtolower(substr(strrchr($_FILES['file1']['name'], '.'), 1));
        if (!in_array($extension_upload, $extensions_valides)) {
            $erreurs['file1'] = 'Erreur extension image';
        }
        $taille = filesize($_FILES['file1']['tmp_name']);
        if ($taille > $taille_maxi) {
            $erreurs['file1'] = 'Veuillez verifier la taille de l\'image';
        }
    }
    if (file_exists($_FILES['file2']['tmp_name'])) {
        $taille_maxi2 = 2097152;
        $extensions_valides2 = array('jpg', 'jpeg', 'png');
        $extension_upload2 = strtolower(substr(strrchr($_FILES['file2']['name'], '.'), 1));
        if (!in_array($extension_upload2, $extensions_valides2)) {
            $erreurs['file2'] = 'Erreur extension image';
        }
        $taille2 = filesize($_FILES['file2']['tmp_name']);
        if ($taille2 > $taille_maxi2) {
            $erreurs['file2'] = 'Veuillez verifier la taille de l\'image';
        }
    }
    if (file_exists($_FILES['filevideo']['tmp_name'])) {
        $taille_maxivideo = 52428800;
        $extensions_validesvideo = array('mp4', 'avi');
        $extension_uploadvideo = strtolower(substr(strrchr($_FILES['filevideo']['name'], '.'), 1));
        if (!in_array($extension_uploadvideo, $extensions_validesvideo)) {
            $erreurs['filevideo'] = 'Erreur extension vidéo';
        }
        $taillevideo = filesize($_FILES['filevideo']['tmp_name']);
        if ($taillevideo > $taille_maxivideo) {
            $erreurs['filevideo'] = 'Veuillez verifier la taille de la vidéo';
        }
    }
    if (count($erreurs)>0){
        $erreurs['message_erreur'] = 'Votre saisie contient des erreurs !';
    }
    return $erreurs;
}
/**
 *
 * @return type
 *
 */
//SELECT `id_reservation`, `nom`, `prenom`, `mail`, `date_naissance`, `age`, `profession`, `adresse`, `cp`, `ville`, `portable`, `urlvideo` FROM `spip_casting`
function formulaires_grand_casting_test_traiter_dist() {
    $nom = ucfirst(trim(addslashes(_request('nom'))));
    $prenom = ucfirst(trim(addslashes(_request('prenom'))));
    $mail = trim(_request('mail'));
    $date_naissance = _request('date_naissance');
    $age = _request('age');
    $profession = _request('profession');
    $adresse = _request('adresse');
    $cp = _request('cp');
    $ville = _request('ville');
    $portable = _request('portable');
    $file1 = _request('file1');
    $file2 = _request('file2');
    $filevideo = _request('filevideo');
    $urlvideo = _request('urlvideo');
    $opt1 = _request('opt1');
    $opt2 = _request('opt2');

    //traitement photos portrait
    $target_dir = "ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/gcportraits/";
    $tmp_fac_file = $_FILES['file1']['name'];
    if (is_uploaded_file($_FILES['file1']['tmp_name'])) {
        $path = pathinfo($tmp_fac_file);
        $filename = $path['filename'];
        $ext = $path['extension'];
        $temp_name = $_FILES['file1']['tmp_name'];
        $pathfilenameext = $target_dir . $filename . "." . $ext;
        $fileupload = $filename . "." . $ext;
        //print_r($pathfilenameext);
        move_uploaded_file($temp_name, $pathfilenameext);
        $lien = "http://cdn.antenne.re/nas/gcportraits/" . $fileupload."";
    }
    //Fin traitement photos portrait
    //traitement photos pied
    $target_dir2 = "ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/gcpieds/";
    $tmp_fac_file2 = $_FILES['file2']['name'];
    if (is_uploaded_file($_FILES['file2']['tmp_name'])) {
        $path2 = pathinfo($tmp_fac_file2);
        $filename2 = $path2['filename'];
        $ext2 = $path2['extension'];
        $temp_name2 = $_FILES['file2']['tmp_name'];
        $pathfilenameext2 = $target_dir2 . $filename2 . "." . $ext2;
        $fileupload2 = $filename2 . "." . $ext2;
        move_uploaded_file($temp_name2, $pathfilenameext2);
        $lien2 = "http://cdn.antenne.re/nas/gcpieds/" . $fileupload2."";
    }
    //Fin traitement photos pied
    //Traitement fichier vidéos
    $live_dir = "ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/gcvideos/";
    if (is_uploaded_file($_FILES['filevideo']['tmp_name'])) {
        $seed = rand(1, 2009) * rand(1, 10);
        $upload = $seed . "_" . basename($_FILES['filevideo']['name']);
        $uploadfile = $live_dir . $upload;
        //print_r($uploadfile);
        move_uploaded_file($_FILES['filevideo']['tmp_name'], $uploadfile);
        $lienvideo = "http://cdn.antenne.re/nas/gcvideos/" . $upload."";
    }
    //Fin traitement fichier vidéo

    $return_enregistrement = sql_insertq(DATA_BASE_TABLE_GRAND_CASTING, array(
        'nom' => $nom,
        'prenom' => $prenom,
        'mail' => $mail,
        'date_naissance' => $date_naissance,
        'age' => $age,
        'profession' => $profession,
        'adresse' => $adresse,
        'cp' => $cp,
        'ville' => $ville,
        'portable' => $portable,
        'urlvideo' => $urlvideo,
        'opt1' => $opt1,
        'opt2' => $opt2,
        'file1' => $lien,
        'file2' => $lien2,
        'filevideo' => $lienvideo,
    ));
    if (!is_null($return_enregistrement)) {
        $id_reservation = mysql_insert_id();
        include_spip('inc/session');
        session_set('id_reservation', $id_reservation);
        return array(
            'message_ok' => 'Votre inscription a bien été enregistrée ! Merci de votre participation. Les équipes d\'Antenne Réunion reviendront vers vous si vous êtes sélectionné(e) pour la suite.',
        );
    } else {
        return array('message_erreur' => 'Erreur d\'insertion.');
    }
}
