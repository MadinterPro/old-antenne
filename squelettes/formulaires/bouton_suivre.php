<?php

function formulaires_bouton_suivre_charger_dist($id_article)
{
    $valeurs = array('email' => '', 'naissance' => '', 'sexe' => '', 'id_article' => $id_article, 'date_envoi' => '', 'date_creation' => '', 'optin_ar' => '', 'optin_alerte' => '', 'optin_nl_thematique' => '', 'id_mot' => $id_mot);
    return $valeurs;
}

function formulaires_bouton_suivre_verifier_dist()
{
    $erreurs = array();
    foreach (array('email', 'naissance') as $obligatoire)
        if (!_request($obligatoire))
            $erreurs[$obligatoire] = 'Ce champ est obligatoire';
    include_spip('inc/filtres');
    if (_request('email') AND !email_valide(_request('email')))
        $erreurs['email'] = 'Cet email n\'est pas valide';
    if (_request('optin_alerte') OR _request('optin_nl_thematique')) {
        $erreurs['optin'] = 'Merci de nous indiquer votre choix : alerte email et/ou newsletter programme';
    }

    if (count($erreurs))
        $erreurs['message_erreur'] = 'Votre saisie contient des erreurs !';
    return $erreurs;
}

function formulaires_bouton_suivre_traiter_dist()
{
    include_spip('inc/session');
    $email = _request('email');
    $date_naissance = split('/', _request('naissance'));
    $timestamp = mktime(0, 0, 0, $date_naissance[1], $date_naissance[0], $date_naissance[2]);
    $naissance = date("Y-m-d H:i:s", $timestamp);
    $sexe = _request('sexe');
    $id_article = _request('id_article');
    $id_mot = _request('id_mot');
    $optin_ar = _request('optin_ar');
//    var_dump($optin_ar);echo '</br>';die;
    $optin_alerte = "0";
    $optin_nl_thematique = "0";
    $optin = _request('optin');
    if ($optin[0] == "1" AND is_null($optin[1])) {
        $optin_alerte = "1";
    }
    if ($optin[0] == "2" AND is_null($optin[1])) {
        $optin_nl_thematique = "1";
    }
    if ($optin[0] == "1" AND $optin[1] == "2") {
        $optin_alerte = "1";
        $optin_nl_thematique = "1";
    }
    /*echo var_dump($optin_alerte)."</br>";
    echo var_dump($optin_nl_thematique);die;*/
    $date = date_create('now');
    $date_creation = date_format($date, 'Y-m-d H:i:s');
    $where = array(
        "email_suivis = '" . $email . "'",
        "id_mot = " . $id_mot
    );
    $resultats = sql_select('email_suivis', 'suivis_infos', $where);
    //$row = sql_fetch($resultats);
    $where_mot = array(
        "id_mot = " . $id_mot
    );
    $res_mot = sql_select('titre', 'spip_mots', $where_mot);
    $row_titre = sql_fetch($res_mot);
    $mot_cle = $row_titre['titre'];
//        echo $mot_cle;die;
    $count = sql_count($resultats);
    //echo $count;
    if ($count > 0) {
        session_set('email_utilise', 'ok');
    } else {
        $tab = array('email_suivis' => $email,
            'date_naissance' => $naissance,
            'sexe_suivis' => $sexe,
            'id_article' => $id_article,
            'id_mot' => $id_mot,
            'date_envois_mail' => '',
            'date_creation' => $date_creation,
            'optin_ar' => $optin_ar,
            'optin_alerte' => $optin_alerte,
            'optin_nl_thematique' => $optin_nl_thematique);
        $id = sql_insertq('suivis_infos', $tab);
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) {
            $passage_ligne = "\r\n";
        } else {
            $passage_ligne = "\n";
        }
        $sujet = "Votre alerte antennereunion.fr - Merci pour votre inscription";
        $header = "From: \"antennereunion.fr\"<contact@antennereunion.fr>" . $passage_ligne;
        $header .= "Reply-to: \"antennreunion.fr\" <contact@antennereunion.fr>" . $passage_ligne;
        $header .= "MIME-Version: 1.0" . $passage_ligne;
        $header .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
        $date_now = (strftime("%A %d %B"));
        $message = '<html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <style type="text/css">
                    #outlook a{
                        padding:0;
                    }
                    a:active,a:focus{
                        outline:none;
                    }
                    img{
                        outline:none;
                        text-decoration:none;
                    }
                    a img{
                        border:none;
                    }
                    table td{
                        border-collapse:collapse;
                    }
                    table{
                        border-collapse:collapse;
                        mso-table-lspace:0pt;
                        mso-table-rspace:0pt;
                    }
                    a{
                        color:inherit;
                        text-decoration:none;
                    }
                </style>
            </head>
            <body bgcolor="#ffffff" link="#333333" vlink="#333333" alink="#333333">
              <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <!--<td id="webversion" style="font-family:Arial,Helvetica,sans-serif; font-size:10px; height:20px; text-align:center"> Si vous n\'arrivez pas &agrave; lire ce message, <a target="_blank" class="adminText" href="*|ARCHIVE|*" style="color: #1f4563; text-decoration: none;">cliquez-ici </a></td>-->
                    </tr>
              </table>
              <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td><a target="_blank" href="http://www.antennereunion.fr/"><img style="display:block;" width="580" height="72" alt="logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/header-ar-nl-alerte.jpg"></a></td>
                    </tr>
              </table>
              <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                     <td height="15">  </td>
                    </tr>
              </table>
              <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                      <td height="36"><font face="Arial" size="2">' . utf8_encode($date_now) . '</font></td>
                    </tr>
              </table>
               <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                     <td height="5">&nbsp;</td>
                    </tr>
              </table>
              <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                    <tr>
                        <td><font face="Arial" size="3" color="#333333">Bonjour,</font></td>
                    </tr>
                    <tr>
                        <td height="12">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <font face="Arial" size="3" color="#333333">
                                                    Votre inscription a bien &eacute;t&eacute; prise en  compte. <br>';
        if ($optin[0] == "1" AND is_null($optin[1])) {
            $message .= 'Vous recevrez une alerte d&egrave;s qu\'un article sur &laquo; ' . $mot_cle . ' &raquo; sera publi&eacute;.';
        }
        if ($optin[0] == "2" AND is_null($optin[1])) {
            $message .= 'Vous recevrez une newsletter sur le programme &laquo; ' . $mot_cle . ' &raquo;  tous les 15 jours. ';
        }
        if ($optin[0] == "1" AND $optin[1] == "2") {
            $message .= 'Vous recevrez une alerte d&egrave;s qu\'un article sur &laquo; ' . $mot_cle . ' &raquo; sera publi&eacute;, ainsi que la newsletter du programme tous les 15 jours.';
        }
        $message .= '</font>
                        </td>
                    </tr>
           </table>
            <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                <tr>
                    <td height="15">  </td>
                </tr>
            </table>
            <table width="580" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td align="left" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif;">Envoy&eacute; par antennereunion.fr</td>
                </tr>
                <tr>
                    <td height="15"></td>
                </tr>
            </table>
            <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                <tr>
                    <td height="15">  </td>
                </tr>
            </table>
            <table width="600" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                    <td width="10">&nbsp;</td>
                    <td width="580" valign="top" height="42">
                        <table width="580" bgcolor="#101f2c" align="center" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td height="42" width="108" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> Suivez-nous </td>
                                <td height="42" width="38" align="center"> <a target="_blank" href="https://www.facebook.com/antennereunion?ref=ts&fref=ts"> <img style="display:block;" alt="facebook" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico_facebook.jpg"> </a> </td>
                                <td height="42" width="36" align="center"> <a target="_blank" href="https://twitter.com/antennereunion"> <img style="display:block;" alt="twitter" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico-twitter.jpg"> </a> </td>
                                <td height="42" width="38" align="center"> <a target="_blank" href="https://plus.google.com/107164283983194784588/posts"> <img style="display:block;" alt="plus google" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico-google.jpg"> </a> </td>
                                <td height="42" width="120" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">&nbsp;  </td>
                                <td height="42" width="100" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> <a target="_blank" href="https://itunes.apple.com/fr/app/antenne-reunion-television/id927960182?mt=8"> <img style="display:block;" alt="" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/app_store.jpg"> </a> </td>
                                <td height="42" width="140" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> <a target="_blank" href="https://play.google.com/store/apps/details?id=com.goodbarber.antennereunion"> <img style="display:block;" alt="antennereunion" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/google_play.jpg"> </a> </td>
                            </tr>
                        </table>
                    </td>
                         <td width="10">&nbsp;</td>
                    </tr>
                    </table>
                    <table width="580" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                    <td height="10"></td>
                            </tr>
                            <tr>
                                    <td height="10"></td>
                            </tr>
                            <tr>
                                    <!--<td align="center" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif;"><font face="Arial" size="1">Si vous ne souhaitez plus recevoir de communication de la part d’AntenneReunion.fr :</font> <a href="*|UNSUB|*" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif; text-decoration:none;"><strong><font face="Arial" size="2">Suivez ce lien</font></strong></a></td>-->
                            </tr>
                            <tr>
                                    <td height="15"></td>
                            </tr>
                    </table>
            </body>
        </html>';
        //echo $message;die;
        //echo "urls".$urls;
        mail($email, $sujet, $message, $header);
        session_set('message_ok', 'ok');
    }
    return true;
}

?>