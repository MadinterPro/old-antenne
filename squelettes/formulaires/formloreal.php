<?php
function formulaires_formloreal_charger_dist() {
    $valeurs = array(
	    'nom' => '',
        'prenom' => '',
        'telephone' => '',
		'mail' => '',
		'cp' => ''
    );

    return $valeurs;
}

function formulaires_formloreal_verifier_dist() {
    $erreurs = array();
    foreach (array('nom', 'prenom','telephone','mail','cp') as $obligatoire)
        if (!_request($obligatoire))
            $erreurs[$obligatoire] = 'Ce champ est obligatoire';

    if (_request('telephone') && strlen(_request('telephone')) < 10 && !is_int(_request('telephone')))
        $erreurs['telephone'] = 'Ce champ est invalide';

    include_spip('inc/filtres');
    if (_request('mail') AND !email_valide(_request('mail')))
            $erreurs['mail'] = 'Adresse email non valide';
    
    if (count($erreurs))
        $erreurs['message_erreur'] = 'Votre saisie contient des erreurs !';

    return $erreurs;
}

/**
 * 
 * @return type
 * Surtitre : Nom ; Titre : Selfie du Nom Prenom, Soustitre :Prenom, descriptif : adresse complet(adresse, ville, codepostal), Texte : texte, Chapo : Telephone
 */
function formulaires_formloreal_traiter_dist() {
	
}
