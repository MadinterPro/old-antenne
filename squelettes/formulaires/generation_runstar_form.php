<?php

define("DATA_BASE_TABLE_NEWSLETTER", "spip_nl_user");
define("DATA_BASE_TABLE_USER_NEWSLETTER", "spip_nl_user_letter");
define("DATA_BASE_TABLE_ENREGISTREMENT_RUNSTAR", "spip_reservation_runstar");

function formulaires_generation_runstar_form_charger_dist() {
    $valeurs = array(
        'nom' => '',
        'prenom' => '',
        'mail' => '',
        'date_enregistrement' => '',
        'nombre_personne' => '',
        'date_enregistrement_texte' => '',
        'newsletter' => '',
    );

    return $valeurs;
}

function formulaires_generation_runstar_form_verifier_dist() {
    $erreurs = array();
    foreach (array('nom', 'prenom', 'mail', 'date_enregistrement', 'nombre_personne') as $obligatoire)
        if (!_request($obligatoire))
            $erreurs[$obligatoire] = 'Ce champ est obligatoire';


    include_spip('inc/filtres');
    if (_request('mail') AND ! email_valide(_request('mail')))
        $erreurs['mail'] = 'Adresse email non valide';

    if (count($erreurs))
        $erreurs['message_erreur'] = 'Votre saisie contient des erreurs !';
    return $erreurs;
}

/**
 * 
 * @return type
 * 
 */
function formulaires_generation_runstar_form_traiter_dist() {

    $nom = ucfirst(trim(addslashes(_request('nom'))));
    $prenom = ucfirst(trim(addslashes(_request('prenom'))));
    $mail = trim(_request('mail'));
    $date_enregistrement_type = _request('date_enregistrement');
    $date_enregistrement = ucfirst(trim(addslashes(_request('date_enregistrement_texte'))));
    $nombre_personne = _request('nombre_personne');

    $return_enregistrement = sql_insertq(DATA_BASE_TABLE_ENREGISTREMENT_RUNSTAR, array(
        'nom' => $nom,
        'prenom' => $prenom,
        'mail' => $mail,
        'date_enregistrement_type' => $date_enregistrement_type,
        'date_enregistrement' => $date_enregistrement,
        'nombre_personne' => $nombre_personne,
        'date_reservation' => date("Y-m-d H:i:s")
    ));
    if (!is_null($return_enregistrement)) {
        $id_reservation = mysql_insert_id();
        include_spip('inc/session');
        session_set('id_reservation', $id_reservation);
        if (_request('newsletter') == "on") {
            $id_exist = sql_getfetsel('id', array(DATA_BASE_TABLE_NEWSLETTER), array("mail_user = '" . trim(_request('mail')) . "'"));
            if (is_null($id_exist)) {
                $last__newsletterid = sql_insertq(DATA_BASE_TABLE_NEWSLETTER, array(
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'mail_user' => $mail,
                    'statut' => 1
                ));
                sql_insertq(DATA_BASE_TABLE_USER_NEWSLETTER, array(
                    'id_user' => $last__newsletterid,
                    'id_letter' => 2
                ));
            } else {
                sql_update(DATA_BASE_TABLE_NEWSLETTER, array('nom' => $nom, 'prenom' => $prenom), "id=$id_exist");
            }
        }

        return array(
            'message_ok' => 'Votre reservation a bien été enregistrée !',
        );
    } else {
        return array('message_erreur' => 'Veuillez imprimer votre invitation.');
    }
}
