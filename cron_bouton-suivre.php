<?php

/**
 * @author 
 * Cron pour envoi de mail aux internautes qui ont fait une inscription depuis le bouton Suivre infos
 * @copyright 2015
 */
set_time_limit(0);
error_reporting(E_ALL);
$rows_article = selectMotCle();
//Recuperation du premier mot clé 
foreach ($rows_article as $row_article) {
    $id_article = $row_article["id_article"];
    $id_mot = $row_article["id_mot"];
    $date_article_suivis = $row_article["date_creation"];
    $date_envois_mail = $row_article["date_envois_mail"];
    $mail = $row_article["email_suivis"];
    if ($id_mot != 0) {
        $rows_envoi_mails = selectArticleEnvoiMail($date_article_suivis, $id_mot, $date_envois_mail, $id_article);
//        $count = selectcountArticleEnvoiMail($date_article_suivis, $id_mot, $date_envois_mail, $id_article);
//        foreach ($rows_envoi_mails as $key=>$val){
//            $tab[]=$val;
//        }
//        var_dump($tab);die;
        $count = $rows_envoi_mails->rowCount();
//        echo '<br>', $count;
        /* if (isset($row_envoi_mails) && count($row_envoi_mails) == 1)
          $libelle = 'Une article vient d\'être publiée sur LINFO.re.
          Cliquez sur le lien ci-dessous pour le lire';
          elseif (isset($row_envoi_mails) && count($row_envoi_mails) != 1) */
        if ($count > 0) {

            if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) {
                $passage_ligne = "\r\n";
            } else {
                $passage_ligne = "\n";
            }
            $titre = selectLibelleMotsCles($id_mot);
            $libelle = $count . ' article(s) sur le sujet &laquo; ' . utf8_encode($titre) . ' &raquo viennent d\'être publiée(s) sur antennereunion.fr.<br>
                        Cliquez sur les liens ci-dessous pour les lire : ';
            $sujet = 'Votre alerte sur le sujet "' . utf8_encode($titre) . '"';
            $header = "From: \"antennereunion.fr\"<contact@antennereunion.fr>" . $passage_ligne;
            $header.= "Reply-to: \"antennereunion.fr\" <contact@antennereunion.fr>" . $passage_ligne;
            $header.= "MIME-Version: 1.0" . $passage_ligne;
            $header.= 'Content-type: text/html; charset=utf-8' . "\r\n";
            setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
            $date_now = (strftime("%A %d %B"));
            $message = '<html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <style type="text/css">
                        #outlook a{
                                padding:0;
                        }
                        a:active,a:focus{
                                outline:none;
                        }
                        img{
                                outline:none;
                                text-decoration:none;
                        }
                        a img{
                                border:none;
                        }
                        table td{
                                border-collapse:collapse;
                        }
                        table{
                                border-collapse:collapse;
                                mso-table-lspace:0pt;
                                mso-table-rspace:0pt;
                        }
                        a{
                                color:inherit;
                                text-decoration:none;
                        }
                    </style>
                </head>
                <body bgcolor="#ffffff" link="#333333" vlink="#333333" alink="#333333">
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                        <tr>
                          <!--<td id="webversion" style="font-family:Arial,Helvetica,sans-serif; font-size:10px; height:20px; text-align:center"> Si vous n\'arrivez pas &agrave; lire ce message, <a target="_blank" class="adminText" href="*|ARCHIVE|*" style="color: #1f4563; text-decoration: none;">cliquez-ici </a></td>-->
                        </tr>
                  </table>
                  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                        <tr>
                          <td><a target="_blank" href="http://www.antennereunion.fr/"><img style="display:block;" width="580" height="72" alt="logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/header-ar-nl-alerte.jpg"></a></td>
                        </tr>
                  </table>
                  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                        <tr>
                         <td height="15">  </td>
                        </tr>
                  </table>
                  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                        <tr>
                          <td height="36"><font face="Arial" size="2">' . utf8_encode($date_now) . '</font></td>
                        </tr>
                  </table>
                   <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                        <tr>
                         <td height="5">&nbsp;</td>
                        </tr>
                  </table>
                  <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                        <tr><td><font face="Arial" size="3" color="#333333">Bonjour,</font></td></tr>
                        <tr><td height="12">&nbsp;</td></tr>';

                    $message .='<tr><td>
                                    <p><font face="Arial" size="3" color="#333333">' . $libelle . '</font></p>';
                    foreach ($rows_envoi_mails as $row_envoi_mails) {
                        $id_suivis_final = $row_article["id_suivis"];
                        $urls_final = $row_envoi_mails["url"];
                        $titre_article_final = $row_envoi_mails["titre_article"];
                        $titre_final = $row_envoi_mails["titre"];
                        $urls = "http://www.antennereunion.fr/" . $urls_final;
                        $liens_desinscription = "http://www.antennereunion.fr/desinscription-alerte-bouton-suivre?id_suivis=" . $id_suivis_final . "&id_mot=" . $id_mot;
                        $message .='<p> - <a target="_blank" href="' . $urls . '" target="_blank"><font face="Arial" size="3" color="#333333">' . utf8_encode($titre_article_final) . '</font></a></p>';
                    }
                    $message .='<p>&nbsp;</p>
                                </td></tr></table>';
                    $message .='<table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                    <tr>
                                        <td height="15"></td>
                                    </tr>
                                </table>
                                <table width="580" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td align="center" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif;"><font face="Arial" size="1">Envoy&eacute; par antennereunion.fr<fon></td>
                                    </tr>
                                </table>
                                <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                    <tr>
                                        <td height="15"></td>
                                    </tr>
                                </table>
                                <table width="600" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td width="10">&nbsp;</td>
                                        <td width="580" valign="top" height="42">
                                            <table width="580" bgcolor="#101f2c" align="center" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td height="42" width="108" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> Suivez-nous </td>
                                                    <td height="42" width="38" align="center"> <a target="_blank" href="https://www.facebook.com/antennereunion?ref=ts&fref=ts"> <img style="display:block;" alt="facebook" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico_facebook.jpg"> </a> </td>
                                                    <td height="42" width="36" align="center"> <a target="_blank" href="https://twitter.com/antennereunion"> <img style="display:block;" alt="twitter" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico-twitter.jpg"> </a> </td>
                                                    <td height="42" width="38" align="center"> <a target="_blank" href="https://plus.google.com/107164283983194784588/posts"> <img style="display:block;" alt="plus google" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico-google.jpg"> </a> </td>
                                                    <td height="42" width="120" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">&nbsp;  </td>
                                                    <td height="42" width="100" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> <a target="_blank" href="https://itunes.apple.com/fr/app/antenne-reunion-television/id927960182?mt=8"> <img style="display:block;" alt="" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/app_store.jpg"> </a> </td>
                                                    <td height="42" width="140" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> <a target="_blank" href="https://play.google.com/store/apps/details?id=com.goodbarber.antennereunion"> <img style="display:block;" alt="antennereunion" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/google_play.jpg"> </a> </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="10">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="580" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td height="10"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif;"><font face="Arial" size="1">Si vous ne souhaitez plus recevoir de communication de la part d\'AntenneReunion.fr :</font> <a href="'. $liens_desinscription .'" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif; text-decoration:none;"><strong><font face="Arial" size="2">Suivez ce lien</font></strong></a></td>
                                    </tr>
                                    <tr>
                                        <td height="15"></td>
                                    </tr>
                                </table>
                </body>
            </html>';
            $mail = utf8_decode(decodeChars($mail));
            $message = utf8_decode(decodeChars($message));
            $header = utf8_decode(decodeChars($header));
            
            if (mail($mail, $sujet, $message, $header)) {
                echo "Mail envoy&eacute;<br>";
                $date_envoi = date("Y-m-d H:i:s");
                updateSuivis($date_envoi, $id_suivis_final);
                echo "MAJ termin&eacute;<br>";
            } else
                echo "Aucun mail à envoyer";
        }
    }
}

function selectDb() {
    $dsn = 'mysql:dbname=antenne_art_spip;host=localhost';
    $user = 'root';
    $password = '';
    global $dbh;
    $user = 'antenne_spipAR';
    $password = 'antv3_2009';
    try {
        $dbh = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }
    return $dbh;
}

//Récupération des articles suivis
function selectMotCle() {
    $dbh = selectDb();
    $sql_article = 'SELECT a.id_article,s.id_mot,s.date_envois_mail,s.id_suivis, s.email_suivis,s.date_creation
    FROM  spip_articles as a , suivis_infos as s
    WHERE a.id_article = s.id_article';
//    echo $sql_article;
    $rows_article = $dbh->query($sql_article);
    return $rows_article;
    $dbh = null;
}

//Récupération du premier mot clé à partir de l'article suivi
function selectLibelleMotsCles($id_mot) {
    $dbh = selectDb();
    $sql_suivre = "SELECT titre FROM  spip_mots  WHERE id_mot=" . $id_mot;
    //echo $sql_suivre;
    $rows_suivre = $dbh->query($sql_suivre);
    foreach ($rows_suivre as $val)
        $titre = $val["titre"];
    return $titre;
    $dbh = null;
}

//Mis à jour de la table par la date de lancement du cron (date d'envoi de mail)
function updateSuivis($date_envoi, $id_suivis_final) {
    $dbh = selectDb();
    $update = "UPDATE suivis_infos SET date_envois_mail=? WHERE id_suivis=?";
    $q = $dbh->prepare($update);
    $q->execute(array($date_envoi, $id_suivis_final));
    $dbh = null;
}

//Recupération des articles à envoyer à partir de la date de creation ou la date du dernier envoi de mail
function selectArticleEnvoiMail($date_article_suivis, $id_mot, $date_envois_mail, $id_article) {
    $dbh = selectDb();
//    $sql_envoi_mails = 'SELECT DISTINCT a.id_article, m.titre, a.titre AS titre_article, u.url
//    FROM spip_mots AS m, spip_mots_liens AS l, spip_articles AS a, spip_urls AS u
//    WHERE m.id_mot = l.id_mot
//    AND a.id_article = l.id_objet
//    AND a.id_article = u.id_objet
//    AND a.statut = "publie"
//    AND a.id_article<>' . $id_article . '
//    AND a.date >= "' . $date_article_suivis . '"
//    AND m.id_mot= ' . $id_mot;

    $sql_envoi_mails = 'SELECT DISTINCT a.id_article, m.titre, a.titre AS titre_article, u.url
    FROM (
        (
            spip_articles a
            INNER JOIN spip_mots_liens l ON a.id_article = l.id_objet
        )
        LEFT JOIN spip_mots m ON l.id_mot = m.id_mot
    )
    INNER JOIN spip_urls u ON a.id_article = u.id_objet
    WHERE a.statut = "publie"
    AND a.id_article <> ' . $id_article . '
    AND a.date >= "' . $date_article_suivis . '"
    AND m.id_mot = ' . $id_mot;

    $date = new DateTime($date_envois_mail);
    $date_form = $date->format('Y-m-d');
    if ($date_form != "-0001-11-30")
        $sql_envoi_mails .= ' AND a.date>"' . $date_envois_mail . '"';
    $sql_envoi_mails .= ' GROUP BY a.id_article';
//    echo $sql_envoi_mails."<br>";
    $rows_envoi = $dbh->query($sql_envoi_mails);
//    var_dump($rows_envoi);
    return $rows_envoi;
    $dbh = null;
}

function selectcountArticleEnvoiMail($date_article_suivis, $id_mot, $date_envois_mail, $id_article) {
    $dbh = selectDb();
    $sql_envoi_mails = 'SELECT count(*) as count
    FROM spip_mots AS m, spip_mots_liens AS l, spip_articles AS a, spip_urls AS u
    WHERE m.id_mot = l.id_mot
    AND a.id_article = l.id_objet
    AND a.id_article = u.id_objet
    AND a.statut="publie"
    AND a.id_article<>' . $id_article . '
    AND a.date >= "' . $date_article_suivis . '"
    AND m.id_mot= ' . $id_mot;
    //echo $sql_envoi_mails;
    $date = new DateTime($date_envois_mail);
    $date_form = $date->format('Y-m-d');
    if ($date_form != "-0001-11-30")
        $sql_envoi_mails .= " AND a.date>'" . $date_envois_mail . "'";
//    echo $sql_envoi_mails;die;
    $rows_envoi = $dbh->query($sql_envoi_mails);
    foreach ($rows_envoi as $val)
        return $val["count"];
    $dbh = null;
}

function decodeChars($phrase) {
    $search = array("\'", "À", "Á", "Â", "Ã", "Ä", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ù", "Ú", "Û", "Ü", "à", "á", "â", "ã", "ä", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", "ñ", "ò", "ó", "ô", "õ", "ö", "ù", "ú", "û", "ü", "ý", "ÿ", "£");
    $replace = array("&apos;", "&Agrave;;", "&Aacute;", "&Acirc;", "&Atilde;", "&Auml;", "&Ccedil;", "&Egrave;", "&Eacute;", "&Ecirc;", "&Euml;", "&Igrave;", "&Iacute;", "	&Icirc;", "&Iuml;", "&Ntilde;", "&Ograve;", "&Oacute;", "&Ocirc;", "&Otilde;", "&Ouml;", "&Ugrave;", "&Uacute;", "&Ucirc;", "&Uuml;", "&agrave;", "&aacute;", "&acirc;", "&atilde;", "&auml;", "&aelig;", "&ccedil;", "&egrave;", "&eacute;", "&ecirc;", "&euml;", "&igrave;", "&iacute;", "&icirc;", "&iuml;", "&ntilde;", "&ograve;", "&oacute;", "&ocirc;", "&otilde;", "&ouml;", "&ugrave;", "&uacute;", "&ucirc;", "&uuml;", "&yacute;", "&yuml;", "&pound;");
    $phrase = str_replace($search, $replace, $phrase);
    return $phrase;
}

// $test = "Vous \"pouvez\" dès à 'présent' vous \'connecter\' au site et poster des commentaires, des photos et des vidéos. à â ël ñino Ñ tête l'Öô ù ÿ £ $ <br /><br /><br />";
//	echo decodeChars("Vous \"pouvez\" dès à 'présent' vous \'connecter\' au site et poster des commentaires, des photos et des vidéos. à â ël ñino Ñ tête l'Öô ù ÿ £ $ <br /><br /><br />");

function template_mail($urls_final, $titre_final, $titre_article_final, $liens_desinscription) {
    //$date_now = (strftime("%A %d %B %Y"));
    $message_html = '<html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<style type="text/css">
                            #outlook a{
                                    padding:0;
                            }
                            a:active,a:focus{
                                    outline:none;
                            }
                            img{
                                    outline:none;
                                    text-decoration:none;
                            }
                            a img{
                                    border:none;
                            }
                            table td{
                                    border-collapse:collapse;
                            }
                            table{
                                    border-collapse:collapse;
                                    mso-table-lspace:0pt;
                                    mso-table-rspace:0pt;
                            }
                            a{
                                    color:inherit;
                                    text-decoration:none;
                            }
			</style>
                    </head>
                    <body bgcolor="#ffffff" link="#333333" vlink="#333333" alink="#333333">
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                            <tr>
                                <!--<td id="webversion" style="font-family:Arial,Helvetica,sans-serif; font-size:10px; height:20px; text-align:center"> Si vous n\'arrivez pas &agrave; lire ce message, <a target="_blank" class="adminText" href="*|ARCHIVE|*" style="color: #1f4563; text-decoration: none;">cliquez-ici </a></td>-->
                            </tr>
                        </table>
                        <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                            <tr>
                                  <td><a target="_blank" href="http://www.antennereunion.fr/"><img style="display:block;" width="580" height="72" alt="logo antennereunion" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/header-ar-nl-alerte.jpg"></a></td>
                            </tr>
                        </table>
                        <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                            <tr>
                                <td height="15">  </td>
                            </tr>
                        </table>
                        <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                            <tr>
                                <td height="36"><font face="Arial" size="2">'.utf8_encode($date_now).'</font></td>
                            </tr>
                        </table>
                        <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                            <tr>
                                <td height="5">&nbsp;</td>
                            </tr>
                        </table>
                        <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                            <tr>
                                <td>
                                    <font face="Arial" size="3" color="#333333">Bonjour,</font>
                                </td>
                            </tr>
                            <tr>
                                <td height="12">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial" size="3" color="#333333">
                                        Une article &laquo; ' . utf8_encode($titre_final) . ' &raquo vient d\'être publiée sur antennereunion.fr.
                                        Cliquez sur le lien ci-dessous pour le lire 
                                        <a color="#333333" target="_blank" href="' . $urls_final . '" target="_blank" >' . $titre_article_final . '</a><br/>
                                    </font>
                                </td>
                            </tr>
                        </table>
                        <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                            <tr>
                                <td height="15"></td>
                            </tr>
                        </table>
                        <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                            <tr>
                                <td height="15">  </td>
                            </tr>
                        </table>
                        <table width="600" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td width="10">&nbsp;</td>
                                <td width="580" valign="top" height="42">
                                    <table width="580" bgcolor="#101f2c" align="center" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td height="42" width="108" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> Suivez-nous </td>
                                            <td height="42" width="38" align="center"> <a target="_blank" href="https://www.facebook.com/antennereunion?ref=ts&fref=ts"> <img style="display:block;" alt="facebook" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico_facebook.jpg"> </a> </td>
                                            <td height="42" width="36" align="center"> <a target="_blank" href="https://twitter.com/antennereunion"> <img style="display:block;" alt="twitter" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico-twitter.jpg"> </a> </td>
                                            <td height="42" width="38" align="center"> <a target="_blank" href="https://plus.google.com/107164283983194784588/posts"> <img style="display:block;" alt="plus google" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/ico-google.jpg"> </a> </td>
                                            <td height="42" width="120" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">&nbsp;  </td>
                                            <td height="42" width="100" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> <a target="_blank" href="https://itunes.apple.com/fr/app/antenne-reunion-television/id927960182?mt=8"> <img style="display:block;" alt="" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/app_store.jpg"> </a> </td>
                                            <td height="42" width="140" align="center" style="color:#ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px;"> <a target="_blank" href="https://play.google.com/store/apps/details?id=com.goodbarber.antennereunion"> <img style="display:block;" alt="antennereunion" src="http://www.antennereunion.fr/newsletter-element/newsletter-ar/google_play.jpg"> </a> </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10">&nbsp;</td>
                            </tr>
                        </table>
                        <table width="580" bgcolor="#ffffff" align="center" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td height="10"></td>
                          </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td align="center" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif;">
                                <font face="Arial" size="1">Si vous ne souhaitez plus recevoir de communication de la part d\'AntenneReunion.fr :</font>
                                <a href="'. $liens_desinscription .'" style="color:#1f4563; font-size: 11px; font-family: Arial, Helvetica, sans-serif; text-decoration:none;"><strong><font face="Arial" size="2">Suivez ce lien</font></strong></a>
                            </td>
                        </tr>
                        <tr>
                            <td height="15"></td>
                        </tr>
                    </table>
                </body>
            </html>';
    //echo $message_html;
    return $message_html;
}

?>