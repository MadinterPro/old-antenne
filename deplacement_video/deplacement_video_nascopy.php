﻿<?php
/**
 * @DESC: Fichier pour le traitement de l'importation automatique Video
 *
 */
set_time_limit(200);
include ("/home/antenne/public_html/scins/tableaux_emissions.php");

$f = fopen("/home/antenne/public_html/deplacement_video/videonas_log", "a+");
echo "<p style='color:#295CAA;'><b><u>DEBUT TRAITEMENT DEPLACEMENT</u></b></p><br />";

//acces au dossier videos_tampon
chdir('/home/antenne/public_html/videos_tampon');
echo 'Dossier à traiter : ' . getcwd() . "<br />";

echo "<p style='color:#1EBABC;'><b><u>Liste des vidéos dans vantage</u></b></p><br />";

/* recupére la liste des fichiers contenu dans le repertoire en cours, soit videos_tampon */
$_list_files_videos = glob("*.*");
if ( sizeof($_list_files_videos) >= 1) {
    echo "<pre>";
    print_r($_list_files_videos) . "<br>";
    echo "</pre>";

    for ($i = 0; $i < sizeof($_list_files_videos); $i++) {

        $_list_podcast_ = explode(' ', $_list_files_videos[$i]);
        $a = sizeof($_list_podcast_);

        $temp_chemin = $_list_podcast_[$a - 1];

        $tab_emissions = explode('_', $temp_chemin);
        $titre_emission = $tab_emissions[0];

        echo "<p style='color:#1EBABC;'><b><u>Vidéos Tampon n° : " . $i . "</u></b></p><br />";

        $file = "/home/antenne/public_html/videos_tampon/" . $_list_podcast_[$a - 1];
        var_dump($file);
        echo "<br />";
        echo "<p style='color:#b82720;'><b><u>Extension et nom du fichier : </u></b></p>" . $_list_podcast_[$a - 1];

        if ($_list_podcast_[$a - 1] != ".flv" && $_list_podcast_[$a - 1] != ".jpeg" && $_list_podcast_[$a - 1] != ".mp4") {
            echo "var_dump : _list_podcast_[a - 1] => ";
            var_dump($_list_podcast_[$a - 1]);
            if (in_array(trim($titre_emission), $tableau_test_emission)) {
                echo "copie vers dossier emission NAS";
                //ecriture dans log
                $tailleemission = filesize($file);
                if (file_exists($file)) {
                    $dateficemission = date('Y-m-d H:i:s');
                }
                fwrite($f, $dateficemission . " : La Taille du fichier : " . $file . " est de : " . $tailleemission . " Octets\r\n");
                //fin ecriture dans log
                if (copy($file, 'ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/scins/emissions/' . $_list_podcast_[$a - 1])) {
                    echo "Copie de " . $file . " <p style='color:green;'><b>Vers public/scins/emissions/ => OK </b></p><br />";
                    unlink($_list_podcast_[$a - 1]);
                    echo "<p style='color:#ad6704;'><b>Suppression du fichier " . $_list_podcast_[$a - 1] . " => OK<b></p><br />";
                } else {
                    echo "<p style='color:red;'><b>1:::Erreur de Transfert et pas de Suppression</b>" . $file . "</b><p><br>";
                    $to = 'johary.rakoto@antennereunion.fr' . ',';
                    $to .= 'rado.randriamalala@antennereunion.fr';
                    $subject = 'Videos avec erreur';
                    $message = 'La vid&eacute;o ' . $file . ' pr&eacute;sente une erreur de d&eacute;placement';
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
                    $headers .= 'From:johary.rakoto@antennereunion.fr' . "\r\n" .
                            'Reply-To: johary.rakoto@antennereunion.fr' . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();
                    mail($to, $subject, $message, $headers);
                }
            } elseif (trim($titre_emission) == "INFO") {
                echo "copie vers dossier infosoiree NAS";
                //ecriture dans log
                $tailleinfosoiree = filesize($file);
                if (file_exists($file)) {
                    $dateficinfosoiree = date('Y-m-d H:i:s');
                }
                fwrite($f, $dateficinfosoiree . " : La Taille du fichier : " . $file . " est de : " . $tailleinfosoiree . " Octets\r\n");
                //fin ecriture dans log
                if (copy($file, 'ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/infosoiree/infosoiree/' . $_list_podcast_[$a - 1])) {
                    echo "Copie de " . $file . " <p style='color:green;'><b>Vers public/infosoiree/infosoiree/ => OK </b></p> <br />";
                    unlink($_list_podcast_[$a - 1]);
                    echo "<p style='color:#ad6704;'><b>Suppression du fichier " . $_list_podcast_[$a - 1] . " => OK<b></p><br />";
                } else {
                    echo "<p style='color:red;'><b>2:::Erreur de Transfert et pas de Suppression" . $file . "</b><p><br>";
                    $to = 'johary.rakoto@antennereunion.fr' . ',';
                    $to .= 'rado.randriamalala@antennereunion.fr';
                    $subject = 'Videos avec erreur';
                    $message = 'La vidéo ' . $file . ' pr&eacute;sente une erreur de d&eacute;placement';
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
                    $headers .= 'From:johary.rakoto@antennereunion.fr' . "\r\n" .
                            'Reply-To: johary.rakoto@antennereunion.fr' . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();
                    mail($to, $subject, $message, $headers);
                }
            }elseif(trim($titre_emission) == "METEOSOIR") {
                echo "copie vers dossier meteosoir NAS";
                //ecriture dans log
                $taillemeteosoir = filesize($file);
                if (file_exists($file)) {
                    $dateficmeteosoir = date('Y-m-d H:i:s');
                }
                fwrite($f, $dateficmeteosoir . " : La Taille du fichier : " . $file . " est de : " . $taillemeteosoir . " Octets\r\n");
                //fin ecriture dans log
                if (copy($file, 'ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/meteosoir/meteosoir/' . $_list_podcast_[$a - 1])) {
                    echo "Copie de " . $file . " <p style='color:green;'><b>Vers public/meteosoir/meteosoir/ => OK </b></p> <br />";
                    unlink($_list_podcast_[$a - 1]);
                    echo "<p style='color:#ad6704;'><b>Suppression du fichier " . $_list_podcast_[$a - 1] . " => OK<b></p><br />";
                } else {
                    echo "<p style='color:red;'><b>2:::Erreur de Transfert et pas de Suppression" . $file . "</b><p><br>";
                    $to = 'johary.rakoto@antennereunion.fr' . ',';
                    $to .= 'rado.randriamalala@antennereunion.fr';
                    $subject = 'Videos avec erreur';
                    $message = 'La vidéo ' . $file . ' pr&eacute;sente une erreur de d&eacute;placement';
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
                    $headers .= 'From:johary.rakoto@antennereunion.fr' . "\r\n" .
                        'Reply-To: johary.rakoto@antennereunion.fr' . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
                    mail($to, $subject, $message, $headers);
                }
            } else {
                //ecriture dans log
                echo "copie vers dossier public NAS";
                $taillepublic = filesize($file);
                if (file_exists($file)) {
                    $dateficpublic = date('Y-m-d H:i:s');
                }
                fwrite($f, $dateficpublic . " : La Taille du fichier : " . $file . " est de : " . $taillepublic . " Octets\r\n");
                //fin ecriture dans log
                if (copy($file, 'ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/' . $_list_podcast_[$a - 1])) {
                    echo "Copie de " . $file . " <p style='color:green;'><b>Vers public/ => OK</b><p> <br />";
                    unlink($_list_podcast_[$a - 1]);
                    echo "<p style='color:#ad6704;'><b>Suppression du fichier " . $_list_podcast_[$a - 1] . " => OK<b></p><br />";
                } else {
                    echo "<p style='color:red;'><b>3:::Erreur de Transfert et pas de Suppression" . $file . "</b><p><br>";
                    $to = 'johary.rakoto@antennereunion.fr' . ',';
                    $to .= 'rado.randriamalala@antennereunion.fr';
                    $subject = 'Videos avec erreur';
                    $message = 'La vid&eacute;o ' . $file . ' pr&eacute;sente une erreur de d&eacute;placement';
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
                    $headers .= 'From:johary.rakoto@antennereunion.fr' . "\r\n" .
                            'Reply-To: johary.rakoto@antennereunion.fr' . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();
                    mail($to, $subject, $message, $headers);
                }
            }
        } else {
            echo "<p style='color:green;font-weight:bold'>Catch lionnel</p>";
        }
        fclose($f);
        echo "------------------------------------*--------------------------------<br />";
    }
} else {
    echo 'Aucune vid&eacute;o trouv&eacute;e <br /><br />';
}

echo "FIN TRAITEMENT DEPLACEMENT";
