<?php

/**
 * Classe permettant de manipuler les fichiers
 */
class Explorer {

    protected $log_path;

    function __construct($log_path) {
        $this->log_path = $log_path;
    }

    /**
     * 
     * @param type $dir
     * @param type $flag : ASC ou DESC par date
     * @return type
     */
    function LocalFilesCollection($dir, $flag = 'DESC') {
        $reps = array();

        if (!is_dir($dir))
            return array();

        if ($handle = opendir($dir)) {
            while ($rep = readdir($handle)) {
                if ($rep != "." && $rep != ".." && !is_dir($dir . '/' . $rep)) {
                    $reps[$rep] = filemtime($dir . "/" . $rep);
                }
            }
        }
        if ($flag === 'DESC') {
            arsort($reps);
        } else {
            asort($reps);
        }

        return $reps;
    }
    
    function LocalFilesCollectionFromFile($path){
        $ret = array();
        $handle = fopen($path, "r");
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                $explode = explode(':',$buffer);
                $ret[$explode[0]] = $explode[1];
            }
            if (!feof($handle)) {
                self::OutText("Erreur: fgets() a échoué", 'ERROR');
            }
            fclose($handle);
        }        
        return $ret;
    }

    function LocalFileCopy($path_source, $path_destination) {
        if(file_exists($path_destination)){
            return false;
        }
        
        if (file_exists($path_source) && is_dir(dirname($path_destination))) {
            if (!copy($path_source, $path_destination)) {
                $errors = error_get_last();
                self::OutText("Erreur dans la copie de " . $path_source . " vers " . $path_destination, 'ERROR');
                self::OutText("COPY ERROR: " . $errors['type'], 'ERROR');
                self::OutText($errors['message'], 'ERROR');
                self::OutSeparateur();
                return false;
            } else {
                self::OutText("Copie terminé de " . $path_source . " vers " . $path_destination, 'SUCCESS');
                self::OutSeparateur();
                return true;
            }
        }
        return false;
    }

    function FileRemoveExt($filename){
        $last_point = strrpos($filename, ".");
        if($last_point === false){
            return $filename;
        }
        return substr($filename, 0, $last_point);
    }    
    
    function LogAppend($data) {
        $out = file_put_contents($this->log_path, PHP_EOL.$data, FILE_APPEND);
        if ($out === false) {
            self::OutText("Un problème avec le log " . $this->log_path, 'ERROR');
            self::OutSeparateur();
        }
    }

    function LogGetLastIndex() {
        $line = '';
        if (file_exists($this->log_path)) {
            $data = file($this->log_path);
            $line = $data[count($data) - 1];
        }
        $tmp = str_replace("\r\n", "", $line);
        $row = explode(":", $tmp);
        return $row[0];
    }

    /**
     * 
     * @param type $text
     * @param type $flag = ERROR, SIMPLE
     */
    static function OutText($text, $flag = 'SIMPLE') {
        switch ($flag) {
            case 'ERROR':
                echo "<pre style='color:red;font-weight:bold'>" . $text . "</pre>";
                break;
            case 'SUCCESS':
                echo "<pre style='color:green;font-weight:bold'>" . $text . "</pre>";
                break;
            default :
                echo "<pre style='color:blue;font-weight:bold'>" . $text . "</pre>";
                break;
        }
    }

    static function OutSeparateur() {
        echo "<HR size=2 align=center width='100%'>";
    }

    static function Lock() {
        file_put_contents(dirname(__FILE__) . '/cron.lock', date('d/m/Y H:i:s'));
    }

    static function Unlock() {
        unlink(dirname(__FILE__) . '/cron.lock');
    }

    static function SystemIsLocked() {
        return file_exists(dirname(__FILE__) . '/cron.lock');
    }

    static function Poids($rep) {
        $r = opendir($rep);
        $t = 0;
        while ($dir = readdir($r)) {
            if (!in_array($dir, array("..", "."))) {
                if (is_dir("$rep/$dir")) {
                    $t += self::Poids("$rep/$dir");
                } else {
                    $t += filesize("$rep/$dir");
                }
            }
        }
        closedir($r);
        return $t;
    }
    
    function ResetPointeur(){
        rename($this->log_path, $this->FileRemoveExt($this->log_path).'_'.date('d_m_Y_H_i_s').'txt');
    }
}
