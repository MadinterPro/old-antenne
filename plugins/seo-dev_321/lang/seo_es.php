<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/seo?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'S.E.O' => 'SEO',

	// A
	'alexa' => 'Alexa',
	'alexa_activate' => 'Activar Alexa',
	'alexa_id' => 'Identificador del sitio para Alexa',

	// B
	'bing_webmaster' => 'Herramientas para administradores web de Bing (Bing Webmaster Tools)',
	'bing_webmaster_activate' => 'Activar Herramientas para administradores web de Bing (Bing Webmaster Tools)',
	'bing_webmaster_id' => 'Metacódigo de verificación',

	// C
	'canonical_url' => 'URL Canónicas',
	'canonical_url_activate' => 'Activar la meta de URL Canónicas',

	// E
	'explication_description' => 'Es aconsejable que la longitud sea inferior o igual a 160 caracteres.',
	'explication_title' => 'Es aconsejable que la longitud oscile entre 5 y 70 caracteres.',

	// F
	'forcer_squelette_descriptif' => 'Las metas SEO son prioritarias para las metas generadas por los esqueletos',
	'forcer_squelette_label' => 'Cargar las metas para todos los esqueletos',

	// G
	'google_analytics' => 'Google Analytics',
	'google_analytics_activate' => 'Activar Google Analytics',
	'google_analytics_id' => 'Google Analytics web property ID',
	'google_webmaster_tools' => 'Herramientas de administradores web de Google (Google Webmaster Tools)',
	'google_webmaster_tools_activate' => 'Activar Herramientas de administradores de Google (Google Webmaster Tools)',
	'google_webmaster_tools_id' => 'Metacódigo de verificación',

	// I
	'info_count_max' => 'Caracteres para alcanzar el límite óptimo:',
	'insert_head' => 'Inserción automática en #INSERT_HEAD',
	'insert_head_activate' => 'Activar la inserción automática',
	'insert_head_descriptif' => 'Inserción automática de la configuración SEO en el &lt;head&gt;',

	// M
	'meta_author' => 'Autor:',
	'meta_copyright' => 'Copyright:',
	'meta_description' => 'Descripción:',
	'meta_keywords' => 'Palabras clave:',
	'meta_page_description_sommaire_value' => 'Valor de la Descripción de la Página + Valor Meta del Sumario',
	'meta_page_description_value' => 'Valor de la Descripción de la Página',
	'meta_page_title_sommaire_value' => 'Valor del Título de la Página + Valor Meta del Sumario',
	'meta_page_title_value' => 'Valor del Título de la Página',
	'meta_robots' => 'Robots:',
	'meta_sommaire_value' => 'Valor de la Meta del Sumario',
	'meta_tags' => 'Meta Tags',
	'meta_tags_activate' => 'Activar las Meta Tags editoriales',
	'meta_tags_default' => 'Valor de las Meta Tags por defecto (para Artículos y Secciones)', # MODIF
	'meta_tags_edit_activate' => 'Activar la edición de las meta tags en las secciones y artículos', # MODIF
	'meta_tags_editing' => 'Edición de las Meta Tags',
	'meta_tags_sommaire' => 'Valor de las Meta Tags del sumario (página de inicio)',
	'meta_title' => 'Título:',

	// S
	'seo' => 'Search Engine Optimisation (Optimización de motor de búsqueda)'
);
