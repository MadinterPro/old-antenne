<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-seo?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'seo_description' => 'SEO is een plugin die de head van je site kan aanpassen met: meta tags, canonieke URL, Google webmaster tools Meta Code en Google Analytics javascript. Je kunt hem configureren met een configuratie-bladzijde in SPIP. Meta tags kun je ook in iedere rubriek en ieder artikel instellen.',
	'seo_nom' => 'SEO',
	'seo_slogan' => 'Optimalisatie voor zoekmachines'
);
