<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-seo?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'seo_description' => 'SEO é um plugin que inclui a possibilidade de se inserir, na área <head> do seu website, meta tags, URL canônica, Google webmaster tools Meta Code e Google Analytics javascript. É parametrizável na página de configuração do SPIP, bem como em todas as seções e matérias, pelas meta tags.',
	'seo_nom' => 'SEO',
	'seo_slogan' => 'Search Engine Optimisation'
);
