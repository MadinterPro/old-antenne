<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-seo?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'seo_description' => 'SEO ist ein Plugin, welches das Einfügen von Canonical URLs, Google Webmaster-Tools Meta-Code und Google Analytics Javascript in den Kopfteil Ihrer Website Meta-Tags ermöglicht. Die Einstellungen können sowohl auf der SPIP Einstellungs Seite sowohl auf jeder Seite eines redaktionellen Objektes verändert werden.',
	'seo_nom' => 'SEO',
	'seo_slogan' => 'Search Engine Optimisation'
);
