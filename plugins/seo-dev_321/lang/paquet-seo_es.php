<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-seo?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'seo_description' => 'SEO es un plugin que añade la posibilidad de insertar en la parte superior de su sitio web: meta tags, URL canónica, Google webmaster tools Meta Code y Google Analytics javascript. Puede configurarse desde la página de configuración de SPIP así como desde cada sección y artículo para las les meta tags.',
	'seo_nom' => 'SEO',
	'seo_slogan' => 'Search Engine Optimisation'
);
