<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rainette?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'coucher_soleil' => 'sunset',

	// D
	'demain' => 'tomorrow',
	'derniere_maj' => 'updated',
	'direction_E' => 'east',
	'direction_ENE' => 'east north-east',
	'direction_ESE' => 'east south-east',
	'direction_N' => 'north',
	'direction_NE' => 'north-east',
	'direction_NNE' => 'north north-east',
	'direction_NNW' => 'north north-west',
	'direction_NW' => 'north-west',
	'direction_S' => 'south',
	'direction_SE' => 'south-est',
	'direction_SSE' => 'south south-est',
	'direction_SSW' => 'south south-west',
	'direction_SW' => 'south-west',
	'direction_W' => 'west',
	'direction_WNW' => 'west north-west',
	'direction_WSW' => 'west south-west',

	// H
	'humidite' => 'humidity',

	// J
	'jour' => 'day',

	// L
	'latitude' => 'latitude',
	'lever_soleil' => 'sunrise',
	'longitude' => 'longitude',

	// M
	'meteo' => 'weather',
	'meteo_0' => 'tornado',
	'meteo_1' => 'tropical storm',
	'meteo_10' => 'freezing rain',
	'meteo_11' => 'showers',
	'meteo_12' => 'showers',
	'meteo_13' => 'snow flurries',
	'meteo_14' => 'light snow showers',
	'meteo_15' => 'blowing snow',
	'meteo_16' => 'snow',
	'meteo_17' => 'hail',
	'meteo_18' => 'sleet',
	'meteo_19' => 'dust',
	'meteo_2' => 'hurricane',
	'meteo_20' => 'foggy',
	'meteo_21' => 'haze',
	'meteo_22' => 'smoky',
	'meteo_23' => 'blustery',
	'meteo_24' => 'windy',
	'meteo_25' => 'cold',
	'meteo_26' => 'cloudy',
	'meteo_27' => 'mostly cloudy (night)',
	'meteo_28' => 'mostly cloudy (day)',
	'meteo_29' => 'partly cloudy (night)',
	'meteo_3' => 'severe thunderstorms',
	'meteo_30' => 'partly cloudy (day)',
	'meteo_31' => 'clear (night)',
	'meteo_32' => 'sunny',
	'meteo_33' => 'fair (night)',
	'meteo_34' => 'fair (day)',
	'meteo_35' => 'mixed rain and hail',
	'meteo_36' => 'hot',
	'meteo_37' => 'isolated thunderstorms',
	'meteo_38' => 'scattered thunderstorms',
	'meteo_39' => 'scattered thunderstorms',
	'meteo_4' => 'thunderstorms',
	'meteo_40' => 'scattered showers',
	'meteo_41' => 'heavy snow',
	'meteo_42' => 'scattered snow showers',
	'meteo_43' => 'heavy snow',
	'meteo_44' => 'partly cloudy',
	'meteo_45' => 'thundershowers',
	'meteo_46' => 'snow showers',
	'meteo_47' => 'isolated thundershowers',
	'meteo_5' => 'mixed rain and snow',
	'meteo_6' => 'mixed rain and sleet',
	'meteo_7' => 'mixed snow and sleet',
	'meteo_8' => 'freezing drizzle',
	'meteo_9' => 'drizzle',
	'meteo_conditions' => 'current conditions',
	'meteo_consultation' => '@ville@ Weather',
	'meteo_de' => '@ville@ weather',
	'meteo_na' => 'unknown',
	'meteo_previsions' => 'forecast conditions',
	'meteo_previsions_aujourdhui' => 'today forecast',
	'meteo_previsions_n_jours' => '@nbj@-days forecast',

	// WWO
	'meteo_113' => 'Clear/Sunny',
	'meteo_116' => 'Partly Cloudy',
	'meteo_119' => 'Cloudy',
	'meteo_122' => 'Overcast',
	'meteo_143' => 'Mist',
	'meteo_176' => 'Patchy rain nearby',
	'meteo_179' => 'Patchy snow nearby',
	'meteo_182' => 'Patchy sleet nearby',
	'meteo_185' => 'Patchy freezing drizzle nearby',
	'meteo_200' => 'Thundery outbreaks in nearby',
	'meteo_227' => 'Blowing snow',
	'meteo_230' => 'Blizzard',
	'meteo_248' => 'Fog',
	'meteo_260' => 'Freezing fog',
	'meteo_263' => 'Patchy light drizzle',
	'meteo_266' => 'Light drizzle',
	'meteo_281' => 'Freezing drizzle',
	'meteo_284' => 'Heavy freezing drizzle',
	'meteo_293' => 'Patchy light rain',
	'meteo_296' => 'Light rain',
	'meteo_299' => 'Moderate rain at times',
	'meteo_302' => 'Moderate rain',
	'meteo_305' => 'Heavy rain at times',
	'meteo_308' => 'Heavy rain',
	'meteo_311' => 'Light freezing rain',
	'meteo_314' => 'Moderate or Heavy freezing rain',
	'meteo_317' => 'Light sleet',
	'meteo_320' => 'Moderate or heavy sleet',
	'meteo_323' => 'Patchy light snow',
	'meteo_326' => 'Light snow',
	'meteo_329' => 'Patchy moderate snow',
	'meteo_332' => 'Moderate snow',
	'meteo_335' => 'Patchy heavy snow',
	'meteo_338' => 'Heavy snow',
	'meteo_350' => 'Ice pellets',
	'meteo_353' => 'Light rain shower',
	'meteo_356' => 'Moderate or heavy rain shower',
	'meteo_359' => 'Torrential rain shower',
	'meteo_362' => 'Light sleet showers',
	'meteo_365' => 'Moderate or heavy sleet showers',
	'meteo_368' => 'Light snow showers',
	'meteo_371' => 'Moderate or heavy snow showers',
	'meteo_374' => 'Light showers of ice pellets',
	'meteo_377' => 'Moderate or heavy showers of ice pellets',
	'meteo_386' => 'Patchy light rain in area with thunder',
	'meteo_389' => 'Moderate or heavy rain in area with thunder',
	'meteo_392' => 'Patchy light snow in area with thunder',
	'meteo_395' => 'Moderate or heavy snow in area with thunder',

	// N
	'nuit' => 'night',

	// P
	'point_rosee' => 'dew point',
	'pression' => 'pressure',

	// R
	'risque_precipitation' => 'precipitation',

	// S
	'station_observation' => 'station',

	// T
	'temperature_max' => 'high',
	'temperature_min' => 'low',
	'temperature_ressentie' => 'feels like',
	'tendance_symbole_falling' => '↓',
	'tendance_symbole_rising' => '↑',
	'tendance_symbole_steady' => '→',
	'tendance_texte_falling' => 'falling',
	'tendance_texte_rising' => 'rising',
	'tendance_texte_steady' => 'steady',

	// U
	'unite_angle_metrique' => '°',
	'unite_angle_standard' => '°',
	'unite_distance_metrique' => 'km',
	'unite_distance_standard' => 'miles',
	'unite_pourcentage_metrique' => '%',
	'unite_pourcentage_standard' => '%',
	'unite_precipitation_metrique' => 'mm',
	'unite_precipitation_standard' => 'inches',
	'unite_pression_metrique' => 'mbar',
	'unite_pression_standard' => 'inches',
	'unite_temperature_metrique' => '°C',
	'unite_temperature_standard' => '°F',
	'unite_vitesse_metrique' => 'km/h',
	'unite_vitesse_standard' => 'mph',

	// V
	'valeur_indeterminee' => 'N/A',
	'vent' => 'wind',
	'visibilite' => 'visibility'
);

?>
