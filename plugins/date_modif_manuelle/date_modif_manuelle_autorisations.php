<?php
/**
 * Définit les autorisations du plugin Date de modification manuelle
 *
 * @plugin     Date de modification manuelle
 * @copyright  2017
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Date_modif_manuelle\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function date_modif_manuelle_autoriser() {
}
