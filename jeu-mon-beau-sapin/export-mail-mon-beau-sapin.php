<?php
require "../squelettes/ajax/PHPMailer-master/PHPMailerAutoload.php";

try {
    $mReturn = getData();

    if (empty($mReturn) || $mReturn == 0) throw new Exception("Empty Data");

    $sFilename = 'export_mon_beau_sapin_' . date("Y-m-d") . '.csv';
    createCsv($mReturn, $sFilename);

    $mail = new PHPMailer;
    $mail->CharSet = "UTF-8";
    $mail->Subject = 'Export Mon Beau Sapin - ' . date("Y-m-d");
    $mail->setFrom('noreply@antennereunion.fr', 'Antenne Réunion');
    $mail->addAddress('jodie.manent@antennereunion.fr');
    $mail->addAddress('camille.ajaguin-soleyen@antennereunion.fr');

    $mail->addBCC('nilaina.rabeony@antennereunion.fr', 'Nilaina Rabeony');
    $mail->addBCC('johary.rakoto@antennereunion.fr', 'Johary RAKOTO');
    $mail->addBCC('ludovic.andrianarimahefa@antennereunion.fr', 'Ludovic Andrianarimahefa');
    $mail->isHTML(true);

    $sBody = "Bonjour <br>";
    $sBody .= "Ci-joint l'export des données Mon beau sapin du " . date("Y-m-d");

    $mail->Body = $sBody;
    $mail->addAttachment($sFilename);

    $mail->send();

    unlink($sFilename);
} catch (Exception $e) {
    echo "Error when executing Cron Mon beau sapin";
    echo $e->getMessage();
    die;
}

function getData()
{
    define("DATA_BASE_HOST", "localhost");
    define("DATA_BASE_NAME", "antenne_art_spip");
    define("DATA_BASE_LOGIN", "antenne_spipAR");
    define("DATA_BASE_PASSWORD", "antv3_2009");
    define("DATA_BASE_TABLE_USER", "user_monbeausapin");

    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION
    );

    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sQuery = "SELECT * FROM " . DATA_BASE_TABLE_USER;

    $oRequest = $db->prepare($sQuery);
    $oRequest->execute();
    $aData = $oRequest->fetchAll(PDO::FETCH_ASSOC);

    return $aData;
}

function createCsv(array &$array, $sFilename)
{
    ob_start();
    $df = fopen("$sFilename", 'w');

    $aHeader = array_keys(reset($array));
    //add BOM for special chars
    fputs($df, chr(0xEF) . chr(0xBB) . chr(0xBF));

    //add header to CSV
    fputcsv($df, $aHeader, ';');

    //add data
    foreach ($array as $row) {
        fputcsv($df, $row, ';');
    }

    fclose($df);
    return ob_get_clean();
}