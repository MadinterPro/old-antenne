<?php
ini_set('display_errors', 1);
error_reporting(e_all);
//définition des constantes

define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "antenne_art_spip");
define("DATA_BASE_LOGIN", "antenne_spipAR");
define("DATA_BASE_PASSWORD", "antv3_2009");
define("DATA_BASE_TABLE_NEWSLETTER", "spip_nl_user_bkup");
define("DATA_BASE_TABLE_USER_OPTINS", "spip_user_optins_bkup");

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);


try {
    //connexion PDO
    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //GET ALL DATA FROM suivis_infos.sql
    $sql = "SELECT * FROM `spip_nl_user`";
    $suivis_infos = $db->query($sql)->fetchAll();

    //Parcourir les données et les enregistrer dans "spip_nl_user"
    foreach ($suivis_infos as $res) {
      $user_Id = null;
      $date_modif = $res['date_modification'];
      if ( $date_modif == '0000-00-00 00:00:00' || is_null($date_modif) ) {
        $date_modif = $res['date_inscription'];
      }

      $sql = "SELECT * FROM spip_user_newsletter WHERE id_nl_user = '" . $res['id'] . "'";
      $is_NL = $db->query($sql)->fetchAll();

      $sexe = NULL;
      if ( $res['civilite_user'] == 'Madame' || $res['civilite_user'] == 'Mademoiselle' || $res['civilite_user'] == 1 || $res['civilite_user'] == 'Mme') {
        $sexe = 2;
      }
      elseif ($res['civilite_user'] == 'Monsieur' || $res['civilite_user'] == 'Mr' || $res['civilite_user'] == 0) {
        $sexe = 1;
      }

      $sql = "SELECT * FROM " . DATA_BASE_TABLE_NEWSLETTER . " WHERE mail_user='" . $res['mail_user'] . "'";
      $mail_user_exist = $db->query($sql)->fetch();
      if ( $mail_user_exist ) {
        $user_Id = $mail_user_exist['id'];
        
        if ( new Datetime($res['date_inscription']) > new Datetime($mail_user_exist['date_modification']) ) {
          // MAJ données de l'utilisateur dans "spip_nl_user_test"
          $sql_update = 'UPDATE '. DATA_BASE_TABLE_NEWSLETTER . ' SET `nom`="'. $res['nom'] .'", `prenom`="'.$res['prenom'].'", `mail_user`="'. $res['mail_user'].'", `civilite_user` ="'.$sexe.'", `date_modification`="'.$date_modif.'", statut="'.$res['statut'].'", `statut_partenaire`="'.$res['statut_partenaire'].'" WHERE id='. $user_Id;
          $db->query($sql_update);
        }

        // Vérification des optins
        $sql = "SELECT * FROM ". DATA_BASE_TABLE_USER_OPTINS ." WHERE id_user='" . $user_Id . "'";
        $user_optins = $db->query($sql)->fetchAll();

        if ($user_optins) {
          foreach ($user_optins as $opt) {
            if ( $opt['id_optin'] == 1 && $res['statut'] == 0 ) {
                // MAJ date de desincription
                $optin_update = "UPDATE `". DATA_BASE_TABLE_USER_OPTINS ."` SET date_desinscription='". $date_modif ."' WHERE id_user_optins='" . $opt['id_user_optins'] . "' AND id_optin = '".$opt['id_optin']."'";
                $db->query($optin_update);
            }
            if ( $opt['id_optin'] == 2 && $res['statut_partenaire'] == 0 ) {
              // MAJ date de desincription
                $optin_update = "UPDATE `". DATA_BASE_TABLE_USER_OPTINS ."` SET date_desinscription='". $date_modif ."' WHERE id_user_optins='" . $opt['id_user_optins'] . "' AND id_optin = '".$opt['id_optin']."'";
                $db->query($optin_update);
            }
            if ( $opt['id_optin'] == 1 && $res['statut'] == 1) {
                // MAJ date abonnement
                $optin_update = "UPDATE `". DATA_BASE_TABLE_USER_OPTINS ."` SET date_abonnement='". $res['date_inscription'] ."' WHERE id_user_optins='" . $opt['id_user_optins'] . "' AND id_optin ='".$opt['id_optin']."'";
                $db->query($optin_update);
            }
            if ( $opt['id_optin'] == 2 && $res['statut_partenaire'] == 1 ) {
              // MAJ date abonnement
                $optin_update = "UPDATE `". DATA_BASE_TABLE_USER_OPTINS ."` SET date_abonnement='". $res['date_inscription'] ."' WHERE id_user_optins='" . $opt['id_user_optins'] . "' AND id_optin ='".$opt['id_optin']."'";
                $db->query($optin_update);
            }
          }

        }else{
          // Enregistrer les optins dans la table intermédiaire "spip_user_optins"
          if ($res['statut'] == 1) {
            $insert_optin = "INSERT INTO " . DATA_BASE_TABLE_USER_OPTINS . " (`id_user`, `id_optin`, `date_abonnement` , `date_desinscription`) VALUES ('". $user_Id ."','". 1 ."','". $res['date_inscription']. "','". $res['date_inscription'] ."')";
            $db->query($insert_optin) or die('Impossible de traiter linsert');
          }
          elseif ($res['statut_partenaire'] == 1) {
            $insert_optin = "INSERT INTO " . DATA_BASE_TABLE_USER_OPTINS . " (`id_user`, `id_optin`, `date_abonnement` , `date_desinscription`) VALUES ('". $user_Id ."','". 2 ."','". $res['date_inscription']. "','". $res['date_inscription'] ."')";
            $db->query($insert_optin) or die('Impossible de traiter linsert');
          }
        }
        

      }else{

        // Enregistrer dans la table spip_nl_user_test
        $requete = 'INSERT INTO `spip_nl_user_bkup` (`nom`, `prenom`, `mail_user`, `civilite_user`, `date_inscription`,`date_modification`, `statut`, `statut_partenaire`) VALUES ( "'.$res['nom'].'","'.$res['prenom'].'","'.$res['mail_user'].'","'.$sexe.'","'.$res['date_inscription'].'","'.$date_modif.'","'.$res['statut'].'","'.$res['statut_partenaire'].'") ';
        $db->query($requete) or die('Impossible de traiter linsert');
        $user_Id = $db->lastInsertId();

        // Enregistrer les optins dans la table intermédiaire "spip_user_optins"
        if ($res['statut'] == 1) {
          $insert_optin = "INSERT INTO " . DATA_BASE_TABLE_USER_OPTINS . " (`id_user`, `id_optin`, `date_abonnement` , `date_desinscription`) VALUES ('". $res['id'] ."','". 1 ."','". $res['date_inscription']. "','". $date_modif ."')";
          $db->query($insert_optin) or die('Impossible de traiter linsert');
        }
        if ($res['statut_partenaire'] == 1) {
          $insert_optin = "INSERT INTO " . DATA_BASE_TABLE_USER_OPTINS . " (`id_user`, `id_optin`, `date_abonnement` , `date_desinscription`) VALUES ('". $res['id'] ."','". 2 ."','". $res['date_inscription']. "','". $date_modif ."')";
          $db->query($insert_optin) or die('Impossible de traiter linsert');
        }
      
      }

      if (count($is_NL)>0) {
        $sql = "INSERT INTO `spip_user_newsletter_bkup` (id_newsletter,id_nl_user,id_action_user,date_action,date_modif_action) VALUES ('".$is_NL[0]['id_newsletter']."','".$user_Id."', '".$is_NL[0][',id_action_user']."','".$is_NL[0]['date_action']."','".$is_NL[0]['date_modif_action']."')";
        $db->query($sql);
      }

      
    }

    /**/

    echo "traitement terminer";

} catch (Exception $e) {
    echo $e->getMessage()."<br/>".$e->getLine();
    echo "Erreur";
    die;
}

?>

