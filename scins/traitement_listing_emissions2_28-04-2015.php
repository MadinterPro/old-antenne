﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>Traitement listing emissions</title>
</head>
<body style="font-family:verdana; font-size:13px;">

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td width="100%" align="center">
		<b style="color:#ccc; font-size:20px;">Traitement des vidéos émissions</b><br><br>
		
		<div style="position:relative; background:#f5f5f5; padding:10px; width:650px; text-align:left;">
			<?php
				/* ////////// AJOUTER émission ////////// */
				if (isset($_POST["ajouter_emission"])){
					
					if(isset($_POST["sigle"])){$sigle=$_POST["sigle"];}
					if(isset($_POST["nom_emission"])){$nom_emission=$_POST["nom_emission"];}
					if(isset($_POST["numero_rubrique"])){$numero_rubrique=$_POST["numero_rubrique"];}
					if (is_numeric($_POST["numero_rubrique"])){
						//echo "<br>C'est ok, c'est un nombre <br>";
					} 
					else if (!is_numeric($_POST["numero_rubrique"])){
						//echo "<br> ce n'est pas un nombre <br>";
						$numero_rubrique="";
					}
					
					if (($sigle!="")and($nom_emission!="")and($numero_rubrique!="")){
						echo "sigle : ".$sigle."<br>";
						echo "nom émission : ".stripslashes($nom_emission)."<br>";
						echo "numéro rubrique  : ".$numero_rubrique."<br>";
						echo "<font color='#dd0000'><b>Emission rajoutée à la liste</b></font>";
						
						$emission=$sigle."_".$nom_emission."_".$numero_rubrique;
						$emission=stripslashes($emission);
						//echo "<br>".$emission."<br>";
						ecrire_dans_fichier_texte($emission);
					}
					else {
						echo "<b>ERREUR</b> rien dans un des champs texte pour l'<b>ajout d'émission.</b>";
					}
				}
				//////////////////////////////////////////////
				
				/* ////////// SUPPRIMER émission ////////// */
				if (isset($_POST["supprimer_emission"])){
					
					if(isset($_POST["ligne"])){$ligne=$_POST["ligne"];}
					
					if (($ligne!="")){
						supprimer_ligne($ligne);
						echo "<font color='#dd0000'><b>Emission supprimée de la liste</b></font>";
					}
					else {
						echo "<b>ERREUR</b> aucune ligne n'est sélectionée pour <b>supprimer une émmission.</b>";
					}
				}
				//////////////////////////////////////////////
				
				/* ////////// AJOUTER mail ////////// */
				if (isset($_POST["ajouter_mail"])){
					
					if(isset($_POST["mail"])){$mail=$_POST["mail"];}
					
					if ($mail!=""){
						echo "mail : ".$mail."<br>";
						echo "<font color='#dd0000'><b>E-mail rajouté à la liste</b></font>";
						
						ecrire_dans_fichier_mail($mail);
					}
					else {
						echo "<b>ERREUR</b> rien dans le champs texte pour l'<b>ajout de mail.</b>";
					}
				}
				//////////////////////////////////////////////
				
				/* ////////// SUPPRIMER mail ////////// */
				if (isset($_POST["supprimer_mail"])){
					
					if(isset($_POST["ligne_mail"])){$ligne_mail=$_POST["ligne_mail"];}
					
					if (($ligne_mail!="")){
						supprimer_ligne_mail($ligne_mail);
						echo "<font color='#dd0000'><b>E-mail supprimé de la liste</b></font>";
					}
					else {
						echo "<b>ERREUR</b> aucune ligne n'est sélectionée pour <b>supprimer un mail.</b>";
					}
				}
				//////////////////////////////////////////////
				
				$url_fichier_list_emissions="/home/antenne/public_html/art.antenne.re/scins/liste_emissions.txt"; 	//url du script principal
				$url_fichier_list_mail="/home/antenne/public_html/art.antenne.re/scins/liste_alerte_mail.txt"; 	//url du script d'insertion d'article dans la BDD SPIP
				
				function ecrire_dans_fichier_texte($emission){
					echo "<br>".$emission."<br>";
					$contenu_fichier = file_get_contents('/home/antenne/public_html/art.antenne.re/scins/liste_emissions.txt');
					$fp = fopen ('/home/antenne/public_html/art.antenne.re/scins/liste_emissions.txt', "r+");
					fseek ($fp, 0);
					fputs($fp, $contenu_fichier."\r\n".$emission);
					fclose ($fp);
				}
				function supprimer_ligne($a){
					$ptr = fopen('/home/antenne/public_html/art.antenne.re/scins/liste_emissions.txt', "r+");
					$contenu = fread($ptr, filesize('/home/antenne/public_html/art.antenne.re/scins/liste_emissions.txt'));
					/* On a plus besoin du pointeur */
					fclose($ptr);

					$contenu = explode(PHP_EOL, $contenu); /* PHP_EOL contient le saut à la ligne utilisé sur le serveur (\n linux, \r\n windows ou \r Macintosh */
				    unset($contenu[$a]); /* On supprime la ligne $a par exemple */
					$contenu = array_values($contenu); /* Ré-indexe l'array */
					/* Puis on reconstruit le tout et on l'écrit */
					$contenu = implode(PHP_EOL, $contenu);
					$ptr = fopen('/home/antenne/public_html/art.antenne.re/scins/liste_emissions.txt', "w");
					fwrite($ptr, $contenu);
				}
				function ecrire_dans_fichier_mail($a){
					$contenu_fichier_mail = file_get_contents('/home/antenne/public_html/art.antenne.re/scins/liste_alerte_mail.txt'); 
					$fp_mail = fopen ('/home/antenne/public_html/art.antenne.re/scins/liste_alerte_mail.txt', "r+");  
					fseek ($fp_mail, 0);
					fputs ($fp_mail, $contenu_fichier_mail."\r\n".$a);  
					fclose ($fp_mail);
				}
				function supprimer_ligne_mail($a){
					$ptr = fopen('/home/antenne/public_html/art.antenne.re/scins/liste_alerte_mail.txt', "r+");
					$contenu_liste_mail = fread($ptr, filesize('/home/antenne/public_html/art.antenne.re/scins/liste_alerte_mail.txt'));
					fclose($ptr);
					$contenu_liste_mail = explode(PHP_EOL, $contenu_liste_mail);
					unset($contenu_liste_mail[$a]);
					$contenu_liste_mail = array_values($contenu_liste_mail);
				   	$contenu_liste_mail = implode(PHP_EOL, $contenu_liste_mail);
					$ptr = fopen('/home/antenne/public_html/art.antenne.re/scins/liste_alerte_mail.txt', "w");
					fwrite($ptr, $contenu_liste_mail);
				}
			?>
		</div>
		<br>
		<form method="post">
		
			<table cellpadding="10" cellspacing="0" style="position:relative; border:1px solid #cccccc;">
			<tr>
				<td align="left" valign="top" style="border-right:1px solid #dddddd; background:#f8f8f8;">
					Liste des vidéos dans le dossier <br><br>
					<?php
						/////////////////////////////////////////////////////////////////////////
						// lecture du dossier de fichiers.
						//
						/*$dirname = '/home/video.antennereunion.fr/public_html/scins/emissions/';
						$dir = opendir($dirname); 
						while($file = readdir($dir)) {
							if($file != '.' && $file != '..' && !is_dir($dirname.$file))
							{
								$filelist[] = $file;
								//echo '<a href="'.$dirname.$file.'">'.$file.'</a>';
							}
						}
						closedir($dir);
						/////////////////////////////////////////////////////////////////////////
						$j=0;
						while(isset($filelist[$j])){
							echo ($j." - ");
							echo("<b>".$filelist[$j]."</b><br>");
							$j++;
						}*/
                                                define('VIDEO_TRAITER', '/scins/emissions/*.flv');
                                                require_once("FTPClass.php");
                                                if ($oFTP = new FTP('ftp.we.binarysec.com', 'systeme%video.antennereunion.fr', '5g3ekf_BAV9')){
                                                    //echo"<br>CONNEXION CHEZ VIDEO.ART<br>";
                                                    $oFTP->enablePassive();
                                                 }else{
                                                    echo"IMPOSSIBLE DE SE CONNECTER CHEZ VIDEO.ART";
                                                $_list_files_audio = $oFTP->ls(VIDEO_TRAITER, true);
                                                for ($i = 0; $i < sizeof($_list_files_audio); $i++) {
                                                    echo("".$_list_files_audio[$i]."<br>");
                                                }
	 }
                                                
                                                
                                                
					?>
				</td>
				
				<td align="left" valign="top">	
					
					Liste des émissions à traiter<br><br>
					
					<?php
						$tableau_titre_emission = array("tab_titre_emission" => array());	
						
						$fichier_emission = file($url_fichier_list_emissions); // Nom du fichier à afficher, son adresse de localisation
						$total = count($fichier_emission); // Nombre total des lignes du fichier

						for($i = 0; $i < $total; $i++) 
						{ // Départ de la boucle
							$sigle_emission=substr($fichier_emission[$i], 0, strpos($fichier_emission[$i], '_'));
							$sigle_emission=strtoupper($sigle_emission);		
							
							$nom_emission=substr(strstr($fichier_emission[$i], '_'), 1);
							$nom_emission=substr($nom_emission, 0, strpos($nom_emission, '_'));
							
							$numero_rubrique=substr(strrchr($fichier_emission[$i], '_'), 1);
							
							//echo $sigle_emission."<br>"; // On affiche ligne par ligne le contenu du fichier
							//echo $nom_emission."<br><br>"; // On affiche ligne par ligne le contenu du fichier
						
							$tableau_test_emission[] = $sigle_emission;	
							$tableau_titre_emission["tab_titre_emission"][$sigle_emission] = $nom_emission;
							$tableau_numero_rubrique[] = $numero_rubrique;
							
						} // Fin de la boucle

						$k=0;
						while(isset($tableau_test_emission[$k])){
							echo ($k." - ");
							echo("<b>".$tableau_test_emission[$k]."</b> : ");
							
							echo stripslashes($tableau_titre_emission["tab_titre_emission"][$tableau_test_emission[$k]]);
							echo(" | rub : <b>".$tableau_numero_rubrique[$k]."</b><br>");
							
							$k++;
						}
						
						
					?>
				</td>
				<td align="left" valign="top" style="border-left:1px solid #dddddd;">
					<div style="position:relative; width:300px; border:0px solid #dddddd; background:#f2f2f2; padding:10px;">
						<b>Ajouter</b> une émission à traiter : <br><br>
						<table cellpadding="0" cellspacing="0" style="position:relative: padding:10px 0px;">
						<tr>
							<td align="left" valign="top">
								sigle : &nbsp;<br>
								<font size="-2" color="#999999">(Ex : KLB , TLMJ, ...)</font> &nbsp;
							</td>
							<td align="left" valign="top">
								<input type="text" name="sigle" />
							</td>
						</tr>
						<tr>
							<td height='10'>
							</td>
						</tr>
						<tr>
							<td align="left" valign="top">
								nom émission : &nbsp;<br>
								<font size="-2" color="#999999">(Ex : Tout le monde joue, &nbsp;<br> Les secrets du bien etre,<br> Noel de reve, ...)</font> &nbsp;
							</td>
							<td align="left" valign="top">
								<input type="text" name="nom_emission" />
							</td>
						</tr>
						<tr>
							<td height='10'>
							</td>
						</tr>
						<tr>
							<td align="left" valign="top">
								numéro de rubrique : &nbsp;<br>
							</td>
							<td align="left" valign="top">
								<input type="text" name="numero_rubrique" />
							</td>
						</tr>
						</table>
						<br>
						<button type="submit" value="ajouter_emission" name="ajouter_emission"><b>ajouter</b> l'émission</button>
						<br><br>
					</div>
					<div style="position:relative; width:300px; border-top:1px solid #dddddd; background:#ccc; padding:10px;">
						<b>Supprimer</b> une émission de la liste : <br><br>
						<table cellpadding="0" cellspacing="0" style="position:relative: padding:10px 0px;">
						<tr>
							<td align="left" valign="top">
								numéro de la ligne  : &nbsp;<br>
								(à supprimer)
							</td>
							<td align="left" valign="top">
								<input type="text" name="ligne" />
							</td>
						</tr>
						</table>
						<br>
						<button type="submit" value="supprimer_emission" name="supprimer_emission"><b>supprimer</b> l'émission</button>
						<br><br>
					</div>
				</td>
			</tr>
			</table>
			<br>
			
			<table cellpadding="10" cellspacing="0" style="position:relative; border:1px solid #cccccc;">
			<tr>
				<td align="left" valign="top" style="border-right:1px solid #dddddd;">
					Liste des mail pour les alertes mail <br><br>
					<?php
						$fichier_mail = file($url_fichier_list_mail); // Nom du fichier à afficher, son adresse de localisation
						$total_mail = count($fichier_mail); // Nombre total des lignes du fichier

						for($m = 0; $m < $total_mail; $m++) 
						{ 
							$tableau_mail[] = $fichier_mail[$m];
						}

						$kad=0;
						while(isset($tableau_mail[$kad])){
							echo ($kad." - ");
							echo("<b>".$tableau_mail[$kad]."</b><br>");
							$kad++;
						}
					?>
				</td>
				<td align="left" valign="top" style="border-right:1px solid #dddddd;">
					<b>E-Mail</b> pour les alertes en cas de vidéo non traité : <br>
					Ajouter ou supprimer un mail<br><br>
					<div style="position:relative; border:0px solid #dddddd; background:#f2f2f2; padding:10px;">
						<b>Ajouter</b> mail à l'alerte : <br><br>
						<table cellpadding="0" cellspacing="0" style="position:relative: padding:10px 0px;">
						<tr>
							<td align="left" valign="top">
								mail : &nbsp;<br>
							</td>
							<td align="left" valign="top">
								<input type="text" name="mail" />
							</td>
						</tr>
						</table>
						<br>
						<button type="submit" value="ajouter_mail" name="ajouter_mail"><b>ajouter</b> le mail</button>
						<br><br>
					</div>
					<div style="position:relative; border-top:1px solid #dddddd; background:#ccc; padding:10px;">
						<b>Supprimer</b> un mail de la liste : <br><br>
						<table cellpadding="0" cellspacing="0" style="position:relative: padding:10px 0px;">
						<tr>
							<td align="left" valign="top">
								numéro de la ligne  : &nbsp;<br>
								(à supprimer)
							</td>
							<td align="left" valign="top">
								<input type="text" name="ligne_mail" />
							</td>
						</tr>
						</table>
						<br>
						<button type="submit" value="supprimer_mail" name="supprimer_mail"><b>supprimer</b> le mail</button>
						<br><br>
					</div>
				</td>
			</tr>
			</table>
		
		</form>
		<?php //mail('nicolas.dijoux@antennereunion.fr', 'test_envoi', 'pas de message pour le moment');  ?>
	</td>
</tr>
</table>
	
</body>
</html>
