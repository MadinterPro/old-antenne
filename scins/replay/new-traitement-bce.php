<?php
set_time_limit (200);
header('Pragma: no-cache');
ini_set('display_errors', 1);
error_reporting(E_ALL);

$urldomain = "";
if (preg_match("/rct/", $_SERVER['HTTP_HOST'])) {
    $urldomain = "http://antenne.ser-rct-01.antennereunion.fr";
} else {
    $urldomain = "http://www.antennereunion.fr";
}

$f = fopen("/home/antenne/public_html/scins/replay/error_log", "a+");
$date_du_jours = date('Ymd');
$datedujour = date("Y-m-d H:i:s");

define('XML_A_TRAITER', '/home/antenne/public_html/replay_automatique/' . $date_du_jours . '/0/');

#Fait une lecture du dossier par FTP
$_list_files_xml = scandir(XML_A_TRAITER); 

$file_liste = '';

if (sizeof($_list_files_xml) > 0) {

    /* Création du fichier copiprogresse */

    $fichier = fopen('/home/antenne/public_html/scins/replay/copieprogresse.txt', 'w') or die("Unable to create file!");
    fclose($fichier);

    #!!!Juste DEBUG!!!
    echo "<br> nombre de fichier XML a traiter :" . sizeof($_list_files_xml) . "<br>";
    echo"<pre>";
    print_r($_list_files_xml) . "<br>";
    echo"</pre>";

    #parcourt du repertoire XML
    for ($i = 0; $i < sizeof($_list_files_xml); $i++) {

        if ($i < 2) {
            continue;
        }

        $_list_replay_ = explode(' ', $_list_files_xml[$i]);

        $a = sizeof($_list_replay_);
        #pour chaque fichier, prendre le nom
        $xml_file = $_list_replay_[$a - 1];
        $xml_file = str_replace('/' . $date_du_jours . '/0/', '', $xml_file);

        //$url_feed = 'http://easywaf:DuUpwBSc7SKsc3IYu1Cy@www.antennereunion.fr/replay_automatique/' . $date_du_jours . '/0/' . $xml_file . '';
        $url_feed = $urldomain . '/replay_automatique/' . $date_du_jours . '/0/' . $xml_file . '';
        echo $url_feed;
        $feed = file_get_contents($url_feed);
        $items = simplexml_load_string($feed);

        //debut donées récupérées à partir du XML
        $bid = $items->BroadcastID;
        echo $bid ."<br>";
        $deprogrammation = strval(strtolower($items->Cancelled));
        $vignette_generic = $items->Picture_generic;
        $vignette = $items->Picture_program;
        $media = $items->Media;
        $duree_video = $items->Duration;
        $parsed = date_parse($duree_video);
        $duree_video_seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
        $datePub = $items->Activation_start;
        //rubrique pour insersion de l'article
        $program = strval(trim($items->Program));

        $array_program = array(
            "DODIL" => "DO DIL",
            "INDIA A LOVE STORY" => "INDIA, A LOVE STORY",
            "INFO SOIREE" => "INFO-SOIREE",
            "KLB" => "KANAL LA BLAGUE",
            "FRANCOFOLIES 2018" => "FRANCOFOLIES",
            "50'INSIDE LE MAG" => "50'INSIDE",
            "50'INSIDE L'ACTU" => "50'INSIDE",
            "THE VOICE, LA SUITE" => "THE VOICE",
            "WILD HORSE" => "WILD HORSES",
            "RENDEZ-VOUS A LA FOIRE !" => "RENDEZ-VOUS A LA FOIRE PASSION TERROIR",
            "RENDEZ-VOUS A LA FOIRE! PASSION ET TERROIR" => "RENDEZ-VOUS A LA FOIRE PASSION TERROIR",
            "LE JOURNAL DE LA COUPE DU MONDE" => "COUPE DU MONDE 2018",
            "BRAS-PANON, MA VILLE VÉLO" => "TOUR CYCLISTE ANTENNE REUNION",
            "BRAS-PANON, MA VILLE VELO" => "TOUR CYCLISTE ANTENNE REUNION",
            "REVE DE MISS" => "MISS RÉUNION",
            "BIEN-VIVRE AU TAMPON" => "BIEN VIVRE AU TAMPON",
            "SEPT A HUIT" => "SEPT À HUIT",
            "SEPT A HUIT LIFE" => "SEPT À HUIT",
            "GENERATION RUN STAR, LA QUOTIDIENNE" => "GÉNÉRATION RUN STAR",
            "GENERATION RUN STAR" => "GÉNÉRATION RUN STAR",
            "LES FLORILEGES" => "LES FLORILÈGES",
            "FISH'N TRUCK" => "FISH’N TRUCK",
            "Fish&rsquo ;n truck" => "FISH’N TRUCK",
            "20 DESANM" => "20 DÉSANM",
            "LA MINUTE DU 20 DESANM" => "20 DÉSANM",
            "ELECTION DE MISS FRANCE 2019" => "MISS FRANCE",
            "DIONYCITE" => "DIONYCITÉ",
            "DIONYCITE L'ACTU" => "DIONYCITÉ",
            "DIONYCITE LE MAG" => "DIONYCITÉ",
            "AU NOM DE L'AMOUR" => "AU NOM DE L’AMOUR",
            "MARIANA &amp; SCARLETT" => "MARIANA & SCARLETT",
            "AU NOM DE L'AMOUR" => "AU NOM DE L’AMOUR",
            "KOH-LANTA, LA GUERRE DES CHEFS" => "KOH-LANTA",
            "JOSEPHINE, ANGE GARDIEN" => "JOSEPHINE ANGE GARDIEN",
            "MASK SINGER, L'ENQUETE CONTINUE" => "MASK SINGER",
            "LE 20H00 DE TF1" => "20H DE TF1",
            "20 DESANM" => "20 DÉSANM"
        );

        $other_program = array(
            "DIVERTISSEMENTS" => "DIVERTISSEMENTS",
            "TELEFILMS DE NOEL" => "TELEFILMS DE NOEL",
            "EMISSION D’INFORMATION EVENEMENTIELLE" => "EMISSION D’INFORMATION EVENEMENTIELLE"
        );

        $meteo_program = array(
            "METEO 12H20" => "MÉTÉO",
            "METEO 13H00" => "MÉTÉO",
            "METEO 13H50" => "MÉTÉO",
            "METEO 18H55" => "MÉTÉO",
            "METEO 19H40" => "MÉTÉO"
        );

        /* Récupération ID_RUBRIQUE et ID_SECTEUR */
        if (array_key_exists($program, $other_program)) {
            $program = $other_program[$program];
            $rows_id_rub = getRubFourreTout($program);
        } elseif (array_key_exists($program, $meteo_program)) {
            $tmp_program = $meteo_program[$program];
            $rows_id_rub = getRubMeteo($tmp_program);
        } elseif (array_key_exists($program, $array_program)) {
            $program = $array_program[$program];
            $rows_id_rub = getRub($program);
        } else {
            $rows_id_rub = getRub($program);
        }

        echo $program; 
        foreach ($rows_id_rub as $row_id_rub) {
            $id_rubrique = $row_id_rub['id_rubrique'];
            $id_secteur = $row_id_rub['id_secteur'];
        }

        // Transert replay JT 12H30 et 19H vers JT Weekend 
        $array_jt = array(
            'LE 19H00' => 'LE 19H00',
            'LE 12H30' => 'LE 12H30'
        );
        $we_array = array('Fri', 'Sat', 'Sun');
        $jour_we = date('D', strtotime($datePub));

        if (array_key_exists(strtoupper($program), $array_jt) && in_array($jour_we, $we_array)) {
            if ($jour_we == 'Fri' && strtoupper($program) == 'LE 19H00') {
                // Save 'LE 19H00' in JT Weekend
                $id_rubrique = 6268;
                $id_secteur = 742;
            } elseif ($jour_we != 'Fri') {
                $id_rubrique = 6268;
                $id_secteur = 742;
            }
        }
        /* Fin récupération ID_RUBRIQUE et ID_SECTEUR */

        $title = $items->Title;
        $title = htmlentities($title);
        $category = $items->Category;
        $saison = $items->Season;
        $episode = $items->Episode;
        $texte = htmlentities($items->Synopsis);
        $dateDiff = $items->Broadcast_datetime;
        $dateDiff = date_create($dateDiff);
        $thisday = date_format($dateDiff, 'd');
        $thismonth = date_format($dateDiff, 'm');
        $thisyear = date_format($dateDiff, 'Y');

        //affichage des dates au format français.
        setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
        $dateDiff = mktime(0,0,0,$thismonth,$thisday,$thisyear);
        $dateDiff = strftime("%A %d %B %Y",$dateDiff);
        $dateDiff = htmlentities($dateDiff);
        $dateRedac = $items->Activation_end;
        $geoloc = $items->Territories;

        //Le Feder
        $program = ucfirst(strtolower(htmlentities($program)));

        if($program == "Le feder de a a z"){
            $title = 'Replay Le FEDER de A &agrave; Z - '.ucfirst($dateDiff);
        }elseif($saison==""){
            $title = 'Replay ' . $program . ' - '.ucfirst($dateDiff);
        }elseif($episode==""){
            $title = 'Replay ' . $program . ' - '.ucfirst($dateDiff);
        }elseif ($episode=="" && $saison==""){
            $title = 'Replay ' . $program . ' - '.ucfirst($dateDiff);
        }elseif($title == ""){
            $title = 'Replay ' . $program . ' -S' . sprintf('%02d', $saison) . '-Ep' . sprintf('%02d', $episode) . ' - '.ucfirst($dateDiff);
        }else{
            $title = htmlentities($items->Title);
        }

        $data_article = array(
            'titre' => utf8_encode($title),
            'id_rubrique' => $id_rubrique,
            'texte' => $texte,
            'date' => $datePub,
            'statut' => ($id_rubrique == 4119) ? 'prepa' : 'publie',
            'id_secteur' => $id_secteur,
            'maj' => $datePub,
            'export' => 'oui',
            'date_redac' => $dateRedac,
            'accepter_forum' => 'pri',
            'lang' => 'fr',
            'broadcastID' => $bid
        );

        //fin donées récupérées à partir du XML
        //vérification si l'article existe déjà
        if (verif_bid($bid) == 0) {
            //création de l'articele si celui-ci n'existe pas encore

            $rep_fichier = "/home/antenne/public_html/replay_automatique/" . $date_du_jours . "/file/";
            $vigngen = $rep_fichier . $vignette_generic;
            $vign = $rep_fichier . $vignette;
            $med = $rep_fichier . $media;
 
            echo '<br>Creation du replay ' . $title . '<br>';
            echo "------------------------------------*--------------------------------";
            echo "<br>DEBUT TRAITEMENT DEPLACEMENT<br>";
            
            echo "Copy de " . $vignette_generic . " Vers le repertoire /public/ du NAS <br />";
            $file_liste .= basename($rep_fichier . $vignette_generic). PHP_EOL . "  ====  " .recup_taille_fichier( filesize($rep_fichier . $vignette_generic) )."o <br />";
            
            $file_liste .= basename($rep_fichier . $vignette). PHP_EOL . "  ====  " .recup_taille_fichier( filesize($rep_fichier . $vignette) )."o <br />";

            $url_image_dynamic = "http://antenne.ser-rct-01.antennereunion.fr/replay_automatique/" . $date_du_jours . "/file/".$vignette;
            
            $url_image = "http://antenne.ser-rct-01.antennereunion.fr/replay_automatique/" . $date_du_jours . "/file/".$vignette_generic;
            $sizes = getimagesize($url_image);
            $largeur = $sizes[0];
            $hauteur = $sizes[1];

            $file_liste .= basename($rep_fichier . $media). PHP_EOL . "  ====  " .recup_taille_fichier( filesize($rep_fichier . $media) )."o <br />";

            $url_video = "http://antenne.ser-rct-01.antennereunion.fr/replay_automatique/" . $date_du_jours . "file/". $media;

            echo "FIN TRAITEMENT DEPLACEMENT<br>";
            echo "------------------------------------*--------------------------------<br>";

            echo "<b>Title : </b>" .$title."<br>";
            echo "<b>ID rubrique : </b>" .$id_rubrique."<br>";
            echo "<p>Texte : </p>" .$texte."</p><br>";
            echo "<b>Date de publication : </b>" .$datePub."<br>";
            echo "<b>URL Image : </b>" .$url_image."<br>";
            echo "<b>Largeur : </b>" .$largeur."<br>";
            echo "<b>Hauteur : </b>" .$hauteur."<br>";
            echo "<b>URL Vid&eacute;o : </b>" .$url_video."<br>";
            echo "<b>Date de d&eacute;publication : </b>" .$dateRedac."<br>";
            echo "<b>ID Secteur : </b>" .$id_secteur."<br>";
            echo "<b>ID Broadcast : </b>" .$bid."<br>";
            insererVersDB($data_article, $url_image, $largeur, $hauteur, $url_video,$duree_video_seconds);

            if($id_rubrique == 89){
                echo 'Mise en attente de validation du replay ' . $title . '</br>';
                deprogram($bid);
                //Envoi mail
                
            }
            if($id_rubrique == 4119){
                deprogram($bid);
            }
            echo '<br>Fin creation du replay ' . $title . '<br>';
        } else {
            //si l'article existe déjà
            if ($deprogrammation == 'true') {
                //vérification si pour déprogrammation
                echo 'Deprogrammation du replay ' . $title . '</br>';
                deprogram($bid);

            } else{
                //update si non déprogrammation
                echo 'Mise a jours du replay ' . $title . '</br>';
                // Get ID article by BID
                $id_article = getIdArticle($bid);
                
                // Update program content
                echo 'update contenu program'; 
                updateContentProgram($id_article, $title, $id_rubrique, $texte, $datePub, $id_secteur, $dateRedac);

                // Update document 
                $rep_fichier = "/home/antenne/public_html/replay_automatique/" . $date_du_jours . "/file/";
                $vigngen_source = $rep_fichier . $vignette_generic; 
                $vignpro_source = $rep_fichier . $vignette; 
                $video = $rep_fichier . $media; 

                //copy de l'image generic
                $file_liste .= basename($rep_fichier . $vignette_generic). PHP_EOL . "  ====  " .recup_taille_fichier( filesize($rep_fichier . $vignette_generic) )."o <br />";

                //copy de l'image program
                $file_liste .= basename($rep_fichier . $vignette). PHP_EOL . "  ====  " .recup_taille_fichier( filesize($rep_fichier . $vignette) )."o <br />";
 
                $url_image = "http://antenne.ser-rct-01.antennereunion.fr/replay_automatique/" . $date_du_jours . "/file/". $vignette_generic;   

                $sizes = getimagesize($url_image);
                $largeur = $sizes[0];
                $hauteur = $sizes[1];

                // copy video
                $file_liste .= basename($rep_fichier . $media). PHP_EOL . "  ====  " .recup_taille_fichier( filesize($rep_fichier . $media) )."o <br />";

                $url_video = "http://antenne.ser-rct-01.antennereunion.fr/replay_automatique/" . $date_du_jours . "/file/". $media;

                // Supprimer les documents déjà existant avant d'insérer les nouveaux
                suppressionDocument($id_article);

                //Videos
                ajoutDocument('ts', $title,  $datePub, $url_video, $duree_video_seconds, NULL, NULL, NULL, 'video', $id_article);
                
                //Images
                ajoutDocument('jpg', $title, $datePub, $url_image, NULL, 7000, $largeur, $hauteur, 'image', 
                    $id_article);

            }
        }
    }
    unlink('/home/antenne/public_html/scins/replay/copieprogresse.txt');

    /* Si au moins un fichier est copié vers le NAS */
    if ($file_liste !== '') {
        // Envoi mail liste fichier copié
        $to = 'tiana.andriambolamanana@antennereunion.fr' . ',';
        $to .= 'johary.rakoto@antennereunion.fr' . ',';
        $subject = 'Liste des fichiers copiés vers NAS - script vidéos';
        $message = "Voici ci-dessous la liste des fichiers copi&eacute; vers le NAS <br />";
        // $message .= "HF-001 : <br /> ";
        // $message .= "R&eacute;solution : ". ( $resolution ) ? true : false  ."<br />";
        $message .= $file_liste ;
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        $headers .= 'From:tiana.andriambolamanana@antennereunion.fr' . "\r\n" .
        'Reply-To: tiana.andriambolamanana@antennereunion.fr' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $message, $headers);
    }

} else {
    echo "Aucun fichier XML a traiter";
}
// $oFTP->close();

function selectDb() {
    $dsn = 'mysql:dbname=antenne_art_spip;host=localhost';
    $user = 'antenne_spipAR';
    $password = 'antv3_2009';
    global $dbh;
    try {
        $dbh = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }
    return $dbh;
}

function verif_bid($bid) {
    $dbh = selectDb();
    //requete récupération article à déprogrammé
    $sql_verif = 'SELECT * FROM  `spip_articles` WHERE `broadcastID` = "' . $bid . '"';
    $stmt = $dbh->prepare($sql_verif);
    $stmt->execute();
    return $stmt->rowCount();
    $dbh = null;
}

function getRub($titre) {
    $dbh = selectDb();
    //requete récupération rubrique correspondant au programme
    $sql_replay_programme = 'SELECT * FROM  `correspondance_replay` AS c WHERE `titre` =  "' . $titre . '"';
    $rows_replay_programme = $dbh->query($sql_replay_programme);
    $result = $rows_replay_programme->fetchAll();
    if($result){
        foreach ($result as $row_id_rub) {
            $id_rubrique = $row_id_rub['id_rubrique'];
        }
        $sql_rub_repl = 'SELECT * FROM  `spip_rubriques` AS r WHERE `id_parent` =' . $id_rubrique . ' AND  `titre` = "Replay"';
        $rows_rub_replay_programme = $dbh->query($sql_rub_repl);
        return $rows_rub_replay_programme;
    }else{
        $id_rubrique = 89;
        $sql_rub_repl = 'SELECT * FROM  `spip_rubriques` AS r WHERE `id_rubrique` =' . $id_rubrique . ' and `id_secteur` =89';
        $rows_rub_replay_programme = $dbh->query($sql_rub_repl);
        return $rows_rub_replay_programme;
    }
    $dbh = null;
}
function getRubFourreTout($titre_fourretout) {
    $dbh = selectDb();
    //requete récupération rubrique correspondant au programme
    $sql_replay_programme_fourretout = 'SELECT * FROM  `correspondance_nonreplay` AS c WHERE `titre` =  "' . $titre_fourretout . '"';
    $rows_replay_programme_fourretout = $dbh->query($sql_replay_programme_fourretout);
    foreach ($rows_replay_programme_fourretout as $row_id_rub_fourretout) {
        $id_rubrique = $row_id_rub_fourretout['id_rubrique'];
    }
    $sql_rub_repl_fourretout = 'SELECT * FROM  `spip_rubriques` AS r WHERE `id_parent` =' . $id_rubrique . ' AND  `titre` = "Replay"';
    $rows_rub_replay_programme_fourretout = $dbh->query($sql_rub_repl_fourretout);
    return $rows_rub_replay_programme_fourretout;
    $dbh = null;
}
function getRubMeteo($titre_meteo) {
    $dbh = selectDb();
    //requete récupération rubrique correspondant au programme
    $sql_replay_programme_meteo = 'SELECT * FROM  `correspondance_replay_meteo` AS c WHERE `titre` =  "' . $titre_meteo . '"';
    $rows_replay_programme_meteo = $dbh->query($sql_replay_programme_meteo);
    foreach ($rows_replay_programme_meteo as $row_id_rub_meteo) {
        $id_rubrique = $row_id_rub_meteo['id_rubrique'];
    }
    $sql_rub_repl_meteo = 'SELECT * FROM  `spip_rubriques` AS r WHERE `id_parent` =742 AND  `id_rubrique` = 4119';
    $rows_rub_replay_programme_meteo = $dbh->query($sql_rub_repl_meteo);
    return $rows_rub_replay_programme_meteo;
    $dbh = null;
}

// Insertion nouveau programme dans BDD
function insererVersDB($data, $url_image, $largeur, $hauteur, $url_video, $duree_video_seconds) {
    $dbh = selectDb();
    $champs = array_keys($data);
    $sql_insert_article = "INSERT INTO spip_articles (" . implode(',', $champs) . ") VALUES (:" . implode(',:', $champs) . ")"; 

    $insert_article = $dbh->prepare($sql_insert_article);
    $insert_article->execute($data);
    $id_article_ = $dbh->lastInsertId();

    //Videos
    ajoutDocument('ts', $data['titre'], $data['date'], $url_video, $duree_video_seconds, NULL, NULL, NULL, 'video', $id_article_);
    //Images
    ajoutDocument('jpg', $data['titre'], $data['date'], $url_image, NULL, 7000, $largeur, $hauteur, 'image', $id_article_);

    //mot clé replay
    $requete_insertion_mots_liens_replay = 'INSERT INTO spip_mots_liens (id_mot, id_objet, objet) '
        . 'VALUES ("' . '1772' . '", "' . $id_article_ . '", "article")';
    $dbh->query($requete_insertion_mots_liens_replay);

    //Auteur BCE
    $requete_insertion_auteurs_liens_replay = 'INSERT INTO `spip_auteurs_liens` (`id_auteur`, `id_objet`, `objet`, `vu`) '
        . 'VALUES ("' . '406' . '", "' . $id_article_ . '", "article","non")';
    $dbh->query($requete_insertion_auteurs_liens_replay);

    $dbh = null;
}

function deprogram($bid) {
    $dbh = selectDb();
    //requete récupération article à déprogrammé
    $sql = "UPDATE  `spip_articles` SET `statut` = 'prepa' WHERE  `broadcastID` = '" . $bid . "'";
    $dbh->query($sql);
    $dbh = null;
}

function updateContentProgram($id_article, $titre, $id_rubrique, $texte, $datePub, $id_secteur, $date_redac) {
    $dbh = selectDb();
    //requete màj article déjà existant
    $sql = 'UPDATE `spip_articles` SET '
        . ' `titre` =  "' . $titre . '",'
        . ' `id_rubrique` =  "' . $id_rubrique . '",'
        . ' `date` =  "' . date_format(date_create($datePub), 'Y-m-d H:i:s') . '",'
        . ' `texte` =  "' . utf8_decode($texte) . '",'
        . ' `id_secteur` =  ' . $id_secteur . ','
        . ' `maj` = "NOW()",'
        . ' `date_redac` =  "' . date_format(date_create($date_redac), 'Y-m-d H:i:s') . '"'
        . ' WHERE  `id_article` = ' . $id_article ;
    echo $sql; 
    $dbh->query($sql);
    $dbh = null;
}

/* 
* Suppression document d'un article
* @param $id_article
 */
function suppressionDocument($id_article){
    $dbh = selectDb();
    $sql = "SELECT id_document FROM spip_documents_liens WHERE id_objet= ".$id_article." AND objet = 'article'";
    $documents = $dbh->query($sql)->fetchAll();
    foreach ($documents as $doc) {
        $del = 'DELETE FROM spip_documents WHERE id_document = :id_document';
        $stmt = $dbh->prepare($del);
        $stmt->bindParam(':id_document', $doc['id_document'], PDO::PARAM_INT);   
        $stmt->execute();
    }
    $del = "DELETE FROM spip_documents_liens WHERE id_objet = :id_objet AND objet = :type_objet";
    $stmt = $dbh->prepare($del); 
    $stmt->execute( array(
        ':id_objet' => $id_article,
        ':type_objet' => 'article'
    ));
    $dbh = null;
}

// Ajout nouveau document
function ajoutDocument($extension, $titre, $date_pub, $fichier, $duree, $taille, $largeur, $hauteur, $type, $id_article){
    $dbh = selectDb();
    if ($type == 'video') {
        $sql = 'INSERT INTO spip_documents ( extension, titre, date, fichier, duree, mode, distant,statut,media) '. 'VALUES ("ts", "' . $titre . '", "' . $date_pub . '", "' . $fichier . '" , "' . $duree . '", "document", "oui","publie","video")';
    } else {
        $sql = 'INSERT INTO spip_documents (extension, titre, date, fichier, taille, largeur, hauteur, mode, distant,statut) '
        . 'VALUES ("jpg", "' . $titre . '", "' . $date_pub . '", "' . $fichier . '", "7000", "' . $largeur . '", "' . $hauteur . '", "image", "non","publie")';
    }

    $dbh->query($sql);
    $sql_last_id = $dbh->query("SELECT LAST_INSERT_ID()");
    $id_document = $sql_last_id->fetchColumn();

    // Add document to spip_documents_liens
    $sql = 'INSERT INTO spip_documents_liens (id_document, id_objet, objet) '. 'VALUES ("' . $id_document . '", "' . $id_article . '", "article")';
    $dbh->query($sql);
    $dbh = null;
}

// Récupération id_article article by BID
function getIdArticle($bid){
    $dbh = selectDb();
    $sql = 'SELECT id_article, date, date_redac FROM  `spip_articles` WHERE `broadcastID` = "' . $bid . '"';
    $article = $dbh->query($sql)->fetchObject();
    $dbh = null;
    return $article->id_article;
}

/* Fonction pour avoir la taille d'un fichier */
function recup_taille_fichier($bytes, $decimals = 2) {
    $sz = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}

?>