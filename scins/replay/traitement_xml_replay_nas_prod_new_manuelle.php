<?php
set_time_limit (200);
header('Pragma: no-cache');

$f = fopen("/home/antenne/public_html/scins/replay/error_log", "a+");
//$date_du_jours = date('Ymd');
$date_du_jours = "20190303";
$datedujour = date("Y-m-d H:i:s");

define('XML_A_TRAITER', '/' . $date_du_jours . '/0/*.xml');

require_once("FTPClass.php"); //librarie de classe FTP

if ($oFTP = new FTP('antennereunion-01.run.hostin.network', 'repl_auto@antennereunion.fr', '54Nku+=r!;&k')) {
    echo"<br>CONNEXION FTP OK<br>";
    $oFTP->enablePassive();
} else {
    echo"IMPOSSIBLE DE SE CONNECTER FTP";
}

#Fait une lecture du dossier par FTP
$_list_files_xml = $oFTP->ls(XML_A_TRAITER, true);

$file_liste = '';

if (sizeof($_list_files_xml) > 0) {

    /* Création du fichier copiprogresse */

    $fichier = fopen('/home/antenne/public_html/scins/replay/copieprogresse.txt', 'w') or die("Unable to create file!");
    fclose($fichier);

    #!!!Juste DEBUG!!!
    echo "<br> nombre de fichier XML a traiter :" . sizeof($_list_files_xml) . "<br>";
    echo"<pre>";
    print_r($_list_files_xml) . "<br>";
    echo"</pre>";

    #parcourt du repertoire XML
    for ($i = 0; $i < sizeof($_list_files_xml); $i++) {

        $_list_replay_ = explode(' ', $_list_files_xml[$i]);

        $a = sizeof($_list_replay_);
        #pour chaque fichier, prendre le nom
        $xml_file = $_list_replay_[$a - 1];
        $xml_file = str_replace('/' . $date_du_jours . '/0/', '', $xml_file);

        $url_feed = 'http://www.antennereunion.fr/replay_automatique/' . $date_du_jours . '/0/' . $xml_file . '';
        $feed = file_get_contents($url_feed);
        $items = simplexml_load_string($feed);

        //debut donées récupérées à partir du XML
        $bid = $items->BroadcastID;
        $deprogrammation = $items->Cancelled;
        $vignette_generic = $items->Picture_generic;
        $vignette = $items->Picture_program;
        $media = $items->Media;
        //rubrique pour insersion de l'article
        $program = $items->Program;

        if($program=="DODIL"){
            $program = "DO DIL";
        }
        if($program=="INDIA A LOVE STORY"){
            $program = "INDIA, A LOVE STORY";
        }
        if($program=='INFO SOIREE'){
            $program = 'INFO-SOIREE';
        }
        if($program=='KLB'){
            $program = 'KANAL LA BLAGUE';
        }
        if($program=='FRANCOFOLIES 2018'){
            $program = 'FRANCOFOLIES';
        }
        if($program=='METEO 12h20'){
            $program = 'METEO';
        }
        if($program=="50'INSIDE LE MAG"){
            $program = "50'INSIDE";
        }
		if($program=="50'INSIDE L'ACTU"){
            $program = "50'INSIDE";
        }
		if($program=="THE VOICE, LA SUITE"){
            $program = "THE VOICE";
        }
        if($program=="WILD HORSE"){
            $program = "WILD HORSES";
        }
        if($program=="RENDEZ-VOUS A LA FOIRE ! "){
            $program = "RENDEZ-VOUS A LA FOIRE PASSION TERROIR";
        }
        if($program=="RENDEZ-VOUS A LA FOIRE! PASSION ET TERROIR"){
            $program = "RENDEZ-VOUS A LA FOIRE PASSION TERROIR";
        }
        if($program=="LE JOURNAL DE LA COUPE DU MONDE"){
            $program = "COUPE DU MONDE 2018";
        }

        if($program=="BRAS-PANON, MA VILLE VÉLO"){
            $program = "TOUR CYCLISTE ANTENNE REUNION";
        }

        if($program=="BRAS-PANON, MA VILLE VELO"){
            $program = "TOUR CYCLISTE ANTENNE REUNION";
        }

        if($program=="REVE DE MISS"){
            $program = "MISS RÉUNION";
        }
        if($program=="BIEN-VIVRE AU TAMPON"){
            $program = "BIEN VIVRE AU TAMPON";
        }
        if($program=="SEPT A HUIT"){
            $program = "SEPT À HUIT";
        }
        if($program=="GENERATION RUN STAR, LA QUOTIDIENNE"){
            $program = "GÉNÉRATION RUN STAR";
        }
        if($program=="GENERATION RUN STAR"){
            $program = "GÉNÉRATION RUN STAR";
        }
        if($program=="LES FLORILEGES"){
            $program = "LES FLORILÈGES";
        }
        if($program=="FISH'N TRUCK"){
            $program = "FISH’N TRUCK";
        }
        if($program=="Fish&rsquo ;n truck"){
            $program = "FISH’N TRUCK";
        }
        if($program=="20 DESANM"){
            $program = "20 DÉSANM";
        }
        if($program=="LA MINUTE DU 20 DESANM"){
            $program = "20 DÉSANM";
        }
        if($program=="ELECTION DE MISS FRANCE 2019"){
            $program = "MISS FRANCE";
        }
        $programdiv = "DIVERTISSEMENTS";
        $programtelefilms = "TELEFILMS DE NOEL";
        $programeminfoev = "EMISSION D’INFORMATION EVENEMENTIELLE";
        $programmeteo = "METEO";

        if($program == $programdiv){
            $rows_id_rub = getRubFourreTout($programdiv);
        }elseif($program == $programtelefilms){
            $rows_id_rub = getRubFourreTout($programtelefilms);
        }elseif($program == $programeminfoev ){
            $rows_id_rub = getRubFourreTout($programeminfoev);
        }elseif($program == $programmeteo ){
            $rows_id_rub = getRubMeteo($programmeteo);
        }else{
            $rows_id_rub = getRub($program);
        }

        foreach ($rows_id_rub as $row_id_rub) {
            $id_rubrique = $row_id_rub['id_rubrique'];
            $id_secteur = $row_id_rub['id_secteur'];
        }

        $title = $items->Title;
        $title = htmlentities($title);
        $category = $items->Category;
        $saison = $items->Season;
        $episode = $items->Episode;
        $texte = htmlentities($items->Synopsis);
        $dateDiff = $items->Broadcast_datetime;
        $dateDiff = date_create($dateDiff);
        $thisday = date_format($dateDiff, 'd');
        $thismonth = date_format($dateDiff, 'm');
        $thisyear = date_format($dateDiff, 'Y');

        //affichage des dates au format français.
        setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
        $dateDiff = mktime(0,0,0,$thismonth,$thisday,$thisyear);
        $dateDiff = strftime("%A %d %B %Y",$dateDiff);
        $dateDiff = htmlentities($dateDiff);
        $datePub = $items->Activation_start;
        $dateRedac = $items->Activation_end;
        $geoloc = $items->Territories;

        //Le Feder
        $program = ucfirst(strtolower(htmlentities($program)));

        if($program == "Le feder de a a z"){
            $title = 'Replay Le FEDER de A &agrave; Z - '.ucfirst($dateDiff);
        }elseif($saison==""){
            $title = 'Replay ' . ucfirst(strtolower(htmlentities($program))) . ' - '.ucfirst($dateDiff);
        }elseif($episode==""){
            $title = 'Replay ' . ucfirst(strtolower(htmlentities($program))) . ' - '.ucfirst($dateDiff);
        }elseif ($episode=="" && $saison==""){
            $title = 'Replay ' . ucfirst(strtolower(htmlentities($program))) . ' - '.ucfirst($dateDiff);
        }elseif($title == ""){
            $title = 'Replay ' . ucfirst(strtolower(htmlentities($program))) . ' -S' . sprintf('%02d', $saison) . '-Ep' . sprintf('%02d', $episode) . ' - '.ucfirst($dateDiff);
        }else{
            $title = htmlentities($items->Title);
        }


        //fin donées récupérées à partir du XML
        //vérification si l'article existe déjà
        $verif = verif_bid($bid);

        if (!$result = $verif->fetchAll()) {
            //création de l'articele si celui-ci n'existe pas encore
            if ($oFTP_NAS = new FTP('antenne.nas.hostin.network', 'antenne', 'no4emaiK')) {
                echo"<br>CONNEXION CHEZ NAS REUSSI<br>";
                $oFTP_NAS->enablePassive();
            } else {
                echo"IMPOSSIBLE DE SE CONNECTER CHEZ VIDEO NAS";
            }

            $depart = "http://www.antennereunion.fr/replay_automatique/" . $date_du_jours . "/0/";
            $rep_fichier = "/home/antenne/public_html/replay_automatique/" . $date_du_jours . "/0/";
            $vigngen = "/home/antenne/public_html/replay_automatique/" . $date_du_jours . "/0/".$vignette_generic;
            $vign = "/home/antenne/public_html/replay_automatique/" . $date_du_jours . "/0/".$vignette;
            $med = "/home/antenne/public_html/replay_automatique/" . $date_du_jours . "/0/".$media;

            echo '<br>Creation du replay ' . $title . '<br>';
            echo "------------------------------------*--------------------------------";
            echo "<br>DEBUT TRAITEMENT DEPLACEMENT<br>";
            //copy de l'image generic
            //$oFTP_NAS->put($depart . $vignette_generic, '/public/' . $vignette_generic);
            copy($vigngen, 'ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/' . $vignette_generic);
            echo "Copy de " . $vignette_generic . " Vers le repertoire /public/ du NAS <br />";
            $file_liste .= basename($rep_fichier . $vignette_generic). PHP_EOL . "  ====  " .recup_taille_fichier( filesize($rep_fichier . $vignette_generic) )."o <br />";
            unlink($vigngen);

            //copy de l'image program
            //$oFTP_NAS->put($depart . $vignette, '/public/' . $vignette);
            copy($vign, 'ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/' . $vignette);
            echo "Copy de " . $vignette . " Vers le repertoire /public/ du NAS <br />";
            $file_liste .= basename($rep_fichier . $vignette). PHP_EOL . "  ====  " .recup_taille_fichier( filesize($rep_fichier . $vignette) )."o <br />";
            unlink($vign);

            $url_image_dynamic = "http://cdn.antenne.re/nas/" . $vignette;
            $url_image = "http://cdn.antenne.re/nas/" . $vignette_generic;
            $sizes = getimagesize($url_image);
            $largeur = $sizes[0];
            $hauteur = $sizes[1];

            //copy de la video
            //$oFTP_NAS->put($depart . $media, '/public/' . $media);
            copy($med, 'ftp://antenne:no4emaiK@antenne.nas.hostin.network/public/' . $media);
            echo "Copy de " . $media . " Vers le repertoire /public/ du NAS <br />";
            $file_liste .= basename($rep_fichier . $media). PHP_EOL . "  ====  " .recup_taille_fichier( filesize($rep_fichier . $media) )."o <br />";
            unlink($med);

            $url_video = "http://cdn.antenne.re/nas/" . $media;

            $oFTP_NAS->close();
            echo "FIN TRAITEMENT DEPLACEMENT<br>";
            echo "------------------------------------*--------------------------------<br>";

            echo "<b>Title : </b>" .$title."<br>";
            echo "<b>ID rubrique : </b>" .$id_rubrique."<br>";
            echo "<p>Texte : </p>" .$texte."</p><br>";
            echo "<b>Date de publication : </b>" .$datePub."<br>";
            echo "<b>URL Image : </b>" .$url_image."<br>";
            echo "<b>Largeur : </b>" .$largeur."<br>";
            echo "<b>Hauteur : </b>" .$hauteur."<br>";
            echo "<b>URL Vid&eacute;o : </b>" .$url_video."<br>";
            echo "<b>Date de d&eacute;publication : </b>" .$dateRedac."<br>";
            echo "<b>ID Secteur : </b>" .$id_secteur."<br>";
            echo "<b>ID Broadcast : </b>" .$bid."<br>";
            insererVersDB($title, $id_rubrique, $texte, $datePub, $url_image, $largeur, $hauteur, $url_video, $dateRedac, $id_secteur, $bid);
            //insererVersDB($title, $id_rubrique, $texte, $datePub, $url_image, $largeur, $hauteur, $url_video, $dateRedac, $id_secteur, $bid);
            $oFTP->put($url_feed, '/traitee/'.$xml_file);
            $xmlf = "/home/antenne/public_html/replay_automatique/" . $date_du_jours . "/0/".$xml_file;
            unlink($xmlf);
            //$oFTP->del('/' . $date_du_jours . '/0/'.$xml_file);

            // $oFTP->del('/' . $date_du_jours . '/0/'.$vignette_generic);
            //unlink($vignette_generic);
            // $oFTP->del('/' . $date_du_jours . '/0/'.$vignette);
            //unlink($vignette);
            // $oFTP->del('/' . $date_du_jours . '/0/'.$media);
            //unlink($media);
            //$oFTP->del($depart . $media);
            if($id_rubrique == 89){
                echo 'Mise en attente de validation du replay ' . $title . '</br>';
                deprogram($bid);
                //Envoi mail
                $to = 'johary.rakoto@antennereunion.fr' . ',';
                $to .= 'rado.randriamalala@antennereunion.fr'.',';
                $to .= 'josee.vero@antennereunion.fr'.',';
                $to .= 'Shariffa.madavjee@antennereunion.fr'.',';
                $to .= 'marion.laine@antennereunion.fr';
                $subject = 'Erreur Rubrique - Replays en attente de validation';
                $message = 'Il y a des articles en attente de validation dans la rubrique antennereunion_replay'. "\r\n";
                $message .= 'Veuillez les retrouver dans http://www.antennereunion.fr/ecrire/?exec=rubrique&id_rubrique=89'. "\r\n";
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
                $headers .= 'From:johary.rakoto@antennereunion.fr' . "\r\n" .
                    'Reply-To: johary.rakoto@antennereunion.fr' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                mail($to, $subject, $message, $headers);
            }
            if($id_rubrique == 4119){
                deprogram($bid);
            }
            echo '<br>Fin creation du replay ' . $title . '<br>';
        } else {
            //si l'article existe déjà
            if ($deprogrammation == 'True') {
                //vérification si pour déprogrammation
                echo 'Deprogrammation du replay ' . $title . '</br>';
                deprogram($bid);
                //$oFTP->put($url_feed, '/traitee/'.$xml_file);
                $oFTP->put($url_feed, '/deprogrammee/'.$xml_file);
                // $oFTP->del('/' . $date_du_jours . '/0/'.$xml_file);
                $xmlf = "/home/antenne/public_html/replay_automatique/" . $date_du_jours . "/0/".$xml_file;
                unlink($xmlf);
            } else{
                //update si non déprogrammation
                echo 'Mise a jours du replay ' . $title . '</br>';
                update_program($bid, $title, $id_rubrique, $texte, $datePub, $id_secteur, $dateRedac);
                //$oFTP->put($url_feed, '/traitee/'.$xml_file);
                $oFTP->put($url_feed, '/maj/'.$xml_file);
                // $oFTP->del('/' . $date_du_jours . '/0/'.$xml_file);
                $xmlf = "/home/antenne/public_html/replay_automatique/" . $date_du_jours . "/0/".$xml_file;
                unlink($xmlf);
            }
        }
    }
    unlink('/home/antenne/public_html/scins/replay/copieprogresse.txt');

    /* Si au moins un fichier est copié vers le NAS */
    if ($file_liste !== '') {
        // Envoi mail liste fichier copié
        $to = 'tiana.andriambolamanana@antennereunion.fr' . ',';
        $to .= 'johary.rakoto@antennereunion.fr' . ',';
        $to .= 'christopher.deboisvilliers@antennereunion.fr';
        $subject = 'Liste des fichiers copiés vers NAS - script vidéos';
        $message = "Voici ci-dessous la liste des fichiers copi&eacute; vers le NAS <br />";
        // $message .= "HF-001 : <br /> ";
        // $message .= "R&eacute;solution : ". ( $resolution ) ? true : false  ."<br />";
        $message .= $file_liste ;
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        $headers .= 'From:tiana.andriambolamanana@antennereunion.fr' . "\r\n" .
        'Reply-To: tiana.andriambolamanana@antennereunion.fr' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $message, $headers);
    }

} else {
    echo "Aucun fichier XML a traiter";
}
$oFTP->close();

function selectDb() {
    $dsn = 'mysql:dbname=antenne_art_spip;host=localhost';
    $user = 'antenne_spipAR';
    $password = 'antv3_2009';
    global $dbh;
    try {
        $dbh = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }
    return $dbh;
}

function getRub($titre) {
    $dbh = selectDb();
    //requete récupération rubrique correspondant au programme
    $sql_replay_programme = 'SELECT * FROM  `correspondance_replay` AS c WHERE `titre` =  "' . $titre . '"';
    $rows_replay_programme = $dbh->query($sql_replay_programme);
    $result = $rows_replay_programme->fetchAll();
    if($result){
        foreach ($result as $row_id_rub) {
            $id_rubrique = $row_id_rub['id_rubrique'];
        }
        $sql_rub_repl = 'SELECT * FROM  `spip_rubriques` AS r WHERE `id_parent` =' . $id_rubrique . ' AND  `titre` = "Replay"';
        $rows_rub_replay_programme = $dbh->query($sql_rub_repl);
        return $rows_rub_replay_programme;
    }else{
        $id_rubrique = 89;
        $sql_rub_repl = 'SELECT * FROM  `spip_rubriques` AS r WHERE `id_rubrique` =' . $id_rubrique . ' and `id_secteur` =89';
        $rows_rub_replay_programme = $dbh->query($sql_rub_repl);
        return $rows_rub_replay_programme;
    }
    $dbh = null;
}
function getRubFourreTout($titre_fourretout) {
    $dbh = selectDb();
    //requete récupération rubrique correspondant au programme
    $sql_replay_programme_fourretout = 'SELECT * FROM  `correspondance_nonreplay` AS c WHERE `titre` =  "' . $titre_fourretout . '"';
    $rows_replay_programme_fourretout = $dbh->query($sql_replay_programme_fourretout);
    foreach ($rows_replay_programme_fourretout as $row_id_rub_fourretout) {
        $id_rubrique = $row_id_rub_fourretout['id_rubrique'];
    }
    $sql_rub_repl_fourretout = 'SELECT * FROM  `spip_rubriques` AS r WHERE `id_parent` =' . $id_rubrique . ' AND  `titre` = "Replay"';
    $rows_rub_replay_programme_fourretout = $dbh->query($sql_rub_repl_fourretout);
    return $rows_rub_replay_programme_fourretout;
    $dbh = null;
}
function getRubMeteo($titre_meteo) {
    $dbh = selectDb();
    //requete récupération rubrique correspondant au programme
    $sql_replay_programme_meteo = 'SELECT * FROM  `correspondance_replay_meteo` AS c WHERE `titre` =  "' . $titre_meteo . '"';
    $rows_replay_programme_meteo = $dbh->query($sql_replay_programme_meteo);
    foreach ($rows_replay_programme_meteo as $row_id_rub_meteo) {
        $id_rubrique = $row_id_rub_meteo['id_rubrique'];
    }
    $sql_rub_repl_meteo = 'SELECT * FROM  `spip_rubriques` AS r WHERE `id_parent` =742 AND  `id_rubrique` = 4119';
    $rows_rub_replay_programme_meteo = $dbh->query($sql_rub_repl_meteo);
    return $rows_rub_replay_programme_meteo;
    $dbh = null;
}
function deprogram($bid) {
    $dbh = selectDb();
    //requete récupération article à déprogrammé
    $sql_replay_programme = "UPDATE  `spip_articles` SET `statut` = 'prepa' WHERE  `broadcastID` = '" . $bid . "'";
    $dbh->query($sql_replay_programme);
    $dbh = null;
}

function update_program($bid, $titre, $id_rubrique, $texte, $date, $id_secteur, $date_redac) {
    $dbh = selectDb();
    //requete màj article déjà existant
    $sql_replay_update_programme = "UPDATE  `art_spip`.`spip_articles` SET "
        . "`titre` =  '" . $titre . "',"
        . "`id_rubrique` =  '" . $id_rubrique . "',"
        . "`date` =  '" . $date . "',"
        . "`texte` =  '" . utf8_decode($texte) . "',"
        . "`id_secteur` =  '" . $id_secteur . "',"
        . "`maj` =  NOW(),"
        . "`date_redac` =  '" . $date_redac . "'"
        . "WHERE  `broadcastID` = '" . $bid . "'";
    $dbh->query($sql_replay_update_programme);
    $dbh = null;
}

function verif_bid($bid) {
    $dbh = selectDb();
    //requete récupération article à déprogrammé
    $sql_verif = 'SELECT * FROM  `spip_articles` WHERE `broadcastID` = "' . $bid . '"';
    $rows_verif = $dbh->query($sql_verif);
    return $rows_verif;
    $dbh = null;
}

function insererVersDB($titre_article, $id_rubrique, $texte, $date_de_publication, $url_image, $largeur, $hauteur, $url_video, $date_redac, $id_secteur, $bid) {
    $statut = "publie";
    $export = "oui";
    $accepter_forum = "pri";
    $lang = "fr";
    $dbh = selectDb();

    $sql_insert_article = "INSERT INTO spip_articles (titre, id_rubrique, texte";
    $sql_insert_article .= ", date, statut, id_secteur, maj, export, date_redac, accepter_forum, lang, broadcastID) VALUES (";
    $sql_insert_article .= "'" . addslashes($titre_article) . "', '" . $id_rubrique . "', '" . addslashes($texte) . "','" . $date_de_publication . "'";
    $sql_insert_article .= ",'" . $statut . "', '" . $id_secteur . "', '" . $date_de_publication . "'";
    $sql_insert_article .= ",'" . $export . "', '" . $date_redac . "','" . $accepter_forum . "', '" . $lang . "', '" . $bid . "')";

    $dbh->query($sql_insert_article);
    $sql_last_id_article = $dbh->query("SELECT LAST_INSERT_ID()");
    $id_article_ = $sql_last_id_article->fetchColumn();

    //Videos
    $requete_insertion_documents_video = 'INSERT INTO spip_documents ( extension, titre, date, fichier, mode, distant,statut) '
        . 'VALUES ("ts", "' . $titre_article . '", "' . $date_de_publication . '", "' . $url_video . '", "document", "oui","publie")';

    $dbh->query($requete_insertion_documents_video);
    $sql_last_id_video = $dbh->query("SELECT LAST_INSERT_ID()");
    $id_document = $sql_last_id_video->fetchColumn();

    // insertion documents_liens
    $requete_insertion_documents_liens_videos = 'INSERT INTO spip_documents_liens (id_document, id_objet, objet) '
        . 'VALUES ("' . $id_document . '", "' . $id_article_ . '", "article")';
    $dbh->query($requete_insertion_documents_liens_videos);

    //Images
    $requete_insertion_documents_images = 'INSERT INTO spip_documents (extension, titre, date, fichier, taille, largeur, hauteur, mode, distant,statut) '
        . 'VALUES ("jpg", "' . $titre_article . '", "' . $date_de_publication . '", "' . $url_image . '", "7000", "' . $largeur . '", "' . $hauteur . '", "image", "non","publie")';
    $dbh->query($requete_insertion_documents_images);
    $sql_last_id_image = $dbh->query("SELECT LAST_INSERT_ID()");
    $id_document_image = $sql_last_id_image->fetchColumn();

    $requete_insertion_documents_liens_image = 'INSERT INTO spip_documents_liens (id_document, id_objet, objet, vu) '
        . 'VALUES ("' . $id_document_image . '", "' . $id_article_ . '", "article", "non")';
    $dbh->query($requete_insertion_documents_liens_image);

    //mot clé replay
    $requete_insertion_mots_liens_replay = 'INSERT INTO spip_mots_liens (id_mot, id_objet, objet) '
        . 'VALUES ("' . '1772' . '", "' . $id_article_ . '", "article")';
    $dbh->query($requete_insertion_mots_liens_replay);

    //Auteur BCE
    $requete_insertion_auteurs_liens_replay = 'INSERT INTO `spip_auteurs_liens` (`id_auteur`, `id_objet`, `objet`, `vu`) '
        . 'VALUES ("' . '406' . '", "' . $id_article_ . '", "article","non")';
    $dbh->query($requete_insertion_auteurs_liens_replay);

    $dbh = null;
}

/* Fonction pour avoir la taille d'un fichier */
function recup_taille_fichier($bytes, $decimals = 2) {
    $sz = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}
?>