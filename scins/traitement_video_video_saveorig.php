﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>Upload video dans la BDD SPIP - Part 01</title>
	</head>
<body style="font-family:arial; font-size:14px;">
<b style="color:#ccc; font-size:20px;">Upload video dans la BDD SPIP</b><br><br>
	
<?php
	$url_script_list_video="http://www.antennereunion.fr/scins/traitement_video_video.php"; 	//url du script principal
	$url_script_insertion_article="http://www.antennereunion.fr/scins/traitement_video_antenne.php"; 	//url du script d'insertion d'article dans la BDD SPIP
	$url_script_video_attente = "http://www.antennereunion.fr/scins/traitement_dossier_attente.php"; 	//url du script pour traiter le dossier de vidéos non traitées
	$url_dossier_video = "/home/video.antennereunion.fr/public_html/";//Dossier dans lequel se trouve toutes les vidéos une fois traiter
	$url_dossier_video_http = "http://video.antennereunion.fr/"; 		//Dossier dans lequel se trouve toutes les vidéos une fois traiter en htpp:// pour passer à la BDD spip
	$url_dossier_video_a_traiter = "/home/video.antennereunion.fr/public_html/scins/emissions/"; 	//Dossier dans lequel se trouve les vidéos à traiter
	$url_dossier_video_en_attente = "/home/video.antennereunion.fr/public_html/scins/emissions/attente/"; 	//Dossier dans lequel se trouve les vidéos non traiter en attente
	$url_dossier_image = "/home/video.antennereunion.fr/public_html/scins/images_emissions/"; 	//Dossier dans lequel se trouve toutes les images émissions
	$url_dossier_image_http = "http://video.antennereunion.fr/scins/images_emissions/"; 	//Dossier dans lequel se trouve toutes les images émissions en htpp:// pour passer à la BDD spip

	$execution_du_script = 0;
	$i=0;
	
	$insert="";
	if(isset($_GET["insert"])){$insert=$_GET["insert"]; echo "<br><br>INSERT = ".$insert."<br><br>";}
	
	include ("/home/antennereunion.fr/httpdocs/scins/tableaux_emissions.php");
	
	/////////////////////////////////////////////////////////////////////////
	// lecture du dossier de fichiers.
	//
	$dirname = $url_dossier_video_a_traiter;
	$dir = opendir($dirname); 
	while($file = readdir($dir)) {
		if($file != '.' && $file != '..' && !is_dir($dirname.$file))
		{
			$filelist[] = $file;
		}
	}
	closedir($dir);
	/////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////
	// lecture du tableau de fichiers.
	//
	if(!isset($filelist[$i])){
		echo "<br><b>Aucun fichier dans le dossier</b><br> Ou l'émission n'est pas dans le listing <br><br>";
	}
	else if(isset($filelist[$i])){
		echo "<br><b>Voici la liste des fichiers existants :</b> <br><br>";
	}
		
	//////////////////////////////////////////////////////
	////   DEPLACER / SUPPRIMER les fichiers vidéos   ////
	
	if($insert=="1"){
	
		echo "test";
		if(isset($filelist[0])){
			/////////////////////////////////////////////////////////////////////////
			// Deplacer les fichiers.
			// déplacer le fichier vers un dossier de traitement pour la BDD de SPIP.
			// + déplacer le fichier vers un dossier d'archive limiter dans le temps. (pas de dossier d'archive pour le moment)
			// + supprimer le fichier du dossier d'origine.
			//
			// Deplacer dans le dossier général qui contient toutes les vidéos traitées.
			if(rename($url_dossier_video_a_traiter.$filelist[0],$url_dossier_video.$filelist[0])){
				echo "<br>Le fichier ".$filelist[0]." <br>a été déplacé dans le répertoire <b>".$url_dossier_video."</b><br>";
								
				fopen($url_script_list_video."?insert=0","r");
			}
			else echo "Erreur de copie<br><br>";
		}
	}
	////   FIN   DEPLACER / SUPPRIMER les fichiers vidéos   ////
	////////////////////////////////////////////////////////////
	
	
	
	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	//
	//
	
	$element=0;
	if(isset($_GET["element"])){$element=$_GET["element"];}
	
	if(isset($filelist[$element])){
		
		echo ($element." - ");
		echo("<b>".$filelist[$element]."</b>");
		
		$nom_emission=substr($filelist[$element], 0, strpos($filelist[$element], '_'));
		$nom_emission=strtoupper($nom_emission);
		
		/////////////////  TEST de traitement du ficier / Si c'est une émission connue ///////////////////
		if(in_array($nom_emission, $tableau_test_emission)) {
			echo "<br><br>Le titre existe dans la liste à traiter, c'est <b>OK</b><br><br>";
			$execution_du_script = 1;
		}
		else if (!in_array($nom_emission, $tableau_test_emission)){
			echo "<br><br><b>WARNING :</b> L'émission <b>".$nom_emission." n'existe pas</b> dans la liste à traiter<br><br>";
			$execution_du_script = 0;
			
			if(rename($url_dossier_video_a_traiter.$filelist[$element],$url_dossier_video_en_attente.$filelist[$element])){
				echo "<br>Le fichier ".$filelist[$element]." <br>a été déplacé dans le répertoire <b>attente/</b><br>";

				fopen($url_script_list_video."?insert=0","r");
			}
		}
		/////////////////  FIN  TEST de traitement du ficier  ///////////////////
		
		/////////////////  Exécution du script si l'émission est connue  ///////////////////
		////////////////////////////////////
		if($execution_du_script==1) {
				
			// Traitement du nom du fichier
			$titre_emission=strstr($filelist[$element], '_', true);
			echo "titre emission : "; echo $titre_emission; echo "<br>";
			
			$date_fichier_publication=substr(strrchr($filelist[$element], '_'), 1, -4);
			$date_fichier_jour=substr($date_fichier_publication, 0, 2);
			$date_fichier_mois=substr(strstr($date_fichier_publication, '-'), 1, 2);
			$date_fichier_annee=substr(strrchr($date_fichier_publication, '-'), 1, 4);
					
			echo "date publication : "; echo $date_fichier_publication; echo "<br>"; echo $date_fichier_jour; echo "/"; echo $date_fichier_mois; echo "/"; echo $date_fichier_annee; 
			
			$date_fichier= $date_fichier_jour."-".$date_fichier_mois."-".$date_fichier_annee;
			$date_fichier_publication= $date_fichier_annee."-".$date_fichier_mois."-".$date_fichier_jour." 04:00:00";
			// FIN  Traitement du nom du fichier
			
			///////////////////////////// ARTICLES /////////////////////////////
			//
			$titre_aticle = utf8_decode($tableau_titre_emission["tab_titre_emission"][$nom_emission]." - émission du ".$date_fichier);		
			$id_rubrique = $tableau_rubrique["tab_rubrique"][$nom_emission];
			$date_de_publication = date("Y-m-d H:i:s", mktime(04, 00, 00, $date_fichier_mois, $date_fichier_jour+1, $date_fichier_annee));  // exemple de date : "2010-12-30 04:00:00"
			$id_secteur = 1;
			//
			///////////////////////////// FIN  ARTICLES /////////////////////////////
			
			
			///////////////////////////// VIDEOS /////////////////////////////
			// document au format vidéo : flv
			//
			$url_video = $url_dossier_video_http.$filelist[$element];
			//
			///////////////////////////// FIN  VIDEOS /////////////////////////////
			
			
			///////////////////////////// IMAGES /////////////////////////////
			// document au format image : jpg, gif, png.
			//
			$nom_dossier_emission = $nom_emission;
						
			// lecture du dossier d'images.
			$dossier_emission = $url_dossier_image.$nom_dossier_emission.'/';
			$dossier_select = @opendir($dossier_emission); 
			if ($dossier_select==true){
				$filelist_image = array();
				while($file_image = readdir($dossier_select)) {
					if($file_image != '.' && $file_image != '..' && !is_dir($dossier_emission.$file_image)){
						$extension = explode(".",$file_image);
						$tab_extension = array("jpg","gif","png");
						if(in_array($extension[1],$tab_extension)){
							echo $extension[1];
							$filelist_image[] = $file_image;
						}
					}
				}
				closedir($dossier_select);
				
				if(!empty($filelist_image)){
					//mélange du tableau pour sélection au hasard
					shuffle($filelist_image);
					// premier élément du tableau
					$premiere_image = array_shift($filelist_image);
					echo $premiere_image;
					
					$sizes = getimagesize($url_dossier_image.$nom_dossier_emission.'/'.$premiere_image);
					echo '<br/>Largeur : '.$sizes[0].'<br/>';
					echo 'Hauteur : '.$sizes[1].'<br/>';
					$url_image = $url_dossier_image_http.$nom_emission.'/'.$premiere_image;
					echo $url_image;
				}
				else if (empty($filelist_image)){
					$url_image="rien";
				}
			}
			else if ($dossier_select==false){
				echo "<br><br><b>WARNIMG !</b> Dossier image inexistant pour cette émission.<br><br>";
				$url_image="rien";
				echo $url_image;
			}
			// FIN  lecture du dossier d'images.
			
			
			//
			///////////////////////////// FIN  IMAGES /////////////////////////////
			
			echo "<br>_____________________________________________<br><br>";			
			$execution_du_script = 0;
		#URL
		$__url_ =  $url_script_insertion_article."?nom_emission=".rawurlencode($titre_aticle);
		$__url_ .= "&id_rubrique=".$id_rubrique."&date=".rawurlencode($date_de_publication)."&url_video=".$url_video;
		$__url_ .= "&url_image=".$url_image."";
			/*fopen($url_script_insertion_article."?nom_emission=".rawurlencode($titre_aticle)."&id_rubrique=".$id_rubrique."&date=".rawurlencode($date_de_publication)."&url_video=".$url_video."&url_image=".$url_image."","r");	
echo($url_script_insertion_article."?nom_emission=".rawurlencode($titre_aticle)."&id_rubrique=".$id_rubrique."&date=".rawurlencode($date_de_publication)."&url_video=".$url_video."&url_image=".$url_image."");			
			
			*/
			if (!fopen($__url_, "r")) {
				
				echo "Impossible d'ouvrir le fichier...";
			}
			
			echo $__url_;
			
		}
		////////////////////////////////////
		/////////////////  FIN  Exécution du script si l'émission est connue  ///////////////////
		
		
	}
		
	//
	//
	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	
?>

</body>
</html>
