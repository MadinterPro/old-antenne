<?php

/**
 * @DESC: Fichier pour le traitement de l'importation automatique Video
 */
//les paramètres en DEV ==> à changer par les paramètres en PROD

//$filename_lock = "LOCKVIDEOVIDEONAS.txt";
//if (!file_exists($filename_lock)) { //s'il n'y a pas de vérouillage
//    $date = new DateTime();
//    file_put_contents($filename_lock, $date->getTimestamp()); //vérouillage du traitement

    header('Pragma: no-cache');

    /* define ('HOST', 'localhost');
      define ('USER', 'antv3_admin');
      define ('PASS', 'antv3_2009');
      define ('DB', 'antennetv_v32009'); */

    define('HOST', 'localhost');
    define('USER', 'antenne_spipAR');
    define('PASS', 'antv3_2009');
    define('DB', 'antennetv_v32009');
    define('DB_NEW', 'antenne_art_spip');

    define('ID_MOT', '1772'); //

    define('VIDEO_TRAITER', 'public/scins/emissions/*.flv');
//define('VIDEO_TRAITER_', '/');
    define('EMISSION', '/');

//define('DOMAINE_VIDEO', 'mobile.antennereunion.fr'); Laatr 1er juillet 2015
//define('URL_VIDEO_HTTP', 'http://'.DOMAINE_VIDEO.'/linfo/IMG/video/'); Laatr 1er juillet 2015
//define('URL_HTTP', 'http://'.DOMAINE_VIDEO.'/linfo/IMG/video/'); Laatr 1er juillet 2015
//define('URL_VIDEO_HTTP', 'http://'.DOMAINE_VIDEO.'/antenne/IMG/archivevideo/'); Laatr 17 juillet 2015
//define('URL_HTTP', 'http://'.DOMAINE_VIDEO.'/antenne/IMG/archivevideo/'); Laatr 17 juillet 2015

    define('DOMAINE_VIDEO', 'cdn.antenne.re');
    define('URL_VIDEO_HTTP', 'http://' . DOMAINE_VIDEO . '/nas/');
    define('URL_HTTP', 'http://' . DOMAINE_VIDEO . '/n/');
    define('URL_VIDEO_ATTENTE', 'public/scins/emissions/attente');
    define('URL_DOSSIER_IMAGE', 'public/scins/images_emissions');

    require_once("FTPClass.php"); //librarie de classe FTP
    include ("/home/antenne/public_html/scins/tableaux_emissions.php");
    setlocale(LC_TIME, 'fr_FR', 'fra');
    date_default_timezone_set("Europe/Paris");
    mb_internal_encoding("UTF-8");

    function dateFR($date, $heure = 'yes') {
        if ($heure == 'yes') {
            $strDate = mb_convert_encoding('%A %d %B %Y', 'ISO-8859-9', 'UTF-8');
        } else {
            $strDate = mb_convert_encoding('%A %d %B %Y', 'ISO-8859-9', 'UTF-8');
        }
        return ucfirst(iconv("ISO-8859-9", "UTF-8", strftime($strDate, strtotime($date))));
    }

    /* if ($oFTP = new FTP('ftp.we.binarysec.com', 'systeme%video.antennereunion.fr', '5g3ekf_BAV9')) {
      echo"<br>CONNEXION CHEZ VIDEO.ART<br>";
      $oFTP->enablePassive();
      } else {
      echo"IMPOSSIBLE DE SE CONNECTER CHEZ VIDEO.ART";
      } */


//if ($oFTP = new FTP('antennereunion.frontal.cahri.net', 'art_video@antennereunion.ext', '@nt3nn3*')) { // Initial - Cloud OVH
//if ($oFTP = new FTP('antennereunion.frontal.cahri.net', 'cahrimobile@antennereunion.ext', 'c@hr1Vide*')) { // sousdomaine mobile - laatr 1er juillet 2015
//if ($oFTP = new FTP('antennereunion.frontal.cahri.net', 'cahrivideo@antennereunion.ext', 'c@hr1Vide*')) {//Archive video - laatr 17 juillet 2015
    if ($oFTP = new FTP('antenne.nas.hostin.network', 'antenne', 'no4emaiK')) {
        echo"<br>CONNEXION CHEZ VIDEO.ART<br>";
        $oFTP->enablePassive();
    } else {
        echo"IMPOSSIBLE DE SE CONNECTER CHEZ VIDEO.ART";
    }


#Fait une lecture du dossier /, par FTP
    $_list_files_audio = $oFTP->ls(VIDEO_TRAITER, true);


#!!!Juste DEBUG!!!
    echo "<br> nbre de fichier dans NAS:public/scins/ :" . sizeof($_list_files_audio) . "<br>";
    echo"<pre>";
    print_r($_list_files_audio) . "<br>";
    echo"</pre>";


#parcourt du repertoire video
    for ($i = 0; $i < sizeof($_list_files_audio); $i++) {

        $_list_podcast_ = explode(' ', $_list_files_audio[$i]);

        #!!!Juste DEBUG!!!
        echo "<pre>";
        print_r($_list_podcast_) . "<br>";
        $a = sizeof($_list_podcast_);
        echo "Fichier: " . $_list_podcast_[$a - 1] . "<br>";
        echo "</pre>";

        #pour chaque fichier, il faut prendre toutes les infos, ex de format: KLB_28-11-2011_.flv
        $temp_chemin = $_list_podcast_[$a - 1];
        $temp_chemin = str_replace('public/scins/emissions/', '', $temp_chemin);
        $tab_emissions = explode('_', $temp_chemin);
        $titre_emission = $tab_emissions[0];

        echo '<br>NOM DU FICHIER EMISSION:' . $titre_emission . '<br>';
        //$titre_emission = str_replace('/scins/emissions/','', $titre_emission);

        $dt_emission = $tab_emissions[1];
        $hr_emission = $tab_emissions[2];

        $dt_emission = explode(".", $dt_emission);
        $dt_emission = $dt_emission[0];
        //$hr_emission1 = $tab_emissions[2];
        //$tab_hr_emission = explode('.', $hr_emission1);
        //$hr_emission = $tab_hr_emission[0];
        $partie = 0;
        $partie_emission = $tab_emissions[2];
        $partie = intval($partie_emission);

        #!!!Juste DEBUG!!!	
        echo "<br> Titre de l'emission: " . $titre_emission . "<br>";
        var_dump($tableau_test_emission);
        //On va prendre le fichier de l'emission
        if (in_array(trim($titre_emission), $tableau_test_emission)) {

            #!!!Juste DEBUG!!!
            echo "Le titre existe dans la liste à traiter, c'est <b>OK</b><br />";

            if ($dt_emission != '') {
                $arr_dt_emission = explode('-', $dt_emission);
            }

            $hr_publication = 04;
            $mn_publication = 00;
            if ($hr_emission != '' && $titre_emission != "PV") {
                $arr_hr_emission = explode('-', $hr_emission);
                $hr_publication = $arr_hr_emission[0];
                $mn_publication = $arr_hr_emission[1];
            }

            //heure
            //if ($hr_emission != '') {
            //	$_arr_hr = explode('-', $hr_emission);
            //}				

            $date_fichier_publication = $arr_dt_emission[0] . "/" . $arr_dt_emission[1] . "/" . $arr_dt_emission[2];

            echo "<br> Date de fichier:" . $date_fichier_publication . "<br>";

            #Infos concernant les articles
            /* $titre_article = utf8_decode($tableau_titre_emission["tab_titre_emission"][$titre_emission]." - émission du ".$date_fichier_publication);
              if 	($partie != 0){
              if($titre_emission == "PV" || $titre_emission == "PR"){ // Punar Vivaah Pavitra
              $titre_article .= " - &eacute;pisode ". $partie;
              }else{
              $titre_article .= " partie ". $partie;
              }

              } */
            $titre_article = 'Replay ';
            $date_titre = dateFR(date("l j F Y", mktime($hr_publication, $mn_publication, 00, $arr_dt_emission[1], $arr_dt_emission[0], $arr_dt_emission[2])));

            if (($titre_emission == "PV") || ($titre_emission == "PR")) {
                $titre_article .= utf8_decode($tableau_titre_emission["tab_titre_emission"][$titre_emission]);
                if ($partie != 0) {
                    $titre_article .= " - &eacute;pisode " . $partie;
                }
                $titre_article .= " - " . $date_fichier_publication;
            } else if ($titre_emission == "RS") {
                if ($hr_publication == "12") {
                    $titre_article = "Runstar Replay La quotidienne" . " - " . $date_titre;
                } else {
                    $titre_article = "Runstar Replay La prime" . " - " . $date_titre;
                }
            } else {

                $titre_article .= utf8_decode($tableau_titre_emission["tab_titre_emission"][$titre_emission] . " - " . $date_titre);
                /* if ($partie != 0) {
                  $titre_article .= " partie " . $partie;
                  } */
            }


            echo "<br />TITRE DE L'ARTICLE : " . $titre_article;

            $id_rubrique = $tableau_rubrique["tab_rubrique"][$titre_emission];

            $date_pub = date("Y-m-d", mktime($hr_publication, $mn_publication, 00, $arr_dt_emission[1], $arr_dt_emission[0], $arr_dt_emission[2]));

            if ($titre_emission == "PR") {
                $date_de_publication = date("Y-m-d H:i:s", mktime(20, 00, 00, $arr_dt_emission[1], $arr_dt_emission[0], $arr_dt_emission[2]));
            } else {
                $date_de_publication = date("Y-m-d H:i:s", mktime($hr_publication, $mn_publication, 00, $arr_dt_emission[1], $arr_dt_emission[0], $arr_dt_emission[2]));
            }
            echo "Dates : " . $date_pub . " " . $date_de_publication;
            $id_secteur = 1;

            #Infos de la video: url de lecture de video
            $url_audio_mp3 = URL_VIDEO_HTTP . str_replace('public/scins/emissions/', '', $_list_podcast_[$a - 1]);
            echo "<br />URL VIDEO HTTP : " . $url_audio_mp3 . '<br />';
            #Lecture dossier image

            $dossier_emission = URL_DOSSIER_IMAGE . '/' . $titre_emission;

            #!!!Juste DEBUG!!!	
            echo "DOSSIER IMAGE EMISSION: " . $dossier_emission . '<br />';
            echo "URL HTTP VIDEO: " . $url_audio_mp3 . '<br />';

            #Fait une lecture du dossier /radio/, par FTP
            $_list_files_audio_pochettes = $oFTP->ls($dossier_emission, true);

            echo"<pre>";
            print_r($_list_files_audio_pochettes);
            echo"</pre>";

            $filelist_image = array();
            for ($j = 0; $j < sizeof($_list_files_audio_pochettes); $j++) {

                $_list_images_pochettes_ = explode(' ', $_list_files_audio_pochettes[$j]);
                $size_tab = sizeof($_list_images_pochettes_);
                //VERIFIER L'IMAGE
                echo '<pre>';
                print($_list_images_pochettes_);
                echo '<pre>';

                echo 'NOMBRE DE FOTOS : ' . $size_tab . '<br>';

                $extension = explode(".", $_list_images_pochettes_[$size_tab - 1]);
                $tab_extension = array("jpg", "gif", "png");
                if (in_array($extension[1], $tab_extension)) {

                    echo $extension[1];
                    $filelist_image[] = $_list_images_pochettes_[$size_tab - 1];
                }
            }//fin for

            echo"Listes des images: <pre>";
            print_r($filelist_image);
            echo"</pre>";

            if (!empty($filelist_image)) {

                //mélange du tableau pour sélection au hasard
                shuffle($filelist_image);

                // premier élément du tableau
                $premiere_image = array_shift($filelist_image);
                echo "Premier image: " . $premiere_image;
                $sizes = getimagesize(URL_HTTP . $dossier_emission . '/' . $premiere_image);
                $largeur = $sizes[0];
                $hauteur = $sizes[1];

                #!!!Juste DEBUG!!!
                echo '<br/>Largeur : ' . $largeur . '<br/>';
                echo 'Hauteur : ' . $hauteur . '<br/>';
                $url_image = URL_HTTP . $dossier_emission . '/' . $premiere_image;
                $url_image = str_replace('/home/adplay/public_html/', '', $url_image);

                echo $url_image;
            } else if (empty($filelist_image)) {
                $url_image = "rien";
            }//fin if image

            $url_image = "rien";
            //http://cdn.antenne.re/linfo/IMG/video/PV_31-07-2014_288.flv
            $photo = $_list_podcast_[$a - 1];
            $photo = str_replace('flv', 'jpeg', $photo);

            //$photo = str_replace('/scins/emissions/', '', $photo);
            //$photo = "http://cdn.antenne.re/linfo/IMG/video".$photo;
            echo "Photo test jpeg : " . $photo;
            try {
                $oFTP->size($photo);
                $newphoto = str_replace('public/scins/emissions/', 'public/', $photo);
                $oFTP->rename($photo, $newphoto);
                //$newphoto = "http://cdn.antenne.re/linfo/IMG/video/".$newphoto;//Laatr - 21062015
                $newphoto = str_replace('public/scins/emissions/', '', $photo);
                $newphoto = URL_VIDEO_HTTP . $newphoto;
                $url_image = $newphoto;
            } catch (Exception $e) {
                
            }

            $photo = $_list_podcast_[$a - 1];
            $photo = str_replace('flv', 'jpg', $photo);

            //$photo = str_replace('/scins/emissions/', '', $photo);
            //$photo = "http://cdn.antenne.re/linfo/IMG/video".$photo;
            echo "Photo test jpg : " . $photo;
            try {
                $oFTP->size($photo);
                $newphoto = str_replace('public/scins/emissions/', 'public/', $photo);
                $oFTP->rename($photo, $newphoto);
                //$newphoto = "http://cdn.antenne.re/linfo/IMG/video/".$newphoto;  Laatr 21062015
                $newphoto = str_replace('public/scins/emissions/', '', $photo);
                $newphoto = URL_VIDEO_HTTP . $newphoto;
                $url_image = $newphoto;
            } catch (Exception $e) {
                
            }


            $photo = $_list_podcast_[$a - 1];
            $photo = str_replace('flv', 'png', $photo);
            echo "Photo test png : " . $photo;
            try {
                $oFTP->size($photo);
                $newphoto = str_replace('public/scins/emissions/', 'public/', $photo);
                $oFTP->rename($photo, $newphoto);
                //$newphoto = "http://cdn.antenne.re/linfo/IMG/video/".$newphoto;Laatr 21062005
                $newphoto = str_replace('public/scins/emissions/', '', $photo);
                $newphoto = URL_VIDEO_HTTP . $newphoto;
                $url_image = $newphoto;
            } catch (Exception $e) {
                
            }

            $newmp4 = '';
            $mp4 = $_list_podcast_[$a - 1];
            $video_mp4 = str_replace('flv', 'mp4', $mp4);
            try {
                $oFTP->size($video_mp4);
                $newmp4 = str_replace('public/scins/emissions/', 'public/', $video_mp4);
                $oFTP->rename($video_mp4, $newmp4);
            } catch (Exception $e) {
                
            }



            echo "Photo : " . $newphoto . " ---- " . $url_image;
            echo "mp4 : " . $newmp4 . " ---- ";

            #Insertion dans la base de donnée
            if ($newmp4 != '') {
                $db = "art_spip";
                insererVersDB($titre_article, $id_rubrique, $date_de_publication, $url_image, $largeur, $hauteur, $url_audio_mp3, $db, $date_pub);

                #deplacement du fichier
                $ext = substr($_list_podcast_[$a - 1], strlen($_list_podcast_[$a - 1]) - 4, 4);
                //echo "Extension: " . $ext;

                if (strtoupper(trim($ext)) == ".FLV") {

                    //reconstruire l'URL
                    $_url_audio_a_traiter = $_list_podcast_[$a - 1];
                    $_url_emission = str_replace('public/scins/emissions/', 'public/', $_list_podcast_[$a - 1]);

                    echo "<br> URL DOSSIER VIDEO A TRAITER : " . $_url_audio_a_traiter . "<br>";
                    echo "<br> URL DOSSIER DESTINATION  : " . $_url_emission . "<br>";
                    //On v transferer les fichiers mp3 vers /emissions
                    //$oFTP->put($_url_audio_a_traiter, $_url_emission);
                    $oFTP->rename($_url_audio_a_traiter, $_url_emission);
                }//fin if flv
            }
        }//fin emission connue
        else if (!in_array($titre_emission, $tableau_test_emission)) {
            echo "<br><br><b>WARNING :</b> L'émission <b>" . $titre_emission . " n'existe pas</b> dans la liste à traiter<br><br>";
            $execution_du_script = 0;

            #deplacement du fichier
            $ext = substr($_list_podcast_[$a - 1], strlen($_list_podcast_[$a - 1]) - 4, 4);

            if (strtoupper(trim($ext)) == ".FLV") {

                //reconstruire l'URL
                $_url_audio_a_traiter = $_list_podcast_[$a - 1];
                //$_url_emission = URL_VIDEO_ATTENTE .str_replace('scins/emissions/','', $_list_podcast_[$a-1]);
                $_url_emission = str_replace('public/scins/emissions/', 'public/scins/emissions/attente/', $_list_podcast_[$a - 1]);

                echo '<br>URL FICHIER ATTENTE : ' . $_url_emission . '<br>';
                //On v transferer les fichiers mp3 vers /emissions
                $oFTP->rename($_url_audio_a_traiter, $_url_emission);

                echo 'TRANSFERT VERS DOSSIER ATTENTE';
            }//fin if FLV
            else {
                echo "<br>Pas de déplacement des videos<br>";
            }
        }//fin emission inconnue => dans attente
    }//fin for ($i = 0 => parcours repertoire audio

    $oFTP->close();

    /**
     * DESC: insertion vers le DB
     */
    function insererVersDB($titre_article, $id_rubrique, $date_de_publication, $url_image, $largeur, $hauteur, $url_video, $db, $date_pub) {

        $statut = "publie";
        $id_secteur = 1;
        $export = "oui";
        $accepter_forum = "pos";
        $lang = "fr";
        $link = mysql_connect(HOST, USER, PASS);

        if (!$link)
            echo "erreur de connexion au serveur " . $host;
        mysql_select_db($db) or die("Impossible de se connecter à la BDD ");

        #Verifier si l'article existe déjà
        echo "date = " . $date_pub . ' ' . $id_rubrique;
        $test_presence = '';
        $test_article = mysql_query('SELECT id_article FROM spip_articles WHERE statut="publie" AND id_rubrique="' . $id_rubrique . '" AND date LIKE "%' . $date_pub . '%"');
        while ($row = mysql_fetch_array($test_article)) {

            echo utf8_encode($row['id_article']);
            $test_presence = $row['id_article'];
        }

        #Si l'article n'existe ps
        if ($test_presence == '') {
            echo "<br>L'article n'existe pas encore<br>";
            $sql = "INSERT INTO spip_articles (titre, id_rubrique";
            $sql .= ", date, statut, id_secteur, maj, export, accepter_forum, lang) VALUES (";
            $sql .= "'" . mysql_real_escape_string($titre_article) . "', '" . $id_rubrique . "', '" . $date_de_publication . "'";
            $sql .= ",'" . $statut . "', '" . $id_secteur . "', '" . $date_de_publication . "'";
            $sql .= ",'" . $export . "', '" . $accepter_forum . "', '" . $lang . "')";

            echo "SQL:" . $sql;
            $requete_insertion_article = mysql_query($sql) or die('Erreur de la requête insertion Article MySQL');

            $id_article_ = mysql_insert_id();

            //Videos
            if ($db == 'antennetv_v32009') {
                $requete_insertion_documents = mysql_query('INSERT INTO spip_documents ( extension, titre, date, fichier, mode, distant) VALUES ("flv", "' . mysql_real_escape_string($titre_article) . '", "' . $date_de_publication . '", "' . $url_video . '", "document", "oui")') or die('Erreur de la requête insertion_Document VIDEO MySQL');
                $id_document = mysql_insert_id();
            } else {
                $requete_insertion_documents = mysql_query('INSERT INTO spip_documents ( extension, titre, date, fichier, mode, distant,statut) VALUES ("flv", "' . mysql_real_escape_string($titre_article) . '", "' . $date_de_publication . '", "' . $url_video . '", "document", "oui","publie")') or die('Erreur de la requête insertion_Document VIDEO MySQL');
                $id_document = mysql_insert_id();
            }


            // insertion documents_liens
            $requete_insertion_documents_liens = mysql_query('INSERT INTO spip_documents_liens (id_document, id_objet, objet) VALUES ("' . $id_document . '", "' . $id_article_ . '", "article")') or die('Erreur de la requête insertion_Document_Liens VIDEO MySQL');

            //Images
            if ($url_image != "rien") {

                #$sizes = getimagesize($url_image);
                if ($db == 'antennetv_v32009') {
                    $requete_insertion_documents = mysql_query('INSERT INTO spip_documents (extension, titre, date, fichier, taille, largeur, hauteur, mode, distant) VALUES ("jpg", "' . mysql_real_escape_string($titre_article) . '", "' . $date_de_publication . '", "' . $url_image . '", "7000", "' . $largeur . '", "' . $hauteur . '", "image", "non")') or die('Erreur de la requête insertion_Document IMAGE MySQL');
                    $id_document_image = mysql_insert_id();
                } else {
                    $requete_insertion_documents = mysql_query('INSERT INTO spip_documents (extension, titre, date, fichier, taille, largeur, hauteur, mode, distant,statut) VALUES ("jpg", "' . mysql_real_escape_string($titre_article) . '", "' . $date_de_publication . '", "' . $url_image . '", "7000", "' . $largeur . '", "' . $hauteur . '", "image", "non","publie")') or die('Erreur de la requête insertion_Document IMAGE MySQL');
                    $id_document_image = mysql_insert_id();
                }

                //
                $requete_insertion_documents_liens = mysql_query('INSERT INTO spip_documents_liens (id_document, id_objet, objet, vu) VALUES ("' . $id_document_image . '", "' . $id_article_ . '", "article", "non")') or die('Erreur de la requête insertion_Document_Liens IMAGE MySQL');
            }

            $id_mot = intval(ID_MOT);

            //
            if ($db == 'antennetv_v32009') {
                $requete_insertion_mots_liens = mysql_query('INSERT INTO spip_mots_articles (id_mot, id_article) VALUES ("' . $id_mot . '", "' . $id_article_ . '")') or die('Erreur de la requête insertion_Mots_Liens VIDEO MySQL');
            } else {
                $requete_insertion_mots_liens = mysql_query('INSERT INTO spip_mots_liens (id_mot, id_objet, objet) VALUES ("' . $id_mot . '", "' . $id_article_ . '", "article")') or die('Erreur de la requête insertion_Mots_Liens VIDEO MySQL');
            }
        } else {
            $id_article_ = $test_presence;
            //Auteur en prod:
            //$requete_insertion_auteur_article = mysql_query('INSERT INTO spip_auteurs_articles (id_auteur, id_article) VALUES ("142", "' . $id_article_ . '")') or die('Erreur de la requête insertion Auteur MySQL');
            //Videos
            if ($db == 'antennetv_v32009') {
                $requete_insertion_documents = mysql_query('INSERT INTO spip_documents ( extension, titre, date, fichier, mode, distant) VALUES ("flv", "' . mysql_real_escape_string($titre_article) . '", "' . $date_de_publication . '", "' . $url_video . '", "document", "oui")') or die('Erreur de la requête insertion_Document VIDEO MySQL');
                $id_document = mysql_insert_id();
            } else {
                $requete_insertion_documents = mysql_query('INSERT INTO spip_documents ( extension, titre, date, fichier, mode, distant,statut) VALUES ("flv", "' . mysql_real_escape_string($titre_article) . '", "' . $date_de_publication . '", "' . $url_video . '", "document", "oui","publie")') or die('Erreur de la requête insertion_Document VIDEO MySQL');
                $id_document = mysql_insert_id();
            }

            // insertion documents_liens
            $requete_insertion_documents_liens = mysql_query('INSERT INTO spip_documents_liens (id_document, id_objet, objet) VALUES ("' . $id_document . '", "' . $id_article_ . '", "article")') or die('Erreur de la requête insertion_Document_Liens VIDEO MySQL');
        }


        ///////////////////////////// MOT CLE /////////////////////////////
        //
        
        
        ///////////////////////////// FIN  MOT CLE /////////////////////////////

        mysql_close($link);
    }
//    unlink($filename_lock); //déverouillage
//} else {
//    echo "un verrou est encore en place !";
    //exit, un autre traitement est encore en cours ...
//}