<?php

chdir(dirname(__FILE__) . '/../');
/** Accés antenne reunion* */
require_once 'ecrire/inc_version.php';
require_once 'ecrire/base/abstract_sql.php';
require_once 'squelettes/mes_fonctions.php';
/* Acces Linfo */
define(DATA_BASE_TABLE_RUB_MOT_A_COPIER, "rub_mot_acopier");
define(DATA_BASE_TABLE_RUB_MOT_A_COPIER_ID_ARTICLE, "rub_mot_acopier_article_id");
define(DATA_BASE_TABLE_MOTS_LIENS, "spip_mots_liens");
define(DATA_BASE_TABLE_ARTICLES, "spip_articles");
define(DATA_BASE_TABLE_DOCUMENT, "spip_documents");
define(DATA_BASE_TABLE_DOCUMENT_LIENS, "spip_documents_liens");
define(DATA_BASE_TABLE_URLS, "spip_urls");


$select_rub_acopier = sql_select(
        array(
    "rmc.id_rub_mot AS id_rub_mot",
    "rmc.id_objet AS id_objet",
    "rmc.objet AS objet",
    "rmc.id_rubrique_destination AS id_rubrique_destination",
    "rmc.id_secteur_destination AS id_secteur_destination",
        ), array(DATA_BASE_TABLE_RUB_MOT_A_COPIER . " AS rmc"), array(), array(), array(), "", array()
);


echo "<pre>";
var_dump(sql_fetch($select_rub_acopier));
exit;

while ($rub_mot = sql_fetch($select_rub_acopier)) {
    echo "* Debut copie des article du ".$rub_mot['objet']." = ".$rub_mot['id_objet']." dans ".$rub_mot['nom_rub_destination'].":<br />";
    $article_deja_copie_res = sql_select(
        array("rmca.id_article AS id_article"), array(
            DATA_BASE_TABLE_RUB_MOT_A_COPIER_ID_ARTICLE." AS rmca",                    
        ), array(
            "rmca.id_rub_mot=" . $rub_mot['id_rub_mot'],
        ), array(), array(), "", array()
    );
    $article_deja_copie = "";
    $i = 0;
    $nombre_articles = sql_count($article_deja_copie_res);
    while($row = sql_fetch($article_deja_copie_res)){
        $article_deja_copie .= $row['id_article'];
        if($nombre_articles != $i +1)
            $article_deja_copie .=',';
        $i++;        
    }
    $listing_article_linfo;
    switch ($rub_mot['objet']) {
        case "mot":
            $listing_article_linfo = sql_select(
                    array("ml.id_objet AS id_article"), array(
                        DATA_BASE_TABLE_MOTS_LIENS . " AS ml LEFT JOIN ".DATA_BASE_TABLE_ARTICLES . " AS a ON ml.id_objet=a.id_article",
                    ), array(
                "ml.id_mot=" . $rub_mot['id_objet'],
                "ml.objet='article'" . ($article_deja_copie != "" ? " AND ml.id_objet NOT IN (" . $article_deja_copie.")" : "") ,
                "a.statut='publie'",
                "a.date>'2015-01-01 00:00:00'"        
                    ), array(), array(), "", array(), "linfo"
            );
            break;
        case "rubrique":
            $listing_article_linfo = sql_select(
                array("a.id_article AS id_article"), array(
                    DATA_BASE_TABLE_ARTICLES . " AS a",                    
                ), array(
                "a.statut='publie'",
                "a.date>'2015-01-01 00:00:00'",    
                "a.id_rubrique=" . $rub_mot['id_objet'] . ($article_deja_copie != "" ? " AND a.id_article NOT IN (" . $article_deja_copie.")" : "") ,
                
                    ), array(), array(), "", array(), "linfo"
            );
            break;
    }
    while ($article_linfos = sql_fetch($listing_article_linfo)) {
        copie_article_linfo_art($article_linfos['id_article'], $rub_mot['id_rub_mot'], $rub_mot['id_rubrique_destination'], $rub_mot['id_secteur_destination']);
    }
}

/*Fin traitement*/

function copie_article_linfo_art($id_article = null, $id_rub_mot = null, $id_rub_destination = null, $id_secteur_destination = null) {
    echo "       - Article linfo avec id = ".$id_article;
    if (is_null($id_article) && is_null($id_rub_destination) && is_null($id_rub_mot))
        return false;
    $details_article_ressource = sql_select(
            array(
                "a.id_article AS id_article",
                "a.surtitre AS surtitre",
                "a.titre AS titre",
                "a.soustitre AS soustitre",
                "a.id_rubrique AS id_rubrique",
                "a.descriptif AS descriptif",
                "a.chapo AS chapo",
                "a.texte AS texte",
                "a.ps AS ps",
                "a.date AS date",
                "a.statut AS statut",
                "a.id_secteur AS id_secteur",
                "a.maj AS maj",
            ), array(
                DATA_BASE_TABLE_ARTICLES . " AS a",
            ), array(
        "a.id_article=" . $id_article
            ), array(), array(), "", array(), "linfo"
    );
    $details_article = sql_fetch($details_article_ressource);
    $url = "";
    recup_url_article_linfo($id_article, 'article', $url);
    $id_article_art = sql_insertq(DATA_BASE_TABLE_ARTICLES, array(
        'surtitre' => $details_article['surtitre'],
        'titre' => $details_article['titre'],
        'soustitre' => $details_article['soustitre'],
        'id_rubrique' => $id_rub_destination,
        'descriptif' => '<link rel="canonical" href="http://www.linfo.re'.$url.'" />',//$details_article['descriptif'],
        'chapo' => $details_article['chapo'],
        'texte' => $details_article['texte'],
        'ps' => $details_article['ps'],
        'date' => $details_article['date'],
        'statut' => $details_article['statut'],
        'id_secteur' => $id_secteur_destination,
        'maj' => $details_article['maj'],
    ));
    if ($id_article_art) {
        echo " - Apres copie id_article_art = ".$id_article_art;
        sql_insertq(DATA_BASE_TABLE_RUB_MOT_A_COPIER_ID_ARTICLE, array(
            'id_rub_mot' => $id_rub_mot,
            'id_article' => $details_article['id_article'],
        ));
        ajout_document_article($details_article['id_article'], $id_article_art);
    }
     echo " - Fin Copie = ".$id_article."<br />";
}

function ajout_document_article($id_article_linfo, $id_article_art) {
    $doc_article_linfo = sql_select(
            array(
                "d.id_document AS id_document",
                "d.extension AS extension",
                "d.titre AS titre",
                "d.date AS date",
                "d.descriptif AS descriptif",
                "d.fichier AS fichier",
                "d.taille AS taille",
                "d.largeur AS largeur",
                "d.hauteur AS hauteur",
                "d.mode AS mode",
                "d.distant AS distant",
                "d.maj AS maj",
                "d.statut AS statut",
                "d.date_publication AS date_publication",
                "d.media AS media",                
            ), array(
                DATA_BASE_TABLE_DOCUMENT . " AS d",
                DATA_BASE_TABLE_DOCUMENT_LIENS. " AS dl"
            ), array(
                "dl.id_objet=" . $id_article_linfo,
                "dl.objet='article'",
                "dl.id_document=d.id_document"
            ), array(), array(), "", array(), "linfo"
    );
    
    
    while($doc_article = sql_fetch($doc_article_linfo)){
        echo " - copie document_linfo id_doc = ".$doc_article['id_document']; 
        
        $id_document = sql_insertq(DATA_BASE_TABLE_DOCUMENT, array(
            'id_vignette' => 0,
            'extension' => $doc_article['extension'],
            'titre' => $doc_article['titre'],
            'date' => $doc_article['date'],
            'descriptif' => $doc_article['descriptif'],
            'fichier' => strstr($doc_article['fichier'], "http://")?$doc_article['fichier']:"http://cdn.antenne.re/linfo/IMG/".$doc_article['fichier'],
            'taille' => $doc_article['taille'],
            'largeur' => $doc_article['largeur'],
            'hauteur' => $doc_article['hauteur'],
            'mode' => $doc_article['mode'],
            'distant' => "oui",
            'statut' => $doc_article['statut'],
            'brise' => 0,
            'media' => $doc_article['media']
        ));
        if ($id_document) {
            echo " - document art id = ".$id_document;
            sql_insertq(DATA_BASE_TABLE_DOCUMENT_LIENS,array(
                'id_document' => $id_document,
                'id_objet' => $id_article_art,
                'objet' => 'article',
                'vu' => 'non'
            ));
        }
    }
}

function recup_url_article_linfo($id_article_linfo = null, $type ='article', &$url){
    if(is_null($id_article_linfo))
        return "";
    $res_urls = sql_select(
            "*",
            array(DATA_BASE_TABLE_URLS),
            array(
                "id_objet=".$id_article_linfo, 
                "type='".$type."'"
                ),
            array(),
            array("date ASC"),
            "",
            array(),
            "linfo"
            );
    $details_urls = sql_fetch($res_urls);
    if($url == "")
        $url = $details_urls['url'];
    else
        $url = $details_urls['url']."/".$url;
    if($details_urls['segments'] != 0){
        recup_url_article_linfo($details_urls['id_parent'], 'rubrique', $url);
    }
}
