<?php
setlocale(LC_TIME, 'french.UTF-8', 'fr_FR.UTF-8');


include_once 'database.php';
include_once 'matchs.php';

$database = new Database();
$db = $database->getConnection();
$match = new Match($db);

$id_tournoi = isset($_GET['id_tournoi']) ? $_GET['id_tournoi'] : 1;
$img_diffusion = ($id_tournoi == 1) ? 'https://cdn.antenne.re/antenne/squelettes/assets/img_coupe_du_monde/image_diffuse_AR.png' : 'https://cdn.antenne.re/antenne/squelettes/assets/img_coupe_du_monde/logo-mention-diffuse-sur.png';

switch ($_GET['action'])
{
    case "all":
    {
        // tous les matchs
        $resultats_all = $match->findAll($id_tournoi);

        $array_alls = array();
        //usort($resultats_all, 'sortByDate');
        foreach ($resultats_all as $key => $value) {
            $array_alls[$value["date"]][] = $value;
        }
        $inc = true;
        foreach ( $array_alls as $key => $matchs) {
        if(($matchs[0]["phasy"] == 3) && $inc){
            $inc = false;
            $ancre = "id='quart'";
        }else{
          $ancre = "data='none_quart'";
        }
        echo'<ul class="content_bloc_list_match" '.$ancre.'>
            <li class="bloc_liste_match">
            <div class="titre_date">'.mb_convert_encoding(strftime("%A, %e %B", strtotime($key)), 'utf-8').'</div>
            <ul>';
            foreach ($matchs as $match) {
                $diff = ($match["diffuse"] == 1) ? "match sur_AR": "match";
                $prov = ($match["diffuse"] == 2) ? "provisoire match sur_AR": "no_prov";
                $termine = ($match["point1"] >= 0) ? "termine": "non-termine";
                $bool = ($match["drp1"]!="none"||$match["drp2"]!="none") ? "drap":"none";
                $href = ($match["drp1"]!="none"||$match["drp2"]!="none") ? "href='#'":"";
                echo'<li class="'.$diff.' '.$termine.' '.$prov.'">
                    <div class="gpe">'.$match["nom"].'</div>
                    <div class="match_gpe">
                        <div class="gpe_gauche">
                            <a '.$href.' title="'.$match["equipe1"].'" class="'.$bool.'" data-idq="'.$match["idq"].'">
                                <span class="nom_pays">'.$match["equipe1"].'</span>
                                <span class="drapeaux">';?>
                                <?php if($match["drp1"]!="none") {                                
                                    echo'<img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($match["equipe1"]).'.jpg" width="30" height="20" alt="'.$match["equipe1"].'">';
                                  }else{
                                    echo '';
                                  }
                                ?>
                        <?php echo'</span>
                            </a>
                        </div>';
                        if ($match["point1"] >= 0) {
                           echo '<div class="score"><span class="score_gauche">'.$match["point_equipe1"].'</span> - <span class="score_droite">'.$match["point_equipe2"].'</span></div>';
                         } else {
                           echo '<div class="heure"><a>'.substr($match["heure"], 0, 5).'</a></div>';
                         }
                        echo '<div class="gpe_droite">
                            <a '.$href.' title="'.$match["equipe2"].'" class="'.$bool.'" data-idq="'.$match["idq2"].'">
                                <span class="drapeaux">';?>
                                <?php if($match["drp2"]!="none") {                                
                                    echo'<img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($match["equipe2"]).'.jpg" width="30" height="20" alt="'.$match["equipe2"].'">';
                                  }else{
                                    echo '';
                                  }
                                ?>
                        <?php echo'</span>
                                <span class="nom_pays">'.$match["equipe2"].'</span>
                            </a>
                        </div>

                    </div>
                    <div class="diffuse_AR">';
                    ?>
                    <?php if($match["diffuse"] == 1){ 
                       echo '<img src="'.$img_diffusion.'" width="53" height="33">'; 
                    } elseif ($match["diffuse"] == 2) {
                      echo '<img src="https://cdn.antenne.re/antenne/squelettes/assets/img_coupe_du_monde/logo-mention-diffuse-sur-1.png" width="64" height="33">';
                    }else{
                        echo '&nbsp;';
                    }?>
                    <?php
                    echo'</div></li>';
        }
        echo"</ul></li></ul>";
        }
    }    
    break;

    case "group":
    {
      // classement
      $resultats_classement = $match->getClassement($id_tournoi, $_GET['group']);
      // par group
      $resultats_alls = $match->findGroup($id_tournoi, $_GET['group']);

        // tritre par groupe

        // par group
        $resultats = $match->getGroup($id_tournoi, $_GET['group']);

        $array_groups = array();
        foreach ($resultats_alls as $key => $values)
        {
            $array_groups[$values["date"]][] = $values;
        }
        ?>      
        <div class="tabcontent">
           <div class="content_classement">
               <div class="titre_blue">CLASSEMENT
                   <span><?php echo $resultats[0]["nom"];?></span>
               </div>
               <div class="resultat_classement">
                   <table class="classement">
                       <thead>
                           <tr>
                               <th class="team">Équipe</th>
                               <th class="points">Pts</th>
                               <th class="stat">J.</th>
                               <th class="stat">G.</th>
                               <th class="stat">N.</th>
                               <th class="stat last">P.</th>
                               <th class="stat">p.</th>
                               <th class="stat">c.</th>
                               <th class="stat">Diff.</th>
                           </tr>
                       </thead>
                       <tbody>
                       <?php 
                          foreach ($resultats_classement as $key => $class)
                          {echo'<tr>
                               <td class="team">'.$class["nom"].'</td>
                               <td class="points">'.$class["points"].'</td>
                               <td class="stat">'.$class["J"].'</td>
                               <td class="stat">'.$class["G"].'</td>
                               <td class="stat">'.$class["P"].'</td>
                               <td class="stat last">'.$class["N"].'</td>
                               <td class="stat">'.$class["soc"].'</td>
                               <td class="stat">'.$class["adv"].'</td>
                               <td class="stat">'.$class["diff"].'</td>
                               </tr>';
                         }
                        ?>
                       </tbody>
                   </table>
               </div>
           </div>

           <div class="titre_blue">CALENDRIER ET RÉSULTATS DU
               <span><?php echo $resultats[0]["nom"];?></span>
           </div>
        <?php
        foreach ($array_groups as $key => $matchs)
        {
            echo'<ul class="content_bloc_list_match">
                   <li class="bloc_liste_match">
                       <div class="titre_date">'.mb_convert_encoding(strftime("%A, %e %B", strtotime($key)), 'utf-8').'</div>
                       <ul>';
                    foreach ($matchs as $match)
                    {
                      $diff = ($match["diffuse"] == 1) ? "match sur_AR": "match";
                      $prov = ($match["diffuse"] == 2) ? "provisoire match sur_AR": "no_prov";
                        $termine = ($match["point1"] >= 0) ? "termine": "non-termine";
                        echo'<li class="'.$diff.' '.$termine.' '.$prov.'">
                               <div class="gpe"></div>
                               <div class="match_gpe">
                                   <div class="gpe_gauche">
                                       <a href="#" title="'.$match["equipe1"].'" class="drap" data-idq="'.$match["idq"].'">
                                           <span class="nom_pays">'.$match["equipe1"].'</span>
                                           <span class="drapeaux">
                                               <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($match["equipe1"]).'.jpg" width="30" height="20" alt="'.$match["equipe1"].'">
                                           </span>
                                       </a>
                                   </div>';
                        if ($match["point1"] >= 0) {
                           echo '<div class="score"><span class="score_gauche">'.$match["point_equipe1"].'</span> - <span class="score_droite">'.$match["point_equipe2"].'</span></div>';
                         } else {
                           echo '<div class="heure"><a>'.substr($match["heure"], 0, 5).'</a></div>';
                         }
                        echo ' <div class="gpe_droite">
                                       <a href="#" title="'.$match["equipe2"].'" class="drap" data-idq="'.$match["idq2"].'">
                                           <span class="drapeaux">
                                               <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($match["equipe2"]).'.jpg" width="30" height="20"
                                                   alt="'.$match["equipe2"].'">
                                           </span>
                                           <span class="nom_pays">'.$match["equipe2"].'</span>
                                       </a>
                                   </div>

                               </div>
                        <div class="diffuse_AR">';
                        ?>
                        <?php if($match["diffuse"] == 1)
                        { 
                            echo '<img src="'.$img_diffusion.'" width="53" height="33">';
                        }elseif ($match["diffuse"] == 2) {
                            echo '<img src="https://cdn.antenne.re/antenne/squelettes/assets/img_coupe_du_monde/logo-mention-diffuse-sur-1.png" width="64" height="33">';
                        }
                        else{
                            echo '&nbsp;';
                        }
                        echo'</div></li>';
                    }
            echo'</ul></li></ul></div>';  
        }
    }
    break;
    case "pays":
    {
      $array_pay=array( 1 =>"DE L'ALLEMAGNE",2 => "DE L'ANGLETERRE",3 => "DE L'ARABIE SAOUDITE",4 => "DE L'ARGENTINE",5 => "DE L'AUSTRALIE",6 => "DE LA BELGIQUE",7 => "DU BRÉSIL",8 => "DE LA COLOMBIE",9 => "DU CORÉE DU SUD",10 => "DU COSTA RICA",11 => "DE LA CROATIE",12 => "DU DANEMARK",13 => "DE L'EGYPTE",14 => "DE L'ESPAGNE",15 => "DE LA FRANCE",16 => "DE L'IRAN",17 => "DE L'ISLANDE",18 => "DU JAPON",19 => "DU MAROC",20 => "DU MEXIQUE",21 => "DU NIGERIA",22 => "DU PANAMA",23 => "DE LA POLOGNE",24 => "DE LA POLOGNE",25 => "DU PÉROU",26 => "DE LA RUSSIE",27 => "DE LA SERBIE",28 => "DE LA SUISSE",29 => "DE LA SUÈDE",30 => "DU SÉNÉGAL",31 => "DE LA TUNISIE",32 => "DE L'URUGUAY");

      $array_pay_euro=array( 67 =>"DE L'ITALIE",68 => "DE LA SUISSE",69 => "DE LA TURQUIE", 70 => "DU PAYS DE GALLES", 71 => "DE LA BELGIQUE",72 => "DU DANEMARK",73 => "DE LA FINLANDE",74 => "DE LA RUSSIE",75 => "DE L'AUTRICHE",76 => "DU PAYS-BAS",77 => "DE LA MACÉDOINE DU NORD",78 => "DE L'UKRAINE",79 => "DE LA CROATIE",80 => "DE LA RÉPUBLIQUE TCHEQUE",81 => "DE L'ANGLETERRE",82 => "DE L'ÉCOSSE",83 => "DE LA POLOGNE",84 => "DE LA SLOVAQUIE",85 => "DE L'ESPAGNE",86 => "DE LA SUEDE",87 => "DE LA FRANCE",88 => "DE L'ALLEMAGNE",89 => "DE LA HONGRIE",90 => "DU PORTUGAL");

        $resultats_pays = $match->findCountry($id_tournoi, $_GET['id_pays']);

        $array_pays = array();
        foreach ($resultats_pays as $key => $value) {
            $array_pays[$value["date"]][] = $value;
        }

        $titre_pays = $db->query('SELECT * FROM  `equipe` eq WHERE eq.`id_equipe` ='.$_GET['id_pays'].' AND id_tournoi = '. $id_tournoi );
        $titre = $titre_pays->fetchAll(PDO::FETCH_ASSOC);

        $render = '<div id="'.$titre[0]["nom"].'-cmd" class="block_match_equipe">';
        if ($id_tournoi == 2) {
          $render .= '<div class="titre_blue">MATCHS '. $array_pay_euro[$_GET['id_pays']].'</div>';
        } else {
          $render .= '<div class="titre_blue">MATCHS '.$array_pay[$_GET['id_pays']].'</div>';
        }

       foreach ( $array_pays as $key => $matchs) {
        $render .= '<ul class="content_bloc_list_match">
		        <li class="bloc_liste_match">
			    <div class="titre_date">'.mb_convert_encoding(strftime("%A, %e %B", strtotime($key)), 'utf-8').'</div>
				<ul>';
            foreach ($matchs as $match) {
                $diff = ($match["diffuse"] == 1) ? "match sur_AR": "match";
                $prov = ($match["diffuse"] == 2) ? "provisoire match sur_AR": "no_prov";
                $termine = ($match["point1"] >= 0) ? "termine": "non-termine";
             $render .= '<li class="'.$diff.' '.$termine.' '.$prov.'">
  				   <div class="gpe">&nbsp;</div>
  				   <div class="match_gpe">
  					 <div class="gpe_gauche">
  						 <a title="'.$match["equipe1"].'">
  							 <span class="nom_pays">'.$match["equipe1"].'</span>
  							 <span class="drapeaux">
  								 <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($match["equipe1"]).'.jpg" width="30" height="20" alt="'.$match["equipe1"].'">
  							 </span>
  						 </a>
  					 </div>';
             if ($match["point1"] >= 0) {
               $render .= '<div class="score"><span class="score_gauche">'.$match["point_equipe1"].'</span> - <span class="score_droite">'.$match["point_equipe2"].'</span></div>';
             } else {
               $render .='<div class="heure"><a>'.substr($match["heure"], 0, 5).'</a></div>';
             }
  					 $render .= '<div class="gpe_droite"> 
  						 <a title="'.$match["equipe2"].'">
  								 <span class="drapeaux">
  										 <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($match["equipe2"]).'.jpg" width="30" height="20"
  											 alt="'.$match["equipe2"].'">
  									 </span>
  							 <span class="nom_pays">'.$match["equipe2"].'</span> 
  						 </a>
  					 </div>
                    </div>
                    <div class="diffuse_AR">';
                    ?>
                    <?php if($match["diffuse"] == 1){ 
                        $render .= '<img src="'.$img_diffusion.'" width="53" height="33">';
                    }elseif ($match["diffuse"] == 2) {
                      $render .= '<img src="https://cdn.antenne.re/antenne/squelettes/assets/img_coupe_du_monde/logo-mention-diffuse-sur-1.png" width="64" height="33">';
                    }else{
                        $render .= '&nbsp;';
                    }?>
                    <?php
                    $render .= '</div></li>';
        }
        $render .= "</ul></li></ul>";
        }
        $render .= "</div><div id='".$_GET['id_pays']."'></div>";
        echo $render;
    }    
    break;

    case "phase":
    {
      echo'<div class="tabcontent" id="phase_finale">';
        // tous les matchs

        $resultats_all = $match->findRound($id_tournoi);

        // regroupemment par phase 
        $array_alls = array();
        $array_alls_phase = array();
        foreach ($resultats_all as $key => $value) {
            $array_alls[$value["phase"]][] = $value;
        }
        // regroupemment par date
        foreach ($array_alls as $key => $value2) {
           foreach ($value2 as $valeur) {
             $array_alls_phase[$key][$valeur["date"]][] = $valeur;
           }
        }
        $inc = true;
        foreach ($array_alls_phase as $key => $array_alls_boucle)
        {
          if ($id_tournoi == 2) {
            $phase = $array_alls_boucle[array_key_first($array_alls_boucle)][0]['nom'];
            $phase_class = ($array_alls_boucle[array_key_first($array_alls_boucle)][0]['phasy'] > 3) ? 'titre_orange' : 'titre_blue';
            echo '<div class="'.$phase_class.'">'.$phase.'</div>';
          }
          echo'<div class="content_phase_finale">';
          foreach ( $array_alls_boucle as $keys => $matchs) {
          if(($matchs[0]["phasy"] == 3) && $inc){
            $inc = false;
            $ancre = "id='quart_to'";
          }else{
           $ancre = "data='none_quart'";
          }
          echo'<ul class="content_bloc_list_match" '.$ancre.'>
            <li class="bloc_liste_match">
            <div class="titre_date">'.mb_convert_encoding(strftime("%A, %e %B", strtotime($keys)), 'utf-8').'</div>
            <ul>';
            foreach ($matchs as $match) {
                $diff = ($match["diffuse"] == 1) ? "match sur_AR": "match";
                $prov = ($match["diffuse"] == 2) ? "provisoire match sur_AR": "no_prov";
                $termine = ($match["point1"] >= 0) ? "termine": "non-termine";
                echo'<li class="'.$diff.' '.$termine.' '.$prov.'">
                    <div class="gpe">'.$match["nom"].'</div>
                    <div class="match_gpe">
                        <div class="gpe_gauche">
                            <a title="'.$match["equipe1"].'">
                                <span class="nom_pays">'.$match["equipe1"].'</span>
                                <span class="drapeaux">';?>
                                <?php if($match["drp1"]!="none") {                                
                                    echo'<img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($match["equipe1"]).'.jpg" width="30" height="20" alt="'.$match["equipe1"].'">';
                                  }else{
                                    echo '';
                                  }
                                ?>
                        <?php echo'</span>
                            </a>
                        </div>';
                        if ($match["point1"] >= 0) {
                           echo '<div class="score"><span class="score_gauche">'.$match["point_equipe1"].'</span>-<span class="score_droite">'.$match["point_equipe2"].'</span></div>';
                         } else {
                           echo '<div class="heure"><a>'.substr($match["heure"], 0, 5).'</a></div>';
                         }
                        
                        echo '<div class="gpe_droite">
                            <a title="'.$match["equipe2"].'">
                                <span class="drapeaux">';?>
                                <?php if($match["drp2"]!="none") {                                
                                    echo'<img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($match["equipe2"]).'.jpg" width="30" height="20" alt="'.$match["equipe2"].'">';
                                  }else{
                                    echo '';
                                  }
                                ?>
                        <?php echo'</span>
                                <span class="nom_pays">'.$match["equipe2"].'</span>
                            </a>
                        </div>

                    </div>
                    <div class="diffuse_AR">';
                    ?>
                    <?php if($match["diffuse"] == 1){ 
                        echo '<img src="'.$img_diffusion.'" width="53" height="33">';
                    }elseif ($match["diffuse"] == 2) {
                      echo '<img src="https://cdn.antenne.re/antenne/squelettes/assets/img_coupe_du_monde/logo-mention-diffuse-sur-1.png" width="64" height="33">';
                    }else{
                        echo '&nbsp;';
                    }?>
                    <?php
                    echo'</div></li>';
        }
        echo"</ul></li></ul>";
        }
        echo"</div>";
      }
        echo"</div>";
    }    
    break;
    case "8eme":
    {

      echo'<ul class="content_huit">';
        // tous les matchs
        $resultats_all = $match->findRound($id_tournoi, 2);
        foreach ($resultats_all as $key => $boucle)
        {          
          $diff = ($boucle["diffuse"] == 1) ? "sur_AR": "match_2";
          echo'<li class="col_8 '.$diff.'">
              <div class="titre_date">
                  '.ucfirst(mb_convert_encoding(strftime("%a  %d/%m", strtotime($boucle['date'])), 'utf-8')).'
              </div>
              <div class="content_col_final">
                  <div class="heure_col">';?>

                  <?php if($boucle["point1"] >= 0)
                    { 
                       echo '&nbsp;';
                    }else{
                      $boucle["point_equipe1"] = '';
                      $boucle["point_equipe2"] = '';
                      echo ''.substr($boucle["heure"], 0, 5);
                       
                    } 

                   if($boucle["diffuse"] == 1){ 
                        echo '<img src="'.$img_diffusion.'" width="53" height="33">';
                    }else{
                        echo '';
                    } 
                  echo'</div>
                  <div class="pays_col">
                      <span class="nom_pays">'.$boucle["equipe1"].'</span>
                      <span class="drapeaux">
                          <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($boucle["equipe1"]).'.jpg" width="30" height="20" alt="'.$boucle["equipe1"].'">
                      </span>
                  </div>
                  <div class="trait">'.$boucle["point_equipe1"].' - '.$boucle["point_equipe2"].'</div>
                  <div class="pays_col">
                      <span class="nom_pays">'.$boucle["equipe2"].'</span>
                      <span class="drapeaux">
                          <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($boucle["equipe2"]).'.jpg" width="30" height="20" alt="'.$boucle["equipe2"].'">
                      </span>
                  </div>
              </div>
          </li>';
      }
        echo'</ul>';
    }    
    break;
    case "quart":
    {

      echo'<ul class="content_quatre">';
        // tous les matchs

        $resultats_all = $match->findRound($id_tournoi,3);
        foreach ($resultats_all as $key => $boucle)
        {
          $diff = ($boucle["diffuse"] == 1) ? "sur_AR": "match_2";
        echo'<li class="col_4 '.$diff.'">
              <div class="titre_date">
                  '.ucfirst(mb_convert_encoding(strftime("%a  %d/%m", strtotime($boucle['date'])), 'utf-8')).'
              </div>
              <div class="content_col_final">
                  <div class="heure_col">';?>

                  <?php if($boucle["point1"] >= 0)
                    { 
                       echo '&nbsp;';
                    }else{
                      $boucle["point_equipe1"] = '';
                      $boucle["point_equipe2"] = '';
                      echo ''.substr($boucle["heure"], 0, 5);
                       
                    } 

                   if($boucle["diffuse"] == 1){ 
                        echo '<img src="'.$img_diffusion.'" width="53" height="33">';
                    }else{
                        echo '';
                    } 
                  echo'</div>
                  <div class="pays_col">
                      <span class="nom_pays">'.$boucle["equipe1"].'</span>
                      <span class="drapeaux">
                          <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($boucle["equipe1"]).'.jpg" width="30" height="20" alt="'.$boucle["equipe1"].'">
                      </span>
                  </div>
                  <div class="trait">'.$boucle["point_equipe1"].' - '.$boucle["point_equipe2"].'</div>
                  <div class="pays_col">
                      <span class="nom_pays">'.$boucle["equipe2"].'</span>
                      <span class="drapeaux">
                          <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($boucle["equipe2"]).'.jpg" width="30" height="20" alt="'.$boucle["equipe2"].'">
                      </span>
                  </div>
              </div>
          </li>';
      }
        echo'</ul>';
    }    
    break;
    case "demi":
    {

      echo'<ul class="content_deux">';
        // tous les matchs

        $resultats_all = $match->findRound($id_tournoi, 4);
        foreach ($resultats_all as $key => $boucle)
        {
          $diff = ($boucle["diffuse"] == 1) ? "sur_AR": "match_2";
          echo'<li class="col_2 '.$diff.'">
              <div class="titre_date">
                  '.ucfirst(mb_convert_encoding(strftime("%a  %d/%m", strtotime($boucle['date'])), 'utf-8')).'
              </div>
              <div class="content_col_final">
                  <div class="heure_col">';?>

                  <?php if($boucle["point1"] >= 0)
                    { 
                       echo '&nbsp;';
                    }else{
                      $boucle["point_equipe1"] = '';
                      $boucle["point_equipe2"] = '';
                      echo ''.substr($boucle["heure"], 0, 5);
                       
                    } 

                   if($boucle["diffuse"] == 1){ 
                        echo '<img src="'.$img_diffusion.'" width="53" height="33">';
                    }else{
                        echo '';
                    } 
                  echo'</div>
                  <div class="pays_col">
                      <span class="nom_pays">'.$boucle["equipe1"].'</span>
                      <span class="drapeaux">
                          <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($boucle["equipe1"]).'.jpg" width="30" height="20" alt="'.$boucle["equipe1"].'">
                      </span>
                  </div>
                  <div class="trait">'.$boucle["point_equipe1"].' - '.$boucle["point_equipe2"].'</div>
                  <div class="pays_col">
                      <span class="nom_pays">'.$boucle["equipe2"].'</span>
                      <span class="drapeaux">
                          <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($boucle["equipe2"]).'.jpg" width="30" height="20" alt="'.$boucle["equipe2"].'">
                      </span>
                  </div>
              </div>
          </li>';
      }
        echo'</ul>';
    }    
    break;   
    case "final":
    {
        // 3eme place
        $troiximes = $match->findRound($id_tournoi,6);
        $diff2 = ($troiximes[0]["diffuse"] == 1) ? "sur_AR": "match_2";  

        // Finale
        $finals = $match->findRound($id_tournoi,5);
        $diff1 = ($finals[0]["diffuse"] == 1) ? "sur_AR": "match_2";

        echo'<ul class="content_trois">';
        echo'<li class="col_3 '.$diff1.'">
            <div class="titre_match_finale">FINALE</div>
                <div class="titre_date">
                  '.ucfirst(mb_convert_encoding(strftime("%a  %d/%m", strtotime($finals[0]['date'])), 'utf-8')).'
                </div>
                <div class="content_col_final">
                    <div class="heure_col">';?>
                    <?php if($finals[0]["point1"] >= 0)
                    { 
                       echo '&nbsp;';
                    }else{
                      $finals[0]["point_equipe1"] = '';
                      $finals[0]["point_equipe2"] = '';
                      echo ''.substr($finals[0]["heure"], 0, 5);
                       
                    } 

                   if($finals[0]["diffuse"] == 1){ 
                        echo '<img src="'.$img_diffusion.'" width="53" height="33">';
                    }else{
                        echo '';
                    }                               
                    echo'</div>
                    <div class="pays_col">
                        <span class="nom_pays">'.$finals[0]["equipe1"].'</span>
                        <span class="drapeaux">
                           <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($finals[0]["equipe1"]).'.jpg" width="30" height="20" alt="'.$finals[0]["equipe1"].'">
                        </span>
                    </div>
                    <div class="trait">'.$finals[0]["point_equipe1"].' - '.$finals[0]["point_equipe2"].'</div>
                    <div class="pays_col">
                        <span class="nom_pays">'.$finals[0]["equipe2"].'</span>
                        <span class="drapeaux">
                           <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($finals[0]["equipe2"]).'.jpg" width="30" height="20" alt="'.$finals[0]["equipe2"].'">
                        </span>
                </div>
            </div>
        </li>
        <li class="col_3 center">
          <img src="https://cdn.antenne.re/antenne/squelettes/assets/img_coupe_du_monde/logo_coupe_du_monde_grand.png" width="53" height="109">
        </li>'; 
        echo'<li class="col_3 '.$diff2.'">
            <div class="titre_match_finale">MATCH 3e PLACE</div>
              <div class="titre_date">
                    '.ucfirst(mb_convert_encoding(strftime("%a  %d/%m", strtotime($troiximes[0]['date'])), 'utf-8')).'
              </div>
              <div class="content_col_final">
                  <div class="heure_col">';?>
                  <?php if($troiximes[0]["point1"] >= 0)
                  { 
                       echo '&nbsp;';
                  }else{
                      $troiximes[0]["point_equipe1"] = '';
                      $troiximes[0]["point_equipe2"] = '';
                      echo ''.substr($troiximes[0]["heure"], 0, 5);
                       
                  } 
                  if($troiximes[0]["diffuse"] == 1){ 
                      echo '<img src="'.$img_diffusion.'" width="53" height="33">';
                  }else{
                      echo '';
                  }                               
                  echo'</div>
                  <div class="pays_col">
                      <span class="nom_pays">'.$troiximes[0]["equipe1"].'</span>
                      <span class="drapeaux">
                           <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($troiximes[0]["equipe1"]).'.jpg" width="30" height="20" alt="'.$troiximes[0]["equipe1"].'">
                      </span>
                  </div>
                  <div class="trait">'.$troiximes[0]["point_equipe1"].' - '.$troiximes[0]["point_equipe2"].'</div>
                  <div class="pays_col">
                      <span class="nom_pays">'.$troiximes[0]["equipe2"].'</span>
                      <span class="drapeaux">
                        <img src="https://www.antennereunion.fr/squelettes/assets/img_coupe_du_monde/flags/'.strtolower($troiximes[0]["equipe2"]).'.jpg" width="30" height="20" alt="'.$troiximes[0]["equipe2"].'">
                      </span>
                  </div>
              </div>
          </li>';  
        echo'</ul>';
    }    
    break;
}
die();

/*if( ! function_exists('array_key_first'))
{*/
   function array_key_first(array $arr)
   {
      foreach($arr as $key => $unused) {
        return $key;
      }
      return NULL;
   }
/*}*/

?>