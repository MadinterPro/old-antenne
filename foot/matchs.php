<?php 
/**
 * Récupération liste matchs
 */
class Match
{
	private $db;
	
	public function __construct($db)
	{
        date_default_timezone_set('Indian/Reunion');
		$this->db = $db;
	}

	public function findAll($id_tournoi)
    {
        $Query = " SELECT DISTINCT m.id_match, m.diffuse, eq1.id_equipe as idq, eq2.id_equipe as idq2 ,m.date, 
            m.heure, eq1.nom AS equipe1, eq1.drapeau as drp1 ,r1.score AS point_equipe1, r1.point AS point1,
            eq2.nom AS equipe2,eq2.drapeau as drp2 ,r2.score AS point_equipe2, 
            IF(m.id_phase > 1,(CONCAT(' ',ph.nom)),(CONCAT(' ',gp.nom))) as nom,m.id_phase as phasy 
            FROM matchs AS m 
            INNER JOIN resultat AS r2 ON m.id_match = r2.id_match
            INNER JOIN resultat AS r1 ON m.id_match = r1.id_match 
            INNER JOIN equipe AS eq1 ON eq1.id_equipe = r1.id_equipe
            INNER JOIN equipe AS eq2 ON eq2.id_equipe = r2.id_equipe AND eq1.id_equipe != eq2.id_equipe
            INNER JOIN groupe AS gp ON gp.id = eq1.id_groupe
            INNER JOIN phase AS ph ON ph.id_phase = m.id_phase
            WHERE m.id_tournoi = $id_tournoi
            GROUP BY m.id_match
            ORDER BY m.date, m.heure ASC";
        try {
            $res = $this->db->query($Query)->fetchAll();
            if ($res) {
                return $res;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }   
    }

    public function getClassement($id_tournoi, $id_groupe){
        $Query = " SELECT eq.id_equipe AS idp,
            ( SELECT SUM( r.score ) FROM  `resultat` r 
                INNER JOIN matchs AS m ON m.id_match = r.id_match AND m.id_phase =1
                INNER JOIN phase AS phs ON phs.id_phase = m.id_phase
                INNER JOIN equipe eq ON eq.id_equipe = r.id_equipe
                INNER JOIN groupe gps ON gps.id = eq.id_groupe
                INNER JOIN (SELECT * FROM resultat r2) AS tab ON tab.id_match = r.id_match WHERE tab.id_equipe <> r.id_equipe AND tab.id_equipe =idp
                GROUP BY tab.id_equipe
            ) as adv,
            ( SELECT COUNT( r.id_match ) FROM  `resultat` r 
                INNER JOIN matchs AS m ON m.id_match = r.id_match AND m.id_phase =1
                INNER JOIN phase AS phe ON phe.id_phase = m.id_phase 
                INNER JOIN equipe eq ON eq.id_equipe = r.id_equipe
                INNER JOIN groupe gp ON gp.id = eq.id_groupe WHERE eq.id_equipe = idp AND r.point != -1
            ) AS J,
            SUM( r.score ) AS soc, SUM( IF( r.point >0, r.point, 0 ) ) AS points, eq.nom,
            ( SELECT COUNT( r.point ) FROM  `resultat` r 
                INNER JOIN matchs AS m ON m.id_match = r.id_match AND m.id_phase =1
                INNER JOIN equipe eq ON eq.id_equipe = r.id_equipe INNER JOIN groupe gp ON gp.id = eq.id_groupe WHERE eq.id_equipe = idp AND r.point =1
            ) AS P,
            ( SELECT COUNT( r.point ) FROM  `resultat` r 
                INNER JOIN matchs AS m ON m.id_match = r.id_match AND m.id_phase =1
                INNER JOIN equipe eq ON eq.id_equipe = r.id_equipe INNER JOIN groupe gp ON gp.id = eq.id_groupe  
                WHERE eq.id_equipe = idp AND r.point =0 
            ) AS N,
            (SELECT COUNT( r.point ) FROM  `resultat` r 
                INNER JOIN matchs AS m ON m.id_match = r.id_match AND m.id_phase =1
                INNER JOIN equipe eq ON eq.id_equipe = r.id_equipe INNER JOIN groupe gp ON gp.id = eq.id_groupe 
                WHERE eq.id_equipe = idp
                AND r.point =3 
            ) AS G,
            ( SELECT (SUM( r.score ) - (SELECT SUM( r.score ) FROM  `resultat` r 
                INNER JOIN matchs AS m ON m.id_match = r.id_match AND m.id_phase =1
                INNER JOIN (SELECT * FROM resultat r2) AS tab ON tab.id_match = r.id_match 
                WHERE tab.id_equipe <> r.id_equipe AND tab.id_equipe =idp GROUP BY tab.id_equipe))
            ) as diff
            FROM  `resultat` r INNER JOIN equipe eq ON eq.id_equipe = r.id_equipe 
            INNER JOIN matchs AS m ON m.id_match = r.id_match AND m.id_phase =1
            INNER JOIN phase AS ph ON ph.id_phase = m.id_phase
            INNER JOIN groupe gp ON gp.id = eq.id_groupe
            WHERE gp.id = $id_groupe AND m.id_phase =1 AND gp.id_tournoi = $id_tournoi
            GROUP BY eq.id_equipe HAVING  points >= 0 
            ORDER BY points DESC , diff DESC ,SUM( r.score ) DESC ,nom ASC 
        ";
        try {
            $res = $this->db->query($Query)->fetchAll();
            if ($res) {
                return $res;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findGroup($id_tournoi, $id_groupe){
        $Query = " SELECT DISTINCT m.id_match, m.diffuse, m.date, m.heure, eq1.nom AS equipe1,eq1.id_equipe as idq, eq2.id_equipe as idq2 ,r1.score AS point_equipe1, eq2.nom AS equipe2, r2.score AS point_equipe2, r1.point AS point1 ,gp.nom
            FROM matchs AS m
            INNER JOIN resultat AS r2 ON m.id_match = r2.id_match
            INNER JOIN resultat AS r1 ON m.id_match = r1.id_match
            INNER JOIN equipe AS eq1 ON eq1.id_equipe = r1.id_equipe
            INNER JOIN equipe AS eq2 ON eq2.id_equipe = r2.id_equipe AND eq1.id_equipe != eq2.id_equipe
            INNER JOIN groupe AS gp ON gp.id = eq1.id_groupe
            INNER JOIN phase AS ph ON ph.id_phase = m.id_phase
            WHERE gp.id = $id_groupe AND ph.id_phase = 1 AND gp.id_tournoi = $id_tournoi
            GROUP BY m.id_match
            ORDER BY m.date,m.heure 
        ";
        try {
            $res = $this->db->query($Query)->fetchAll();
            if ($res) {
                return $res;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findCountry($id_tournoi, $id_country){
        $Query = " SELECT DISTINCT m.id_match, m.diffuse, m.date, m.heure, eq1.nom AS equipe1, 
            r1.score AS point_equipe1, r1.point AS point1 ,eq2.nom AS equipe2, r2.score AS point_equipe2 
            FROM matchs AS m 
            INNER JOIN resultat AS r2 ON m.id_match = r2.id_match 
            INNER JOIN resultat AS r1 ON m.id_match = r1.id_match 
            INNER JOIN equipe AS eq1 ON eq1.id_equipe = r1.id_equipe 
            INNER JOIN equipe AS eq2 ON eq2.id_equipe = r2.id_equipe AND eq1.id_equipe != eq2.id_equipe 
            WHERE eq1.id_equipe = $id_country OR eq2.id_equipe = $id_country AND m.id_tournoi = $id_tournoi
            GROUP BY m.id_match ORDER BY m.date, m.heure ASC";
        try {
            $res = $this->db->query($Query)->fetchAll();
            if ($res) {
                return $res;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findRound($id_tournoi, $id_round = false){
        $Query = " SELECT DISTINCT m.id_match, m.diffuse, m.date, m.heure,eq1.id_equipe as idq, 
            eq2.id_equipe as idq2 ,eq1.nom AS equipe1,eq1.drapeau as drp1 ,r1.score AS point_equipe1,
            r1.point AS point1,ph.id_phase as phase ,eq2.nom AS equipe2,eq2.drapeau as drp2 ,
            r2.score AS point_equipe2, ph.nom,ph.id_phase as phasy 
            FROM matchs AS m 
            INNER JOIN resultat AS r2 ON m.id_match = r2.id_match 
            INNER JOIN resultat AS r1 ON m.id_match = r1.id_match 
            INNER JOIN equipe AS eq1 ON eq1.id_equipe = r1.id_equipe 
            INNER JOIN equipe AS eq2 ON eq2.id_equipe = r2.id_equipe AND eq1.id_equipe != eq2.id_equipe 
            INNER JOIN phase AS ph ON ph.id_phase = m.id_phase
            WHERE m.id_tournoi = $id_tournoi ";
        $Query .= ($id_round) ? " AND ph.id_phase = $id_round" : " AND ph.id_phase != 1 ";
        $Query .= "GROUP BY m.id_match ORDER BY m.date, m.heure ";
        try {
            $res = $this->db->query($Query)->fetchAll();
            if ($res) {
                return $res;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function getGroup($id_tournoi, $id_groupe){
        $Query = " SELECT * FROM  groupe 
            WHERE groupe.id = $id_groupe AND groupe.id_tournoi = $id_tournoi ";
        try {
            $res = $this->db->query($Query)->fetchAll();
            if ($res) {
                return $res;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}
?>