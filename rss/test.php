<?php

header("Content-Type: text/xml;");
header("Cache-Control: max-age=0, private");
$url_ = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
date_default_timezone_set('Indian/Reunion');
$datejour = date("2017-08-01");
$pubDate = date("D, d M Y H:i:s O");

$rss = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$rss .="<rss version=\"2.0\">\n";
$rss .="\t<channel>\n";
$rss .= "\t\t<title><![CDATA[ RSS AR.fr]]></title>";
$rss .= "\t\t<link>http://www.antennereunion.fr</link>";
$rss .= "\t\t<description><![CDATA[Flux RSS articles AR.fr]]></description>";
$rss .= "\t\t<language>fr</language>";
$rss .= "\t\t<pubDate>" . $pubDate . "</pubDate>";

function wd_remove_accents($str, $charset = 'utf-8') {
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    return $str;
}

function msword_text_to_ascii($str) {
    $search = array("\x82", "\x84", "\x85", "\x91", "\x92", "\x93", "\x94", "\x95", "\x96", "\x97", "\xBB", "\xAB", "\x91", "\x92", "\x80", "\xE9");
    $replace = array("\"", "\"", "...", "'", "'", "\"", "\"", "*", "-", "--", "\"", "\"", "'", "'", "€", "e");
    $str = str_replace($search, $replace, $str);
    return $str;
}

function correction($str) {
    $search = array("&amp;nbsp;","&amp;apos;");
    $replace = array("&nbsp;","&apos;");
    $str = str_replace($search, $replace, $str);
    return $str;
}

function decode($str) {
    return mb_convert_encoding($str,'HTML-ENTITIES','UTF-8');
}

$unwanted_array = array('Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
    'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U',
    'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c',
    'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
    'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y');

$baseAR = 'antenne_art_spip';
$host = 'localhost';
$user = 'antenne_spipAR';
$pass = 'antv3_2009';

try {
    $bdd = new PDO("mysql:host=$host;dbname=$baseAR", $user, $pass);
} catch (Exception $e) {
    exit('Impossible de se connecter &agrave; la base de donn&eacute;es.');
}
$sqlpple = "SELECT articles.id_rubrique, articles.id_secteur, articles.id_article, articles.date, articles.chapo, articles.date_modif, articles.titre, articles.lang
            FROM spip_articles AS `articles`
            WHERE (articles.statut = 'publie')
            AND  articles.date < NOW()
            AND  (articles.id_secteur IN (732,742,14,4721,4679))
            AND  (articles.id_rubrique NOT IN (5640,5090,5088))
            AND  (articles.id_article NOT IN (726374))
            AND  (LEAST((UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(articles.date))/86400,
                  TO_DAYS(NOW())-TO_DAYS(articles.date),
                  DAYOFMONTH(NOW())-DAYOFMONTH(articles.date)+30.4368*(MONTH(NOW())-MONTH(articles.date))+365.2422*(YEAR(NOW())-YEAR(articles.date))) < 365)
            ORDER BY articles.date DESC";
$reqpple = $bdd->query($sqlpple)or die(print_r($bdd->errorInfo()));
while ($rowpple = $reqpple->fetch()) {
    $date_article = date_create($rowpple['date']);
    $date_article = date_format($date_article, 'Y-m-d');
    if ($date_article == $datejour) {
        $sqlpar = "SELECT id_parent FROM  `spip_rubriques` WHERE  `id_rubrique` =" . $rowpple['id_rubrique']."";
        $reqpar = $bdd->query($sqlpar)or die(print_r($bdd->errorInfo()));
        $rowpar = $reqpar->fetch();
        $sqlhierarchie = "SELECT rubriques.id_rubrique, rubriques.titre, rubriques.lang FROM spip_rubriques AS `rubriques` "
                . "WHERE (rubriques.id_rubrique IN (0," . $rowpple['id_secteur'] . "," . $rowpar['id_parent'] . "," . $rowpple['id_rubrique'] . ")) "
                . "ORDER BY FIELD(rubriques.id_rubrique, 0," . $rowpple['id_secteur'] . "," . $rowpar['id_parent'] . "," . $rowpple['id_rubrique'] . ")";
        $reqhierarchie = $bdd->query($sqlhierarchie)or die(print_r($bdd->errorInfo()));
        $i = 0;
        $hierarchie = array();
        while ($rowhierarchie = $reqhierarchie->fetch()) {
            $hierarchie[$i] = $rowhierarchie["titre"];
            $i++;
        }
        $urlsecteur = $hierarchie[0];
        $urlsecteur = utf8_encode(msword_text_to_ascii($urlsecteur));
        $urlparent = $hierarchie[1];
        $urlparent = utf8_encode(msword_text_to_ascii($urlparent));
        $urlrubrique = $hierarchie[2];
        $urlrubrique = utf8_encode(msword_text_to_ascii($urlrubrique));
        
        $url_init = "http://www.antennereunion.fr/" . $urlsecteur . "/" . $urlparent . "/" . $urlrubrique."/";
        $url_init = strtolower($url_init);
        $url_init = str_replace(" ", "-", $url_init);
        $url_init = str_replace("-/", "/", $url_init);
        $url_init = str_replace("'", "-", $url_init);

        if ($urlrubrique = 'Videos' || $urlrubrique = 'News' || $urlrubrique = 'Replay' || $urlrubrique = 'Photos' || $urlrubrique = 'Episodes' || $urlrubrique = 'Candidates 2016' || $urlrubrique = 'Candidates 2017') {
            $requrlarticle = $bdd->query("SELECT * FROM `spip_articles` a
                                  JOIN `spip_mots_liens` m ON ( a.`id_article` = m.`id_objet` && m.`objet` = 'article')
                                  WHERE a.`id_article`= '" . $rowpple['id_article'] . "' ")or die(print_r($bdd->errorInfo()));
            if ($resurlarticle = $requrlarticle->fetch()) {
                $requrl = $bdd->query('SELECT url FROM  `spip_urls` WHERE  `id_objet` = "' . $resurlarticle['id_article'] . '"')or die(print_r($bdd->errorInfo()));
                $resurl = $requrl->fetch();
                $url_articles = $resurl["url"];
            } else {
                $requrl = $bdd->query('SELECT url FROM  `spip_urls` WHERE  `id_objet` = "' . $rowpple['id_article'] . '"')or die(print_r($bdd->errorInfo()));
                $resurl = $requrl->fetch();
                $url_articles = $resurl["url"];
            }
            $urlarticle = $url_init.$url_articles;
            //documents
            $reqimages = $bdd->query("SELECT documents.date, documents.fichier
                                    FROM spip_documents AS `documents`
                                    INNER JOIN spip_documents_liens AS L1 ON ( L1.id_document = documents.id_document )
                                    WHERE (documents.statut = 'publie')
                                        AND (documents.mode IN ('image','document'))
                                        AND (documents.taille > 0 OR documents.distant='oui')
                                        AND (L1.id_objet = '" . $rowpple['id_article'] . "')
                                        AND (L1.objet = 'article')
                                        AND (documents.extension REGEXP 'jpeg|jpg|png')
                                    ORDER BY documents.date DESC
                                    LIMIT 0,1")or die(print_r($bdd->errorInfo()));
            $resimages = $reqimages->fetch();

            //gestion des dates
            $date = date_create($rowpple['date']);
            $date = date_format($date, 'D, d M Y H:i:s O');

            //gestion des urls
            $urlarticle = str_replace('km²', 'km-carre', $urlarticle);
            $urlarticle = str_replace('m²', 'm-carre', $urlarticle);
            $urlarticle = str_replace('½', 'demi', $urlarticle);
            //gestion titre
            $titre = utf8_encode(msword_text_to_ascii($rowpple['titre']));
            $titre = str_replace('', 'eo', $titre);
            //texte 
            $texte = utf8_encode(msword_text_to_ascii(strip_tags($rowpple['chapo'])));
            $texte = str_replace("'" , "&apos;", $texte);
            $texte = str_replace('', 'eo', $texte);

            if(strpos($resimages['fichier'], 'http') === 0)
                $pre = '';
            else
                $pre = 'http://cdn.antenne.re/antenne/IMG/';  
            
            $rss.="\t\t<item>\n";
            $rss.="\t\t\t<title><![CDATA[" . htmlentities($titre) . "]]></title>\n";
            $rss.="\t\t\t<link>" . $urlarticle . "</link>\n";
            $rss.="\t\t\t<description>\n"
                    . "<![CDATA[<a href='" . $urlarticle . "'><img src='". $pre .$resimages['fichier'] . "'></a>]]>\n";
            if ($texte == ""){
                $rss .= "<![CDATA[" . htmlentities($titre) . "]]>\n";
            }else{
                $rss .= "<![CDATA[" . correction(htmlentities($texte)) . "]]>\n";
            }                    
            $rss.="</description>\n";
            $rss.="\t\t\t<category>" . $urlsecteur . "</category>\n";
            $rss.="\t\t\t<pubDate>" . $date . "</pubDate>\n";
            $rss.="\t\t\t<guid>" . $urlarticle . "</guid>\n";
            $rss.="\t\t</item>\n";
        }
    }
}
$rss .="\t\t</channel>\n";
$rss .="</rss>";
echo $rss;
?>