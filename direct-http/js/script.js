

function getDernierReplay() {
    let html = '';
    $.ajax({
        url: 'http://direct.antennereunion.fr/traitement.php',
        data: {type : 'derniers_replay'},
        headers: {
            'Cache-Control': 'max-age=0'
        },
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            for(let item of data){
                for(let item of data){
                    html += '<li><div class="emissionInfo clearfix">';
                    html += '<div class="heur-replay">';
                    /*html += '<p class="heureReplay">'++'</p>';
                    html += '<p class="dateReplay">'++'</p>';*/
                    html += '<p class="ligne-heurreplay"><img src="http://www.antennereunion.fr/squelettes/assets/img/ligne-heur-replay.png" alt="" style="width: 7px; height: 385px;" width="7"></p>';
                    html += '</div>';
                    html += '<div class="videoReplay">';
                    html += '<div class="titre-replay"><a href="'+item.url_article+'">'+item.titre+'</a></div>';
                    html += '<h3 class="titre-emission-replay"><a href="'+item.url_parent+'">'+item.parent+'</a></h3>';
                    html += '<div class="video-replay">';
                    html += '<a href="'+item.url_article+'"><img src="'+item.fichier+'" width="500" height="280"><span class="icon-video post-format"></span></a>';
                    html += '</div>';
                    html += '<p class="resumeReplay">'+item.chapo+'<p>';
                    html += '<div class="social-replay"><table cellpadding="0" cellspacing="0" ><tr>';
                    html += '<td width="80px"><div class="fb-like" data-href="'+item.url_article+'" data-send="false" data-layout="button_count" data-show-faces="true" style="margin-left:5px; float: left"></div></td>';
                    html += '<td width="100px"><div id="cont_desc_twit" style="margin-left:5px"><a href="https://twitter.com/share" class="twitter-share-button" data-url="'+item.url_article+'" data-lang="fr"  data-hashtags="linfo" > Tweet</a></div></td>';
                    html += '</tr></table></div>';
                    html += '</div>';
                    html += '</div></li>';
                }
            }
        }
    });
    return html;
}

function getTopReplay(){
    let html = '';
    let i = 0;
    $.ajax({
        url: 'http://direct.antennereunion.fr/traitement.php',
        data: {type : 'top_replay'},
        headers: {
            'Cache-Control': 'max-age=0'
        },
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            for(let item of data){
                i++;
                html += '<li><div class="figure-player format-video">';
                html += '<a href="'+item.url_article+'" class="fond_noir">';
                html += '<span style="opacity: 0;" class="roll ">&nbsp;</span>';
                html += '<img src="'+item.fichier+'" alt="'+item.titre+'">';
                html += '<span class="meta-title">REPLAY</span>';
                html += '<span class="icon-video post-format"></span>';
                html += '</a></div>';
                html += '<div class="title-medias">';
                html += '<span class="num-count">'+i+'</span>';
                html += '<div class="item-title"><a href="'+item.url_article+'">'+item.titre+'</a></div>';
                html += '</div>';
                html += '</li>';
            }
        }
    });
    return html;
}

function getTopMagazine(){
    let html = '';
    let i = 0;
    $.ajax({
        url: 'http://direct.antennereunion.fr/traitement.php',
        data: {type : 'top_replay'},
        headers: {
            'Cache-Control': 'max-age=0'
        },
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            for(let item of data){
                i++;
                html += '<li><div class="figure-player" style="width:100%;max-height: 174px;">';
                html += '<a href="'+item.url_article+'" class="fond_noir">';
                html += '<span style="opacity: 0;" class="roll ">&nbsp;</span>';
                html += '<img src="'+item.fichier+'" alt="'+item.titre+'">';
                html += '</a></div>';
                html += '<div class="title-medias">';
                html += '<span class="num-count">'+i+'</span>';
                html += '<div class="item-title"><a href="'+item.url_article+'">'+item.titre+'</a></div>';
                html += '</div>';
                html += '</li>';
            }
        }
    });
    return html;
}