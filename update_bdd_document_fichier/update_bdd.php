<?php
//Local - laatr
//define("DATA_BASE_HOST", "localhost");
//define("DATA_BASE_NAME", "antennetv_v32009");
//define("DATA_BASE_LOGIN", "root");
//define("DATA_BASE_PASSWORD", "");

//En ligne ART 
//define("DATA_BASE_HOST", "localhost");
//define("DATA_BASE_NAME", "art_spip");
//define("DATA_BASE_LOGIN", "spipAR");
//define("DATA_BASE_PASSWORD", "antv3_2009");

//En ligne Linfo 
define("DATA_BASE_HOST", "localhost");
define("DATA_BASE_NAME", "linfo_art_linfo");
define("DATA_BASE_LOGIN", "linfo_spip");
define("DATA_BASE_PASSWORD", "2010@ud!");

define("DATA_BASE_TABLE_DOCUMENT", "spip_documents");
$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);
try {
    //connexion PDO
    $db = new PDO('mysql:host=' . DATA_BASE_HOST . ';dbname=' . DATA_BASE_NAME, DATA_BASE_LOGIN, DATA_BASE_PASSWORD, $options);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $sql = "SELECT id_document, fichier FROM " . DATA_BASE_TABLE_DOCUMENT . " WHERE fichier LIKE '%mobile.antennereunion.fr%'";
    $mobiles = $db->query($sql)->fetchAll();
    foreach($mobiles as $key => $mobile){
        $fichier = array_reverse(preg_split('[/]', $mobile['fichier']));
        $nouveau_fichier = "http://cdn.antenne.re/antenne/IMG/archivevideo/".$fichier[0]; 
        $update = "UPDATE " . DATA_BASE_TABLE_DOCUMENT . " SET fichier = :fichier WHERE id_document = :id_doc";
        $stmt = $db->prepare($update); 
        $stmt->bindParam(':fichier', $nouveau_fichier, PDO::PARAM_STR);   
        $stmt->bindParam(':id_doc', $mobile['id_document'], PDO::PARAM_INT); 
        $ret = $stmt->execute();
        if($ret)
            echo "id_doc = ".$mobile['id_document']." && Ancien = ".$mobile['fichier']." && Nouveau = ".$nouveau_fichier."<br /><br />";
        else
            echo "Erreur de mise à jout de id_doc = ".$mobile['id_document'];
    }
    
} catch (Exception $e) {
    echo $e->getMessage()."<br/>".$e->getLine();
    echo "Erreur PDO";
    die;
}