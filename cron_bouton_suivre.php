<?php

/* 
 * @author 
 * Cron pour envoi de mail aux internautes qui ont fait une inscription depuis le bouton Suivre infos
 * @copyright 2016
 */
 error_reporting(E_STRICT);
$dsn = 'mysql:dbname=antenne_art_spip;host=localhost';
$user = 'root';
$password = '';
$user = 'antenne_spipAR';
$password = 'antv3_2009!';
$date_envoi = date("Y-m-d H:i:s");
try {
    $dbh = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}
    //Recuperation du premier mot clé 
    $sql_article =  'SELECT a.id_article,s.date_envois_mail,s.id_suivis, s.email_suivis
    FROM  spip_articles as a , suivis_infos as s
    WHERE a.id_article = s.id_article'; 
    $rows_article = $dbh->query($sql_article);
    foreach  ($rows_article as $row_article) {
        $id_article = $row_article["id_article"]; 
        //Recuperation des articles du premier mot clé 
        $sql_suivre = "SELECT b.id_mot FROM  spip_articles as a, spip_mots as b, spip_mots_liens as c
        WHERE c.id_mot = b.id_mot AND a.id_article = c.id_objet  and id_article =".$id_article;
        $sql_suivre .= " limit 0,1";
        $rows_suivre = $dbh->query($sql_suivre);  
        foreach  ($rows_suivre as $row_suivre) {
            $mail = $row_article["email_suivis"];             
            if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) 
            {
            	$passage_ligne = "\r\n";
            }
            else
            {
            	$passage_ligne = "\n";
            } 
            $id_mot = $row_suivre["id_mot"];   
            //Recupération des infos utiles
            $sql_envoi_mails =  'SELECT DISTINCT a.id_article, m.titre, a.titre AS titre_article, u.url
            FROM spip_mots AS m, spip_mots_liens AS l, spip_articles AS a, spip_urls AS u
            WHERE m.id_mot = l.id_mot
            AND a.id_article = l.id_objet
            AND a.id_article = u.id_objet
            AND a.date <= NOW()
            AND m.id_mot= '.$id_mot; 
            $date = new DateTime($row_article["date_envois_mail"]);
            $date_form = $date->format('Y-m-d');
            if ($date_form!="-0001-11-30")
                $sql_envoi_mails .= " AND a.date>'".$row_article["date_envois_mail"]."'";
            echo $sql_envoi_mails."<br>";
            $rows_envoi_mails = $dbh->query($sql_envoi_mails);  
            foreach  ($rows_envoi_mails as $row_envoi_mails) {                    
                echo "titre".$row_envoi_mails["titre"]."<br>";
                setlocale(LC_TIME, 'fr_FR', 'fra');
                $date_now = (strftime("%A %d %B %Y"));
                $liens_desinscription = "http://www.linfo.re/desinscription_bouton_suivre.php?id_suivis=".$row_article["id_suivis"];
                $urls = "http://www.antennereunion.fr/".$row_envoi_mails["url"];
                $message_txt = "Bonjour,";
                $message_html = '<html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>Newsletter antennereunion.fr | Résultats du baccalauréat 2015 J-1</title>
                <link href="http://fonts.googleapis.com/css?family=Maven+Pro:400,700,500" rel="stylesheet" type="text/css">
                
                <style type="text/css">
                		#outlook a{
                			padding:0;
                		}
                		a:active,a:focus{
                			outline:none;
                		}
                		img{
                			outline:none;
                			text-decoration:none;
                		}
                		a img{
                			border:none;
                		}
                		table td{
                			border-collapse:collapse;
                		}
                		table{
                			border-collapse:collapse;
                			mso-table-lspace:0pt;
                			mso-table-rspace:0pt;
                		}
                		a{
                			color:inherit;
                			text-decoration:none;
                		}
                </style></head>
                <body style="background:#ffffff;">
                <div id="global" style="background-color:#ffffff">
                  <table cellpadding="0" cellspacing="0" width="598" style="margin:0 auto;" bgcolor="#ffffff">
                    <tr>
                      <td><a href="http://www.antennereunion.fr/" title="antennereunion.fr" target="_blank"><img src="http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/entete.jpg" alt="antennereunion.fr"></a></td>
                    </tr>
                  </table>
                  
                    <table cellpadding="0" cellspacing="0" width="598" style="margin:0 auto;" bgcolor="#ffffff">
                    <tr>
                    <td width="19"></td>
                    <td width="560">
                  <table cellpadding="0" cellspacing="0" width="560" style="margin:0 auto;" bgcolor="#ffffff">
                    <tr>
                      <td><p style="font-size:12px;margin:5px 0 0 0;padding:0; font-family:Arial, Helvetica, sans-serif;color:#333333;"> '.utf8_encode($date_now).' </p></td>
                    </tr>
                    <tr>
                      <td height="64">&nbsp;
                      
                      </td>
                    </tr>
                    <tr>
                    <td align="center"><img src="http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/titre_mail.png" alt="antennereunion.fr"></td>
                    </tr>
                    <tr>
                    <td height="93">&nbsp;</td>
                    </tr>
                    <tr><td><p style="font-size:17px;margin:0;padding:0; font-family:Arial, Helvetica, sans-serif;">Bonjour,</p></td></tr>  
                    <tr><td height="33">&nbsp;</td></tr>
                    <tr><td>
                        <p style="font-size:16px;margin:0;padding:0; font-family:Arial, Helvetica, sans-serif;color:#333333;">
                            Une article "'.utf8_encode($row_envoi_mails["titre"]).'" vient d\'être publiée sur antennereunion.fr.
                            Cliquez sur le lien ci-dessous pour le lire 
                            <a href="'.$urls.'" target="_blank" style="color:#002c6a;">'.utf8_encode($row_envoi_mails["titre_article"]).'</a><br/>
                        </p>
                        <p>&nbsp;</p>
                        <p>Pour vous désabonner, veuillez cliquer sur <a href="'.$liens_desinscription.'">ce lien</a></p>
                    </td></tr>
                    <tr>
                    <td height="87">&nbsp;</td>
                    </tr>
                    <tr><td><p style="font-size:10px;margin:0;padding:0; font-family:Arial, Helvetica, sans-serif;">Envoyé par antennereunion.fr</p></td></tr>
                    <tr>
                    <td height="97">&nbsp;</td>
                    </tr>
                    </table>
                  <table cellpadding="0" cellspacing="0" width="598" style="margin:0 auto;" bgcolor="#111111">
                    <tr height="14">
                      <td colspan="6" height="14">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="160" align="center" style="color:#ffffff; font-size: 18px;">Suivez-nous sur</td>
                      <td width="50"><a href="https://www.facebook.com/linfo.re?ref=ts&fref=ts" target="_blank"><img style="display:block" src="http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/facebook_linfo.jpg" alt="linfo.re facebook"></a></td>
                      <td width="50"><a href="https://twitter.com/linfore" target="_blank"><img style="display:block" src="http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/twitter_linfo.jpg"></a></td>
                      <td width="50"><a href="https://plus.google.com/110043057610796001535/posts" target="_blank"><img style="display:block" src="http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/google_plus_linfo.jpg"></a></td>
                      <td width="147" align="right" valign="middle"><a href="https://itunes.apple.com/fr/app/linfo/id640496786?mt=8" target="_blank"><img style="display:block" src="http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/appstore_linfo.jpg"></a></td>
                      <td width="124" align="center" valign="middle"><a href="https://play.google.com/store/apps/details?id=com.goodbarber.linfo&hl=fr_FR" target="_blank"><img style="display:block" src="http://www.antennereunion.fr/newsletter-element/newsletter_alerte_linfo/google_playlinfo.jpg"></a></td>
                    </tr>
                    <tr height="14">
                      <td colspan="6" height="14">&nbsp;</td>
                    </tr>
                  </table>
                  <table>
                    <tr>
                      <td height="5"></td>
                    </tr>
                  </table>
                  <table cellpadding="0" cellspacing="0" width="598" style="margin:0 auto;">
                    <tr>
                      <td align="center"><p style="font-size:11px;color:#000000;">Si vous n\'arrivez à voir cet email : <a href="#" style="font-size:12px; font-weight: bold;">Voir ici</a></p>
                        <p style="font-size:11px;color:#000000;">Si vous ne souhaitez plus recevoir les communications de la part de antennereunion.fr : <a href="*|UNSUB|*" style="font-size:12px;font-weight:bold;color:#000000;">Suivez ce lien</a></p></td>
                    </tr>
                  </table>
                </div>
                </body>
                </html>';
                $boundary = "-----=".md5(rand());
                $sujet = "Alerte";
                
                $header = "From: \"antennereunion.fr\"<contact@linfo.re>".$passage_ligne;
                $header.= "Reply-to: \"antennereunion.fr\" <contact@linfo.re>".$passage_ligne;
                $header.= "MIME-Version: 1.0".$passage_ligne;
                $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
                
                $message = $passage_ligne."--".$boundary.$passage_ligne;
                
                $message.= "Content-Type: text/plain; charset=\"utf-8\"".$passage_ligne;
                $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
                $message.= $passage_ligne.$message_txt.$passage_ligne;
                
                $message.= $passage_ligne."--".$boundary.$passage_ligne;
                
                $message.= "Content-Type: text/html; charset=\"utf-8\"".$passage_ligne;
                $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
                $message.= $passage_ligne.$message_html.$passage_ligne;
                
                $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
                $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
                if (mail($mail,$sujet,$message,$header)){               
                    $update = "UPDATE suivis_infos SET date_envois_mail=? WHERE id_suivis=?";
                    $q = $dbh->prepare($update);
                    $q->execute(array($date_envoi,$row_article["id_suivis"]));
            }
        }
     } 
}

?>