<?php

class Admin_matchs extends CI_Controller {

 

    /**

    * Responsable for auto load the model

    * @return void

    */

    public function __construct()

    {

        parent::__construct();

        $this->load->model('matchs_model');

        $this->load->model('groupe_model');

        $this->load->model('phase_model');

        $this->load->model('equipe_model');

        $this->load->model('resultat_model');

        $this->load->model('tournoi_model');



/*        if(!$this->session->userdata('is_logged_in')){

            redirect('admin/login');

        }*/

    }

 

    /**

    * Load the main view with all the current model model's data.

    * @return void

    */

    public function index()

    {



        //all the posts sent by the view       

        $search_string = $this->input->post('search_string');        

        $groupe= $this->input->post('id_groupe'); 

        $phase = $this->input->post('phase'); 

        $tournoi = ($this->input->post('id_tournoi')) ? $this->input->post('id_tournoi') : 2;

        //fetch sql data into arrays

        $data['groupes'] = $this->groupe_model->get_groupes($tournoi);

        $data['phase'] = $this->phase_model->get_phase();

        $data['tournoi'] = $this->tournoi_model->get_tournois();

        $data['id_tournoi'] = $tournoi;

        $data['id_phase'] = $phase;

        $data['id_groupe'] = $groupe;

        $data['equipe'] = $search_string;

        if($search_string)

        {

           $data['matchs'] = $this->matchs_model->get_matchs($search_string,'','', $tournoi);

          if ($groupe)

          {

            $data['matchs'] = $this->matchs_model->get_matchs($search_string,$groupe,'', $tournoi);

          }else if ($phase) {

              $data['matchs'] = $this->matchs_model->get_matchs($search_string,'',$phase, $tournoi);

          }

        }

        elseif($groupe)

        {

            $data['matchs'] = $this->matchs_model->get_matchs('',$groupe,'', $tournoi);

        }

        elseif($phase)

        {

            $data['matchs'] = $this->matchs_model->get_matchs('','',$phase, $tournoi);

        }

        else{

            $data['matchs'] = $this->matchs_model->get_matchs('','','',$tournoi);

        }        



        //load the view

        $data['main_content'] = 'admin/matchs/list';

        $this->load->view('includes/template', $data);  



    }//index



    public function add()

    {



        $data['phase'] = $this->phase_model->get_phase();

        // id_tournoi = 2 pour Euro
        $data['equipes'] = $this->equipe_model->get_equipes(2);  

        $data['tournoi'] = $this->tournoi_model->get_tournois();

        $data['id_tournoi'] = 2;        

        //load the view

        $data['main_content'] = 'admin/matchs/add';

        $this->load->view('includes/template', $data);  

    }       

    public function add_match()

    {



        $data_to_store = array(

         'date' => $this->input->post('date'),

         'heure' => $this->input->post('heure'),

         'diffuse' => $this->input->post('diffuse'),

         'id_phase' => $this->input->post('phase'),

         'id_tournoi' => $this->input->post('tournoi')

        );

          $id_match = $this->matchs_model->store_match($data_to_store);

         //if the insert has returned true then we show the flash message

         if($id_match)

         {

           $data_to_store_equipe1 = array(

         'score' => 0,

         'point' => -1,

         'id_match' => $id_match,

         'id_equipe' => $this->input->post('equipe1'));

          $this->resultat_model->store_resultat($data_to_store_equipe1);

           

          $data_to_store_equipe2 = array(

         'score' => 0,

         'point' => -1,

         'id_match' => $id_match,

         'id_equipe' => $this->input->post('equipe2'));

          $this->resultat_model->store_resultat($data_to_store_equipe2); 



          echo json_encode(array('data' => 1));

            

         }



    } 

    /**

    * Update item by his id

    * @return void

    */

    public function update()

    {

        //product id 

        $id = $this->uri->segment(4);

  

        

        $data['match'] = $this->matchs_model->get_matchs_by_id($id);

        $data['phase'] = $this->phase_model->get_phase();

        $data['equipes'] = $this->equipe_model->get_equipes(2);

        $data['resultat'] = $this->resultat_model->get_result_by_id($id); 

        

        //var_dump($data['resultat']);exit();

        //load the view

        $data['main_content'] = 'admin/matchs/edit';

        $this->load->view('includes/template', $data);            



    }//update



    public function update_match()

    { 

        

        $data_to_store = array(

          'date' => $this->input->post('date'),

          'heure' => $this->input->post('heure'),

          'diffuse' => $this->input->post('diffuse'),

          'id_phase' => $this->input->post('phase'));



        $this->matchs_model->update_match($this->input->post('match'), $data_to_store);



        // resultat 1

          $data_to_store_res1 = array(

          'id_equipe' => $this->input->post('equipe1'),

          'score' => $this->input->post('score1'),

          'point' => $this->input->post('point1'));



        $this->resultat_model->update_resultat($this->input->post('resultat1'),$data_to_store_res1);

 

        $data_to_store_res2 = array(

          'id_equipe' => $this->input->post('equipe2'),

          'score' => $this->input->post('score2'),

          'point' => $this->input->post('point2'));



        $this->resultat_model->update_resultat($this->input->post('resultat2'),$data_to_store_res2);



        echo json_encode(array('data' => 1));        

    }//update





    /**

    * Delete match by his id

    * @return void

    */

    public function delete()

    {

        //product id 

        $id = $this->uri->segment(4);

        $this->matchs_model->delete_match($id);

        $this->resultat_model->delete_resultat($id);

        redirect('admin/matchs?'.time().'');

    }//edit

    public function team()

    {



        /*$data['phase'] = $this->phase_model->get_phase();

        $data['equipes'] = $this->equipe_model->get_equipes();  */

        $data['tournoi'] = $this->tournoi_model->get_tournois();        

        //load the view

        $data['main_content'] = 'admin/matchs/addteam';

        $this->load->view('includes/template', $data);  

    }

    public function add_team()

    {



        $data_to_store = array(

         'nom' => $this->input->post('nom_equipe'),

         'id_groupe' => $this->input->post('groupe'),

         'id_tournoi' => $this->input->post('tournoi')

        );

        $id_match = $this->equipe_model->store_team($data_to_store);
         echo json_encode(array('data' => 1));
    } 

    public function get_tournoi()
    {
      if ($_POST) {
        $groupes = $this->groupe_model->get_groupes($this->input->post('tournoi'));
        echo json_encode($groupes);
      }
    }

    public function get_teams()
    {
      if ($_POST) {
        $equipes = $this->equipe_model->get_equipes($this->input->post('tournoi'));
        echo json_encode($equipes);
      }
    }

}