    <div class="container top">

      

      <ul class="breadcrumb">

        <li>

          <a href="<?php echo site_url("admin"); ?>">

            <?php echo ucfirst($this->uri->segment(1));?>

          </a> 

          <span class="divider">/</span>

        </li>

        <li>

          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">

            <?php echo ucfirst($this->uri->segment(2));?>

          </a> 

          <span class="divider">/</span>

        </li>

        <li class="active">

          <a href="#">Modification</a>

        </li>

      </ul>

      

      <div class="page-header">

        <h2>

          Modification <?php echo ucfirst($this->uri->segment(2));?>

        </h2>

      </div>

 



      <div class="alert alert-success" style="display: none;">

        <a class="close" data-dismiss="alert">×</a>

          <strong>Success! </strong> La mis à jour a été effectué avec succès.

      </div>       

        

      <div class="alert alert-error" style="display: none;">

       <a class="close" data-dismiss="alert">×</a>

       <strong>Error! </strong> La mis à jour n'a pas pu être effectué.

      </div>         



      

      <?php

      //form data

      $attributes = array('class' => 'form-horizontal', 'id' => 'formulaire_update_match');

      foreach ($phase as $row)

      {

        $options_phase[$row['id_phase']] = $row['nom'];

      }

      $options_equipe = array('default' =>' ');

      foreach ($equipes as $row)

      {

        $options_equipe[$row['id_equipe']] = $row['nom'];

      }

      $options_diffuse = array('0' => 'Non', '1' => 'Oui','2' =>'Provisoire');

      //form validation

      //echo validation_errors();

      

      echo form_open('admin/matchs/add', $attributes);

      ?>

        <fieldset>

          <div class="control-group">

            <label for="date" class="control-label">Date</label>

            <div class="controls">

              <input type="text" id="" name="date" class="datepicker" value="<?php echo $match[0]['date']; ?>">

            <input type="hidden" name="match" value="<?php echo $match[0]['id_match']; ?>">

            </div>

          </div>

          <div class="control-group">

            <label for="heure" class="control-label">Heure</label>

            <div class="controls">

              <input type="text" id="" name="heure" class="time" value="<?php echo $match[0]['heure']; ?>">

            </div>

          </div>

          <?php

          echo '<div class="control-group">';

            echo '<label for="diffuse" class="control-label">Diffuse</label>';

            echo '<div class="controls">';              

              echo form_dropdown('diffuse', $options_diffuse,$match[0]['diffuse'], 'class="span3"');

            echo '</div>';

          echo '</div>';

          ?>

          <?php

          echo '<div class="control-group">';

            echo '<label for="phase" class="control-label">Phase</label>';

            echo '<div class="controls">';              

              echo form_dropdown('phase', $options_phase,$match[0]['id_phase'], 'class="span3"');

            echo '</div>';

          echo '</div>';

          ?>

          <fieldset> 

            <div class="control-group">

             <label for="date" class="control-label">Equipe 1</label>

             <div class="controls">

              <?php             

                  echo form_dropdown('equipe1', $options_equipe,$resultat[0]['id_equipe'], 'class="span3 equipe1"');

              ?>

              <input type="hidden" name="resultat1" value="<?php echo $resultat[0]['id_res']; ?>">

             </div>

             <label for="date" class="control-label" style="margin-right: 12px;">Score equipe 1</label>

             <div class="controls">

              <input type="text" name="score1" value="<?php echo $resultat[0]['score']; ?>">

             </div>

             <label for="date" class="control-label" style="margin-right: 12px;">Point equipe 1</label>

             <div class="controls">

              <input type="text" name="point1" value="<?php echo $resultat[0]['point']; ?>">

             </div>

          </fieldset>

          <fieldset>

            <div class="control-group">

             <label for="date" class="control-label">Equipe 2</label>

             <div class="controls">

              <?php             

                  echo form_dropdown('equipe2', $options_equipe,$resultat[1]['id_equipe'], 'class="span3 equipe2"');

              ?>

              <input type="hidden" name="resultat2" value="<?php echo $resultat[1]['id_res']; ?>">

             </div>

             <label for="date" class="control-label" style="margin-right: 12px;">Score equipe 2</label>

             <div class="controls">

              <input type="text" name="score2" value="<?php echo $resultat[1]['score']; ?>">

             </div>

             <label for="date" class="control-label" style="margin-right: 12px;">Point equipe 2</label>

             <div class="controls">

              <input type="text" name="point2" value="<?php echo $resultat[1]['point']; ?>">

             </div>

          </fieldset>

          <div class="form-actions">

            <button class="btn btn-primary" type="submit">Enregistrer</button>

            <a href="<?php echo site_url("admin").'/matchs?'.time();?>" class="btn btn-danger">Retour listes</a>

          </div>

        </fieldset>



      <?php echo form_close(); ?>

      </div>



      <script>

      $(document).ready(function() {



         $('.datepicker').datetimepicker({

          format: 'YYYY-MM-DD',

          autoClose: true

         });

         $('.time').timepicker({ 

                maxHours:24,

                showMeridian:false

         });

          // add the rule here

         $.validator.addMethod("valueNotEquals", function(value, element, arg){

             return arg !== value;

         }, "Value must not equal arg.");

         // validate signup form on keyup and submit

         $("#formulaire_update_match").validate({

            rules: {

               date: {

                  required: true 

               },

               heure: {

                  required: true 

               },

               score1: {

                  required: true,

                  number: true

               },

               score2: {

                  required: true,

                  number: true 

               },

               point1: {

                  required: true,

                  number: true 

               },

               point2: {

                  required: true,

                  number: true 

               },

            },

            messages: {

               date: {

                  required: "veuillez renseigner le champ date"

               },

               heure: {

                  required: "veuillez renseigner le champ heure"

               },

               score1: {

                  required: "veuillez renseigner le champ score",

                  number: "ce champ doit être un nombre"

               },

               score2: {

                  required: "veuillez renseigner le champ score",

                  number: "ce champ doit être un nombre"

               },

               point1: {

                  required: "veuillez renseigner le champ point",

                  number: "ce champ doit être un nombre"

               },

               point2: {

                  required: "veuillez renseigner le champ point",

                  number: "ce champ doit être un nombre"

               },

            },

            submitHandler: function() {

                $.ajax({

                  url : "<?php echo site_url('admin').'/update_match';?>",

                  type : 'POST',

                  cache:false,

                  data: $('#formulaire_update_match').serialize(),

                  dataType : 'json', // On désire recevoir du HTML

                  success : function(code_html, statut)

                  { 

                     if(code_html.data == 1)

                     {

                       $('.alert-success').css('display','block');

                       //$('#formulaire_match')[0].reset();



                     }else

                     {

                      $('.alert-error').css('display','block');

                     }

                  }

              });

            }

         });

      });

   </script> 