    <div class="container top">

      

      <ul class="breadcrumb">

        <li>

          <a href="<?php echo site_url("admin"); ?>">

            <?php echo ucfirst($this->uri->segment(1));?>

          </a> 

          <span class="divider">/</span>

        </li>

        <li>

          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">

            <?php echo ucfirst($this->uri->segment(2));?>

          </a> 

          <span class="divider">/</span>

        </li>

        <li class="active">

          <a href="#">Ajout</a>

        </li>

      </ul>

      

      <div class="page-header">

        <h2>

          Ajout équipe

        </h2>

      </div>

 



      <div class="alert alert-success" style="display: none;">

        <a class="close" data-dismiss="alert">×</a>

          <strong>Success !</strong> L'enregistrement a été effectué avec succès.

      </div>       

        

      <div class="alert alert-error" style="display: none;">

       <a class="close" data-dismiss="alert">×</a>

       <strong>Error !</strong> L'enregistrement n'a pas pu être effectué.

      </div>         



      

      <?php

      //form data

      $attributes = array('class' => 'form-horizontal', 'id' => 'formulaire_equipe');

      foreach ($tournoi as $row)

      {

        $options_tournoi[$row['id_tournoi']] = $row['nom_tournoi'];

      }

      //form validation

      //echo validation_errors();

      

      echo form_open('admin/matchs/add_team', $attributes);

      ?>

        <fieldset>

          <div class="control-group">

            <label for="date" class="control-label">Nom équipe</label>

            <div class="controls">

              <input type="text" id="" name="nom_equipe" class="">

            </div>

          </div>

          <div class="control-group">

            <label for="date" class="control-label">Groupe</label>

            <div class="controls">

              <select name="groupe" class="span3">
                <option value="19">A</option>
                <option value="20">B</option>
                <option value="21">C</option>
                <option value="22">D</option>
                <option value="23">E</option>
                <option value="24">F</option>
              </select>
            </div>

          </div>

          
          <?php

          echo '<div class="control-group">';

            echo '<label for="tournoi" class="control-label">Tournoi</label>';

            echo '<div class="controls">';              

              echo form_dropdown('tournoi', $options_tournoi,' ', 'class="span3"');

            echo '</div>';

          echo '</div>';

          ?>

          <div class="form-actions">

            <button class="btn btn-primary" type="submit">Enregistrer</button>

            <a href="<?php echo site_url("admin").'/matchs?'.time();?>" class="btn btn-danger">Retour listes</a>

          </div>

        </fieldset>



      <?php echo form_close(); ?>

      </div>



      <script>

      $(document).ready(function() {

          // add the rule here

         $.validator.addMethod("valueNotEquals", function(value, element, arg){

             return arg !== value;

         }, "Value must not equal arg.");

         // validate signup form on keyup and submit

         $("#formulaire_equipe").validate({

            rules: {

               nom_equipe: {

                  required: true 

               },

            },

            messages: {

               nom_equipe: {

                  required: "veuillez renseigner le champ nom équipe"

               },

            },

            submitHandler: function() {

                $.ajax({

                  url : "<?php echo site_url('admin').'/matchs/add_team';?>",

                  type : 'POST',

                  cache:false,

                  data: $('#formulaire_equipe').serialize(),

                  dataType : 'json', // On désire recevoir du HTML

                  success : function(code_html, statut)

                  { 

                     if(code_html.data == 1)

                     {

                       $('.alert-success').css('display','block');

                       $('#formulaire_equipe')[0].reset();



                     }else

                     {

                      $('.alert-error').css('display','block');

                     }

                  }

              });

            }

         });

      });

   </script> 