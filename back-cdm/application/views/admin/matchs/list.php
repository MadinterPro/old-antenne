    <div class="container top">



      <ul class="breadcrumb">

        <li>

          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2).'?'.time(); ?>">

            <?php echo ucfirst($this->uri->segment(1));?>

          </a> 

          <span class="divider">/</span>

        </li>

        <li class="active">

          <?php echo ucfirst($this->uri->segment(2));?>

        </li>

      </ul>



      <div class="page-header users-header">

        <h2>

          <?php echo ucfirst($this->uri->segment(2));?> 

          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Ajouter un match</a>

          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/team" class="btn btn-success">Ajouter équipe</a>

        </h2>

      </div>

      

      <div class="row">

        <div class="span12 columns">

          <div class="well">
            <form action="<?php echo base_url() . 'admin/matchs' ?>" method="post" accept-charset="utf-8" class="form-inline reset-margin" id="myform">
              <div class="form-group col-md-3">
                <label for="id_tournoi">Filtre groupe:</label>
                <select name="id_tournoi" class="form-control span2">
                  <?php foreach ($tournoi as $key => $row): ?>
                    <?php if ($row['id_tournoi'] == $id_tournoi): ?>
                      <option value="<?php echo $row['id_tournoi']; ?>" selected><?php echo $row['nom_tournoi'] ?></option>  
                    <?php else: ?>
                      <option value="<?php echo $row['id_tournoi']; ?>"><?php echo $row['nom_tournoi'] ?></option>
                    <?php endif ?>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="search_string">Recherche equipe:</label>
                <input type="text" name="search_string" class="form-control" value="<?php echo $equipe; ?>">
              </div>
              <div class="form-group col-md-3">
                <label for="id_groupe">Filtre groupe:</label>
                <select name="id_groupe" class="form-control span2">
                  <option value="0">Tous</option>
                  <?php foreach ($groupes as $key => $row): ?>
                    <?php if ($row['id'] == $id_groupe): ?>
                      <option value="<?php echo $row['id']; ?>" selected ><?php echo $row['nom'] ?></option> 
                    <?php else: ?>
                      <option value="<?php echo $row['id']; ?>"><?php echo $row['nom'] ?></option>
                    <?php endif ?>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="phase">OU par phase:</label>
                <select name="phase" class="form-control span2">
                  <option value="0">Tous</option>
                  <?php foreach ($phase as $key => $row): ?>
                    <?php if ($row['id_phase'] == $id_phase): ?>
                      <option value="<?php echo $row['id_phase']; ?>" selected ><?php echo $row['nom'] ?></option>  
                    <?php else: ?>
                      <option value="<?php echo $row['id_phase']; ?>"><?php echo $row['nom'] ?></option>  
                    <?php endif ?>
                  <?php endforeach ?>
                </select>
              </div>
              <input type="submit" name="mysubmit" value="OK" class="btn btn-primary">
            </form>

          </div>



          <table class="table table-striped table-bordered table-condensed">

            <thead>

              <tr>

                <th class="header">Groupe</th>

                <th class="yellow header headerSortDown">Date</th>

                <th class="green header">Score</th>

                <th class="red header">Equipe 1</th>

                <th class="red header">Heure</th>

                <th class="red header">Score</th>

                <th class="red header">Equipe 2</th>

                <th class="red header">Diffuse</th>

                <th class="red header">Actions</th>

              </tr>

            </thead>

            <tbody>

              <?php

              foreach($matchs as $key => $row)

              {

                echo '<tr>';

                echo '<td>'.$row["nom"].'</td>';

                echo '<td>'.$row["date"].'</td>';

                echo '<td>'.$row["point_equipe1"].'</td>';

                echo '<td>'.$row["equipe1"].'</td>';

                echo '<td>'.$row["heure"].'</td>';

                echo '<td>'.$row["point_equipe2"].'</td>';

                echo '<td>'.$row["equipe2"].'</td>';

                echo '<td>'.$row["diffuse"].'</td>';

                echo '<td class="crud-actions">

                  <a href="'.site_url("admin").'/matchs/update/'.$row['id_match'].'?'.time().'" class="btn btn-info">Modifier</a>  

                  <a href="'.site_url("admin").'/matchs/delete/'.$row['id_match'].'?'.time().'" class="btn btn-danger">Supprimer</a>

                </td>';

                echo '</tr>';

              }

              ?>      

            </tbody>

          </table>

      </div>

    </div>
    <script type="text/javascript">
      $(document).ready(function() {
        $(document).on('change', 'select[name="id_tournoi"]', function(){
          $.ajax({
            url : "<?php echo site_url('admin').'/matchs/get_tournoi';?>",
            type : 'POST',
            cache:false,
            data : {tournoi : $(this).val()},
            dataType : 'json',
            success : function(code_html, statut){
              let render = '';
              $('select[name="id_groupe"]').html('');
              code_html.forEach(item => {
                render += '<option value = '+ item.id +'>'+ item.nom + '</option>';
              });
              $('select[name="id_groupe"]').html(render);
            }
          });
        });
      });
    </script>