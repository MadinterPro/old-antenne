    <div class="container top">

      

      <ul class="breadcrumb">

        <li>

          <a href="<?php echo site_url("admin"); ?>">

            <?php echo ucfirst($this->uri->segment(1));?>

          </a> 

          <span class="divider">/</span>

        </li>

        <li>

          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">

            <?php echo ucfirst($this->uri->segment(2));?>

          </a> 

          <span class="divider">/</span>

        </li>

        <li class="active">

          <a href="#">Ajout</a>

        </li>

      </ul>

      

      <div class="page-header">

        <h2>

          Ajout <?php echo ucfirst($this->uri->segment(2));?>

        </h2>

      </div>

 



      <div class="alert alert-success" style="display: none;">

        <a class="close" data-dismiss="alert">×</a>

          <strong>Success !</strong> L'enregistrement a été effectué avec succès.

      </div>       

        

      <div class="alert alert-error" style="display: none;">

       <a class="close" data-dismiss="alert">×</a>

       <strong>Error !</strong> L'enregistrement n'a pas pu être effectué.

      </div>         



      

      <?php

      //form data

      $attributes = array('class' => 'form-horizontal', 'id' => 'formulaire_match');

      foreach ($phase as $row)

      {

        $options_phase[$row['id_phase']] = $row['nom'];

      }

      foreach ($tournoi as $row)

      {

        $options_tournoi[$row['id_tournoi']] = $row['nom_tournoi'];

      }

      $options_equipe = array('default' =>' ');

      foreach ($equipes as $row)

      {

        $options_equipe[$row['id_equipe']] = $row['nom'];

      }

      $options_diffuse = array('0' => 'Non', '1' => 'Oui', '2' =>'Provisoire');

      //form validation

      //echo validation_errors();

      

      echo form_open('admin/matchs/add', $attributes);

      ?>

        <fieldset>

          <div class="control-group">

            <label for="date" class="control-label">Date</label>

            <div class="controls">

              <input type="text" id="" name="date" class="datepicker">

            </div>

          </div>

          <div class="control-group">

            <label for="heure" class="control-label">Heure</label>

            <div class="controls">

              <input type="text" id="" name="heure" class="time">

            </div>

          </div>

          <?php

          echo '<div class="control-group">';

            echo '<label for="equipe1" class="control-label">Equipe 1</label>';

            echo '<div class="controls">';              

              echo form_dropdown('equipe1', $options_equipe,' ', 'class="span3 equipe1"');

            echo '</div>';

          echo '</div>';

          ?>

          <?php

          echo '<div class="control-group">';

            echo '<label for="equipe2" class="control-label">Equipe 2</label>';

            echo '<div class="controls">';              

              echo form_dropdown('equipe2', $options_equipe,' ', 'class="span3 equipe2"');

            echo '</div>';

          echo '</div>';

          ?>

          <?php

          echo '<div class="control-group">';

            echo '<label for="diffuse" class="control-label">Diffuse</label>';

            echo '<div class="controls">';              

              echo form_dropdown('diffuse', $options_diffuse,'', 'class="span3"');

            echo '</div>';

          echo '</div>';

          ?>

          <?php

          echo '<div class="control-group">';

            echo '<label for="phase" class="control-label">Phase</label>';

            echo '<div class="controls">';              

              echo form_dropdown('phase', $options_phase,' ', 'class="span3"');

            echo '</div>';

          echo '</div>';

          ?>

          <?php

          echo '<div class="control-group">';

            echo '<label for="tournoi" class="control-label">Tournoi</label>';

            echo '<div class="controls">';              

              echo form_dropdown('tournoi', $options_tournoi,$id_tournoi, 'class="span3"');

            echo '</div>';

          echo '</div>';

          ?>

          <div class="form-actions">

            <button class="btn btn-primary" type="submit">Enregistrer</button>

            <a href="<?php echo site_url("admin").'/matchs?'.time();?>" class="btn btn-danger">Retour listes</a>

          </div>

        </fieldset>



      <?php echo form_close(); ?>

      </div>



      <script>

      $(document).ready(function() {



         $('.datepicker').datetimepicker({

          format: 'YYYY-MM-DD'

         });

         $('.time').timepicker({ 

                maxHours:24,

                showMeridian:false

         });

          // add the rule here

         $.validator.addMethod("valueNotEquals", function(value, element, arg){

             return arg !== value;

         }, "Value must not equal arg.");

         // validate signup form on keyup and submit

         $("#formulaire_match").validate({

            rules: {

               date: {

                  required: true 

               },

               heure: {

                  required: true 

               },

               equipe1: {

                  valueNotEquals: "default"

               },

               equipe2: {

                  valueNotEquals: "default" 

               },

            },

            messages: {

               date: {

                  required: "veuillez renseigner le champ date"

               },

               heure: {

                  required: "veuillez renseigner le champ heure"

               },

               equipe1: {

                  valueNotEquals: "veuillez selectioner un equipe"

               },

               equipe2: {

                  valueNotEquals: "veuillez selectioner un equipe"

               },

            },

            submitHandler: function() {

                $.ajax({

                  url : "<?php echo site_url('admin').'/matchs/add_match';?>",

                  type : 'POST',

                  cache:false,

                  data: $('#formulaire_match').serialize(),

                  dataType : 'json', // On désire recevoir du HTML

                  success : function(code_html, statut)

                  { 

                     if(code_html.data == 1)

                     {

                       $('.alert-success').css('display','block');

                       $('#formulaire_match')[0].reset();



                     }else

                     {

                      $('.alert-error').css('display','block');

                     }

                  }

              });

            }

         });

         $(document).on('change', 'select[name="tournoi"]', function(){
          $.ajax({
            url : "<?php echo site_url('admin').'/matchs/get_teams';?>",
            type : 'POST',
            cache:false,
            data : {tournoi : $(this).val()},
            dataType : 'json',
            success : function(code_html, statut){
              let render = '';
              $('select[name="equipe1"]').html('');
              $('select[name="equipe2"]').html('');
              code_html.forEach(item => {
                render += '<option value = '+ item.id_equipe +'>'+ item.nom + '</option>';
              });
              $('select[name="equipe1"]').html(render);
              $('select[name="equipe2"]').html(render);
            }
          });
        });

      });

   </script> 