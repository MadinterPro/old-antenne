<?php
class Groupe_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }



    public function get_groupes($id_tournoi = null)
    {
        $ignore = array(14,15,16,17,18);	    
		$this->db->select('*');
		$this->db->from('groupe');
        if (!is_null($id_tournoi)) {
            $this->db->where('groupe.id_tournoi', $id_tournoi);
        }
        $this->db->where_not_in('groupe.id', $ignore);
		$query = $this->db->get();
		return $query->result_array(); 	
    }
}
