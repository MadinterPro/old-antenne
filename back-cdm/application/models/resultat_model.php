<?php
class Resultat_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get matchs by his is
    * @param int $id_match 
    * @return array
    */
    public function get_result_by_id($id)
    {
		$this->db->select('*');
		$this->db->from('resultat');
        $this->db->join('equipe', 'equipe.id_equipe = resultat.id_equipe');
		$this->db->where('id_match', $id);
		$query = $this->db->get();
		return $query->result_array(); 
    }

    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_resultat($data)
    {
		$insert = $this->db->insert('resultat', $data);
	    return $insert;
	}  

    /**
    * Update resultat
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_resultat($id, $data)
    {
		$this->db->where('id_res', $id);
		$this->db->update('resultat', $data);
	}

    /**
    * Delete resultat
    * @param int $id - match id
    * @return boolean
    */
	function delete_resultat($id){
		$this->db->where('id_match', $id);
		$this->db->delete('resultat'); 
	}
 
}
