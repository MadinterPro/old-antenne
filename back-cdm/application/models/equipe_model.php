<?php
class Equipe_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }



    public function get_equipes($id_tournoi)
    {
	    
		$this->db->select('*');
		$this->db->from('equipe');
        $this->db->where('equipe.id_tournoi', $id_tournoi);
		$query = $this->db->get();
		return $query->result_array(); 	
    } 

    function store_team($data)
    {
        $this->db->insert('equipe', $data);
          $last_id = $this->db->insert_id();
        return $last_id;
    }
}
