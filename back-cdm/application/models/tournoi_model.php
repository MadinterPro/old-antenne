<?php
class Tournoi_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }



    public function get_tournois()
    {
	    
		$this->db->select('*');
		$this->db->from('tournoi');
		$query = $this->db->get();
		return $query->result_array(); 	
    } 
}
