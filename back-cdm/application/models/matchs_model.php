<?php
class Matchs_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get matchs by his is
    * @param int $id_match 
    * @return array
    */
    public function get_matchs_by_id($id)
    {
		$this->db->select('*');
		$this->db->from('matchs');
		$this->db->where('id_match', $id);
		$query = $this->db->get();
		return $query->result_array(); 
    }

    public function get_matchs($search_string=null, $groupe=null, $phase=null, $tournoi)
    {
    	$sql ='';
    	$sql .= 'SELECT DISTINCT m.id_match, m.diffuse, m.date, m.heure, eq1.nom AS equipe1, r1.score AS point_equipe1, eq2.nom AS equipe2, r2.score AS point_equipe2, IF(m.id_phase > 1,(CONCAT(" ",ph.nom)),(CONCAT(" ",gp.nom))) as nom
    	    FROM matchs AS m
    	    INNER JOIN resultat AS r2 ON m.id_match = r2.id_match
    	    INNER JOIN resultat AS r1 ON m.id_match = r1.id_match 
    	    INNER JOIN equipe AS eq1 ON eq1.id_equipe = r1.id_equipe 
    	    INNER JOIN equipe AS eq2 ON eq2.id_equipe = r2.id_equipe
    	    AND eq1.id_equipe != eq2.id_equipe
    	    INNER JOIN groupe AS gp ON gp.id = eq1.id_groupe
    	    INNER JOIN phase AS ph ON ph.id_phase = m.id_phase WHERE 1=1';

    	if($search_string)
    	{
    	  $sql .= " AND (eq1.nom LIKE '%".$search_string."%' OR eq2.nom LIKE '%".$search_string."%')";	
    	} 

    	if($groupe != null && $groupe != 0){
		  $sql .= " AND gp.id = ".$groupe." AND ph.id_phase = 1";
		}

		if($phase != null && $phase != 0){
		  $sql .= " AND ph.id_phase = ".$phase."";
		}      

    	$sql.=" AND m.id_tournoi = $tournoi GROUP BY m.id_match ORDER BY m.date, m.heure ASC";
		$query = $this->db->query($sql);
		return $query->result_array(); 	
    }

    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_match($data)
    {
		$this->db->insert('matchs', $data);
	      $last_id = $this->db->insert_id();
        return $last_id;
	}

    /**
    * Update match
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_match($id, $data)
    {
		$this->db->where('id_match', $id);
		$this->db->update('matchs', $data);
	}

    /**
    * Delete match
    * @param int $id - match id
    * @return boolean
    */
	function delete_match($id){
		$this->db->where('id_match', $id);
		$this->db->delete('matchs'); 
	}
 
}	
